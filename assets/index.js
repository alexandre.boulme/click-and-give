export const icons = {
    "app-logo.png": require('./images/app-logo.png'),
    "like.png": require('./images/like.png'),
    "friends-unfilled.png": require('./icons/friends-unfilled.png'),
    "friends-filled.png": require('./icons/friends-filled.png'),
    "photo.png": require('./images/photo.png'),
    "notification.png": require('./images/notification.png')
};
