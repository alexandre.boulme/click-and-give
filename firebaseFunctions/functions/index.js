const functions = require('firebase-functions');
const admin = require('firebase-admin');
const paypal = require('paypal-rest-sdk');
const stageProd = true;
const STRIPE_SECRET_KEY_DEV='sk_test_51EfXCpLDvhGrVUL7LjCwA4GwSrflSjKhvqTR4VHAlds3PMgd4THsxtp7wAKCa4zUtfGyvND0sBBpUXsoVxHo7oGW00C4xOACQ3'; //Test
const STRIPE_SECRET_KEY_PROD='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'; //Prod
const STRIPE_SECRET_KEY = stageProd ? STRIPE_SECRET_KEY_PROD : STRIPE_SECRET_KEY_DEV;
const stripe = require('stripe')('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');
const {CloudTasksClient} = require('@google-cloud/tasks');
const DYNAMIC_LINKS_BASE = stageProd ? "https://clickandgive.page.link/" : "https://ardianapp.page.link/";
const project = stageProd ? 'aridan-click-and-give-prod' : 'ardian-click-and-give';

const location = 'europe-west1';
const queue = 'delayed-donation-notification';
const BASE_URL = 'https://us-central1-' + project + '.cloudfunctions.net';

admin.initializeApp({projectId: project});

const firestore = admin.firestore();
const usersRef = firestore.collection("ClickAndGive_Users");
const projectsRef = firestore.collection('ClickAndGive_Projects');
const postsRef = firestore.collection('ClickAndGive_Posts');
const commentsRef = firestore.collection('ClickAndGive_Comments');
const donationsRef = firestore.collection('ClickAndGive_Donations');
const channelsParticipantsRef = firestore.collection('ClickAndGive_ChannelParticipation');

exports.onCreateNewDonation = functions.firestore
  .document('ClickAndGive_Donations/{donation}')
  .onCreate((snapshot, context) => {

    const donationId = context.params.donation;
    const donation = snapshot.data();

    console.log(donation);

    //Notification calculation
    const delayMinNotif = 5;
    const expiresInNotif = 60 * delayMinNotif;
    const expiresAtNotif = donation.createdAt.seconds + expiresInNotif;

    //Email calculation
    const delayMinEmail = 60;
    const expiresInEmail = 60 * delayMinEmail;
    const expiresAtEmail = donation.createdAt.seconds + expiresInEmail;

    //Time to trigger function
    const tasksClient = new CloudTasksClient();
    const queuePath = tasksClient.queuePath(project, location, queue);

    const urlNotif = BASE_URL + '/donationDelayedNotification';
    const urlEmail = BASE_URL + '/sendDonationDelayedEmail';
    const payload = { donationId };

    const taskNotif = {
        httpRequest: {
          httpMethod: 'POST',
          url: urlNotif,
          headers: {
            'Content-Type': 'application/json',
          },
          body: Buffer.from(JSON.stringify(payload)).toString('base64')
        },
        scheduleTime: {
          seconds: expiresAtNotif
        }
    };

    const taskEmail = {
        httpRequest: {
          httpMethod: 'POST',
          url: urlEmail,
          body: Buffer.from(JSON.stringify(payload)).toString('base64'),
          headers: {
            'Content-Type': 'application/json',
          },
        },
        scheduleTime: {
          seconds: expiresAtEmail
        }
    };

    tasksClient.createTask({ parent: queuePath,task: taskEmail,  resource: { retryConfig: { maxAttempts: 1 }}});

    return tasksClient.createTask({ parent: queuePath,task: taskNotif });
});

exports.donationDelayedNotification = functions.https.onRequest(async (request, response) => {

  try {
    const payload = request.body;

    console.log(payload);

    const don = await donationsRef
      .doc(payload.donationId)
      .get();

    if (don.exists) {
      const donation = don.data();
      const projectData = await projectsRef
        .doc(donation.projectId)
        .get();
      const project = projectData.data();

      if(donation.author.pushToken && donation.status === "Pending"){
        const payloadTarget = {
            notification: {
                title: `${project.name}`,
                body: `Please don’t forget to validate your donation on Click & Give for it to appear !`,
            },
            data: {
                action:'donation',
                id: payload.donationId
            }
        };
        sendPushToUser(donation.author.id, payloadTarget);
        response.send();
        return;
      } else {
        response.send();
        return;
      }
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
        response.status(404).send();
        return;
    }
  } catch (error) {
    console.log(error);
    response.status(400).send();
    return;
  }
});

exports.sendDonationDelayedEmail = functions.https.onRequest(async (request, response) => {

  const payload = request.body;

  console.log(payload);

  const don = await donationsRef
    .doc(payload.donationId)
    .get();

  if (don.exists) {
    const donation = don.data();
    const projectData = await projectsRef
      .doc(donation.projectId)
      .get();
    const project = projectData.data();

    if(donation.author.email && donation.status === "Pending"){

      let deepLink = "https://apps.apple.com/us/app/click-give-by-ardian/id1528186730?donationId=" + payload.donationId;
      deepLink = encodeURIComponent(deepLink);
      const link = DYNAMIC_LINKS_BASE + "?link=" + deepLink + "&apn=com.ardian.ios.clickandgive";

      //Sent to donator
      firestore.collection('ClickAndGive_Emails').add({
        to: donation.author.email,
        template: {
          name: 'donationDelayedEmail',
          data: {
            firstname: donation.author.firstName,
            name: project.name,
            url: link
          }
        },
      })
      .catch(err => {
        console.log("Error email creator : " + err);
      });
    }
  }
  response.status(200).send();
});

exports.sendChatPushNotification = functions.firestore
  .document('ClickAndGive_Channels/{channel}/ClickAndGive_MessagesThread/{message}')
  .onWrite((change, context) => {

    const channel = context.params.channel;
    const data = change.after.data();

    const senderId = data.senderID;
    const senderFirstName = data.senderFirstName;
    const content = data.content;
    const url = data.url;

    let payload = {};

    if (url) {
      payload = {
        notification: {
          title: senderFirstName + ' sent a new message',
          body: `Photo received`,
        },
      };
    } else {
      payload = {
        notification: {
          title: senderFirstName + ' sent a new message',
          body: content
        }
      };
    }

    payload.data = {
        action:'message'
    };

    return channelsParticipantsRef
        .where('channel', '==', channel)
        .get()
        .then(querySnapshot => {

            const participants = querySnapshot.docs;

            console.log("Nombre de participants " + participants.length);

            participants.forEach((document) => {
                const participant = document.data();

                if(participant.user !== senderId){
                    console.log(participant.user);
                    usersRef
                        .doc(participant.user)
                        .get()
                        .then(userInfo => {
                            const participantInformations = userInfo.data();
                            console.log(JSON.stringify(participantInformations));

                            console.log("Push token " + JSON.stringify(participantInformations.pushToken));

                            console.log(JSON.stringify(payload));

                            if(participantInformations.pushToken){
                                const badgeValue = participantInformations.badgeCount ? participantInformations.badgeCount + 1 : 1;
                                payload.notification.badge = badgeValue.toString();
                                usersRef.doc(participant.user).update({ badgeCount: badgeValue });

                                admin.messaging().sendToDevice(participantInformations.pushToken, payload);
                            }
                        })
                        .catch(error => console.log(error))
                }
            });
        })
        .catch(error => {
            console.log(error);
        });
});

exports.sendDonationPushNotification = functions.firestore
  .document('ClickAndGive_Donations/{some_donation_update}')
  .onWrite((change, context) => {

    const donationData = change.after.data();

    if (!donationData.foundationDonation && donationData.status === "Confirmed"){

        const recipientID = donationData.projectAuthor.id;
        const projectID = donationData.projectId;

        const donatorFirstName = donationData.author.firstName;
        const donatorLastName = donationData.author.lastName;

        const notificationMessage = donationData.type === "time" ? `just offered you some of their time ! Time to get in touch !` : `just financially supported your project !`;

        let pushToken = '';

        return projectsRef
            .doc(projectID)
            .get()
            .then(doc => {

                const payload = {
                    notification: {
                        title: doc.data().name ? doc.data().name : "New donation received",
                        body:  `${donatorFirstName} ${donatorLastName} ` + notificationMessage,
                    },
                    data: {
                        action:'project',
                        id: projectID
                    }
                };

                return usersRef
                    .doc(recipientID)
                    .get()
                    .then(doc => {
                        const userData = doc.data();
                        pushToken = userData.pushToken;
                        const badgeValue = userData.badgeCount ? userData.badgeCount + 1 : 1;
                        payload.notification.badge = badgeValue.toString();
                        usersRef.doc(userData.id).update({ badgeCount: badgeValue });

                        return admin.messaging().sendToDevice(pushToken, payload);
                    });
            });
    } else {
        return null;
    }
});

exports.sendProjectPushNotification = functions.firestore
    .document('ClickAndGive_Projects/{project}')
    .onWrite((change, context) => {

        const projectId = context.params.project;
        const projectData = change.after.data();
        const projectPreviousData = change.before.data();


        console.log("NEW DATA");
        console.log(projectData.donations.fund.amount);

        console.log("PREVIOUS DATA");
        console.log(projectPreviousData.donations.fund.amount);

        if( projectData.donations.fund.amount === 0 && projectData.donations.fund.number > 0 ){
            if(projectPreviousData.donations.fund.amount > 0){
                projectsRef
                    .doc(projectId)
                    .update(
                        {
                            donations: {
                                fund: {
                                    amount: projectPreviousData.donations.fund.amount,
                                    number: projectPreviousData.donations.fund.number
                                },
                                volunteer:  projectData.donations.volunteer
                            }
                        })
                    .catch(err => console.log(err));
            }
        }

        const payloadTargetFund = {
            notification: {
                title: `${projectData.name}`,
                body: `CONGRATULATIONS ! The project just reached his fund target !`,
            },
            data: {
                action:'project',
                id: projectId
            }
        };
        const payloadTargetVolunteer = {
            notification: {
                title: `${projectData.name}`,
                body: `CONGRATULATIONS ! The project just reached his volunteer target !`,
            },
            data: {
                action:'project',
                id: projectId
            }
        };

        const fundTargetNotificationSent = projectData.notifications.includes("fundTargetReached");
        const fundTargetReached = projectData.needFund && parseInt(projectData.donations.fund.amount) >= parseInt(projectData.fundRaisingTarget) && !fundTargetNotificationSent;

        const volunteerTargetNotificationSent = projectData.notifications.includes("volunteerTargetReached");
        const volunteerTargetReached = projectData.needVolunteer && projectData.donations.volunteer.toString() === projectData.volunteerTarget.toString() && !volunteerTargetNotificationSent;

        console.log("target fund : " + fundTargetReached);
        console.log("target time : " + volunteerTargetReached);

        if (projectData.approved){

            //If target reached notification needs to be sent
            if (fundTargetReached || volunteerTargetReached){
                const donationFilter = fundTargetReached ? "money":"time";
                const notificationName = fundTargetReached ? "fundTargetReached":"volunteerTargetReached";
                const payloadTarget = fundTargetReached ? payloadTargetFund : payloadTargetVolunteer;

                sendPushToUser(projectData.author.id, payloadTarget);

                //Retrieve donations
                return donationsRef
                    .where('projectId', '==', projectData.id)
                    .get()
                    .then(data => {
                        const donations = data.docs;
                        const donatorsNotified = [projectData.author.id];
                        var donationCount = 0;

                        //Send push to filtered donation authors
                        donations.forEach((donationT) => {
                            const donation = donationT.data();
                            donationCount += 1;

                            if(donation.type === donationFilter && !donatorsNotified.includes(donation.author.id)){
                                sendPushToUser(donation.author.id, payloadTarget);
                                donatorsNotified.push(donation.author.id);
                            }

                        });

                        projectsRef
                            .doc(projectId)
                            .update({ notifications: [...projectData.notifications, notificationName] })
                            .catch(err => console.log(err));

                    }).catch(error => console.log(error));

            //If project creation notification to send
            } else if (!(projectData.notifications && projectData.notifications.includes("projectCreation"))){
                console.log("inside the second test");

                return usersRef
                    .doc(projectData.author.id)
                    .get()
                    .then(doc => {
                        const userData = doc.data();
                        var authorName ='';

                        if(userData.firstName === "Ardian"){
                            authorName = 'The ' + userData.firstName + ' ' + userData.lastName;
                        } else {
                            authorName = userData.firstName + ' ' + userData.lastName[0].toUpperCase() + '.';
                        }

                        const payloadCreation = {
                            notification: {
                                title: `${authorName} just created a Project on Click & Give`,
                                body: `${projectData.name}`,
                            },
                            data: {
                                action:'project',
                                id: projectId
                            }
                        };

                        return sendPushToAllUsers(payloadCreation, projectId);
                    });
        } else {
            console.log("inside the else");
            return null;
        }
    }
});

exports.sendPostPublishedPushNotification = functions.firestore
    .document('ClickAndGive_Posts/{post}')
    .onWrite((change, context) => {

        const postId = context.params.post;
        const postData = change.after.data();

        if(!postData.publishedNotification && postData.id){

            return usersRef
                .doc(postData.authorID)
                .get()
                .then(doc => {
                    const userData = doc.data();
                    var authorName ='';

                    if(userData.firstName === "Ardian"){
                        authorName = 'The ' + userData.firstName + ' ' + userData.lastName;
                    } else {
                        authorName = userData.firstName + ' ' + userData.lastName[0].toUpperCase() + '.';
                    }

                    const payloadTarget = {
                        notification: {
                            title: `${authorName} posted on Click & Give`,
                            body: `${postData.postText}`,
                        },
                        data: {
                            action:'post',
                            id: postId
                        }
                    };

                    postsRef
                        .doc(postId)
                        .update({ publishedNotification: true })
                        .catch(err => console.log(err));

                    console.log('envoi des messages');

                    return sendPushToAllUsers(payloadTarget, null);

                })
                .catch(err => console.log(err));

        } else {
            console.log("Notification already sent");
            return null;
        }

});

function sendPushToAllUsers(payload, projectDataId){

    if(projectDataId !== null){
        projectsRef
            .doc(projectDataId)
            .update({ notifications: ['projectCreation'] })
            .catch(err => console.log(err));
    }

    //Retrieve donations
    return usersRef
        .get()
        .then(data => {
            const users = data.docs;
            var userCount = 0;

            console.log("users in this interest : " + users.length);

            var tokens = [];

            //Send push to filtered donation authors
            users.forEach((user) => {
                userCount += 1;
                var userInfo = user.data();
                if(userInfo.pushToken){
                    tokens.push(userInfo.pushToken);
                }
            });

            admin.messaging().sendMulticast({
                tokens: tokens,
                notification: payload.notification,
                data: payload.data
            })
            .then(data => console.log(data))
            .catch(error => console.log(error));

        }).catch(error => console.log(error));


}

function sendPushToUser(userId, payloadTarget){
    console.log("send push to user id : " + userId);
    return usersRef
        .doc(userId)
        .get()
        .then(doc => {

            const userData = doc.data();
            const pushToken = userData.pushToken;
            console.log(pushToken);
            const badgeValue = userData.badgeCount ? userData.badgeCount + 1 : 1;

            payloadTarget.notification.badge = badgeValue.toString();

            if(pushToken){
                usersRef.doc(userId).update({ badgeCount: badgeValue });
                admin.messaging().sendToDevice(pushToken, payloadTarget);
            }

        }).catch(err => console.log(err));
}

exports.sendLikePushNotification = functions.firestore
    .document('ClickAndGive_Reactions/{notification}')
    .onWrite((change, context) => {

        const notificationData = change.after.data();

        console.log("Notif data : " + JSON.stringify(notificationData));

        const postID = notificationData.postID;
        const notificationAuthorID = notificationData.reactionAuthorID;

        const notificationReactionMessage = 'just liked your post.';

        let notification = {
            title: notificationReactionMessage,
        };

        let payload = {
            notification,
        };

        return usersRef
            .doc(notificationAuthorID)
            .get()
            .then(doc => {
                const data = doc.data();
                const username = data.firstName || data.fullname;
                payload = {
                    notification: {
                        ...payload.notification,
                        title: `${username} ${payload.notification.title}`,
                    },
                    data: {
                        action:'post',
                        id: postID
                    }
                };

                console.log("Payload with user data : " + JSON.stringify(notification));

                return postsRef.doc(postID).get().then(doc => {

                    const postData = doc.data();

                    return usersRef
                        .doc(postData.author.id)
                        .get()
                        .then(doc => {
                            const userData = doc.data();
                            const pushToken = userData.pushToken;
                            const badgeValue = userData.badgeCount ? userData.badgeCount + 1 : 1;
                            payload.notification.badge = badgeValue.toString();
                            usersRef.doc(userData.id).update({ badgeCount: badgeValue });

                            console.log("Payload" + JSON.stringify(payload));

                            if (pushToken){
                                admin.messaging().sendToDevice(pushToken, payload);
                            }
                        })
                        .catch(error => {
                            console.log("error sending message : " + error);
                        });

                })
                .catch(error => {
                    console.log("error getting post : " + error);
                });


            })
            .catch(error => {
                console.log("error getting user : " + error);
            });
});

exports.sendCommentPushNotification = functions.firestore
    .document('ClickAndGive_Comments/{notification}')
    .onWrite((change, context) => {

        const notificationData = change.after.data();

        console.log("Comment data : " + JSON.stringify(notificationData));

        const postID = notificationData.postID;
        const commentID = notificationData.commentID;
        const notificationAuthorID = notificationData.authorID;

        const notificationCommentMessage = 'commented on your post.';

        let notification = {
            title: notificationCommentMessage,
        };

        let payload = {
            notification,
        };

        if(notificationData.commentID && !notificationData.publishedNotification ){

            console.log("Sending push");

            return usersRef
                .doc(notificationAuthorID)
                .get()
                .then(doc => {
                    const data = doc.data();
                    const username = data.firstName || data.fullname;
                    payload = {
                        notification: {
                            ...payload.notification,
                            title: `${username} ${payload.notification.title}`,
                        },
                        data: {
                            action:'post',
                            id: postID
                        }
                    };

                    console.log("Payload with user data : " + JSON.stringify(notification));

                    return postsRef.doc(postID).get().then(doc => {

                        const postData = doc.data();

                        return usersRef
                            .doc(postData.author.id)
                            .get()
                            .then(doc => {
                                const userData = doc.data();
                                const pushToken = userData.pushToken;
                                const badgeValue = userData.badgeCount ? userData.badgeCount + 1 : 1;
                                payload.notification.badge = badgeValue.toString();
                                usersRef.doc(userData.id).update({ badgeCount: badgeValue });

                                console.log("Payload" + JSON.stringify(payload));

                                if (pushToken){
                                  admin.messaging().sendToDevice(pushToken, payload);
                                  return commentsRef
                                      .doc(commentID)
                                      .update({ publishedNotification: true })
                                      .catch(err => console.log(err));
                                }
                            })
                            .catch(error => {
                                console.log("error sending message : " + error);
                            });

                    })
                        .catch(error => {
                            console.log("error getting post : " + error);
                        });


                })
                .catch(error => {
                    console.log("error getting user : " + error);
                });
        } else {
          return null;
        }
});

exports.payWithStripe = functions.https.onRequest((request, response) => {

      console.log('Stripe key  : ', STRIPE_SECRET_KEY);

      stripe.charges.create({
          amount: request.body.amount * 100,
          currency: request.body.currency,
          source: request.body.tokenId,
          description: request.body.description,
          receipt_email: request.body.email
      })
      .then((charge) => {
        response.send(charge);
      })
      .catch(err =>{
        console.log("Erreur paiement  : " + err);
        response.send(err);
      });
});

exports.initiatePaypalPayment = functions.https.onRequest((request, response) => {

      const configDev = {
          mode: 'sandbox',
          client_id: "AQTttTGm0VjVTQyk4YF81jAtI98ntc_EAKks4KRqdkBrx_GwVknkhsaGxBcOGA1fUWuX1AY-TPfIXaDT",
          client_secret: "EKdj_6r5G6LnS9P8IjkoV6iHbaJNWRSFUBU4LDEYg6Rf8_4Oa7KcskNYtwYB0k3IN6TzJ7yfpwv0jqhn",
      };
      const configProd = {
          mode: 'live',
          client_id: "AUNFWd1bYZMIpHkIGYcAhzwEtL3sdPsDjU9psn2iOC5kXlq7us9x47qqVdfO9JZ3tAcKW9I-vbN8dCBZ",
          client_secret: "EIdQGJWAgF2SMBj97PKZ715yg1r5raeA8QKlb3j3mOIKGjRmv38RwHFIPuDzGPbiadvmYkD8F4qimucn",
      };

      const config = stageProd ? configProd : configDev;

    paypal.configure(config);

    let requestBody = JSON.parse(request.body);
    console.log(requestBody.amount);

    var create_payment_json = {
        intent: "sale",
        payer: {
            payment_method: "paypal"
        },
        redirect_urls: {
            return_url: "com.ardian.clickandgive.paymentcreationSuccess",
            cancel_url: "com.ardian.clickandgive.paymentcreationError"
        },
        transactions: [
            {
              amount: {
                  currency: requestBody.currency,
                  total: requestBody.amount
              },
              description: requestBody.description
            }
        ]
    };

    paypal.payment.create(create_payment_json, (error, payment)  => {
        if (error) {
            response.send(error);
        } else {
            response.send(payment);
            console.log(payment);
            console.log(error);
        }
    });

});

exports.executePaypalPayment = functions.https.onRequest((request, response) => {

    const configDev = {
        mode: 'sandbox',
        client_id: "AQTttTGm0VjVTQyk4YF81jAtI98ntc_EAKks4KRqdkBrx_GwVknkhsaGxBcOGA1fUWuX1AY-TPfIXaDT",
        client_secret: "EKdj_6r5G6LnS9P8IjkoV6iHbaJNWRSFUBU4LDEYg6Rf8_4Oa7KcskNYtwYB0k3IN6TzJ7yfpwv0jqhn",
    };
    const configProd = {
        mode: 'live',
        client_id: "AUNFWd1bYZMIpHkIGYcAhzwEtL3sdPsDjU9psn2iOC5kXlq7us9x47qqVdfO9JZ3tAcKW9I-vbN8dCBZ",
        client_secret: "EIdQGJWAgF2SMBj97PKZ715yg1r5raeA8QKlb3j3mOIKGjRmv38RwHFIPuDzGPbiadvmYkD8F4qimucn",
    };

    const config = configProd;

    paypal.configure(config);

    let requestBody = JSON.parse(request.body);
    console.log(requestBody.paymentId);


    var paymentId = requestBody.paymentId;
    var payerId = { payer_id: requestBody.payerId };

    paypal.payment.execute(paymentId, payerId, function(error, payment){
        if(error){
            console.error(JSON.stringify(error));
            response.send(error);
        } else {
            if (payment.state === 'approved'){
                console.log('payment completed successfully');
            } else {
                console.log('payment not successful');
            }
            response.send(payment);
        }
    });
  });
