export const projectsList = [
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"foundation", ardianEngagement: "Aridan foundation support this project since 2011 and helped this project reach more mentees throught financial support and access to mentor.",
      highlighted:false,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
         region:"Americas",
         city:"New York",
         country:"USA"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 1",
      donations:{
         fund:{
            number: 12,
            amount: 12500
         },
         volunteer: 3
      },
      needFund:false,
      currency:{
         sign:"£",
         name:"pound sterling"
      },
      endDate:{
         seconds:1609448876,
         nanoseconds:0
      },
      categories:[
         "Environnement", "Artistic projects",
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:true,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"Employee",
      highlighted: true,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
         region:"Europe",
         city:"Luxembourg",
         country:"Luxembourg"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 2",
      donations:{
         fund:{
            number: 12,
            amount: 62500
         },
         volunteer: 3
      },
      needFund:true,
      currency:{
         sign:"$",
         name:"dollar"
      },
      endDate:{
         seconds:1598908076,
         nanoseconds:0
      },
      categories:[
        "Artistic projects", "Primary education",
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:false,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Asia",
            city:"Seoul",
            country:"South Korea"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"foundation", ardianEngagement: "Aridan foundation support this project since 2011 and helped this project reach more mentees throught financial support and access to mentor.",
      highlighted: true,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
         region:"Americas",
         city:"New York",
         country:"USA"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 3",
      displayVolunteerProgressBarOnList: true,
      donations:{
         fund:{
            number: 12,
            amount: 27500
         },
         volunteer: 3
      },
      needFund:true,
      currency:{
         sign:"£",
         name:"pound sterling"
      },
      endDate:{
         seconds:1609448876,
         nanoseconds:0
      },
      categories:[
         "Primary education", "Secondary and Higher education",
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:true,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"Employee",
      highlighted:true,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
        city:	"Beijing",
        country: "China",
        region: "Asia"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 4",
      donations:{
         fund:{
            number: 12,
            amount: 12500
         },
         volunteer: 8
      },
      needFund:false,
      currency:{
         sign:"£",
         name:"pound sterling"
      },
      endDate:{
         seconds:1598908076,
         nanoseconds:0
      },
      categories:[
         "Secondary and Higher education", "Social integration",
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:true,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"foundation", ardianEngagement: "Aridan foundation support this project since 2011 and helped this project reach more mentees throught financial support and access to mentor.",
      highlighted:true,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
        city:	"Frankfurt",
        country: "Germany",
        region: "Europe"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 5",
      donations:{
         fund:{
            number: 12,
            amount: 7650
         },
         volunteer: 3
      },
      needFund:true,
      currency:{
         sign:"€",
         name:"euro"
      },
      endDate:{
         seconds:1609448876,
         nanoseconds:0
      },
      categories:[
        "Social integration", "Disasters",
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:false,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"Employee",
      highlighted: false,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
        city:	"Santiago",
        country: "Chile",
        region: "Americas"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 6",
      donations:{
         fund:{
            number: 12,
            amount: 42500
         },
         volunteer: 3
      },
      needFund:true,
      currency:{
         sign:"$",
         name:"dollar"
      },
      endDate:{
         seconds:1593637676,
         nanoseconds:0
      },
      categories:[
         "Disasters", "Humanitarian causes"
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:true,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   },
   {
      author:{
         role:"user",
         postsCount:2,
         created_at:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         firstName:"firstName",
         cityLocation:{
            region:"Americas",
            city:"New York",
            country:"USA"
         },
         emailVerified:true,
         createdAt:{
            seconds:1584970931,
            nanoseconds:473000000
         },
         pushToken:"cyCSvrrd1yM:APA91bGQm27YBKZp0jkpchFU0fUyiUOqA-XAFzRT7pRRp7bvUF6l5ySXLLtxz4Gv4cMqHYXq_uORwzVXXETKil9xDWUxU-RsSRAgO08ALnmpnw48HHePMWPbovGxmAtJGO3SHl0-p2n2",
         userID:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         title:"Dev ",
         profilePictureURL:"https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg",
         lastName:"lastName",
         password:"password",
         email:"alexandre@samesame.co",
         id:"WWHJHKxE4yWB8wTUEJzOtth4e2s2",
         selectedInterests:[
            "Artistic projects",
            "Secondary and Higher education"
         ],
         isOnline:false
      },
      id:"Bq1WRuxUmpqLY3RCN1Gr",
      type:"Employee",
      highlighted:false,
      projectPictureURL:"https://firebasestorage.googleapis.com/v0/b/ardian-click-and-give.appspot.com/o/88F09807-F08B-470C-BD2F-A0E6D7A807D8.jpg?alt=media&token=fae60bc2-0fce-4cd3-864a-f521362f25cd",
      cityLocation:{
        city: "Singapore",
        country: "Singapore",
        region: "Asia"
      },
      volunteerInstruction:"Mentor a young student during 3 months by participating in very interactive workshops. It is fun, rewarding and will have a huge impact on the mentees.",
      volunteerTarget:"12",
      description:"ThinkForward provides long-term, personalised coaching to young people in schools with the highest risk of unemployment to enable them to transition into sustained work.",
      name:"ThinkForward 7",
      donations:{
         fund:{
            number: 12,
            amount: 12500
         },
         volunteer: 13
      },
      needFund:false,
      currency:{
         sign:"£",
         name:"pound sterling"
      },
      endDate:{
         seconds:1604178476,
         nanoseconds:0
      },
      categories:[
         "Environnement", "Humanitarian causes"
      ],
      createdAt:{
         seconds:1585240375,
         nanoseconds:817000000
      },
      needVolunteer:true,
      volunteerFrequency:"Every 2 months",
      fundRaisingTarget:50000
   }
 ];
