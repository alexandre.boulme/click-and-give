import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { CreateProjectAdmin } from '../../components';
import AppStyles from '../../AppStyles';
import { firebaseAnalytics } from '../../Core/firebase';

class CreateProjectAdminScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: navigation.state.params.projectDetails.createdAt ? 'Edit your project' : 'Create your project',
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'createProjectAdmin'});
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onNextButtonPress = (user, projectDetails) => {
    this.props.navigation.navigate('CreateProjectSummaryScreen', {user: user, projectDetails: projectDetails});
    return true;
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <CreateProjectAdmin
          user={this.props.user}
          projectDetails={this.props.navigation.state.params.projectDetails}
          onNextButtonPress={this.onNextButtonPress}
        />
      </View>
    );
  }
}

CreateProjectAdminScreen.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object
};

const mapStateToProps = ({ auth, friends }) => {
  return {
    user: auth.user
  };
};

export default connect(mapStateToProps)(CreateProjectAdminScreen);
