import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CreateProjectSummary } from '../../components';
import {addProject, buildProjectDeepLink, updateProject} from '../../Core/project/firebase/project';
import { firebaseStorage } from '../../Core/firebase/storage';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { firebaseAnalytics } from '../../Core/firebase';

class CreateProjectSummaryScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: navigation.state.params.projectDetails.createdAt ? 'Edit your project' : 'Create your project',
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'createProjectSummary_FinalPage'});
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onValidateButtonPress = async () => {
    this.setState({
      loading: true
    });

    if(this.props.navigation.state.params.projectDetails.createdAt){
      console.log("Edit project !");
      updateProject(this.props.navigation.state.params.projectDetails.id, this.props.navigation.state.params.projectDetails).then(response => {
        this.setState({
          loading: false
        });
        this.props.navigation.navigate('ProjectDetail', {
          project: this.props.navigation.state.params.projectDetails,
          editProject: this.props.navigation.state.params.projectDetails.author.id === this.props.user.id});
      }).catch(error => console.log("ERROR UPDATE PROJECT : ", error));
    } else {
      this.startProjectUpload();
    }

  };

  onModifyPress = (projectDetails) => {
    console.log("PROJECT SENT TO FIRST PAGE :");
    console.log(projectDetails);
    this.props.navigation.navigate('CreateProjectFirstScreen', {project: projectDetails});
  };

  startProjectUpload = async () => {

    const projectDetails = this.props.navigation.state.params.projectDetails;
    const user = this.props.navigation.state.params.user;

    var projectFinal = {
      donations: {fund: {amount: 0, number: 0}, volunteer: 0},
      ...projectDetails
    }

    const uploadPromises = [];
    const mediaSources = [];

    console.log("sending Image");

    new Promise((resolve, reject) => {
      firebaseStorage
        .uploadImage(projectFinal.projectPictureURL)
        .then(response => {
            console.log("inside response", response);
            if (!response.error) {
              mediaSources.push({ url: response.downloadURL});

              console.log("File uploaded : OK");

              projectFinal.projectPictureURL = mediaSources[0].url;

              firebaseAnalytics.sendEvent('project_creation', {
                project: projectFinal.name,
                endDate: projectFinal.endDate,
                projectType: projectFinal.type,
                projectSupportArdianFoundation: projectFinal.projectSupportArdianFoundation,
                volunteerTarget: projectFinal.volunteerTarget,
                projectTarget: projectFinal.fundRaisingTarget,
                projectLeader: user.firstName + ' ' + user.lastName,
                interest1: projectFinal.categories.length > 0 ? projectFinal.categories[0] : "",
                interest2: projectFinal.categories.length > 1 ? projectFinal.categories[1] : "",
                interest3: projectFinal.categories.length > 2 ? projectFinal.categories[2] : "",
              });

              console.log("Project JSON created ")

              addProject(
                  projectFinal,
                  user
                ).then(response => {

                  console.log("Project created ");

                  this.setState({
                    loading: false
                  });

                  var message = '';
                  if(user.type == 'admin'){
                    message= 'adminCreatedProject';
                  } else if (projectFinal.needFund && projectFinal.needVolunteer){
                    message= 'createProjectTimeAndMoney';
                  } else if (projectFinal.needFund) {
                    message= 'createProjectMoney';
                  } else if (projectFinal.needVolunteer) {
                    message= 'createProjectTime';
                  }

                  this.props.navigation.navigate('CongratsScreen', {message: message, projectName: projectDetails.name, modalVisible: true});
                }).catch(error => console.log("ADD PROJECT ERROR : ",error));

            } else {
              alert(IMLocalized("Oops! An error occured while uploading your project. Please try again."));
            }
            resolve();
          })
          .catch(error => console.log("UPLOAD FILE ERROR : ",error));
        })
  };

  render() {
    return (
        <CreateProjectSummary
          loading={this.state.loading}
          user={this.props.user}
          projectDetails={this.props.navigation.state.params.projectDetails}
          onValidateButtonPress={this.onValidateButtonPress}
          onModifyButtonPress={this.onModifyPress}
        />
    );
  }
}

CreateProjectSummaryScreen.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object,
};

const mapStateToProps = ({ auth, friends }) => {
  return {
    user: auth.user
  };
};

export default connect(mapStateToProps)(CreateProjectSummaryScreen);
