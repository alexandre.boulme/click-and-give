import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CreateProjectFirst } from '../../components';
import AppStyles from '../../AppStyles';
import { firebaseAnalytics } from '../../Core/firebase';

class CreateProjectFirstScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: navigation.state.params.project.createdAt ? 'Edit your project' : 'Create your project',
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'createProjectFirstPage'});
    console.log("PROJECT FROM SUMMARY");
    console.log(this.props.navigation.state.params.project);
  }


  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onNextButtonPress = (user, projectDetails) => {
    this.props.navigation.navigate('CreateProjectSecondScreen', {user: user, projectDetails: projectDetails});
  };


  render() {
    return (
      <CreateProjectFirst
        user={this.props.user}
        project={this.props.navigation.state.params.project}
        onNextButtonPress={this.onNextButtonPress}
      />
    );
  }
}

CreateProjectFirstScreen.propTypes = {
  user: PropTypes.object,
};

const mapStateToProps = ({ auth, friends }) => {
  return {
    user: auth.user
  };
};

export default connect(mapStateToProps)(CreateProjectFirstScreen);
