import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View,  } from 'react-native';
import { connect } from 'react-redux';
import { CreateProjectSecond } from '../../components';
import AppStyles from '../../AppStyles';
import { firebaseAnalytics } from '../../Core/firebase';

class CreateProjectSecondScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: navigation.state.params.projectDetails.createdAt ? 'Edit your project' : 'Create your project',
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'createProjectSecondPage'});
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onNextButtonPress = (user, projectDetails) => {
    console.log(projectDetails);

    if(user.role == 'admin'){
      this.props.navigation.navigate('CreateProjectAdminScreen', {user: user, projectDetails: projectDetails});
    } else {
      this.props.navigation.navigate('CreateProjectSummaryScreen', {user: user, projectDetails: projectDetails});
    }
    return true;
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <CreateProjectSecond
          user={this.props.user}
          projectDetails={this.props.navigation.state.params.projectDetails}
          projects={this.props.projects}
          onNextButtonPress={this.onNextButtonPress}
        />
      </View>
    );
  }
}

CreateProjectSecondScreen.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object
};

const mapStateToProps = ({ auth, projectFeed }) => {
  return {
    user: auth.user,
    projects: projectFeed.feedProjects,
  };
};

export default connect(mapStateToProps)(CreateProjectSecondScreen);
