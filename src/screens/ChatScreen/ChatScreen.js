import PropTypes from 'prop-types';
import {View, Image} from 'react-native';
import React, { Component } from 'react';
import { connect, ReactReduxContext } from 'react-redux';
import { IMChatHomeComponent } from '../../Core/chat';
import { TNTouchableIcon } from '../../Core/truly-native';
import AppStyles from '../../AppStyles';
import {
  setFriends,
  setFriendships
} from '../../Core/socialgraph/friendships/redux';
import { setUsers } from '../../Core/onboarding/redux/auth';
import { TNHeaderView } from '../../Core/truly-native';
import FriendshipTracker from '../../Core/socialgraph/friendships/firebase/tracker';
import { firebaseAnalytics } from '../../Core/firebase';

class ChatScreen extends Component {

  static contextType = ReactReduxContext;

  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;
    return {
      headerTitle: <Image source={AppStyles.iconSet.clickandgive} resizeMode="contain" style={currentTheme.headerTitle} />,
      headerLeft:
        <TNTouchableIcon
          imageStyle={{tintColor: currentTheme.fontColor}}
          iconSource={AppStyles.iconSet.menuHamburger}
          onPress={params.openDrawer}
          appStyles={AppStyles}
        />
      ,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      isSearchModalOpen: false,
      filteredFriendships: [],
      loading: true,
    };
    this.searchBarRef = React.createRef();
  }

  componentDidMount() {
    const user = this.props.user;
    this.friendshipTracker = new FriendshipTracker(this.context.store, user.id || user.userID, true, false, true);

    const self = this;
    self.props.navigation.setParams({
      toggleCamera: self.toggleCamera,
      openDrawer: self.openDrawer,
    });

    this.friendshipTracker.subscribeIfNeeded();

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'chat_conversationlist'});
  }

  componentWillUnmount() {
    if(this.friendshipTracker){
      this.friendshipTracker.unsubscribe();
    }
  }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  onUserItemPress = user => {
    this.setState({ isSearchModalOpen: false });

    const id1 = this.props.user.id || this.props.user.userID;
    const id2 = user.id || user.userID;

    if(id1 != null && id2 != null){
      const channel = {
        id: id1 < id2 ? id1 + id2 : id2 + id1,
        participants: [user]
      }

      this.props.navigation.navigate('PersonalChat', { channel, appStyles: AppStyles });

    }
  };

  onCreateConversation = () => {
    this.props.navigation.navigate('CreateGroup', { appStyles: AppStyles })
  };

  onChatAction = user => {
    this.setState({ isSearchModalOpen: false });

    const id1 = this.props.user.id || this.props.user.userID;
    const id2 = user.id || user.userID;

    const channel = {
      id: id1 < id2 ? id1 + id2 : id2 + id1,
      participants: [user]
    }

    this.props.navigation.navigate('PersonalChat', { channel, appStyles: AppStyles });
  };

  onEmptyStatePress = () => {
    this.navigation.navigate('CreateGroup', { appStyles: AppStyles })
  }

  onSenderProfilePicturePress = (item) => {
    console.log(item);
  }

  render() {
    return (
      <View style={{flex:1}}>
        <TNHeaderView
          headerViewConfig={{
            text: "Messages",
            buttonText: null,
            displayButton: true
          }}
          onPress={this.onCreateConversation}
        />
        <IMChatHomeComponent
          loading={this.state.loading}
          searchBarRef={this.searchBarRef}
          friends={this.props.friends}
          onUserItemPress={this.onUserItemPress}
          onChatAction={this.onChatAction}
          onSearchBarPress={this.onSearchBar}
          searchData={this.state.filteredFriendships}
          onSearchTextChange={this.onSearchTextChange}
          isSearchModalOpen={this.state.isSearchModalOpen}
          onSearchModalClose={this.onSearchModalClose}
          onSearchBarCancel={this.onSearchBar}
          onSearchClear={this.onSearchClear}
          onFriendAction={this.onFriendAction}
          appStyles={AppStyles}
          navigation={this.props.navigation}
          onEmptyStatePress={this.onEmptyStatePress}
          onSenderProfilePicturePress={this.onSenderProfilePicturePress}
        />
      </View>
    );
  }
}

ChatScreen.propTypes = {
  friends: PropTypes.array,
  users: PropTypes.array,
};

const mapStateToProps = ({ friends, auth }) => {
  return {
    user: auth.user,
    friends: friends.friends,
    users: auth.users,
    friendships: friends.friendships,
  };
};

export default connect(mapStateToProps, {
  setFriends,
  setUsers,
  setFriendships
})(ChatScreen);
