import React, { Component } from 'react';
import {connect, ReactReduxContext} from 'react-redux';
import {View, Alert} from 'react-native';
import { DetailProject } from '../../components';
import ProjectManager from '../../Core/project/ProjectManager';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import {firebaseAnalytics} from "../../Core/firebase";
import TNTouchableIcon from "../../Core/truly-native/TNTouchableIcon/TNTouchableIcon";
import ActionSheet from "react-native-actionsheet";
import {deleteProject} from "../../Core/project/firebase/project";
import Toast from "react-native-tiny-toast";
import Clipboard from '@react-native-clipboard/clipboard';

class DetailProjectScreen extends Component {
  static contextType = ReactReduxContext;

  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: IMLocalized(''),
      headerStyle: {
        backgroundColor: '#00000035',
        borderBottomColor: 'transparent',
      },
      headerTintColor: "#FFF",
      headerRight: <TNTouchableIcon
          imageStyle={{tintColor: currentTheme.backgroundColor, padding:15}}
          iconSource={AppStyles.iconSet.moreTopBar}
          onPress={navigation.getParam("onProjectMenuPress")}
          appStyles={AppStyles}/>
    };
  };

  constructor(props) {
    super(props);
    const project = this.props.navigation.state.params.project || null;
    this.projectDialogRef = React.createRef();
  }

  componentDidMount() {
    this.projectManager = new ProjectManager(this.context.store, this.props.user.id);
    this.projectManager.subscribeIfNeeded();
    this.props.navigation.setParams({onProjectMenuPress: () => this.onProjectMenuPress()});

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'projectDetail', project: this.props.navigation.state.params.project.name});
  }

  componentWillUnmount() {
    this.projectManager.unsubscribe();
  }

  onProjectMenuPress = () => {
    this.projectDialogRef.current.show()
  };

  onProjectDialogDone = index => {
    console.log("index",index);
    switch(index){
      case 0:
        const deeplink = this.props.navigation.state.params.project.projectDeeplinkUrl;
        Clipboard.setString(deeplink);
        console.log("Link : ", deeplink);
        Toast.show(IMLocalized('Project link copied to clipboard !'), {duration: 2000});
        break;
      case 1:
        if(this.props.navigation.state.params.editProject){
          this.props.navigation.navigate('CreateProjectFirstScreen', {project: this.props.navigation.state.params.project});
        }
        break;
      case 2:
        if(this.props.navigation.state.params.editProject) {
          const donations = this.props.donations.filter(item => item.projectId == this.props.navigation.state.params.project.id && item.status == "Confirmed" && item.author != null);
          console.log('Liste des donations : ', donations.length);
          if (donations.length == 0) {
            Alert.alert(
                "Delete project",
                "Are you sure ? This action can't be undone.",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  {
                    text: "Delete", onPress: () => {
                      deleteProject(this.props.navigation.state.params.project);
                      Toast.show(IMLocalized('Project successfully deleted !'), {duration: 2000});
                      this.props.navigation.goBack();
                    }
                  }
                ]
            );
          } else {
            Toast.show(IMLocalized('You can\'t delete a project with donations, please contact Click & Give admin team.'), {duration: 5000});
          }
        }
        break;
      default:
        break;
    }
  };


  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onFundDonation = (project) => {
    this.props.navigation.navigate('DonationAmountScreen', {project: project, foundationDonation: false});
  };

  onVolunteerDonation = (project) => {
  };

  onDonatorItemPress = (donator) => {
    if (donator.id === this.props.user.id) {
        this.props.navigation.navigate('Profile');
    } else {
      this.props.navigation.navigate('DonatorProfile', {
        user: donator,
        stackKeyTitle: 'DonatorProfile',
        lastScreenTitle: 'DetailProject',
      });
    }
  };

  onGiveFundButtonPress = (project, donationType) => {
    switch (donationType) {
      case 'time':
        this.props.navigation.navigate('GiveFundScreen', {project: project, foundationDonation: false, volunteerDonation: true});
        break;
      case 'money':
        this.props.navigation.navigate('DonationAmountScreen', {project: project, foundationDonation: false});
        break;
      default:
        break;
    }
  };



  render() {
    return (
      <View
        style={{height: "100%"}}>
        <DetailProject
          onGiveFundButtonPress={this.onGiveFundButtonPress}
          project={this.props.navigation.state.params.project}
          donationsList={this.props.donations}
          user={this.props.user}
          onDonatorItemPress={this.onDonatorItemPress}
        />
        <ActionSheet
            ref={this.projectDialogRef}
            options={this.props.navigation.state.params.editProject ? [IMLocalized('Copy project link'),IMLocalized('Edit project'), IMLocalized('Delete project'), IMLocalized('Cancel')] : [IMLocalized('Copy project link'), IMLocalized('Cancel')]}
            destructiveButtonIndex={this.props.navigation.state.params.editProject ? 2 : 1}
            cancelButtonIndex={this.props.navigation.state.params.editProject ? 3 : 1}
            onPress={this.onProjectDialogDone}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ projectFeed, auth }) => {
  return {
    user: auth.user,
    donations: projectFeed.feedProjectsDonations
  };
};

export default connect(mapStateToProps)(DetailProjectScreen);
