import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function TopEllipse(props) {
  return (
    <View style={styles.container}>
      <Svg viewBox="0 0 530.20 295.35" style={styles.ellipse}>
        <Ellipse
          strokeWidth={1}
          fill="#F73750"
          stroke="#FFF"
          cx={235}
          cy={50}
          rx={315}
          ry={87}
        ></Ellipse>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: -20,
    left: 0
  },
  ellipse: {
    width: 430,
    height: 295,
    marginTop: -56,
    alignSelf: "center"
  }
});

export default TopEllipse;
