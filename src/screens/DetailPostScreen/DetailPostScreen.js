import React, { Component } from 'react';
import { connect } from 'react-redux';
import {BackHandler, Modal, Share, Text, TouchableOpacity, View} from 'react-native';
import {CreatePost, DetailPost} from '../../components';
import {
  firebaseComment,
  firebasePost,
} from '../../Core/socialgraph/feed/firebase';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { reportingManager } from '../../Core/user-reporting';
import {firebaseAnalytics} from "../../Core/firebase";
import {getPost} from "../../Core/socialgraph/feed/firebase/post";
import {TNTouchableIcon} from "../../Core/truly-native";
import ActionSheet from "react-native-actionsheet";
import CreatePostModal from "../../components/createPostModal/CreatePostModal";
import Toast from "react-native-tiny-toast";

class DetailScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;
    return {
      headerTitle: IMLocalized('Post'),
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
      headerRight: params.editPost && (<TNTouchableIcon
            imageStyle={{tintColor: currentTheme.fontColor, padding:15}}
            iconSource={AppStyles.iconSet.moreTopBar}
            onPress={navigation.getParam("onPostMenuPress")}
            appStyles={AppStyles}/>)
    };
  };

  constructor(props) {
    super(props);

    this.item = this.props.navigation.getParam('item');

    this.state = {
      comments: [],
      feedItem: this.item,
      selectedMediaIndex: null,
      selectedFeedItems: [],
      commentsLoading: true,
      isMediaViewerOpen: false,
      shouldUpdate: false,
      selectedComment: null,
      showEditCommentModal: false,
      editPostModalVisible: false
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );

    this.postDialogRef = React.createRef();
    this.scrollViewRef = React.createRef();
    this.lastScreenTitle = this.props.navigation.getParam('lastScreenTitle');
    this.ProfileScreenTitle = this.lastScreenTitle + 'Profile';
    this.likedPost = false;
    this.commentDialogRef = React.createRef();

  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
    this.unsubscribeSinglePost = firebasePost.subscribeToSinglePost(this.item.id, this.onFeedItemUpdate);
    this.unsubscribeComments = firebaseComment.subscribeComments(this.item.id, this.onCommentsUpdate);
    this.setState({ shouldUpdate: true });

    this.props.navigation.setParams({onPostMenuPress: () => this.onPostMenuPress()});

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'postDetail', post: this.state.feedItem.postText});
  }

  componentWillUnmount() {
    this.unsubscribeComments && this.unsubscribeComments();
    this.unsubscribeSinglePost && this.unsubscribeSinglePost();

    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onPostMenuPress = () => {
    this.postDialogRef.current.show()
  }

  onPostDialogDone = index => {
    switch (index){
      case 0:
        console.log("Edit the post !!");
        this.setState({editPostModalVisible : true});
        break;
      case 1:
        console.log("the post ", this.state.feedItem)
        this.onDeletePost(this.state.feedItem);
        break;
      default:
        break;
    }
  }

  loadPost = async postId => {
    const post = await getPost(postId);
    this.item = post;
    this.setState({feedItem: post});
  }

  onFeedItemUpdate = (feedItem) => {
    const myReaction = this.props.myReactions.find(reaction => (reaction.postID == feedItem.id));
    if (myReaction) {
      const finalFeedItem = { ...feedItem, myReaction: myReaction.reaction };
      this.setState({ feedItem: finalFeedItem });
    } else {
      this.setState({ feedItem, myReaction: null });
    }
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onCommentsUpdate = comments => {
    this.postCommentLength = comments.length;
    this.setState({
      comments,
      commentsLoading: false,
    });
  };

  onCommentSend = async value => {
    const commentObject = {
      postID: this.state.feedItem.id,
      commentText: value,
      authorID: this.props.user.id
    };
    firebaseComment.addComment(commentObject, this.props.user, this.state.feedItem, false);
    firebaseAnalytics.sendEvent("comment_on_post", {postText: commentObject.commentText, postId: commentObject.postID});
  };

  onReaction = async (reaction, item) => {
    if(reaction){
      firebaseAnalytics.sendEvent("like_post", {postId: this.state.feedItem.id});
    } else {
      firebaseAnalytics.sendEvent("unlike_post", {postId: this.state.feedItem.id});
    }

    await firebaseComment.handleReaction(reaction, this.props.user, item, false, this.props.users);
  };

  onMediaPress = (media, mediaIndex) => {
    this.setState({
      selectedMediaIndex: mediaIndex,
      selectedFeedItems: media,
      isMediaViewerOpen: true,
    });
  };

  onMediaClose = () => {
    this.setState({ isMediaViewerOpen: false });
  };

  onFeedUserItemPress = async item => {
    if (item.id === this.props.user.id) {
      this.props.navigation.navigate(this.ProfileScreenTitle, {
        stackKeyTitle: this.ProfileScreenTitle,
        lastScreenTitle: this.lastScreenTitle,
      });
    } else {
      this.props.navigation.navigate(this.ProfileScreenTitle, {
        user: item,
        stackKeyTitle: this.ProfileScreenTitle,
        lastScreenTitle: this.lastScreenTitle,
      });
    }
  };

  onSharePost = async item => {
    let url = '';
    if (item.postMedia && item.postMedia.length > 0) {
      url = item.postMedia[0];
    }
    try {
      const result = await Share.share(
        {
          title: 'Share SocialNetwork post.',
          message: item.postText,
          url,
        },
        {
          dialogTitle: 'Share SocialNetwork post.',
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };

  onDeletePost = async item => {
    const res = await firebasePost.deletePost(item);
    if (res.error) {
      alert(res.error);
    } else {
      Toast.show(IMLocalized('Post successfully deleted !'), {duration: 2000});
      this.props.navigation.goBack();
    }
  };

  onUserReport = async (item, type) => {
    await reportingManager.markAbuse(this.props.user.id, item.authorID, type)
    this.props.navigation.goBack();
  }

  onCommentPress = () => {
    console.log('Comment press');
  };

  onCommentItemPress = item => {
    this.commentDialogRef.current.show();
    this.setState({selectedComment: item});
  };

  onCommentDialogDone = index => {
    switch (index){
      case 0:
        this.setState({showEditCommentModal: true});
        break;
      case 1:
        firebaseComment.deleteComment(this.state.selectedComment);
        Toast.show(IMLocalized('Comment successfully deleted !'), {duration: 2000});
        break;
      default:
        break;
    }
  };

  hideEditCommentModal = () => {
    this.setState({showEditCommentModal: false});
  };

  hideEditPostModal = () => {
    this.setState({editPostModalVisible: false});
  };


  render() {
    return (
        <View style={{height:"100%"}}>
          <DetailPost
              scrollViewRef={this.scrollViewRef}
              feedItem={this.state.feedItem}
              commentItems={this.state.comments}
              onCommentSend={this.onCommentSend}
              onFeedUserItemPress={this.onFeedUserItemPress}
              onMediaPress={this.onMediaPress}
              feedItems={this.state.selectedFeedItems}
              onMediaClose={this.onMediaClose}
              isMediaViewerOpen={this.state.isMediaViewerOpen}
              selectedMediaIndex={this.state.selectedMediaIndex}
              onReaction={this.onReaction}
              shouldUpdate={this.state.shouldUpdate}
              onSharePost={this.onSharePost}
              onDeletePost={this.onDeletePost}
              onUserReport={this.onUserReport}
              user={this.props.user}
              commentsLoading={this.state.commentsLoading}
              commentDialogRef={this.commentDialogRef}
              onCommentPress={this.onCommentPress}
              onCommentItemPress={this.onCommentItemPress}
              onCommentDialogDone={this.onCommentDialogDone}
              showEditCommentModal={this.state.showEditCommentModal}
              selectedComment={this.state.selectedComment}
              hideEditCommentModal={this.hideEditCommentModal}
          />
          <CreatePostModal
              visible={this.state.editPostModalVisible}
              user={this.props.user}
              postToEdit={this.state.feedItem}
              hideModal={this.hideEditPostModal}/>
          <ActionSheet
              ref={this.postDialogRef}
              options={[IMLocalized('Edit post'), IMLocalized('Delete post'), IMLocalized('Cancel')]}
              destructiveButtonIndex={1}
              cancelButtonIndex={2}
              onPress={this.onPostDialogDone}
          />
        </View>
    );
  }
}

const mapStateToProps = ({ feed, auth, friends }) => {
  return {
    comments: feed.comments,
    myReactions: feed.feedPostReactions,
    user: auth.user,
    users: auth.users,
    friends: friends.friends
  };
};

export default connect(mapStateToProps)(DetailScreen);
