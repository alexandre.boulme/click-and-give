import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, Alert} from 'react-native';
import { DonationAmount } from '../../components';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import {firebaseAnalytics} from "../../Core/firebase";

class DonationAmountScreen extends Component {

  static navigationOptions = ({screenProps, navigation}) => {

    return {
      headerTitle: <Image source={AppStyles.iconSet.clickandgivewhite} resizeMode="contain" style={{width: 130, height: 30}} />,
      headerStyle: {
        backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
        borderBottomColor: AppStyles.colorSet.mainThemeForegroundColor,
      },
      headerTintColor: "#FFF",
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'setDonationAmount'});
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onNextButtonPress = (donation, currency, foundationDonation) => {
    if (this.checkDonationAmount(donation, currency, foundationDonation)){
      if (foundationDonation){
        this.props.navigation.navigate('GiveFoundationScreen', {donationAmount: donation, currency: currency, foundationDonation: foundationDonation, project: this.props.navigation.state.params.project});
      } else {
        this.props.navigation.navigate('GiveFundScreen', {donationAmount: donation,  currency: this.props.navigation.state.params.project.currency, project: this.props.navigation.state.params.project, foundationDonation: false});
      }
    }
  };

  checkDonationAmount = (donation, currency, foundationDonation) => {
    if (donation == 0 || donation == null){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please set a correct amount.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
        return false;
    } else if (foundationDonation && currency == null){
      Alert.alert(IMLocalized('Error'), IMLocalized('Please choose a currency for your donation.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else {
      return true;
    }
  };

  render() {
    return (
      <DonationAmount
        onNextButtonPress={this.onNextButtonPress}
        project={this.props.navigation.state.params.project}
        foundationDonation={this.props.navigation.state.params.foundationDonation}
      />
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user
  };
};

export default connect(mapStateToProps)(DonationAmountScreen);
