import React, {Component} from 'react';
import { connect } from 'react-redux';
import { WireTransferInfo } from '../../components';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import {firebaseAnalytics} from "../../Core/firebase";

class WireTransferInfoScreen extends Component {
    static navigationOptions = ({ screenProps }) => {
        let currentTheme = AppStyles.navThemeConstants[screenProps.theme];

        return {
            headerTitle: IMLocalized(),
            headerStyle: {
                backgroundColor: currentTheme.backgroundColor,
                borderBottomColor: currentTheme.backgroundColor,
            },
            headerTintColor: currentTheme.fontColor,
        };
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'wireTransferPage', country: this.props.user.cityLocation.country});
    }

    onBackButtonPressAndroid = () => {
        this.props.navigation.goBack();
        return true;
    };

    goToDonationValidationScreen = (url)  => {
        this.props.navigation.navigate("DonationValidationScreen", {message: "moneyDonation", project: this.props.navigation.state.params.project, donationId: this.props.navigation.state.params.donationId, url: url, foundationDonation:this.props.navigation.state.params.foundationDonation});
    };

    render() {
        return (
            <WireTransferInfo
                userCountry={this.props.user.cityLocation.country}
                donationAmount={this.props.navigation.state.params.donationAmount}
                currency={this.props.navigation.state.params.currency}
                goToDonationValidationScreen={this.goToDonationValidationScreen}
            />
        );
    }
}

const mapStateToProps = ({ feed, auth, friends }) => {
    return {
        user: auth.user
    };
};

export default connect(mapStateToProps)(WireTransferInfoScreen);
