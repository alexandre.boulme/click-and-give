import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DonationValidation } from '../../components';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import {Linking} from "react-native";
import {firebaseAnalytics} from "../../Core/firebase";

class DonationValidationScreen extends Component {
    static navigationOptions = ({ screenProps }) => {
        let currentTheme = AppStyles.navThemeConstants[screenProps.theme];

        return {
            headerTitle: IMLocalized(),
            headerStyle: {
                backgroundColor: currentTheme.backgroundColor,
                borderBottomColor: currentTheme.backgroundColor,
            },
            headerTintColor: currentTheme.fontColor,
        };
    };

    constructor(props) {
        super(props);
        if (this.props.navigation.state.params.url){
            Linking.openURL(this.props.navigation.state.params.url);
        }
    }

    componentDidMount() {
        firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'donationValidation'});
    }

    onBackButtonPressAndroid = () => {
        this.props.navigation.goBack();
        return true;
    };

    onVolunteerDonationValidationButton = () => {
        this.props.navigation.navigate("CongratsScreen", {message: "timeDonation", owner: this.props.navigation.state.params.project.author});
    };

    onDonationValidationButton = (status) => {

        switch (status){
            case "Confirmed":
                if (this.props.navigation.state.params.foundationDonation){
                    this.props.navigation.navigate("CongratFoundationsScreen", {message: "moneyFoundationDonation", foundationDonation: this.props.navigation.state.params.foundationDonation});
                } else {
                    this.props.navigation.navigate("CongratsScreen", {message: "moneyDonation", foundationDonation: false });
                }
                break;
            case "Pending":
                this.props.navigation.navigate('Project');
                break;
            case "Canceled":
                this.props.navigation.navigate('Project');
                break;
            default:
                this.props.navigation.navigate('Project');
                break;
        }
    };

    render() {
        return (
            <DonationValidation
                user={this.props.user}
                project={this.props.navigation.state.params.project}
                donationId={this.props.navigation.state.params.donationId}
                currency={this.props.navigation.state.params.currency}
                foundationDonation={this.props.navigation.state.params.foundationDonation || false}
                onVolunteerDonationValidationButton={this.onVolunteerDonationValidationButton}
                onDonationValidationButton={this.onDonationValidationButton}
            />
        );
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        user: auth.user
    };
};

export default connect(mapStateToProps)(DonationValidationScreen);
