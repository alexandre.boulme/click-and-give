import React, {Component} from 'react';
import { Share, View, Image} from 'react-native';
import {connect, ReactReduxContext} from 'react-redux';
import {Feed} from '../../components';
import TNTouchableIcon from '../../Core/truly-native/TNTouchableIcon/TNTouchableIcon';
import {
  firebasePost,
  firebaseComment,
} from '../../Core/socialgraph/feed/firebase';
import AppStyles from '../../AppStyles';
import CreatePostModal from '../../components/createPostModal/CreatePostModal'
import FeedManager from '../../Core/socialgraph/feed/FeedManager';
import { TNHeaderView, TNFloatingButton } from '../../Core/truly-native';
import UpdateModal from '../../components/UpdateModal/UpdateModal';
import { firebaseAnalytics } from '../../Core/firebase';
import DeviceInfo from "react-native-device-info";
import VersionCheck from 'react-native-version-check';
import '@react-native-firebase/messaging';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import {getPost} from "../../Core/socialgraph/feed/firebase/post";
import {getProject} from "../../Core/project/firebase/project";
import {getDonation} from "../../Core/project/firebase/donation";
import firebase from "@react-native-firebase/app";
import "@react-native-firebase/dynamic-links";
import {NavigationEvents} from "react-navigation";

class DiscoverScreen extends Component {
  static contextType = ReactReduxContext;

  static navigationOptions = ({screenProps, navigation}) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const {params = {}} = navigation.state;
    return {
      headerTitle: <Image source={AppStyles.iconSet.clickandgive} resizeMode="contain" style={currentTheme.headerTitle} />,
      headerLeft:
        <TNTouchableIcon
          imageStyle={{tintColor: currentTheme.fontColor}}
          iconSource={AppStyles.iconSet.menuHamburger}
          onPress={params.openDrawer}
          appStyles={AppStyles}
        />,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      isCameraOpen: false,
      isMediaViewerOpen: false,
      selectedFeedItems: [],
      selectedMediaIndex: null,
      isFetching: false,
      willBlur: false,
      modalVisible: false,
      needUpdate: false
    };

    this.fetchCallCount = 0;
    this.isFetching = false;
    this.flatlistReady = false;

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload => {
        this.setState({willBlur: false});
      },
    );

  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload => {
        this.setState({willBlur: true});
      },
    );

    this.props.navigation.setParams({
      openDrawer: this.openDrawer,
    });

    this.feedManager = new FeedManager(this.context.store, this.props.user.id);
    this.feedManager.subscribeIfNeeded();

    //UpdatePopUp
    VersionCheck.getLatestVersion()
      .then(latestVersion => {
        var deviceVersion = DeviceInfo.getVersion();

        if(latestVersion != deviceVersion){
          this.setState({needUpdate: true});
        }
      });

    firebase.dynamicLinks().onLink(link => this.handleDynamicLink(link));
    firebase.dynamicLinks().getInitialLink().then(link => this.handleDynamicLink(link));

    PushNotificationIOS.addEventListener('notification', notification => this.handleNotification(notification, 'addEventListener'));
    PushNotificationIOS.getInitialNotification().then(notification => this.handleNotification(notification, 'getInitialNotif'));

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'newsFeed'});
  }

  handleNotification = async (notification, name) => {
    console.log("NOTIFICATION FROM : ", name);
    console.log(notification);

    if(notification){
      var action = notification._data.action;
      var id = notification._data.id;
    }

    console.log(action);

    switch (action){
      case "message":
        this.props.navigation.navigate("Messages");
        break;
      case "post":
        const post = await getPost(id);
        console.log(post);
        this.props.navigation.navigate('DiscoverDetailPost', {
          item: post.data,
          lastScreenTitle: 'Discover',
          editPost: item.author.id === this.props.user.id || this.props.user.role == "admin"
        });
        break;
      case "project":
        const projectNotif = await getProject(id);
        console.log(projectNotif);
        this.props.navigation.navigate('ProjectDetail', {project: projectNotif.data});
        break;
      case "projectFeed":
        this.props.navigation.navigate("Projects");
        break;
      case "donation":
        console.log("Donation id : ",id);
        var donation = await getDonation(id);
        console.log(donation);
        const projectDonation = await getProject(donation.data.projectId);
        console.log(projectDonation);
        this.props.navigation.navigate('DonationValidationScreen', {project: projectDonation.data, donationId: id, currency: projectDonation.data.currency});
        break;
    }
  };

  getQueryParam = (link,param) => {
    var rx = new RegExp("[?&]" + param + "=([^&]+).*$");
    var returnVal = link.search.match(rx);
    return returnVal === null ? "" : returnVal[1];
  }
  handleDynamicLink = async link => {
    console.log("DYNAMIC LINK HANDLED !", link);

    console.log("Url : ", link.url);

    if(link.url && link.url.indexOf('=') > -1 && link.url.indexOf('projectId') > -1){
      const projectId = link.url.substring(link.url.indexOf('=') + 1,link.url.length);

      console.log("Project id : ",projectId);
      if(projectId){
        console.log("Go to project page !");
        const projectDeepLink = await getProject(projectId);
        console.log(projectDeepLink);
        this.props.navigation.navigate('ProjectDetail', {project: projectDeepLink.data});
      }
    }

    if(link.url && link.url.indexOf('=') > -1 && link.url.indexOf('donationId') > -1){
      const donationId = link.url.substring(link.url.indexOf('=') + 1,link.url.length);
      console.log("Donation id : ",donationId);
      if(donationId){
        var donation = await getDonation(donationId);
        console.log(donation);
        const projectDonation = await getProject(donation.data.projectId);
        console.log(projectDonation);
        this.props.navigation.navigate('DonationValidationScreen', {project: projectDonation.data, donationId: donationId, currency: projectDonation.data.currency});
      }
    }

  };

  componentWillUnmount() {
    this.willBlurSubscription && this.willBlurSubscription.remove();
    this.didFocusSubscription && this.didFocusSubscription.remove();

    this.feedManager.unsubscribe();
  }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  onHearderButtonPress = () => {
    this.setState({
      modalVisible: true
    });
  }

  onFloattingButtonPress = () => {
    this.props.navigation.navigate('FoundationAmountScreen', {foundationDonation: true});
  }

  onCommentPress = item => {
    let copyItem = {...item};
    console.log(item);
    this.props.navigation.navigate('DiscoverDetailPost', {
      item: {...copyItem},
      lastScreenTitle: 'Discover',
      editPost: item.author.id === this.props.user.id || this.props.user.role == "admin"
    });
  };

  onFeedUserItemPress = async author => {
    if (author.id === this.props.user.id) {
      this.props.navigation.navigate('Profile');
    } else {
      this.props.navigation.navigate('DiscoverProfile', {
        user: author,
        stackKeyTitle: 'DiscoverProfile',
        lastScreenTitle: 'Discover',
      });
    }
  };

  onMediaClose = () => {
    this.setState({isMediaViewerOpen: false});
  };

  onMediaPress = (media, mediaIndex) => {
    this.setState({
      selectedFeedItems: media,
      selectedMediaIndex: mediaIndex,
      isMediaViewerOpen: true,
    });
  };

  onReaction = async (reaction, item) => {
    this.feedManager.applyReaction(reaction, item, false);
    firebaseComment.handleReaction(reaction, this.props.user, item, false, this.props.users);
  };

  onSharePost = async item => {
    let url = '';
    if (item.postMedia && item.postMedia.length > 0) {
      url = item.postMedia[0];
    }
    try {
      const result = await Share.share(
        {
          title: 'Share SocialNetwork post.',
          message: item.postText,
          url,
        },
        {
          dialogTitle: 'Share SocialNetwork post.',
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };

  onDeletePost = async item => {
    const res = await firebasePost.deletePost(item, true);
    if (res.error) {
      alert(res.error);
    }
  };

  handleOnEndReached = distanceFromEnd => {
    if (!this.flatlistReady) {
      return;
    }

    if (this.state.isFetching || this.isFetching) {
      return;
    }
    if (this.fetchCallCount > 1) {
      return;
    }
  };

  onFeedScroll = () => {
    this.flatlistReady = true;
  };

  filterOutRelatedPosts = (posts) => {
    // we filter out posts with no media from self & friends
    if (!posts) {
      return posts;
    }
    return posts.filter(post => {
      return (
        post
      );
    })
  }

  hideModal = () => {
    this.setState({modalVisible: false, preFillText: ''});
  }

  hideUpdateModal = () => {
    this.setState({needUpdate: false});
  };

  triggerPreFilledPost = () => {
    if( this.props.navigation.state.params.postPreFill && !this.state.disablePreFill){
      console.log("Prefill : ",this.props.navigation.state.params.postPreFill);
      this.setState({
        modalVisible: true,
        preFillText: this.props.navigation.state.params.postPreFill,
        disablePreFill : true
      });
    } else {
      console.log("No prefill state");
    }

  }

  render() {
    return (
      <View style={{flex:1}}>
      <NavigationEvents onDidFocus={() => this.triggerPreFilledPost()} />
      <TNHeaderView
        headerViewConfig={{
          text: "Newsfeed",
          buttonText: "Give news",
          displayButton: true
        }}
        onPress={this.onHearderButtonPress}
      />
      <Feed
        loading={this.props.discoverFeedPosts == null}
        feed={this.props.discoverFeedPosts}
        onFeedUserItemPress={this.onFeedUserItemPress}
        onCommentPress={this.onCommentPress}
        isMediaViewerOpen={this.state.isMediaViewerOpen}
        feedItems={this.state.selectedFeedItems}
        onMediaClose={this.onMediaClose}
        onMediaPress={this.onMediaPress}
        selectedMediaIndex={this.state.selectedMediaIndex}
        handleOnEndReached={this.handleOnEndReached}
        isFetching={this.state.isFetching}
        onReaction={this.onReaction}
        onSharePost={this.onSharePost}
        onDeletePost={this.onDeletePost}
        user={this.props.user}
        onFeedScroll={this.onFeedScroll}
        willBlur={this.state.willBlur}
        onEmptyStatePress={this.onHearderButtonPress}
      />
      <TNFloatingButton
        onPress={this.onFloattingButtonPress}
      />
      <CreatePostModal
        visible={this.state.modalVisible}
        user={this.props.user}
        hideModal={this.hideModal}
        postToEdit={null}
        preFillText={this.state.preFillText}
      />
      <UpdateModal
          modalVisible={this.state.needUpdate}
          hideModal={this.hideUpdateModal}
      />
      </View>
    );
  }
}

const mapStateToProps = ({feed, auth, friends}) => {
  return {
    discoverFeedPosts: feed.discoverFeedPosts,
    user: auth.user,
    users: auth.users,
    friends: friends.friends,
  };
};

export default connect(mapStateToProps)(DiscoverScreen);
