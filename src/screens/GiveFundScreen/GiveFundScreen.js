import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GiveFund } from '../../components';
import AppStyles from '../../AppStyles';
import PaymentManager from '../../Core/payment/PaymentManager';
import {channelManager} from "../../Core/chat/firebase";
import {notificationManager} from "../../Core/notifications";
import {reportingManager} from "../../Core/user-reporting";
import {setChannelParticipations, setChannels, setChannelsSubcribed} from "../../Core/chat/redux";
import {setBannedUserIDs} from "../../Core/user-reporting/redux";
import {firebaseAnalytics, firebaseUser} from "../../Core/firebase";
import {ADMIN_RECEIVING_DONNATION_CHAT_ID} from 'react-native-dotenv';

class GiveFundScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];

    return {
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);

    this.paymentManager = new PaymentManager();
    this.paymentProcess = this.definePaymentMethod();
    console.log("Current payment process : " + this.paymentProcess);
  }

  componentDidMount() {
    const self = this;
    const userId = self.props.user.id || self.props.user.userID;
    if (!self.props.areChannelsSubcribed) {
      self.unsubscribeAbuseDB = reportingManager.unsubscribeAbuseDB(userId, this.onAbuseDBUpdate);
      self.props.setChannelsSubcribed(true);
    } else {
      self.setState({ loading: false });
    }
    firebaseAnalytics.sendEvent('CandG_screen_view',
        {screenName:'donationPaymentPage'});
  }

  componentWillUnmount() {
    if (this.channelPaticipationUnsubscribe) {
      this.channelPaticipationUnsubscribe();
    }
    if (this.channelsUnsubscribe) {
      this.channelsUnsubscribe();
    }
    if (this.unsubscribeAbuseDB) {
      this.unsubscribeAbuseDB();
    }
  }

  onAbuseDBUpdate = (abuses) => {

    const bannedUserIDs = [];
    abuses.forEach(abuse => bannedUserIDs.push(abuse.dest));
    this.props.setBannedUserIDs(bannedUserIDs);

    this.props.setChannels(this.channelsWithNoBannedUsers(this.props.channels, bannedUserIDs));

    const userId = this.props.user.id || this.props.user.userID;
    this.channelParticipantUnsubscribe = channelManager.subscribeChannelParticipation(
        userId,
        this.onChannelParticipationCollectionUpdate,
    );
  }

  onChannelParticipationCollectionUpdate = (data) => {
    const channels = this.props.channels.filter(channel => {
      return (
          data.filter(participation => channel.id === participation.channel)
              .length > 0
      );
    });

    this.props.setChannels(this.channelsWithNoBannedUsers(channels, this.props.bannedUserIDs));
    this.props.setChannelParticipations(data);
  };

  onChannelCollectionUpdate = querySnapshot => {
    const self = this;
    const { data, channelPromiseArray } = channelManager.filterQuerySnapshot(
        self,
        querySnapshot,
        channelManager,
    );

    Promise.all(channelPromiseArray).then(() => {
      const sortedData = data.sort(function (a, b) {
        if (!a.lastMessageDate) {
          return 1;
        }
        if (!b.lastMessageDate) {
          return -1;
        }
        a = new Date(a.lastMessageDate.seconds);
        b = new Date(b.lastMessageDate.seconds);
        return a > b ? -1 : a < b ? 1 : 0;
      });

      self.props.setChannels(self.channelsWithNoBannedUsers(sortedData, self.props.bannedUserIDs));
      self.setState({ loading: false });
    });
  };

  channelsWithNoBannedUsers = (channels, bannedUserIDs) => {

    const channelsWithNoBannedUsers = [];

    channels.forEach(channel => {
      if (
          !bannedUserIDs
          || !channel.participants
          || channel.participants.length != 1
          || !bannedUserIDs.includes(channel.participants[0].id)) {
        channelsWithNoBannedUsers.push(channel);
      }
    });
    return channelsWithNoBannedUsers;
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  definePaymentMethod = () => {

    let user = this.props.user;
    const userCountry = user.cityLocation.country;
    const userRegion = user.cityLocation.region;
    const project = this.props.navigation.state.params.project;
    let paymentMode = 'alizee';

    console.log("Foundation project");
    console.log(this.props.navigation.state.params.foundationDonation);
    console.log("User country : " + userCountry);
    console.log("For alexandre@samesame.co account, payment forced to Alizee (for apple review)");

    if (this.props.navigation.state.params.foundationDonation) {
      if (userCountry == "France" || userCountry == "Luxembourg") {
        paymentMode = 'alizee';
      } else if (userCountry == "USA") {
        paymentMode = 'card';
      } else {
        paymentMode = 'alizee';
      }
    } else if (project.type == "foundation" || (project.type == "employee" && project.supportArdianFoundation)){
      const projectCountry = project.cityLocation.country;
      if (projectCountry === "USA"){
        paymentMode =  'card';
      } else {
        console.log("Outside the card");
        if(userCountry == "France" || userCountry == "Luxembourg"){
          paymentMode = 'alizee';
        } else if (userCountry == "USA") {
          paymentMode =  'alizee';
        } else if (userRegion == "Europe") {
          paymentMode = 'wireTransfer';
        } else {
          paymentMode = 'alizee';
        }
      }
    } else if (project.type == "employee" && !project.supportArdianFoundation){
      paymentMode = 'donationPlatform';
    }

    if (user.email === "alexandre@samesame.co" ){
      return 'alizee';
    } else {
      return paymentMode;
    }
  };

  onVolunteerDonationValidationButton = (url) => {
    this.props.navigation.navigate("CongratsScreen", {message: "timeDonation", owner: this.props.navigation.state.params.project.author, url: url ? url  : "https://www.justgiving.com/"});
  };

  onMoneyDonationValidationButton = () => {
    this.props.navigation.navigate("CongratFoundationsScreen", {message: "moneyFoundationDonation", foundationDonation: this.props.navigation.state.params.foundationDonation});
  };

  goToWireTranfertPage = (donationId) => {
    this.props.navigation.navigate("WireTransferInfoScreen", { project:  this.props.navigation.state.params.project, donationId: donationId, currency: this.props.navigation.state.params.currency, donationAmount: this.props.navigation.state.params.donationAmount, foundationDonation: this.props.navigation.state.params.foundationDonation});
  };

  goToDonationValidationScreen = (url, donationId)  => {
    this.props.navigation.navigate("DonationValidationScreen", {project:  this.props.navigation.state.params.project, foundationDonation: this.props.navigation.state.params.foundationDonation,  message: "moneyDonation", donationId: donationId, url: url ? url : "https://www.justgiving.com/", currency: this.props.navigation.state.params.currency});
  };

  createOne2OneChannel = async () => {

    const participant = await firebaseUser.getUserById(this.state.participantId);
    const ortherParticipant = [participant];

    return new Promise(resolve => {
      channelManager
          .createChannel(this.props.user, ortherParticipant)
          .then(response => {

            this.setState({ channel: response.channel });

            this.threadUnsubscribe = channelManager.subscribeThreadSnapshot(
                response.channel,
                this.onThreadCollectionUpdate,
            );

            resolve(response.channel);
          })
          .catch(error => {
            console.log("ERROR : " + error)
          });
    });
  };

  onThreadCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const message = doc.data();
      data.push({ ...message, id: doc.id });
    });

    this.setState({ thread: data });
  };

  onSendInput = async (sendToUserId, message) => {
    const participantsId = sendToUserId ? sendToUserId : ADMIN_RECEIVING_DONNATION_CHAT_ID;
    const userId = this.props.user.id || this.props.user.userID;

    const messageToSend = 'Time donation message - ' + message;

    this.setState({participantId : participantsId, inputValue: messageToSend});

    channelManager.getChannelById(userId, participantsId, this.handleChannelResponse);
  };

  handleChannelResponse = (channel) => {
    const self = this;

    if(channel != null){
      self.sendMessage(this.state.inputValue, channel);
    } else {
      // If we don't have a chat id, we need to create it first together with the participations
      this.createOne2OneChannel().then(_response => {
        self.sendMessage(this.state.inputValue, _response);
      });
    }
  };

  sendMessage = (inputValue, channel) => {
    channelManager
      .sendMessage(this.props.user, channel, inputValue, '')
      .then(response => {
        if (response.error) {
          alert(error);
        } else {
          this.broadcastPushNotifications(inputValue, channel);
        }
      });
  };

  broadcastPushNotifications = (inputValue, channel) => {
    const participants = channel.participants;
    if (!participants || participants.length == 0) {
      return;
    }
    const sender = this.props.user;
    const fromTitle = sender.firstName + ' ' + sender.lastName;
    var message;

    message = inputValue;

    participants.forEach(participant => {
      if (participant.id != this.props.user.id) {
        notificationManager.sendPushNotification(
            participant,
            fromTitle,
            message,
            'chat_message',
            { fromUser: sender },
        );
      }
    });
  };

  render() {
    return (
      <GiveFund
        user={this.props.user}
        project={this.props.navigation.state.params.project}
        donationAmount={this.props.navigation.state.params.donationAmount}
        currency={this.props.navigation.state.params.currency}
        paymentProcess={this.paymentProcess}
        foundationDonation={this.props.navigation.state.params.foundationDonation || false}
        volunteerDonation={this.props.navigation.state.params.volunteerDonation || false}
        onVolunteerDonationValidationButton={this.onVolunteerDonationValidationButton}
        onMoneyDonationValidationButton={this.onMoneyDonationValidationButton}
        goToDonationValidationButton={this.goToDonationValidationScreen}
        goToWireTranfertPage={this.goToWireTranfertPage}
        sendNotificationChatMessage={this.onSendInput}
        />
    );
  }
}

const mapStateToProps = ({ auth, chat }) => {
  return {
    channels: chat.channels,
    channelParticipations: chat.channelParticipations,
    areChannelsSubcribed: chat.areChannelsSubcribed,
    thread: chat.thread,
    user: auth.user,
    users: auth.users
  };
};

export default connect(mapStateToProps, {
  setChannels,
  setChannelParticipations,
  setChannelsSubcribed,
  setBannedUserIDs
})(GiveFundScreen);
