import React, {Component} from 'react';
import { View, Image } from 'react-native';
import {connect, ReactReduxContext} from 'react-redux';
import {ProjectFeed, HighlightedProjectFeed} from '../../components';
import TNTouchableIcon from '../../Core/truly-native/TNTouchableIcon/TNTouchableIcon';
import AppStyles from '../../AppStyles';
import ProjectManager from '../../Core/project/ProjectManager';
import { TNHeaderView, TNFloatingButton } from '../../Core/truly-native';
import Svg, { Ellipse } from "react-native-svg";
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {firebaseAnalytics} from "../../Core/firebase";

class ProjectScreen extends Component {
  static contextType = ReactReduxContext;

  static navigationOptions = ({screenProps, navigation}) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const {params = {}} = navigation.state;
    return {
      headerTitle: <Image source={AppStyles.iconSet.clickandgivewhite} resizeMode="contain" style={currentTheme.headerTitle} />,
      headerLeft:
        <TNTouchableIcon
          imageStyle={{tintColor: currentTheme.backgroundColor}}
          iconSource={AppStyles.iconSet.menuHamburger}
          onPress={params.openDrawer}
          appStyles={AppStyles}
        />,
      headerStyle: {
        backgroundColor: currentTheme.activeTintColor,
        borderBottomColor: currentTheme.activeTintColor,
      },
      headerTintColor: currentTheme.backgroundColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      openDrawer: this.openDrawer,
    });

    this.projectManager = new ProjectManager(this.context.store, this.props.user.id);
    this.projectManager.subscribeIfNeeded();

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'projectFeed'});

  }

  componentWillUnmount() {
    this.projectManager.unsubscribe();
  }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  onFloattingButtonPress = project => {
    const projectInit = {
      needFund: false,
      needVolunteer: false,
      requestMatching: false,
      displayVolunteerProgressBarOnList: false,
      highlighted: false,
      approved: false,
      notifications: []
    };
    this.props.navigation.navigate('CreateProjectFirstScreen', {project: projectInit});
  };

  onFeedItemPress = project => {
      this.props.navigation.navigate('ProjectDetail', {
        project: project,
        editProject: project.author.id === this.props.user.id || this.props.user.role == "admin"});
  };

  onFeedUserItemPress = async author => {
      if (author.id === this.props.user.id) {
        this.props.navigation.navigate('Profile');
      } else {
        this.props.navigation.navigate('DonatorProfile', {
          user: author,
          stackKeyTitle: 'AuthorProfile',
          lastScreenTitle: 'ProjectScreen',
        });
      }
  };

  render() {
    return (
      <View style={{flex:1}}>
        <View style={{...ifIphoneX({height: '38%'},{height: '34%'})}}>
          <Svg viewBox="0 0 530.20 295.35" style={{
            width: 430,
            height: 295,
            marginTop: -56,
            alignSelf: "center",
            position: 'absolute',
            top: 0,
            left: 0
          }}>
            <Ellipse
              strokeWidth={1}
              fill="#F73750"
              stroke="#FFF"
              cx={265}
              cy={0}
              rx={415}
              ry={247}
            />
          </Svg>
          <TNHeaderView
            headerViewConfig={{
              text: "Spotlight",
              displayButton: false
            }}
            onPress={this.onHearderButtonPress}
          />
          <HighlightedProjectFeed
            loading={this.props.feedProjects == null}
            feed={this.props.feedProjects}
            onFeedItemPress={this.onFeedItemPress}
            user={this.props.user}
          />
        </View>
        <ProjectFeed
          loading={this.props.feedProjects == null}
          feed={this.props.feedProjects}
          onFeedItemPress={this.onFeedItemPress}
          onFeedUserItemPress={this.onFeedUserItemPress}
          user={this.props.user}
        />
        <TNFloatingButton
            onPress={this.onFloattingButtonPress}
            isRound={true}
            buttonStyle={{...ifIphoneX({},{bottom: 7, right: 7})}}
          />
      </View>
    );
  }
}

const mapStateToProps = ({projectFeed, auth,}) => {
  return {
    feedProjects: projectFeed.feedProjects,
    user: auth.user,
  };
};

export default connect(mapStateToProps)(ProjectScreen);
