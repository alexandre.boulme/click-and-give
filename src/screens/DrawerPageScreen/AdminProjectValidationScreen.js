import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {connect, ReactReduxContext} from 'react-redux';
import { Text, TouchableOpacity, Image } from 'react-native';
import AppStyles from '../../AppStyles';
import ProjectManager from '../../Core/project/ProjectManager';
import AdminProjectValidation from '../../components/screens/DrawerPage/AdminProjectValidation';
import {firebaseAnalytics} from "../../Core/firebase";

class AdminProjectValidationScreen extends Component {
  static contextType = ReactReduxContext;

  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: 'Admin Project Validation',
      headerLeft:
        <TouchableOpacity
          style={{ marginRight: 12, color: AppStyles.colorSet.mainThemeForegroundColor, flexDirection: 'row', alignItems: 'center'}}
          onPress={()=>{
            navigation.navigate('Newsfeed');
          }}>
          <Image style={{width: 20, height: 20, tintColor: AppStyles.colorSet.black}} source={AppStyles.iconSet.backArrow} />
          <Text>Back</Text>
        </TouchableOpacity>,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.projectManager = new ProjectManager(this.context.store, this.props.user.id);
    this.projectManager.subscribeIfNeeded();

    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'adminProjectValidation'});
  }

  componentWillUnmount() {
    this.projectManager.unsubscribe();
  }

  onFeedItemPress = project => {
    this.props.navigation.navigate('ProjectDetail', {project: project, goBackToMenu: true});
  };

  onFeedUserItemPress = async author => {
    if (author.id === this.props.user.id) {
      this.props.navigation.navigate('Profile', {goBackToMenu: true});
    } else {
      this.props.navigation.navigate('DonatorProfile', {
        user: author,
        stackKeyTitle: 'AuthorProfile',
        lastScreenTitle: 'MenuScreen'
      });
    }
  };

  render() {
    return (
      <AdminProjectValidation
        loading={this.props.feedProjects == null}
        feed={this.props.feedProjects}
        onFeedItemPress={this.onFeedItemPress}
        onFeedUserItemPress={this.onFeedUserItemPress}
      />
    );
  }
}

AdminProjectValidationScreen.propTypes = {
  user: PropTypes.object,
};

const mapStateToProps = ({projectFeed, auth,}) => {
  return {
    feedProjects: projectFeed.feedProjects,
    user: auth.user,
  };
};

export default connect(mapStateToProps)(AdminProjectValidationScreen);
