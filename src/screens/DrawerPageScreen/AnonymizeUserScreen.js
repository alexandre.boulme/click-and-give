import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {Text, TouchableOpacity, Image, View, Alert, ScrollView, Linking, Modal} from 'react-native';
import AppStyles from '../../AppStyles';
import {IMCreateGroupComponent} from "../../Core/chat";
import {connect} from "react-redux";
import {channelManager} from "../../Core/chat/firebase";
import {APPSTORE_URL} from "react-native-dotenv";
import styles from './styles';
import {IMLocalized} from "../../Core/localization/IMLocalization";
import {updateUserData} from "../../Core/firebase/user";
import {deactivateUser} from "../../Core/firebase/auth";

class AnonymizeUserScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    return {
      headerTitle: 'Anonymize user',
      headerLeft:
        <TouchableOpacity
          style={{ marginRight: 12, color: AppStyles.colorSet.mainThemeForegroundColor, flexDirection: 'row', alignItems: 'center'}}
          onPress={()=>{
            navigation.goBack(null);
            navigation.openDrawer();
          }}>
          <Image style={{width: 20, height: 20, tintColor: AppStyles.colorSet.black}} source={AppStyles.iconSet.backArrow} />
          <Text>Back</Text>
        </TouchableOpacity>,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.appStyles = AppStyles;
    const userToDisplay = this.props.users.filter(userInList => userInList.id != this.props.user.id);
    this.state = {
          users: userToDisplay,
          userToDelete: {},
          isNameDialogVisible: false,
          groupName: '',
          modalVisible: false
    };
  }

  onCheck = user => {
    this.setState({modalVisible: true, userToDelete: user});
  };

  onCancel = () => {};

  onSubmitName = (name, pictureUrl) => {};

  onEmptyStatePress = () => {};

  anonymizeAccount = async () => {
      console.log("Anonymize on going");
      this.setState({modalVisible:false});
      await updateUserData(this.state.userToDelete.id, {email: "anonymous@ardian.com", firstName: "Anonymous", lastName: "Profile", profilePictureURL: "https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg", selectedInterests: [], title: "Desactivated account", pushToken: "", desactivated: true}).catch(error => console.log("Update error : ", error));
  };

  onUserAnonymization = () => {
      Alert.alert("Last warning", "Are you sure you want to anonymize this account ?", [{ text: IMLocalized('Yes'), onPress:() => this.anonymizeAccount()},{ text: IMLocalized('Cancel'), onPress:()=> this.setState({modalVisible:false}) }], {
          cancelable: true,
      });
  };


  render() {
    return (
    <View style={{flex: 1}}>
      <IMCreateGroupComponent
          onCancel={this.onCancel}
          isNameDialogVisible={this.state.isNameDialogVisible}
          users={this.state.users}
          onSubmitName={this.onSubmitName}
          onCheck={this.onCheck}
          appStyles={this.appStyles}
          onEmptyStatePress={this.onEmptyStatePress}
      />
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
            <ScrollView
                style={styles.scrollViewStyle}>
                <View style={styles.container}>
                    <View style={styles.rowContainer}>
                        <Text style={styles.popUpTextTitle}>{"Caution !"}</Text>
                        <TouchableOpacity
                            style={styles.cancelButtonContainer}
                            onPress={() => {
                                this.setState({modalVisible: false});
                            }}>
                            <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.popUpText}>
                        <Text>{"You are about to anonymize the account of "}</Text>
                        <Text style={{fontWeight: "bold"}}>{this.state.userToDelete.firstName + " " + this.state.userToDelete.lastName}</Text>
                        <Text>{".\n\n It means that this person wont be able to connect to Click & Give anymore, all her posts, projets, chats will display anonymous informations and she will have to create a new account in order to use it again in the future. \n\nDo this only when you have no other choice, for instance the person left ardian."}</Text>
                    </Text>
                    <View style={{flexDirection:"row", justifyContent: 'space-between'}}>
                        <TouchableOpacity
                            style={styles.updateButton}
                            onPress={() => this.onUserAnonymization()}>
                            <Text style={[styles.updateButtonText]}>ANONYMIZE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.updateButton,{backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor, borderColor: AppStyles.colorSet.mainThemeForegroundColor, borderWidth: 2}]}
                            onPress={() => this.setState({modalVisible:false})}>
                            <Text style={[styles.updateButtonText,{color: AppStyles.colorSet.mainThemeForegroundColor}]}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </Modal>
    </View>

    );
  }
}

AnonymizeUserScreen.propTypes = {
  user: PropTypes.object,
  users: PropTypes.array
};

const mapStateToProps = ({ auth }) => {
    return {
        user: auth.user,
        users: auth.users
    };
};

export default connect(mapStateToProps)(AnonymizeUserScreen);
