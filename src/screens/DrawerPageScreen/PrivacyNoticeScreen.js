import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, TouchableOpacity, Image, View, Dimensions } from 'react-native';
import AppStyles from '../../AppStyles';
import Pdf from 'react-native-pdf';
import {firebaseAnalytics} from "../../Core/firebase";

class PrivacyNoticeScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: 'Privacy Notice',
      headerLeft:
        <TouchableOpacity
          style={{ marginRight: 12, color: AppStyles.colorSet.mainThemeForegroundColor, flexDirection: 'row', alignItems: 'center'}}
          onPress={()=>{
            navigation.goBack(null);
            navigation.openDrawer();
          }}>
          <Image style={{width: 20, height: 20, tintColor: AppStyles.colorSet.black}} source={AppStyles.iconSet.backArrow} />
          <Text>Back</Text>
        </TouchableOpacity>,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };


  constructor(props) {
    super(props);
    this.state = {
      langEn: true
    };
  }

  componentDidMount() {
      firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'privacyNotice'});
  }

    switchLang = () => {
      this.setState({langEn: !this.state.langEn})
  }


  render() {

      const sourceEn = {uri:"bundle-assets://DataPrivacyNotice(Eng).pdf"};
      const sourceFr = {uri:"bundle-assets://DataPrivacyNotice(FR).pdf"};

      return (
        <View style={{flex: 1,justifyContent: 'flex-start',alignItems: 'flex-end', backgroundColor: "#333333"}}>
            <Pdf
                source={this.state.langEn ? sourceEn : sourceFr}
                onError={(error)=>{
                    console.log(error);
                }}
                style={{flex:1, width: Dimensions.get('window').width, height:Dimensions.get('window').height, backgroundColor: "#333333"}}
            />
            <TouchableOpacity
                style={{padding: 10, position: "absolute", top: 0, right: 0}}
                onPress={()=>{
                    this.switchLang()
                }}>
                <Text style={{color: AppStyles.colorSet.mainThemeForegroundColor}}>{this.state.langEn ? "FR" : "EN"}</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

PrivacyNoticeScreen.propTypes = {
  user: PropTypes.object,
};

export default PrivacyNoticeScreen;
