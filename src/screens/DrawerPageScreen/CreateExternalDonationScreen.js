import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {Text, TouchableOpacity, Image, View, TextInput} from 'react-native';
import AppStyles from '../../AppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import styles from './styles';
import {firebaseAnalytics} from "../../Core/firebase";
import {Container, Content, Form, Icon, Picker} from "native-base";
import CheckBox from "@react-native-community/checkbox";
import {connect} from "react-redux";
import Toast from "react-native-tiny-toast";
import {addDonation} from "../../Core/project/firebase/donation";
import ClickAndGiveConfig from "../../ClickAndGiveConfig";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

class CreateExternalDonationScreen extends Component {

    static navigationOptions = ({ screenProps, navigation }) => {
        let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
        const { params = {} } = navigation.state;

        return {
            headerTitle: 'Create external donation',
            headerLeft:
                <TouchableOpacity
                    style={{ marginRight: 12, color: AppStyles.colorSet.mainThemeForegroundColor, flexDirection: 'row', alignItems: 'center'}}
                    onPress={()=>{
                        navigation.goBack(null);
                        navigation.openDrawer();
                    }}>
                    <Image style={{width: 20, height: 20, tintColor: AppStyles.colorSet.black}} source={AppStyles.iconSet.backArrow} />
                    <Text>Back</Text>
                </TouchableOpacity>,
            headerStyle: {
                backgroundColor: currentTheme.backgroundColor,
                borderBottomColor: currentTheme.hairlineColor,
            },
            headerTintColor: currentTheme.fontColor,
        };
    };

    constructor(props) {
        super(props);
        console.log("constructor");
        this.state = {
          selectedProject: null,
          selectedUser: null,
          anonymousDonation: false,
          externalDonator: false,
          donationAmount: '',
          paymentInfo: ''
        };
    }

    componentDidMount() {
      firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'createExternalDonation'});
    }

    getUserFromSeletion = (user) => {
        console.log("User selected : ",user);
        this.setState({selectedUser: user});
    }

    createExternalDonation = async () => {
        console.log("project ", this.state.selectedProject);
        console.log("user ", this.state.selectedUser);
        console.log("amount ", this.state.donationAmount);
        console.log("infos ", this.state.paymentInfo);

        if (this.state.selectedProject && (this.state.selectedUser || this.state.externalDonator) && this.state.donationAmount != '' && this.state.paymentInfo != '') {
            var donationDetails = {
                paymentInfos: this.state.paymentInfo,
                foundationDonation: false,
                type: "money",
                projectId: this.state.selectedProject.id,
                projectAuthor: this.state.externalDonator ? "" : this.state.selectedProject.author,
                anonymous: this.state.externalDonator ? true : this.state.anonymousDonation,
                amount: this.state.donationAmount,
                currency: this.state.selectedProject.currency,
                status: "Confirmed"
            };

            await addDonation(this.state.selectedProject, donationDetails, this.state.selectedUser);

            Toast.show(IMLocalized('Donation created !'), {duration: 2000});

            this.setState({
                selectedProject: null,
                selectedUser: null,
                anonymousDonation: false,
                externalDonator: false,
                donationAmount: '',
                paymentInfo: ''
            })

        } else {
            Toast.show(IMLocalized('Please fill all the fields to create the donation'), {duration: 2000});
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                {this.props.navigation.state.params && (
                    <View style={{flexDirection: 'row', paddingBottom: 20}}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image style={AppStyles.styleSet.backArrowStyle} source={AppStyles.iconSet.backArrow} />
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>{IMLocalized('Create external donation')}</Text>
                    </View>
                )}
                <Text style={styles.subTitle}>{IMLocalized('Project')}</Text>
                <View style={styles.pickerContainer}>
                   <Container>
                        <Content>
                            <Form>
                                <Picker
                                    ref={(input) => { this.locationInput = input; }}
                                    mode="dropdown"
                                    placeholder="Select the project"
                                    headerBackButtonTextStyle={{ color: AppStyles.colorSet.mainThemeForegroundColor }}
                                    iosIcon={<Icon name="chevron-down-sharp" />}
                                    placeholderStyle={{ color: "#aaaaaa", fontSize: AppStyles.fontSet.middle, fontWeight: '400'}}
                                    textStyle={styles.pickerTextStyle}
                                    itemStyle={styles.pickerItemStyle}
                                    itemTextStyle={styles.pickerTextStyle}
                                    selectedValue={this.state.selectedProject}
                                    onValueChange={value => {
                                        this.setState({selectedProject: value});
                                    }}
                                >
                                    {
                                        this.props.projects.map((item, i) => (
                                            <Picker.Item label={item.name + "\nAuthor : " + item.author.firstName + " " + item.author.lastName} value={item} />
                                        ))
                                    }
                                </Picker>
                            </Form>
                        </Content>
                    </Container >
                </View>
                {!this.state.externalDonator && (
                  <View>
                    <Text style={styles.subTitle}>{IMLocalized('Donator')}</Text>
                    <TouchableOpacity
                        style={{width:"80%", marginLeft: 20, marginBottom: 20}}
                        onPress={()=> this.props.navigation.navigate('CreateGroup', { appStyles: AppStyles, donationCreation: true,getUserFromSeletion: this.getUserFromSeletion })}>
                        {this.state.selectedUser ? (
                            <Text style={styles.pickerTextStyle}>{this.state.selectedUser.firstName + ' ' + this.state.selectedUser.lastName}</Text>
                        ):(
                            <Text style={{ color: "#aaaaaa", fontSize: AppStyles.fontSet.middle, fontWeight: '400'}}>{"Select the donator"}</Text>
                        )}
                    </TouchableOpacity>
                  </View>
                )}
                {this.state.selectedProject ? (
                    <Text style={styles.subTitle}>{IMLocalized('Amount (' + this.state.selectedProject.currency.sign + ')')}</Text>
                ) : (
                    <Text style={styles.subTitle}>{IMLocalized('Amount')}</Text>
                )}
                <TextInput
                    returnKeyType = { "next" }
                    style={[styles.InputContainer, {width: "80%", marginHorizontal: 20, marginBottom: 20}]}
                    placeholder={IMLocalized('500')}
                    placeholderTextColor="#aaaaaa"
                    onChangeText={text => {this.setState({donationAmount: text})}}
                    value={this.state.donationAmount}
                    underlineColorAndroid="transparent"
                />
                <View
                    style={[styles.rowContainer,{marginVertical: 20, marginLeft: 20, width: "80%"}]}>
                    <CheckBox
                        value={this.state.anonymousDonation}
                        lineWidth={1.0}
                        onCheckColor={AppStyles.colorSet.mainThemeForegroundColor}
                        onTintColor={AppStyles.colorSet.mainThemeForegroundColor}
                        onValueChange={() => this.setState({externalDonator: !this.state.externalDonator})}
                    />
                    <View
                        style={[{marginLeft: 8, flexDirection: 'row', flexWrap: 'wrap', width: "100%"}]}>
                        <Text style={{marginRight: 3}}>
                            External donator
                        </Text>
                    </View>
                </View>
                {!this.state.externalDonator && (
                  <View
                      style={[styles.rowContainer,{marginVertical: 20, marginLeft: 20, width: "80%"}]}>
                      <CheckBox
                          value={this.state.anonymousDonation}
                          lineWidth={1.0}
                          onCheckColor={AppStyles.colorSet.mainThemeForegroundColor}
                          onTintColor={AppStyles.colorSet.mainThemeForegroundColor}
                          onValueChange={() => this.setState({anonymousDonation: !this.state.anonymousDonation})}
                      />
                      <View
                          style={[{marginLeft: 8, flexDirection: 'row', flexWrap: 'wrap', width: "100%"}]}>
                          <Text style={{marginRight: 3}}>
                              Anonymous donation
                          </Text>
                      </View>
                  </View>
                )}
                <Text style={styles.subTitle}>{IMLocalized('Payment Information')}</Text>
                <Text style={styles.smallText}>{IMLocalized('Used to describre where does the money come from')}</Text>
                <TextInput
                    returnKeyType = { "next" }
                    style={[styles.InputContainer, {width: "80%", marginHorizontal: 20}]}
                    placeholder={IMLocalized('External platform - Just giving')}
                    placeholderTextColor="#aaaaaa"
                    onChangeText={text => {this.setState({paymentInfo: text})}}
                    value={this.state.paymentInfo}
                    underlineColorAndroid="transparent"
                />
                <TouchableOpacity
                    style={[styles.floatingButton, {backgroundColor: AppStyles.colorSet.moneyColor}]}
                    onPress={() => this.createExternalDonation()}
                >
                    <Text style={styles.floatingButtonText}>Create donation</Text>
                </TouchableOpacity>
            </KeyboardAwareScrollView>
        );
    }
}

CreateExternalDonationScreen.propTypes = {
    user: PropTypes.object,
    users: PropTypes.array
};

const mapStateToProps = ({ auth, projectFeed }) => {
    return {
        user: auth.user,
        users: auth.users,
        projects: projectFeed.feedProjects,
    };
};

export default connect(mapStateToProps)(CreateExternalDonationScreen);
