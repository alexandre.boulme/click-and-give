import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';
import AppStyles from '../../AppStyles';
import AboutArdianFoundation from '../../components/screens/DrawerPage/AboutArdianFoundation';
import {firebaseAnalytics} from "../../Core/firebase";

class AboutArdianFoundationScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];

    return {
      headerTitle: 'About Ardian Foundation',
      headerLeft:
        <TouchableOpacity
          style={{ marginRight: 12, color: AppStyles.colorSet.mainThemeForegroundColor, flexDirection: 'row', alignItems: 'center'}}
          onPress={()=>{
            navigation.goBack(null);
            navigation.openDrawer();
          }}>
          <Image style={{width: 20, height: 20, tintColor: AppStyles.colorSet.black}} source={AppStyles.iconSet.backArrow} />
          <Text>Back</Text>
        </TouchableOpacity>,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName:'aboutArdianFoundation'});
  }


  render() {
    return (
      <AboutArdianFoundation/>
    );
  }
}

AboutArdianFoundationScreen.propTypes = {
  user: PropTypes.object,
};

export default AboutArdianFoundationScreen;
