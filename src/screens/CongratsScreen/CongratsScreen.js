import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Congrats } from '../../components';
import AppStyles from '../../AppStyles';
import { firebaseAnalytics } from '../../Core/firebase';


class CongratsScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let currentTheme = AppStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: '',
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.backgroundColor,
      },
      headerLeft: null,
      gesturesEnabled: false,
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    firebaseAnalytics.sendEvent('CandG_screen_view', {screenName: 'donation_congrats'});
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  shareProjectCreation = () => {
    const projectName = this.props.navigation.state.params.projectName;
    const postText = "Dear all,\n\nI am delighted to share with you my new project ! " + projectName + "\n\nLook up the project's page to find out more ! Thank you for your support."
    this.props.navigation.navigate('Project');
    this.props.navigation.navigate('Discover', {postPreFill: postText});
  }

  onBottomButtonPress = (message) => {
    switch (message) {
      case "createProjectTimeAndMoney":
        this.props.navigation.navigate('Project');
        break;
      case "createProjectMoney":
        this.props.navigation.navigate('Project');
        break;
      case "createProjectTime":
        this.props.navigation.navigate('Project');
        break;
      case "timeDonation":
        this.props.navigation.navigate('Project');
        break;
      case "moneyFoundationDonation":
        this.props.navigation.navigate('Project');
        this.props.navigation.navigate('Discover');
        break;
      case "moneyDonation":
        this.props.navigation.navigate('Project');
      default:

    }
  };

  render() {
    return (
      <Congrats
        user={this.props.user}
        message={this.props.navigation.state.params.message}
        owner={this.props.navigation.state.params.owner}
        onBottomButtonPress={this.onBottomButtonPress}
        shareProjectCreation={this.shareProjectCreation}
        modalVisible={this.props.navigation.state.params.modalVisible}
      />
    );
  }
}

CongratsScreen.propTypes = {
  user: PropTypes.object,
};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user
  };
};

export default connect(mapStateToProps)(CongratsScreen);
