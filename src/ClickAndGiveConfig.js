import { IMLocalized } from './Core/localization/IMLocalization';
import AppStyles from './AppStyles';

const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;

const ClickAndGiveConfig = {
    isSMSAuthEnabled: true,
    appIdentifier: 'rn-click-and-give-ios',
    onboardingConfig: {
        welcomeTitle: IMLocalized('Welcome to your app'),
        welcomeCaption: IMLocalized('Use this codebase to build your own social network in minutes.'),
        walkthroughScreens: [
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Posts"),
                description: IMLocalized("Share posts, photos and comments with your network.")
            },
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Stories"),
                description: IMLocalized("Share stories that disappear after 24h.")
            },
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Reactions"),
                description: IMLocalized("React to posts and photos with likes, dislikes, laughs and more..")
            },
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Chat"),
                description: IMLocalized("Communicate with your friends via private messages.")
            },
            {
                icon: require("../assets/icons/profile-unfilled.png"),
                title: IMLocalized("Group Chats"),
                description: IMLocalized("Have fun with your gang in group chats.")
            },
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Send Photos & Videos"),
                description: IMLocalized("Have fun with your matches by sending photos and videos to each other.")
            },
            {
                icon: require("../assets/images/photo.png"),
                title: IMLocalized("Check ins"),
                description: IMLocalized("Check in when posting to share your location with friends.")
            },
            {
                icon: require("../assets/images/notification.png"),
                title: IMLocalized("Get Notified"),
                description: IMLocalized("Receive notifications when you get new messages and matches.")
            }
        ]
    },
    tabIcons: {
        Newsfeed: {
          focus: AppStyles.iconSet.homefilled,
          unFocus: AppStyles.iconSet.homeUnfilled,
        },
        Projects: {
          focus: AppStyles.iconSet.projectFilled,
          unFocus: AppStyles.iconSet.projectUnfilled,
        },
        Messages: {
          focus: AppStyles.iconSet.chatFilled,
          unFocus: AppStyles.iconSet.chatUnfilled,
        },
        Profile: {
          focus: AppStyles.iconSet.profileFilled,
          unFocus: AppStyles.iconSet.profileUnfilled,
        },
    },
    editProfileFields: {
      sections: [
        {
          title: IMLocalized("PUBLIC PROFILE"),
          fields: [
            {
              displayName: IMLocalized("First Name"),
              type: 'text',
              editable: true,
              regex: regexForNames,
              key: 'firstName',
              placeholder: 'Your first name'
            },
            {
              displayName: IMLocalized("Last Name"),
              type: 'text',
              editable: true,
              regex: regexForNames,
              key: 'lastName',
              placeholder: 'Your last name'
            },
            {
              displayName: IMLocalized("Title"),
              type: 'text',
              editable: true,
              key: 'title',
              placeholder: 'Your job title'
            },
            {
              displayName: IMLocalized("Email (non editable)"),
              type: 'text',
              editable: false,
              key: 'email',
              placeholder: 'Your email address'
            }
          ]
        }
      ]
    },
    userSettingsFields: {
      sections: [
        {
          title: IMLocalized("GENERAL"),
          fields: [
            {
              displayName: IMLocalized("Allow Push Notifications"),
              type: 'switch',
              editable: true,
              key: 'push_notifications_enabled',
              value: false,
            },
            {
              displayName: IMLocalized("Enable Face ID / Touch ID"),
              type: 'switch',
              editable: true,
              key: 'face_id_enabled',
              value: false
            }
          ]
        },
        {
          title: '',
          fields: [
            {
              displayName: IMLocalized("Save"),
              type: 'button',
              key: 'savebutton',
            }
          ]
        }
      ]
    },
    emailsContent: {
      sendFeedback: {
        to:'ardianclickandgive@ardian.com ',
        subject: 'Click & Give - Feedback - '
      },
      contactUs: {
        to:'ardianclickandgive@ardian.com ',
        subject:'Click And Give - User query - '
      },
    },
    aboutArdianFoundation: {
      about: 'The Ardian Foundation is dedicated to helping people from disadvantaged backgrounds realize their potential and achieve social mobility by gaining education and new skills by',
      aims: [{
        title: 'Partnering with charities',
        description:'to help children gain access to education, culture, music and art.'
      },
      {
        title: 'Providing educational scholarships',
        description:'and personal mentoring for students.'
      },
      {
        title: 'Coaching and funding',
        description:'entrepreneurs from disadvantaged neighborhoods through the 3,2,1 project.'
      },
      {
        title: 'Enabling Ardian employees to support charities',
        description:'that are important to them.'
      }]
    },
    categoryOfInterest: [
      {name: 'Artistic projects', selected: false},
      {name: 'Primary education', selected: false},
      {name: 'Secondary and Higher education', selected: false},
      {name: 'Social integration', selected: false},
      {name: 'Environment', selected: false},
      {name: 'Disasters', selected: false},
      {name: 'Humanitarian causes', selected: false}
    ],
    categoryOfInterestNames: [
        'Artistic projects',
        'Primary education',
        'Secondary and Higher education',
        'Social integration',
        'Environment',
        'Disasters',
        'Humanitarian causes'
    ],
    projectVolunteerFrequency: [
        {
            frequency:"One-off",
            hours: 1.5
        },
        {
            frequency:"Every week",
            hours: 80
        },
        {
            frequency:
            "Every 2 weeks",
            hours: 40
        },
        {
            frequency:"Every 3 weeks",
            hours: 26.5
        },
        {
            frequency:"Every month",
            hours: 18
        },
        {
            frequency:"Every 2 months",
            hours: 9
        },
        {
            frequency:"Every 3 months",
            hours: 6
        },
        {
            frequency:"Every 4 months",
            hours: 4.5
        },
        {
            frequency:"Every 5 months",
            hours: 3.6
        },
        {
            frequency:"Every 6 months",
            hours: 3
        },
        {
            frequency: "Once a year",
            hours: 1.5
        }
    ],
    cityLocationList : [
      {city: "Beijing", country: "China", region: "Asia"},
      {city: "Frankfurt", country: "Germany", region: "Europe"},
      {city: "Jersey", country: "UK", region: "Europe"},
      {city: "London", country: "UK",region: "Europe"},
      {city: "Luxembourg", country: "Luxembourg", region: "Europe"},
      {city: "Madrid", country: "Spain", region: "Europe"},
      {city: "Milan", country: "Italy", region: "Europe"},
      {city: "New York", country: "USA", region: "Americas"},
      {city: "Paris", country: "France", region: "Europe"},
      {city: "San Francisco", country: "USA", region: "Americas"},
      {city: "Santiago", country: "Chile", region: "Americas"},
      {city: "Seoul", country: "South Korea", region: "Asia"},
      {city: "Singapore", country: "Singapore", region: "Asia"},
      {city: "Tokyo", country: "Japan", region: "Asia"},
      {city: "Zurich", country: "Switzerland", region: "Europe"},
      {city: "Other location", country: "", region: ""}
    ],
    preTypedAmounts: ["30", "100",  "200", "300", "400", "500", "750", "1000"],
    regionList: ["Asia", "America", "Europe"],
    currencyList: [
      {name: 'euro', sign: '€', code:'eur'},
      {name: 'dollar', sign: '$', code:'usd'},
      {name: 'pound sterling', sign: '£', code:'gbp'}
    ],
    currencyExchangeRates: {
        eur: 1,
        usd: 0.833,
        gbp: 1.111
    },
    congratsTextList: {
      adminCreatedProject: {
        title: "Good job, ",
        subTitle: "This project is now approved and visible to all the Click & Give community !",
        text: "May its success exceed your expectations !",
        buttonText: "Go to projects"
      },
      createProjectTime: {
        title: "You're the Best, ",
        subTitle: "Welcome to the Click & Give community.",
        text: "The Foundation is currently reviewing your project. It will be available on the project page as soon as it has been approved.",
        buttonText: "Go to projects"
      },
      createProjectMoney: {
        title: "You Rock, ",
        subTitle: "Welcome to the Click & Give community.",
        text: "The Foundation is currently reviewing your project. It will be available on the project page as soon as it has been approved.",
        buttonText: "Go to projects"
      },
      createProjectTimeAndMoney: {
        title: "You're Awesome, ",
        subTitle: "Welcome to the Click & Give community.",
        text: "The Foundation is currently reviewing your project. It will be available on the project page as soon as it has been approved.",
        buttonText: "Go to projects"
      },
      timeDonation:{
        title: "Way To Go ",
        subTitle: "Thank you for your precious time! It will help make a difference.",
        text: " will reach out to you.",
        buttonText: "Go back to projects"
      },
      moneyDonation:{
        title: "You Legend, ",
        subTitle: "Thank you so much for your generosity! It will help make a difference.",
        text: "You will receive your donation receipt as soon as possible via e-mail.",
        buttonText: "Go back to projects feed"
      },
      moneyFoundationDonation:{
        title: "You Legend, ",
        subTitle: "Thank you so much for your generosity! It will help make a difference.",
        text: "The Foundation will send you will receive your donation receipt as soon as possible via e-mail.",
        buttonText: "Go back to feed"
      },
    },
    wireTransferCountry: {
        Germany : {
            intro: "German donors can make their donations on following account:",
            accountInfo:{
                holder :"Maecenata Stiftung",
                bankName: "Bankhaus Löbbecke",
                address: "Bankhaus Löbbecke, Behrenstraße 36, 10 117 Berlin",
                bankAccount: "1061 0007 01",
                iban:"DE89 1003 0500 1061 0007 01",
                bic:"LOEB DEBB XXX",
                bankCode: "10030500"
            },
            communication: {
                label: "TGE",
                name: "Name organisation”",
                country: "Country of destination"
            },
            contact: {
                name: "Christian Schreier",
                email: "csc@maecenata.eu",
                phone: "+49 30 28 38 79 09"
            },
        },
        Italy :{
            intro: "Italian donors can make their donations on following account:",
            accountInfo:{
                holder :"Fondazione Lang Europe Onlus",
                bankName: "Banca Generali SpA",
                address: "Piazza Sant’ Alessandro 4 20123 Milano",
                bankAccount: "CC151 0750 719",
                iban:"IT45 I030 7501 603C C151 0750 719",
                bic:"BGENIT2T",
            },
            communication: {
                label: "TGE",
                name: "Name organisation”",
                country: "Country of destination"
            },
            contact: {
                name: "Elena Redini",
                email: "e.redini@fondazionelangeurope.it",
                phone: "+39 02 36635131"
            },
        },
        Spain :{
            intro: "Spanish donors can make their donations by following these steps: \n\n1. Send an email to fabad@empresaysociedad.org indicating:\n- The amount of the donation. \n- The beneficiary entity, which must have been approved after evaluating the documentation requested through the GEAF. \n- Donor identification data: \n* Name and surnames (individual donor) or business name (corporate donor) \n* Postal address \n* Telephone number \n* Email \n* Tax identification (NIF, NIE or CIF)\n\n2. Attach a copy of your ID or card with the CIF of the donor (if it is Spanish) or with the NIE (otherwise). It is a key document to process the information to the AEAT about the tax treatment of the donation in Spain.\n\n3. Make the payment by transfer on the following account:",
            accountInfo:{
                holder :"Fundación Empresa y Sociedad",
                bankName: "Banco Caminos",
                address: "Almagro 8 / 28010 Madrid (SPAIN)",
                iban:"ES79 0234 0001 0490 3028 5466",
                bic:"CCOCESMM",
            },
            communication: {
                label: "TGE",
                name: "Name organisation”",
                country: "Country of destination"
            },
            contact: {
                name: "Francisco Abad",
                email: "fabad@empresaysociedad.org",
                phone: "+34 676270816"
            },
        },
        Switzerland :{
            intro: "Donors living in Switzerland and wanting to make a donation of at least 500 CHF should first contact the Swiss Philanthropy Foundation before making their donations.",
            communication: {
                label: "TGE",
                name: "Name organisation”",
                country: "Country of destination"
            },
            contact: {
                name: "Sabrina Grassi",
                email: "TGE@swissphilanthropy.ch",
                phone: "+41 22 732 55 54"
            },
        },
        Swizerland :{
            intro: "Donors living in Switzerland and wanting to make a donation of at least 500 CHF should first contact the Swiss Philanthropy Foundation before making their donations.",
            communication: {
                label: "TGE",
                name: "Name organisation”",
                country: "Country of destination"
            },
            contact: {
                name: "Sabrina Grassi",
                email: "TGE@swissphilanthropy.ch",
                phone: "+41 22 732 55 54"
            },
        },
        UK :{
            intro: "Donors living in the UK and wanting to benefit from Gift Aid should first contact the Charities Aid Foundation before making their donations.After a short KYC (Know Your Customer) procedure, CAF will provide the donor with all the instructions to make his/her donation.",
            contact: {
                name: "Andrew Fakley",
                email: "tge@cafonline.org",
                phone: "+44 30 00 12 33 80"
            },
        }
    },
    formDisclaimer:{
        signUp: "Ardian SAS, as the controller, processes your personal data for the purposes of creating, managing and monitoring your account as well as your identification details on the Click & Give Application. \nThe data collected is essential for processing purposes and its intended recipients are Ardian Foundation and possible processors and service providers. Data is kept for the entire duration of your registration plus the statutory retention periods.\n\nIn accordance with current legislation, you have the rights of inquiry, access, rectification, erasure, restriction of processing, objection, portability of your data as well as the right to define the fate of your data after your death. You can exercise these rights either by emailing dataprivacyofficer@ardian.com or by writing to Ardian, Data Protection, 20 Place Vendôme, 75001 Paris, including a copy of an identity document.\n\nFinally, you have the right to lodge a complaint with a data protection authority as National Commission for Information Technologies and Civil Liberties (CNIL).\n\nTo learn more about your rights and how Ardian processes your personal data, you are invited to consult the Users Data Privacy Notice available at https://www.ardian.com/privacy-notice-users.\n\nContact details of the Data Privacy Team : dataprivacyofficer@ardian.com",
        givingMoney : "Ardian SAS, as the controller, processes your personal data for the purposes of monitoring your donations made on the Click & Give Application. \n\nThe data collected is essential for processing purposes and its intended recipients are Ardian Foundation and possible processors and service providers. Payment data is kept for 6 years.\n\nIn accordance with current legislation, you have the rights of inquiry, access, rectification, erasure, restriction of processing, objection, portability of your data as well as the right to define the fate of your data after your death. You can exercise these rights either by emailing dataprivacyofficer@ardian.com or by writing to Ardian, Data Protection, 20 Place Vendôme, 75001 Paris, including a copy of an identity document.\n\nFinally, you have the right to lodge a complaint with a data protection authority as National Commission for Information Technologies and Civil Liberties (CNIL).\n\nTo learn more about your rights and how Ardian France processes your personal data, you are invited to consult the Users Data Privacy Notice available at https://www.ardian.com/privacy-notice-users.\n\nContact details of the Data Privacy Team : dataprivacyofficer@ardian.com"
    }
};

export default ClickAndGiveConfig;
