import {createSwitchNavigator} from 'react-navigation';
import AppStyles from '../AppStyles';
import {LoadScreen, WalkthroughScreen} from '../Core/onboarding';
import MainStackNavigator from './MainStackNavigator';
import LoginStack from './AuthStackNavigator';
import ClickAndGiveConfig from "../ClickAndGiveConfig"

export const RootNavigator = createSwitchNavigator(
  {
    LoadScreen: LoadScreen,
    Walkthrough: WalkthroughScreen,
    LoginStack: LoginStack,
    MainStack: MainStackNavigator,
  },
  {
    initialRouteName: 'LoginStack',
    initialRouteParams: {
      appStyles: AppStyles,
      appConfig: ClickAndGiveConfig
    }
  },
);

export default RootNavigator;
