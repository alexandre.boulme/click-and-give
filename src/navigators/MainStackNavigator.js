import {Platform} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import DrawerNavigator from './DrawerNavigator';
import {IMChatScreen} from '../Core/chat';
import AppStyles from '../AppStyles';

const MainStackNavigator = createStackNavigator(
  {
    NavStack: {
      screen: DrawerNavigator,
    },
  },
  {
    initialRouteName: 'NavStack',
    headerMode: 'none',
  }
);

export default MainStackNavigator;
