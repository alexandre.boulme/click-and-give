import {createStackNavigator} from 'react-navigation-stack';
import AnonymizeUserScreen from '../screens/DrawerPageScreen/AnonymizeUserScreen';
import PrivacyNoticeScreen from '../screens/DrawerPageScreen/PrivacyNoticeScreen';
import {LoginScreen, SignupScreen, ForgotPasswordScreen } from '../Core/onboarding';
import AppStyles from '../AppStyles';
import ClickAndGiveConfig from "../ClickAndGiveConfig"
import { StyleSheet } from "react-native";

const AuthStackNavigator = createStackNavigator({
    Login: {
      screen: LoginScreen,
      navigationOptions: () => ({
        headerStyle: styles.headerStyle
      })
    },
    Signup: {
      screen: SignupScreen,
      navigationOptions: () => ({
        headerStyle: styles.headerStyle
      })
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: () => ({
        headerStyle: styles.headerStyle
      })
    },
    TermsAndConditions: {
      screen: AnonymizeUserScreen,
    },
    PrivacyNotice: {
      screen: PrivacyNoticeScreen,
    }
  },
  {
    initialRouteName: 'Login',
    initialRouteParams: {
      appStyles: AppStyles,
      appConfig: ClickAndGiveConfig
    },
    headerMode: 'none',
    headerBackTitleVisible: false,
    cardStyle: { backgroundColor: "#FFFFFF" },
    cardShadowEnabled: false
  },
);

const styles = StyleSheet.create({
  headerStyle: {
    borderBottomWidth: 0,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    elevation: 0, // remove shadow on Android
  }
});

export default AuthStackNavigator;
