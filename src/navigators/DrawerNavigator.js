import {createDrawerNavigator} from 'react-navigation-drawer';
import {DrawerContainer} from '../components';
import {
  InnerProfileNavigator,
  InnerAnonymizeUserNavigator,
  InnerPrivacyNoticeNavigator,
  InnerCreateExternalDonationNavigator,
  InnerAboutArdianFoundationNavigator,
  InnerAdminProjectValidationNavigator
} from './InnerStackNavigators';
import BottomTabNavigator from './BottomTabNavigator';

const DrawerNavigator = createDrawerNavigator(
  {
    Profile: {
      screen: InnerProfileNavigator,
    },
    AnonymizeUser: {
      screen: InnerAnonymizeUserNavigator,
    },
    PrivacyNotice: {
      screen: InnerPrivacyNoticeNavigator,
    },
    CreateExternalDonation: {
        screen: InnerCreateExternalDonationNavigator,
    },
    AboutArdianFoundation: {
      screen: InnerAboutArdianFoundationNavigator,
    },
    AdminProjectValidation: {
      screen: InnerAdminProjectValidationNavigator,
    },
    Bottom: {
      screen: BottomTabNavigator,
    }
  },
  {
    drawerPosition: 'left',
    initialRouteName: 'Bottom',
    drawerWidth: 340,
    contentComponent: DrawerContainer,
    headerMode: 'screen',
    navigationOptions: ({navigation}) => {
      const routeIndex = navigation.state.index;
      return {
        title: navigation.state.routes[routeIndex].key,
        header: null,
      };
    },
  },
);

export default DrawerNavigator;
