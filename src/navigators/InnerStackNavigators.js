import { createStackNavigator } from 'react-navigation-stack';
import {
  DetailPostScreen,
  DiscoverScreen,
  ProfileScreen,
  ChatScreen,
  ProjectScreen,
  DetailProjectScreen,
  CreateProjectFirstScreen,
  CreateProjectSecondScreen,
  CreateProjectAdminScreen,
  CreateProjectSummaryScreen,
  CongratsScreen,
  DonationAmountScreen,
  GiveFundScreen,
  AnonymizeUserScreen,
  CreateExternalDonationScreen,
  PrivacyNoticeScreen,
  AboutArdianFoundationScreen,
  AdminProjectValidationScreen,
  DonationValidationScreen,
  WireTransferInfoScreen
} from '../screens';
import { IMCreateGroupScreen } from '../Core/chat';
import { IMAllFriendsScreen } from '../Core/socialgraph/friendships';
import {
  IMEditProfileScreen,
  IMUserSettingsScreen,
  IMContactUsScreen,
  IMProfileSettingsScreen
} from '../Core/profile';
import {IMChatScreen} from '../Core/chat';
import { IMNotificationScreen } from '../Core/notifications';

const InnerChatNavigator = createStackNavigator(
  {
    Chat: { screen: ChatScreen },
    CreateGroup: { screen: IMCreateGroupScreen },
    PersonalChat: {screen: IMChatScreen},
    PersonalChatProfile: {screen: ProfileScreen}
  },
  {
    initialRouteName: 'Chat',
    headerMode: 'float'
  },
);

InnerChatNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    tabBarVisible = false;
  } else {
    tabBarVisible = true;
  }
  return {
    tabBarVisible
  }
};

const InnerProjectNavigator = createStackNavigator(
  {
    Project: { screen: ProjectScreen },
    ProjectDetail: { screen: DetailProjectScreen },
    CreateProjectFirstScreen: {screen: CreateProjectFirstScreen },
    CreateProjectSecondScreen: {screen: CreateProjectSecondScreen },
    CreateProjectAdminScreen: {screen: CreateProjectAdminScreen},
    CreateProjectSummaryScreen: {screen: CreateProjectSummaryScreen },
    DonationAmountScreen: {screen: DonationAmountScreen},
    GiveFundScreen: {screen: GiveFundScreen},
    DonationValidationScreen: {screen: DonationValidationScreen},
    WireTransferInfoScreen: {screen: WireTransferInfoScreen},
    CongratsScreen: {screen: CongratsScreen},
    DonatorProfile: {screen: ProfileScreen}
  },
  {
    initialRouteName: 'Project',
    headerMode: 'float'
  },
);

InnerProjectNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    tabBarVisible = false;
  } else {
    tabBarVisible = true;
  }
  return {
    tabBarVisible
  }
};

const InnerDiscoverNavigator = createStackNavigator(
  {
    Discover: { screen: DiscoverScreen },
    DiscoverDetailPost: { screen: DetailPostScreen },
    DiscoverProfile: { screen: ProfileScreen },
    DiscoverNotification: { screen: IMNotificationScreen },
    DiscoverProfileSettings: { screen: IMProfileSettingsScreen },
    DiscoverEditProfile: { screen: IMEditProfileScreen },
    DiscoverAppSettings: { screen: IMUserSettingsScreen },
    DiscoverContactUs: { screen: IMContactUsScreen },
    DiscoverAllFriends: { screen: IMAllFriendsScreen },
    FoundationAmountScreen: {screen: DonationAmountScreen},
    GiveFoundationScreen: {screen: GiveFundScreen},
    WireTransferInfoScreen: {screen: WireTransferInfoScreen},
    CongratFoundationsScreen: {screen: CongratsScreen},
  },
  {
    initialRouteName: 'Discover',
    headerMode: 'float'
  },
);

InnerDiscoverNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    tabBarVisible = false;
  } else {
    tabBarVisible = true;
  }
  return {
    tabBarVisible
  }
};
// working
const InnerProfileNavigator = createStackNavigator(
  {
    Profile: { screen: ProfileScreen },
    ProfileNotification: { screen: IMNotificationScreen },
    ProfileProfileSettings: { screen: IMProfileSettingsScreen },
    ProfileEditProfile: { screen: IMEditProfileScreen },
    ProfileAppSettings: { screen: IMUserSettingsScreen },
    ProfileContactUs: { screen: IMContactUsScreen },
    ProfileAllFriends: { screen: IMAllFriendsScreen },
    ProfilePostDetails: { screen: DetailPostScreen },
    ProfileProjectDetail: { screen: DetailProjectScreen, },
    ProfileDetailPostProfile: { screen: ProfileScreen },
  },
  {
    initialRouteName: 'Profile',
    headerMode: 'float'
  },
);

InnerProfileNavigator.navigationOptions = ({ navigation }) => {
    let tabBarVisible;
    if (navigation.state.routes.length > 1) {
        tabBarVisible = false;
    } else {
        tabBarVisible = true;
    }
    return {
        tabBarVisible
    }
};

const InnerAnonymizeUserNavigator = createStackNavigator(
  {
    AnonymizeUser: { screen: AnonymizeUserScreen }
  },
  {
    initialRouteName: 'AnonymizeUser',
    headerMode: 'float'
  },
);

const InnerPrivacyNoticeNavigator = createStackNavigator(
  {
    PrivacyNotice: { screen: PrivacyNoticeScreen }
  },
  {
    initialRouteName: 'PrivacyNotice',
    headerMode: 'float'
  },
);

const InnerCreateExternalDonationNavigator = createStackNavigator(
    {
        CreateExternalDonation: { screen: CreateExternalDonationScreen },
        CreateGroup: { screen: IMCreateGroupScreen },
    },
    {
        initialRouteName: 'CreateExternalDonation',
        headerMode: 'float'
    },
);

const InnerAboutArdianFoundationNavigator = createStackNavigator(
  {
    AboutArdianFoundation: { screen: AboutArdianFoundationScreen }
  },
  {
    initialRouteName: 'AboutArdianFoundation',
    headerMode: 'float'
  },
);

const InnerAdminProjectValidationNavigator = createStackNavigator(
  {
    AdminProjectValidation: { screen: AdminProjectValidationScreen }
  },
  {
    initialRouteName: 'AdminProjectValidation',
    headerMode: 'float'
  },
);

export {
  InnerChatNavigator,
  InnerDiscoverNavigator,
  InnerProfileNavigator,
  InnerProjectNavigator,
  InnerAnonymizeUserNavigator,
  InnerPrivacyNoticeNavigator,
  InnerCreateExternalDonationNavigator,
  InnerAboutArdianFoundationNavigator,
  InnerAdminProjectValidationNavigator
};
