import {createBottomTabNavigator} from 'react-navigation-tabs';
import {
  InnerChatNavigator,
  InnerDiscoverNavigator,
  InnerProfileNavigator,
  InnerProjectNavigator
} from './InnerStackNavigators';
import {tabBarBuilder} from '../Core/ui';
import ClickAndGiveConfig from '../ClickAndGiveConfig'
import AppStyles from '../AppStyles';

const BottomTabNavigator = createBottomTabNavigator(
  {
    Newsfeed: {
      screen: InnerDiscoverNavigator,
    },
    Projects: {
      screen: InnerProjectNavigator,
    },
    Messages: {
      screen: InnerChatNavigator,
    },
    Profile: {
      screen: InnerProfileNavigator,
    },
  },
  {
    initialRouteName: 'Newsfeed',
    tabBarComponent: tabBarBuilder(ClickAndGiveConfig.tabIcons, AppStyles),
    navigationOptions: ({navigation}) => {
      const {routeName} = navigation.state.routes[navigation.state.index];
      return {
        headerTitle: routeName,
        header: null,
        tabBarVisible: false
      };
    },
  },
);

export default BottomTabNavigator;
