import {Platform, Dimensions, I18nManager} from 'react-native';
import TNColor from './Core/truly-native/TNColor';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const colorSet = {
  mainThemeBackgroundColor: '#ffffff',
  mainThemeForegroundColor: '#F73750',
  mainTextColor: '#151723',
  mainSubtextColor: '#7e7e7e',
  hairlineColor: '#e0e0e0',
  grey0: TNColor('#eaeaea'),
  grey3: TNColor('#e6e6f2'),
  grey6: TNColor('#d6d6d6'),
  grey9: TNColor('#939393'),
  grey12: TNColor('#777777'),
  tint: '#F73750',
  grey: 'grey',
  whiteSmoke: '#f5f5f5',
  headerTintColor: '#000000',
  bottomTintColor: 'grey',
  mainButtonColor: '#e8f1fd',
  subButtonColor: '#eaecf0',
  bottomborderColor: '#000000',
  progressBarLow: "#E13C32",
  progressBarMedium: "#f8e082",
  progressBarHigh: "#6399a0",
  timeColor: "#324146",
  moneyColor: "#E13C32",
  black: "#000",
  white: "#FFF",
  lightGrey: "#ccc",
  green:"#9ba77b",
  yellow:"#f8e082",
  yellow40:"#f8e08210",
  greenTag:"#3CB371",
  greenTag10:"#3CB37110"

};

const navThemeConstants = {
  light: {
    backgroundColor: '#fff',
    fontColor: '#000',
    activeTintColor: '#F73750',
    inactiveTintColor: '#ccc',
    hairlineColor: '#e0e0e0',
    headerTitle: {
      ...ifIphoneX({
        width: 130,
        height: 30
      },{
        width: 100,
        height: 25
      })
    },
  },
  dark: {
    backgroundColor: '#fff',
    fontColor: '#000',
    activeTintColor: '#F73750',
    inactiveTintColor: '#ccc',
    hairlineColor: '#e0e0e0',
    headerTitle: {
      ...ifIphoneX({
        width: 130,
        height: 30
      },{
        width: 85,
        height: 20
      })
    },
  },
  main: '#F73750',
};

const imageSet = {
  emptyAvatar: require('../assets/images/emptyavatar.jpg'),
  notification: require('../assets/images/notification.png'),
  photo: require('../assets/images/photo.png'),
  yayPurple: require('../assets/images/yay-purple.png'),
  yayDarkGrey: require('../assets/images/yay-darkgrey.png'),
  yayRed: require('../assets/images/yay-red.png'),
  loginBackground: require('../assets/images/login-background.png'),
  ardianFoundationLogoW: require('../assets/images/ardian-foundation-logo-w.jpg'),
  foundationBeneficiaries: require('../assets/images/foundationBeneficiaries.png'),
  foundationBudget: require('../assets/images/foundationBudget.png'),
  foundationEmployee: require('../assets/images/foundationEmployee.png'),
  foundationProjects: require('../assets/images/foundationProjects.png'),
  foundationTimeDedicated: require('../assets/images/foundationTimeDedicated.png'),
  foundationCountries: require('../assets/images/foundationCountries.png'),
  notificationRedirection: require('../assets/images/notification-redirection.png'),
};

const iconSet = {
  add: require("../assets/icons/add-icon.png"),
  applepay: require("../assets/icons/applepay-icon.png"),
  bell: require('../assets/icons/bell.png'),
  backArrow: require('../assets/icons/back-arrow.png'),
  backSpace: require('../assets/icons/backspace-icon.png'),
  camera: require('../assets/icons/camera.png'),
  cameraFilled: require('../assets/icons/camera-filled.png'),
  cameraRotate: require('../assets/icons/camera-rotate.png'),
  chatFilled: require("../assets/icons/chat-filled.png"),
  chatUnfilled: require("../assets/icons/chat-unfilled.png"),
  checked: require('../assets/icons/done-icon.png'),
  clickandgive: require("../assets/images/clickandgive.png"),
  clickandgivewhite: require("../assets/images/clickandgivewhite.png"),
  close: require("../assets/icons/close-icon.png"),
  commentFilled: require('../assets/icons/comment-filled.png'),
  contactUs: require('../assets/icons/contactus-icon.png'),
  creditCard: require('../assets/icons/creditcard-icon.png'),
  easyGive: require('../assets/icons/easygive-icon.png'),
  edit: require('../assets/icons/editicon.png'),
  write: require('../assets/icons/edit-solid.png'),
  done: require('../assets/icons/done-icon.png'),
  filter: require('../assets/icons/filter-icon.png'),
  groupConversation: require('../assets/icons/group-conversation-icon.png'),
  heartUnfilled: require('../assets/icons/heart-unfilled.png'),
  heartFilled: require('../assets/icons/heart-filled.png'),
  homeUnfilled: require('../assets/icons/home-unfilled.png'),
  homefilled: require('../assets/icons/home-filled.png'),
  foundation: require('../assets/icons/foundation-icon.png'),
  foundationBlack: require('../assets/icons/foundationblack-icon.png'),
  info: require('../assets/icons/info-icon.png'),
  inscription: require('../assets/icons/inscription.png'),
  libraryLandscape: require('../assets/icons/library-landscape.png'),
  location: require('../assets/icons/location-icon.png'),
  logout: require('../assets/icons/logout-icon.png'),
  userAvatar: require('./Core/CoreAssets/default-avatar.jpg'),
  menuHamburger: require('../assets/icons/menu.png'),
  magnifier: require('../assets/icons/search-icon.png'),
  money: require('../assets/icons/money-icon.png'),
  more: require('../assets/icons/more.png'),
  moreTopBar: require('../assets/icons/more-topbar.png'),
  profileUnfilled: require('../assets/icons/profile-unfilled.png'),
  profileFilled: require('../assets/icons/profile-filled.png'),
  pinpoint: require('../assets/icons/location-icon.png'),
  projectUnfilled: require('../assets/icons/project-unfilled.png'),
  projectFilled: require('../assets/icons/project-filled.png'),
  playButton: require('../assets/icons/play-button.png'),
  paypal: require("../assets/icons/paypal-icon.png"),
  time: require("../assets/icons/time-icon.png"),
  timeLeft: require("../assets/icons/timeleft-icon.png"),
  uploadImage: require("../assets/icons/uploadimage-icon.png"),
  userAnonymize: require("../assets/icons/user-anonymize.png"),
  radioButtonFilled: require("../assets/icons/radioButtonFilled-icon.png"),
  radioButtonUnfilled: require("../assets/icons/radioButtonUnfilled-icon.png"),
  rightArrow: require("../assets/icons/right-arrow.png"),
  search: require('../assets/icons/search-icon.png'),
  send: require('../assets/icons/send.png'),
  volunteer: require('../assets/icons/volunteer.png'),
};

const fontFamily = {
  boldFont: '',
  semiBoldFont: '',
  regularFont: '',
  mediumFont: '',
  lightFont: '',
  extraLightFont: '',
};

const fontSet = {
  ...ifIphoneX(
    {
      xxlarge: 38,
      xlarge: 28,
      large: 22,
      middleplus: 20,
      middle: 18,
      middleminus: 16,
      normal: 14,
      small: 12,
      xsmall: 9,
      title: 28,
      content: 18,
    },
    {
      xxlarge: 32,
      xlarge: 23,
      large: 20,
      middleplus: 18,
      middle: 16,
      middleminus: 15,
      normal: 14,
      small: 11,
      xsmall: 9,
      title: 26,
      content: 16,
    },
  ),

};

const loadingModal = {
  color: '#FFFFFF',
  size: 20,
  overlayColor: 'rgba(0,0,0,0.5)',
  closeOnTouch: false,
  loadingType: 'Spinner', // 'Bubbles', 'DoubleBounce', 'Bars', 'Pulse', 'Spinner'
};

const sizeSet = {
  buttonWidth: '50%',
  inputWidth: '80%',
  radius: 100,
};

const styleSet = {
  menuBtn: {
    container: {
      backgroundColor: colorSet.grayBgColor,
      borderRadius: 22.5,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    icon: {
      tintColor: 'black',
      width: 15,
      height: 15,
    },
  },
  searchBar: {
    container: {
      marginLeft: Platform.OS === 'ios' ? 30 : 0,
      backgroundColor: 'transparent',
      borderBottomColor: 'transparent',
      borderTopColor: 'transparent',
      flex: 1,
    },
    input: {
      backgroundColor: colorSet.inputBgColor,
      borderRadius: 10,
      color: 'black',
    },
  },
  rightNavButton: {
    marginRight: 10,
  },
  borderRadius: {
    main: 25,
    small: 5,
  },
  textInputWidth: {
    main: '80%',
  },
  backArrowStyle: {
    resizeMode: 'contain',
    tintColor: '#000000',
    width: 25,
    height: 25,
    marginTop: Platform.OS === 'ios' ? 50 : 20,
    marginLeft: 10,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  darkHeaderText: {
    fontSize: fontSet.xxlarge,
    fontWeight: '700',
    paddingTop: 8,
    color: colorSet.mainTextColor
  },
  lightHeaderText: {
    fontSize: fontSet.xxlarge,
    fontWeight: '700',
    paddingTop: 8,
    color: colorSet.mainThemeBackgroundColor
  },
};

const StyleDict = {
  imageSet,
  iconSet,
  fontFamily,
  colorSet,
  navThemeConstants,
  fontSet,
  sizeSet,
  styleSet,
  loadingModal,
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
};

export default StyleDict;
