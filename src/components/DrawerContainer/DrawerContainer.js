import React, {useEffect, useContext} from 'react';
import DeviceInfo from 'react-native-device-info';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import { connect, ReactReduxContext } from 'react-redux';
import DrawerItem from '../DrawerItem/DrawerItem';
import {logout, setUsers} from '../../Core/onboarding/redux/auth';
import AppStyles from '../../AppStyles';
import dynamicStyles from './styles';
import authManager from '../../Core/onboarding/utils/authManager';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import FastImage from 'react-native-fast-image';
import ClickAndGiveConfig from '../../ClickAndGiveConfig';
import { sendEmail } from 'react-native-email-action';
import {feedStore} from "../../Core/socialgraph/feed/redux/feedStore";
import {projectStore} from "../../Core/project/redux/projectStore";
import {firebaseAnalytics, firebaseUser} from "../../Core/firebase";
import FriendshipTracker from '../../Core/socialgraph/friendships/firebase/tracker';
import ProjectManager from "../../Core/project/ProjectManager";

function DrawerContainer(props) {
  const { navigation, user } = props;

  const context = useContext(ReactReduxContext);
  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    const friend = new FriendshipTracker(context.store, user.id || user.userID, true, false, true);
    friend.subscribeIfNeeded();

    const projectManager = new ProjectManager(context.store, props.user.id);
    projectManager.subscribeIfNeeded();
  },[])

  const onLogout = async () => {
    props.logout();
    firebaseAnalytics.sendEvent("user_logout", null);
    authManager.logout(user);
    navigation.navigate('LoginStack', {
      appStyles: AppStyles,
    });
  };

  const handleEmail = (emailType) => {
    const options = {
      to: ClickAndGiveConfig.emailsContent[emailType].to,
      subject: ClickAndGiveConfig.emailsContent[emailType].subject +  user.firstName + " " + user.lastName,
      body: ""
    };
    sendEmail(options);
  }

  const onUserInfoPress = () => {
    navigation.navigate('Profile');
  };

  return (
    <View style={styles.content}>
      <View style={styles.container}>
        <View style={styles.containerFullWidth}>
        <TouchableOpacity
          style={styles.closeButton}
          onPress={() => {
            navigation.closeDrawer();
          }}>
          <Image source={AppStyles.iconSet.close}  style={styles.closeIcon}/>
        </TouchableOpacity>
        <TouchableOpacity
         style={styles.userInfoContainer}
         onPress={() => onUserInfoPress()}>
          <FastImage
            style={[styles.userImage,]}
            source={{ uri: user.profilePictureURL }}
            resizeMode="cover"
          />
          <Text style={styles.userInfoText}>{user.firstName} {"\n"}{user.lastName}</Text>
        </TouchableOpacity>
        <View style={styles.spacer}/>
        {user.role == 'admin' &&(
            <View>
              <DrawerItem
                title={IMLocalized("Project validation")}
                source={AppStyles.iconSet.done}
                colorStyle={{tintColor: AppStyles.colorSet.mainThemeForegroundColor}}
                textStyle={{color: AppStyles.colorSet.mainThemeForegroundColor}}
                onPress={() => {
                  navigation.navigate('AdminProjectValidation');
                }}/>
              <DrawerItem
                title={IMLocalized("Anonymize user")}
                colorStyle={{tintColor: AppStyles.colorSet.mainThemeForegroundColor}}
                textStyle={{color: AppStyles.colorSet.mainThemeForegroundColor}}
                source={AppStyles.iconSet.userAnonymize}
                onPress={() => {navigation.navigate('AnonymizeUser');}}
                />
              <DrawerItem
                title={IMLocalized("Create external donation")}
                colorStyle={{tintColor: AppStyles.colorSet.mainThemeForegroundColor}}
                textStyle={{color: AppStyles.colorSet.mainThemeForegroundColor}}
                source={AppStyles.iconSet.volunteer}
                onPress={() => {
                  if(props.users.length != 0 && props.projects.length != 0){
                    navigation.navigate('CreateExternalDonation');
                  }
                }}
              />
            </View>
        )}
        <DrawerItem
          title={IMLocalized("About Ardian Foundation")}
          source={AppStyles.iconSet.foundationBlack}
          onPress={() => {
            navigation.navigate('AboutArdianFoundation');
          }}
        />
        <DrawerItem
          title={IMLocalized("Privacy Notice")}
          source={AppStyles.iconSet.info}
          onPress={() => {
            navigation.navigate('PrivacyNotice');
          }}
        />
        <DrawerItem
          title={IMLocalized("Contact us")}
          source={AppStyles.iconSet.contactUs}
          onPress={() => handleEmail('contactUs')}
        />
        </View>
        <View style={styles.containerFullWidth}>
        <TouchableOpacity
            onPress={() => {
              handleEmail('sendFeedback');
            }}>
          <Text style={styles.feedbackText}>Send us your feedback</Text>
        </TouchableOpacity>
        <View style={styles.spacer}/>
        <DrawerItem
          title={IMLocalized("Logout")}
          source={AppStyles.iconSet.logout}
          colorStyle={{tintColor: AppStyles.colorSet.mainThemeForegroundColor}}
          textStyle={{color: AppStyles.colorSet.mainThemeForegroundColor}}
          onPress={() => onLogout()}
        />

        <Text style={styles.versionText}>Version {DeviceInfo.getVersion()}</Text>
        </View>
      </View>
    </View>
  );
}

const mapStateToProps = ({ auth, projectFeed }) => {
  return {
    user: auth.user,
    users: auth.users,
    projects: projectFeed.feedProjects
  };
};

export default connect(mapStateToProps, {
  logout
})(DrawerContainer);
