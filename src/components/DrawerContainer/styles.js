import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
  content: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    height: '100%',
  },
  containerFullWidth:{
    width: '100%',
    paddingHorizontal: 20,
  },
  closeButton: {
    ...ifIphoneX({
      marginTop: 50,
      marginBottom: 20
    },{
      marginTop: 35,
      marginBottom: 15
    }),
  },
  closeIcon: {
    ...ifIphoneX({
      width: 50,
      height: 50,
    },{
      width: 35,
      height: 35,
    }),
  },
  userInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImage: {
    ...ifIphoneX({
      width: '35%',
    },{
      width: '25%',
    }),
    height: undefined,
    aspectRatio: 1,
    borderRadius: 50,
    marginRight: 20,
  },
  userInfoText:{
    fontSize: AppStyles.fontSet.middleplus,
    color: AppStyles.colorSet.mainTextColor,
    fontWeight: '700'
  },
  spacer: {
    height: 1,
    backgroundColor: "#999",
    ...ifIphoneX({
      marginVertical: 30,
    },{
      marginVertical: 15,
    }),
  },
  feedbackText :{
    fontSize: AppStyles.fontSet.middleminus,
    textDecorationLine: 'underline',
    fontWeight: '300'
  },
  versionText:{
    ...ifIphoneX({
      margin: 30,
    },{
      margin: 15,
    }),
    fontSize: AppStyles.fontSet.normal,
    color: '#AAA',
    textAlign: 'center',
    fontWeight: '300'
  }
});

export default dynamicStyles;
