import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';

const reactionIconSize = Math.floor(AppStyles.WINDOW_WIDTH * 0.09);

const dynamicStyles = new DynamicStyleSheet({
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  mainContainer: {
    backgroundColor: "#FFF",
    borderRadius: 10,
    padding: 20
  },
  shadow: {
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#CCC",
    shadowOpacity: 1.0,
  },
  projectImage: {
    width: 80,
    height: 80,
    backgroundColor: "#FFF",
    borderRadius: 8
  },
  smallText: {
    fontSize: AppStyles.fontSet.normal,
    color: AppStyles.colorSet.mainTextColor,
  },
  boldText: {
    fontWeight: '800'
  },
  foundationIcon: {
    marginLeft: 5,
    width: 15,
    height: 15,
    alignSelf: 'center'
  },
  pendingTagContainer: {
    backgroundColor: AppStyles.colorSet.greenTag10,
    borderColor: AppStyles.colorSet.greenTag,
    borderWidth: 1,
    margin: 5,
    padding: 4,
    borderRadius: 5,
  },
  pendingTag: {
    textAlign: "center",
    color: AppStyles.colorSet.greenTag,
    fontWeight: "800",
    fontStyle: "italic"
  },
});

export default dynamicStyles;
