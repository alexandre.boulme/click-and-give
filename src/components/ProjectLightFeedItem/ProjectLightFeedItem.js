import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../AppStyles';
import FastImage from 'react-native-fast-image';

function ProjectLightFeedItem(props) {
  const {
    item,
    containerStyle,
    onItemPress,
    displayValidationTag
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);
  const [donationPending, setDonationPending] = useState(item.donationPending);

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => onItemPress(item)}
      style={[styles.mainContainer, containerStyle]}>
      <View style={styles.rowContainer}>
        <FastImage
          style={styles.projectImage}
          source={{ uri: item.projectPictureURL}}
          resizeMode={FastImage.resizeMode.cover}
        />
        <View style={{flex: 1, marginLeft: 15}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: AppStyles.fontSet.middleplus, fontWeight: '700'}}>{item.name}</Text>
            {item.type === 'foundation' && (
              <Image source={AppStyles.iconSet.foundation} style={styles.foundationIcon} />
            )}
          </View>
          <View style={styles.rowContainer}>
            <Text>By </Text>
            {item.type === 'foundation' ? (
              <Text style={{color: AppStyles.colorSet.mainThemeForegroundColor, fontWeight: '600'}}>{'Ardian Foundation'}</Text>
              ) : (
              <Text style={{color: AppStyles.colorSet.mainThemeForegroundColor, fontWeight: '600'}}>{item.author.firstName + ' ' + item.author.lastName[0].toUpperCase() + '.'}</Text>
            )}
            <Text style={{color: "#CCC"}}> | </Text>
            <Text>{item.cityLocation.city}</Text>
          </View>
          <View style={displayValidationTag ? styles.pendingTagContainer : {}}>
            <Text style={displayValidationTag ? styles.pendingTag: {color: "#FFFFFF"}}>Validate donation {item.donationWaiting > 1 ? "(" + item.donationWaiting + ")" : ''}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

ProjectLightFeedItem.propTypes = {
  item: PropTypes.object,
};

export default ProjectLightFeedItem;
