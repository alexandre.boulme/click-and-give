import React, {useState} from 'react';
import {
    Modal,
    Text,
    View,
    Alert,
    TouchableOpacity,
    Image,
    ScrollView,
    Linking
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import dynamicStyles from '../UpdateModal/style';
import {APPSTORE_URL} from 'react-native-dotenv';

const PostProjectOnNewsfeedModal = (props) => {

    const { modalVisible, hideModal, goToPlatform  } = props;
    const styles = useDynamicStyleSheet(dynamicStyles);

    return (
        <View style={{alignSelf: 'flex-start', marginVertical: 10}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <ScrollView
                    style={styles.scrollViewStyle}>
                    <View style={styles.container}>
                        <View style={styles.rowContainer}>
                            <Text style={styles.popUpTextTitle}>{"Hey !"}</Text>
                            <TouchableOpacity
                                style={styles.cancelButtonContainer}
                                onPress={() => {
                                    hideModal();
                                }}>
                                <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                            </TouchableOpacity>
                        </View>
                        <Text style={[styles.popUpText, {marginTop:0}]}>
                            <Text>{"Do you want to share this new project on Click & Give Newsfeed ?"}</Text>
                        </Text>
                        <TouchableOpacity
                            style={styles.updateButton}
                            onPress={() => {
                                hideModal();
                                goToPlatform();
                            }}>
                            <Text style={styles.updateButtonText}>SHARE</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Modal>
        </View>
    );
}

export default PostProjectOnNewsfeedModal;
