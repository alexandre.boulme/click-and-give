import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
  btnClickContain: {
    flexDirection: 'row',
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  btnIcon: {
    ...ifIphoneX({
      height: 25,
      width: 25,
    },{
      height: 20,
      width: 20,
    }),
  },
  btnText: {
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '300',
    marginLeft: 14,
    marginTop: 2,
    color: AppStyles.colorSet.mainTextColor,
  },
});

export default dynamicStyles;
