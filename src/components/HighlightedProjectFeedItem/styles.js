import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const reactionIconSize = Math.floor(AppStyles.WINDOW_WIDTH * 0.09);

const dynamicStyles = new DynamicStyleSheet({
  container: {
    ...ifIphoneX({
      marginVertical: 5,
    },{
      marginVertical: 3,
    }),
    marginRight: 10,
    marginLeft: 10,
    height:'94%',
    width: undefined,
    ...ifIphoneX({
      aspectRatio: 18/12,
    },{
      aspectRatio: 20/12,
    }),
    borderRadius: 20,
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: AppStyles.colorSet.lightGrey,
    shadowOpacity: 1.0,
    backgroundColor:AppStyles.colorSet.white
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    borderRadius: 20
  },
  contentContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderRadius: 20,
    height: '100%',
    backgroundColor: "#00000000"
  },
  topContainer: {
    ...ifIphoneX({
      marginTop: 20,
      marginLeft: 15
    },{
    marginTop: 10,
    marginLeft: 10
  })
  },
  tagContainer: {
    width: '80%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  tagStyle: {
    backgroundColor: AppStyles.colorSet.white,
    padding: 4,
    borderRadius: 5,
    marginRight: 8,
    marginBottom: 3
  },
  tagTextStyle: {
    fontSize: AppStyles.fontSet.xsmall,
    color: AppStyles.colorSet.black,
    fontWeight:'900'
  },
  tagNeedStyle:{
    ...ifIphoneX({
      marginTop: 5,
    },{
      marginTop: 2,
    }),
    marginRight: 5,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  tagTextNeedStyle:{
    fontSize: AppStyles.fontSet.xsmall,
    color: AppStyles.colorSet.white,
    fontWeight:'900'
  },
  needIconContainer:{
    width: 25,
    marginRight: 3,
    marginBottom: 3,
    height: undefined,
    aspectRatio: 1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  needIconStyle:{
    tintColor: AppStyles.colorSet.whitemoneyColor,
    width: '80%',
    height: undefined,
    aspectRatio: 1
  },
  bottomContentContainer: {
    ...ifIphoneX({
      marginLeft: 15,
      marginBottom: 20,
    },{
      marginLeft: 10,
      marginBottom: 10,
    }),
    justifyContent: 'flex-end',
    display: "none"
  },
  rowContainer: {
    flexDirection: 'row'
  },
  projectNameText: {
    fontSize: AppStyles.fontSet.middleplus,
    color: AppStyles.colorSet.white,
    fontWeight:'900',
    marginRight: 10
  },
  foundationIcon: {
    width: 20,
    height: 20,
    borderRadius: 10,
    alignSelf: 'center'
  },
  projectDescriptionText: {
    fontSize: AppStyles.fontSet.small,
    color: AppStyles.colorSet.white,
    fontWeight:'400',
    marginTop: 10
  }
});

export default dynamicStyles;
