import React from 'react';
import PropTypes from 'prop-types';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import Swiper from 'react-native-swiper';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import TruncateText from 'react-native-view-more-text';
import {Viewport} from '@skele/components';
import dynamicStyles from './styles';
import AppStyles from '../../AppStyles';
import FastImage from 'react-native-fast-image';

const ViewportAwareSwiper = Viewport.Aware(Swiper);

function HighlightedProjectFeedItem(props) {
  const {
    item,
    onItemPress,
    user,
    containerStyle
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);

  const renderNull = () => {
    return (
      <Text style={{height: 0}}></Text>
    );
  };

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={onItemPress}
      style={[styles.container, containerStyle]}>
      <FastImage
        style={styles.backgroundImage}
        source={{uri: item.projectPictureURL }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.contentContainer}>
        <View style={styles.topContainer}>
          <View style={styles.tagContainer}>
            {false && item.categories.map(item => {
              return     (
                <View style={styles.tagStyle}>
                  <Text style={styles.tagTextStyle}>{item.toUpperCase()}</Text>
                </View>
              )
            })}
          </View>
          <View style={[styles.rowContainer,]} >
            {item.needFund && (
              <View style={[styles.tagNeedStyle, {backgroundColor: AppStyles.colorSet.moneyColor}]}>
                <Text style={styles.tagTextNeedStyle}>{"Money".toUpperCase()}</Text>
              </View>
            )}
            {item.needVolunteer && (
              <View style={[styles.tagNeedStyle, {backgroundColor:AppStyles.colorSet.timeColor}]}>
                <Text style={styles.tagTextNeedStyle}>{"Time".toUpperCase()}</Text>
              </View>
            )}
          </View>
        </View>
        <View style={styles.bottomContentContainer}>
          <View style={styles.rowContainer}>
            <Text style={styles.projectNameText}>{item.name}</Text>
            {item.type === 'foundation' && (
              <Image source={AppStyles.iconSet.foundation} style={styles.foundationIcon} />
            )}
          </View>
          <TruncateText
            numberOfLines={2}
            renderViewMore={renderNull}
            renderViewLess={renderNull}
            textStyle={styles.body}>
            <Text  style={styles.projectDescriptionText}>{item.description}</Text>
          </TruncateText>
        </View>
      </View>
    </TouchableOpacity>
  );
}

HighlightedProjectFeedItem.propTypes = {
  onPress: PropTypes.func,
  onOtherReaction: PropTypes.func,
  onLikeReaction: PropTypes.func,
  onUserItemPress: PropTypes.func,
  onCommentPress: PropTypes.func,
  onMediaPress: PropTypes.func,
  item: PropTypes.object,
  shouldUpdate: PropTypes.bool,
  iReact: PropTypes.bool,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default HighlightedProjectFeedItem;
