import React, {useState} from 'react';
import {
    Modal,
    Text,
    View,
    Alert,
    TouchableOpacity,
    Image,
    ScrollView,
    Linking
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import dynamicStyles from './style';
import {APPSTORE_URL} from 'react-native-dotenv';

const UpdateModal = (props) => {

    const { modalVisible, hideModal  } = props;
    const styles = useDynamicStyleSheet(dynamicStyles);

    return (
        <View style={{alignSelf: 'flex-start', marginVertical: 10}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <ScrollView
                    style={styles.scrollViewStyle}>
                    <View style={styles.container}>
                        <View style={styles.rowContainer}>
                            <Text style={styles.popUpTextTitle}>{"Hey !"}</Text>
                            <TouchableOpacity
                                style={styles.cancelButtonContainer}
                                onPress={() => {
                                    hideModal();
                                }}>
                                <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.popUpText}>{"A new version of Click & Give is available on the AppStore, download it right now to get the last updates ! \n\nIt’ll will only take 1 min"}</Text>
                        <TouchableOpacity
                            style={styles.updateButton}
                            onPress={() => {
                                console.log(APPSTORE_URL);
                                Linking.openURL(APPSTORE_URL);
                                hideModal();
                            }}>
                            <Text style={styles.updateButtonText}>UPDATE</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Modal>
        </View>
    );
}

export default UpdateModal;
