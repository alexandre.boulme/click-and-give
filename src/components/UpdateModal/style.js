import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
    modalViewContainer: {
        marginTop: '20%',
        alignSelf: 'center',
        width: '90%',
        ...ifIphoneX({
            padding: 20,
        },{
            padding: 10,
        }),
        backgroundColor: "#FFF",
        borderRadius: 20,
        shadowOffset:{  width: 0,  height: 5,  },
        shadowColor: "#999",
        shadowOpacity: 1.0,
    },
    scrollViewStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: "#00000070",
    },
    container: {
        backgroundColor: "#FFF",
        borderRadius: 15,
        padding: 20,
        paddingTop: 0,
        marginTop: "50%",
        width: '90%',
        alignSelf: "center"
    },
    rowContainer:{
        flexDirection: 'row',
        justifyContent: "space-between",
        alignContent: 'center',
        alignItems: 'center',
        marginVertical: 5
    },
    headlineText: {
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '500',
        color: AppStyles.colorSet.mainTextColor,
        ...ifIphoneX({
            marginVertical: 15
        },{
            marginVertical: 10
        }),
    },
    basicButton: {
        ...ifIphoneX({
            padding: 15,
        },{
            padding: 10,
        }),
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: 10,
        borderColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    buttonText: {
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '400',
        color: AppStyles.colorSet.mainThemeForegroundColor,
        textAlign: 'center'
    },
    cancelButtonContainer: {
        ...ifIphoneX({
            padding: 20,
        },{
            padding: 10,
        }),
    },
    updateButton:{
        ...ifIphoneX({
            padding: 20,
        },{
            padding: 10,
        }),
        backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
        borderRadius: 30
    },
    updateButtonText: {
        fontSize: AppStyles.fontSet.normal,
        fontWeight: '600',
        color: AppStyles.colorSet.mainThemeBackgroundColor,
        textAlign: 'center'
    },
    applyButtonContainer: {
        ...ifIphoneX({
            padding: 20,
            paddingLeft: 30,
            paddingRight: 30,
        },{
            padding: 10,
            paddingLeft: 20,
            paddingRight: 20,
        }),
        borderRadius: 30,
        backgroundColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    applyButton : {
        fontSize: AppStyles.fontSet.normal,
        fontWeight: '600',
        color: "#FFF",
        textAlign: 'center'
    },
    resetFiltersButton: {
        marginVertical: 15,
        alignItems: 'center',
        padding: 15,
        backgroundColor: "#FFF",
        borderColor: AppStyles.colorSet.mainThemeForegroundColor,
        borderWidth: 2,
        borderRadius: 10
    },
    resetFiltersButtonText: {
        color: AppStyles.colorSet.mainThemeForegroundColor,
        fontSize: AppStyles.fontSet.middleminus,
        fontWeight: '400'
    },
    containerInactive: {
        borderColor: "#222"
    },
    textInactive: {
        color: "#222"
    },
    modalVisibleContainer: {
        alignSelf: 'center',
        ...ifIphoneX({
            width: 40,
            height: 40,
            marginRight: 15,
            shadowOffset:{  width: 0,  height: 5,  },
        },{
            width: 35,
            height: 35,
            marginRight: 10,
            shadowOffset:{  width: 0,  height: 3,  },
        }),
        backgroundColor: "#FFF",
        shadowColor: "#999",
        shadowOpacity: 1.0,
        borderRadius: 5
    },
    modalVisibleIcon: {
        alignSelf: 'flex-end',
        ...ifIphoneX({
            width: 30,
            height: 30,
            margin: 5,
        },{
            width: 23,
            height: 23,
            margin: 5,
        }),
        tintColor: AppStyles.colorSet.black
    },
    InputContainer: {
        padding: 5,
        paddingBottom: 10,
        color: AppStyles.colorSet.mainTextColor,
        width: '100%',
        alignSelf: 'stretch',
        alignItems: 'center',
        fontSize:   AppStyles.fontSet.middle,
        borderBottomWidth :1,
        borderBottomColor: "#000",
        textAlign: 'left',
    },
    popUpText: {
        color: AppStyles.colorSet.mainTextColor,
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '400',
        marginVertical: 15,
        marginBottom: 30
    },
    popUpTextTitle: {
        color: AppStyles.colorSet.mainTextColor,
        fontSize: AppStyles.fontSet.large,
        fontWeight: '800'
    }
});

export default dynamicStyles;
