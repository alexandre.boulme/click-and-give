import React, {useEffect, useState} from 'react';
import {Text, View, Modal, TouchableOpacity, Keyboard} from 'react-native';
import { CreatePost } from '../';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import {firebasePost} from '../../Core/socialgraph/feed/firebase';
import { firebaseStorage } from '../../Core/firebase/storage';
import AppStyles from '../../AppStyles';
import dynamicStyles from './styles';
import TNActivityIndicator from '../../Core/truly-native/TNActivityIndicator';
import { firebaseAnalytics } from "../../Core/firebase";

const defaultPost = {
  postText: '',
  commentCount: 0,
  reactionsCount: 0,
  reactions: {
    surprised: 0,
    angry: 0,
    sad: 0,
    laugh: 0,
    like: 0,
    cry: 0,
    love: 0,
  },
};

const CreatePostModal = (props) => {

  const {visible, user, hideModal, postToEdit, preFillText} = props;
  const [isPosting, setIsPosting] = useState(false);
  const [post, setPost] = useState(postToEdit ? postToEdit : defaultPost);
  const [postMedia, setPostMedia] = useState(postToEdit ? postToEdit.postMedia : []);
  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(()=>{
      if(preFillText){
          console.log("Prefill in createPostModal : ", preFillText);
          setPost({postText: preFillText, postMedia:[], commentCount: 0, reactionsCount: 0, reactions: {surprised: 0,angry: 0, sad: 0, laugh: 0, like: 0, cry: 0, love: 0,}});
      }
  },[preFillText])

  const onPostDidChange = post => {
    setPost(post);
  };

  const onSetMedia = photos => {
    setPostMedia([...photos]);
  };

  const onPost = async () => {

    setIsPosting(true);

    console.log("USER : ", user);
    console.log("post : ", post);
    console.log("postToEdit : ", postToEdit);

    firebaseAnalytics.sendEvent("post_creation",
        {
          creator: user.firstName + ' ' + user.lastName
        });

    if(!post){
        setPost(defaultPost);
    }

    console.log("POst before : ", post);
    const postToPublish = post;
    console.log("Post : ", post);
    console.log(user);
    postToPublish.authorID = user.id;
    postToPublish.postMedia = postMedia;
    console.log("Post : ", postToPublish);
    setPost(postToPublish);

    if (post && postMedia && post.postMedia.length === 0) {
      await firebasePost
        .addPost(
          post,
          null,
          user
        );
      setIsPosting(false);
      setPostMedia([]);
      hideModal();
    } else {
      startPostUpload(true);
    }

  }

  const editPost = async () => {

        setIsPosting(true);

        console.log(postToEdit.postText);
        console.log(post.postText);
        console.log(postToEdit.postMedia);
        console.log(postMedia);

        if (postToEdit.postText != post.postText && postToEdit.postMedia == postMedia || postMedia.length == 0) {

            postToEdit.postText = post.postText;
            postToEdit.postMedia = postMedia;
            await firebasePost
                .updatePost(
                    postToEdit.id,
                    postToEdit
                );
            setIsPosting(false);
            setPostMedia([]);
            hideModal();
        } else if (postToEdit.postMedia != postMedia) {
            console.log(postMedia);
            await startPostUpload(false);
        } else {
            hideModal();
        }
    }

  const startPostUpload = async (createPost) => {
    console.log("begin image upload");
    const uploadPromises = [];
    const mediaSources = [];

    console.log(post.postMedia);

    if(createPost){
        post.postMedia.forEach(media => {
            const { uploadUri, mime } = media;
            console.log(media);
            uploadPromises.push(
                new Promise((resolve, reject) => {
                    firebaseStorage
                        .uploadImage(uploadUri)
                        .then(response => {
                            if (!response.error) {
                                mediaSources.push({ url: response.downloadURL, mime });
                            } else {
                                alert(IMLocalized("Oops! An error occured while uploading your post. Please try again."));
                            }
                            resolve();
                        })
                }),
            );
        });
    } else {
        postMedia.forEach(media => {
            const { uploadUri, mime } = media;
            console.log(media);
            uploadPromises.push(
                new Promise((resolve, reject) => {
                    firebaseStorage
                        .uploadImage(uploadUri)
                        .then(response => {
                            if (!response.error) {
                                mediaSources.push({ url: response.downloadURL, mime });
                            } else {
                                alert(IMLocalized("Oops! An error occured while uploading your post. Please try again."));
                            }
                            resolve();
                        })
                }),
            );
        });
    }

    Promise.all(uploadPromises).then(async () => {
        if(createPost){
            const postToUpload = { ...post, postMedia: [...mediaSources] };
            firebasePost
                .addPost(
                    postToUpload,
                    null,
                    user
                );
            setPostMedia([]);
            setIsPosting(false);
            hideModal();
        } else {
            postToEdit.postText = post.postText;
            postToEdit.postMedia = [...mediaSources];
            firebasePost
                .updatePost(
                    postToEdit.id,
                    postToEdit
                );
            setPostMedia([]);
            setIsPosting(false);
            hideModal();
        }
    });
  };

  return (
    <Modal
     animationType="slide"
     transparent={true}
     visible={visible}
   >
    <View style={styles.modalBackground}>
    <KeyboardAwareView
      animated={true}
      style={styles.modalContainer}>
         <View style={styles.modalButtonContainer}>
              <TouchableOpacity
                style={[styles.modalButton]}
                onPress={hideModal}>
                <Text style={[styles.modalTextButton,{color: AppStyles.colorSet.black}]}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.modalButton]}
                onPress={() => {
                    console.log(post);
                    console.log(postToEdit);
                    if(postToEdit){
                        editPost()
                    } else {
                        onPost()
                    }
                }}>
                <Text style={[styles.modalTextButton,{color: AppStyles.colorSet.mainThemeForegroundColor}]}>{postToEdit ? "Done":"Publish"}</Text>
              </TouchableOpacity>
         </View>
           <CreatePost
             user={user}
             postToEdit={postToEdit ? postToEdit : {postText: preFillText, postMedia:[], commentCount: 0, reactionsCount: 0, reactions: {surprised: 0,angry: 0, sad: 0, laugh: 0, like: 0, cry: 0, love: 0,}}}
             onPostDidChange={onPostDidChange}
             onSetMedia={onSetMedia}
           />
       </KeyboardAwareView>
      </View>
      {isPosting && (
        <TNActivityIndicator appStyles={AppStyles} />
      )}
     </Modal>
  );

};

export default CreatePostModal;
