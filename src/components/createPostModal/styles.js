import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';


const dynamicStyles = new DynamicStyleSheet({
  modalBackground: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    height: '100%',
  },
  modalContainer: {
    marginTop: '25%',
    borderRadius: 30,
    height: '100%',
    backgroundColor: "#FFF"
  },
  modalButtonContainer : {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    borderBottomColor: AppStyles.colorSet.lightGrey,
    borderBottomWidth: 1
  },
  modalButton:{},
  modalTextButton: {
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: '400'
  }
});

export default dynamicStyles;
