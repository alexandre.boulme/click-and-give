import React, {useState, useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Text, View, TouchableOpacity, Alert, Modal, Image, ScrollView} from 'react-native';
import Swiper from 'react-native-swiper';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import ActionSheet from 'react-native-actionsheet';
import TruncateText from 'react-native-view-more-text';
import {Viewport} from '@skele/components';
import FeedMedia from './FeedMedia';
import {TNTouchableIcon, TNStoryItem} from '../../Core/truly-native';
import dynamicStyles from './styles';
import AppStyles from '../../AppStyles';
import { timeFormat } from '../../Core';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import CommentItem from '../screens/DetailPost/CommentItem';
import { getPostLastComment, getPostLikeUserList } from '../../Core/socialgraph/feed/firebase/comment';
import {IMConversationIconView} from "../../Core/chat";
import {firebaseAnalytics} from "../../Core/firebase";
import Hyperlink from 'react-native-hyperlink';
import CategorySelector from "../../Core/onboarding/components/categorySelector";

const ViewportAwareSwiper = Viewport.Aware(Swiper);
const reactionIcons = ['like', 'love', 'laugh', 'surprised', 'cry', 'angry'];

function FeedItem(props) {

  const {
    item,
    onCommentPress,
    containerStyle,
    onUserItemPress,
    onMediaPress,
    onReaction,
    onSharePost,
    onDeletePost,
    onUserReport,
    user,
    willBlur,
    detailPage
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);

  let defaultReaction = 'heartUnfilled';
  const [postMediaIndex, setPostMediaIndex] = useState(0);
  const [inViewPort, setInViewPort] = useState(false);
  const [otherReactionsVisible, setOtherReactionsVisible] = useState(false);
  const [selectedIconName, setSelectedIconName] = useState(item.myReaction ? item.myReaction : defaultReaction);

  const [reactionCount, setReactionCount] = useState(item.reactionsCount);
  const [previousReaction, setPreviousReaction] = useState(item.myReaction); // the source of truth - coming directly from the database
  const [lastComment, setLastComment] = useState(null);
  const [lastComments, setLastComments] = useState(null);
  const [lastCommentVisible, setLastCommentVisible] = useState(false);
  const [likeUserList, setLikeUserList] = useState(null);
  const [showLikes, setShowLikes] = useState(false);
  const moreRef = useRef();

  useEffect(() => {
      setSelectedIconName(item.myReaction ? item.myReaction : defaultReaction);
      setReactionCount(item.reactionsCount);
      setPreviousReaction(item.myReaction); // this only changes when database reaction changes

      if(!detailPage && item.commentCount > 0 && !lastComments){
        getPostLastComment(item.id,comment => {
          setLastComments(comment);
          setLastCommentVisible(true);
        });
      } else if (item.commentCount == 0){
        setLastCommentVisible(false);
        setLastComments(null);
      }

      if(item.reactionsCount > 0){
        getPostLikeUserList(item.id,likesUser => {
          setLikeUserList(likesUser);
        });
      }

  }, [item]);

  const tintColor = styles.tintColor;

  const onReactionPress = async reaction => {

    if(reaction){
      firebaseAnalytics.sendEvent("like_post", {postId: item.id});
    } else {
      firebaseAnalytics.sendEvent("unlike_post", {postId: item.id});
    }

    if (reaction == null) {
      // this was a single tap on the inline icon, therefore a like or unlike
      if (item.myReaction) {
        setSelectedIconName(defaultReaction);
        onReaction(null, item); // sending a null reaction will undo the previous action
      } else {
        setSelectedIconName('heartFilled');
        onReaction('heartFilled', item); // there were no reaction before, and there was a single tap on the inline action button
      }
      return;
    }
    // this was a reaction on the reactions tray, coming after a long press + one tap
    if (item.myReaction && item.myReaction == reaction) {
      // Nothing changes, since this is the same reaction as before
      return;
    }
    //setSelectedIconName(reaction ? reaction : defaultReaction);
    setOtherReactionsVisible(false);
    onReaction(reaction, item);
  };

  const onReactionLongPress = () => {
    setOtherReactionsVisible(!otherReactionsVisible);
  };

  const reactionCountText = (reactionCount, myReaction) => {
    var text = '';

    if(reactionCount > 2 && myReaction){
      text = 'You and ' + (reactionCount - 1) + " others";
    } else if (reactionCount == 2 && myReaction) {
      text = 'You and 1 other';
    } else if (reactionCount > 0 && myReaction) {
      text = 'You';
    } else {
      text = reactionCount;
    }

    return text;
  };

  const reactionCountTextWithNames = (reactionCount, myReaction) => {

    var text = '';
    var firstUserLikingName = '';
    var secondUserLikingName = '';

    if(reactionCount > 1 && likeUserList.length > 1){
      firstUserLikingName = likeUserList[0].id != user.id ? likeUserList[0].firstName : likeUserList[1].firstName;
      secondUserLikingName = likeUserList[1].firstName;
    } else if (reactionCount == 1 && likeUserList.length == 1){
      firstUserLikingName = likeUserList[0].firstName;
    }

    if(reactionCount > 2 && reactionCount == likeUserList.length){
      var otherWritting = reactionCount == 2 ? " other" : " others";
      if(myReaction){
        text = 'You and ' + (reactionCount - 1) + otherWritting;
      } else {
        text = firstUserLikingName + ' and ' + (reactionCount - 1) + otherWritting;
      }
    } else if (reactionCount > 1  && reactionCount == likeUserList.length) {
      if(myReaction){
        text = 'You and ' + firstUserLikingName;
      } else {
        text = firstUserLikingName + ' and ' + secondUserLikingName;
      }
    } else if (reactionCount > 0  && reactionCount == likeUserList.length) {
      if(myReaction){
        text = 'You like';
      } else {
        text = firstUserLikingName + " likes";
      }
    } else {
      text = reactionCount;
    }
    return text;
  };

  const onMorePress = () => {
    moreRef.current.show();
  };

  const didPressComment = (userLikesList) => {
    onCommentPress(item, userLikesList);
  };

  const moreArray = [IMLocalized('Share Post')];

  if (item.authorID === user.id) {
    moreArray.push(IMLocalized('Delete Post'));
  } else {
    moreArray.push(IMLocalized('Block User'));
    moreArray.push(IMLocalized('Report Post'));
  }

  moreArray.push(IMLocalized('Cancel'));

  const onMoreDialogDone = index => {
    if (index === moreArray.indexOf(IMLocalized('Share Post'))) {
      onSharePost(item);
    }

    if (
      index === moreArray.indexOf(IMLocalized('Report Post'))
    || index === moreArray.indexOf(IMLocalized('Block User'))
    ) {
      onUserReport(item, moreArray[index]);
    }

    if (index === moreArray.indexOf(IMLocalized('Delete Post'))) {
      onDeletePost(item);
    }
  };

  const inactiveDot = () => <View style={styles.inactiveDot} />;

  const activeDot = () => <View style={styles.activeDot} />;

  const renderTouchableIconIcon = (src, tappedIcon, index) => {
    return (
      <TNTouchableIcon
        containerStyle={styles.reactionIconContainer}
        iconSource={src}
        imageStyle={styles.reactionIcon}
        onPress={() => onReactionPress(tappedIcon)}
        appStyles={AppStyles}
      />
    );
  };

  const renderViewMore = onPress => {
    return (
      <Text onPress={onPress} style={styles.moreText}>
        {IMLocalized('View More')}
      </Text>
    );
  };

  const renderViewLess = onPress => {
    return (
      <Text onPress={onPress} style={styles.moreText}>
        {IMLocalized('View Less')}
      </Text>
    );
  };

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={didPressComment}
      style={[styles.container, containerStyle]}>
      <View style={styles.headerContainer}>
        <TNStoryItem
          imageContainerStyle={styles.userImageContainer}
          imageStyle={styles.userImage}
          item={item.author}
          onPress={() => onUserItemPress(item.author)}
          appStyles={AppStyles}
        />
        <View style={styles.titleContainer}>
            <Text style={styles.title}>
              {item.author.firstName + (item.author.lastName ? ' ' + item.author.lastName : '')}
            </Text>
          <View style={styles.mainSubtitleContainer}>
            <View style={styles.subtitleContainer}>
              <Text style={styles.subtitle}>{timeFormat(item.createdAt)}</Text>
            </View>
          </View>
        </View>
        {false && (
          <TNTouchableIcon
            onPress={onMorePress}
            imageStyle={styles.moreIcon}
            containerStyle={styles.moreIconContainer}
            iconSource={AppStyles.iconSet.more}
            appStyles={AppStyles}
          />
        )}
      </View>
      {detailPage ? (
              <Hyperlink
                  linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
                  linkDefault>
                <Text style={styles.body}>{item.postText}</Text>
              </Hyperlink>
      ):(
          <Hyperlink
              linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
              linkDefault>
            <TruncateText
                numberOfLines={5}
                renderViewMore={renderViewMore}
                renderViewLess={renderViewLess}
                textStyle={styles.body}>
                <Text>{item.postText}</Text>
            </TruncateText>
          </Hyperlink>
      )}
      {item.postMedia && item.postMedia.length > 0 && (
        <View style={styles.bodyImageContainer}>
          <ViewportAwareSwiper
            removeClippedSubviews={false}
            containerStyle={{flex: 1}}
            dot={inactiveDot()}
            activeDot={activeDot()}
            paginationStyle={{
              bottom: 20,
            }}
            onIndexChanged={swiperIndex => setPostMediaIndex(swiperIndex)}
            loop={false}
            onViewportEnter={() => setInViewPort(true)}
            onViewportLeave={() => setInViewPort(false)}
            preTriggerRatio={-0.6}>
            {item.postMedia.map((media, index) => (
              <FeedMedia
                inViewPort={inViewPort}
                index={index}
                postMediaIndex={postMediaIndex}
                media={media}
                item={item}
                onImagePress={onMediaPress}
                dynamicStyles={styles}
                willBlur={willBlur}
              />
            ))}
          </ViewportAwareSwiper>
          {otherReactionsVisible && (
            <View style={[styles.reactionContainer]}>
              {reactionIcons.map((icon, index) =>
                renderTouchableIconIcon(AppStyles.iconSet[icon], icon, index),
              )}
            </View>
          )}
        </View>
      )}
      <View style={styles.footerContainer}>
          <TouchableOpacity
            style={{flexDirection: 'row'}}
            onPress={() => {
              if(item.reactionsCount > 0){
                setShowLikes(!showLikes)
              }
            }}>
            <TNTouchableIcon
              containerStyle={styles.footerIconContainer}
              iconSource={AppStyles.iconSet[selectedIconName]}
              imageStyle={[styles.footerIcon, tintColor]}
              renderTitle={true}
              title={null}
              titleStyle={{fontSize: AppStyles.fontSet.middleminus, fontWeight: '300', marginLeft: 3}}
              onLongPress={() => onReactionPress(item.myReaction ? null : "heartFilled")}
              onPress={() => onReactionPress(item.myReaction ? null : "heartFilled")}
              appStyles={AppStyles}
            />
            <Text style={{fontSize: AppStyles.fontSet.middleminus, fontWeight: '300', alignSelf: 'center'}}>{likeUserList ? reactionCountTextWithNames(reactionCount, item.myReaction) : ''}</Text>
          </TouchableOpacity>
        {!detailPage && (
          <TouchableOpacity
            style={{flexDirection: 'row'}}
            onPress={didPressComment}>
            <TNTouchableIcon
              containerStyle={styles.footerIconContainer}
              iconSource={AppStyles.iconSet.commentFilled}
              imageStyle={[styles.footerIcon, styles.tintColor]}
              renderTitle={true}
              titleStyle={{fontSize: AppStyles.fontSet.middleminus, fontWeight: '300', marginLeft: 3}}
              title={item.commentCount < 1 ? '' : item.commentCount == 1 ? ' ' + item.commentCount + ' comment' : ' ' +  item.commentCount + ' comments'}
              onPress={didPressComment}
              appStyles={AppStyles}
            />
            {item.commentCount == 0 && (<Text style={{fontSize: AppStyles.fontSet.middleminus, fontWeight: '300', alignSelf: 'center'}}>Add a comment</Text>)}
          </TouchableOpacity>
        )}
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showLikes}
        presentationStyle={"overFullScreen"}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={{backgroundColor: "#00000050", height:'100%', width:'100%'}}>
        <View style={{backgroundColor: AppStyles.colorSet.white, marginTop: 100, margin: 30, padding: 10, borderRadius: 20}}>
          <View
            style={styles.modalRowContainer}>
            <Text style={styles.modalTextTitle}>Post's likes</Text>
            <TouchableOpacity
                style={styles.cancelButtonContainer}
                onPress={() => {
                  setShowLikes(!showLikes);
                }}>
              <Image source={AppStyles.iconSet.close} style={styles.modalCloseIcon} />
            </TouchableOpacity>
          </View>
          <ScrollView>
            {likeUserList && (
                likeUserList.map((item, i) => (
                    <TouchableOpacity
                        onPress={() => {
                          setShowLikes(!showLikes)
                          onUserItemPress(item)
                        }}
                        style={styles.itemContainer}>
                      <View style={styles.chatIconContainer}>
                        <IMConversationIconView
                            style={styles.photo}
                            imageStyle={styles.photo}
                            participants={[item]}
                            appStyles={AppStyles}
                        />
                        <Text style={styles.name}>{item.firstName + ' ' + item.lastName}</Text>
                      </View>
                      <View style={styles.divider} />
                    </TouchableOpacity>
                ))
            )}
          </ScrollView>
        </View>
        </View>
      </Modal>
      <ActionSheet
        ref={moreRef}
        title={IMLocalized('More')}
        options={moreArray}
        destructiveButtonIndex={moreArray.indexOf('Delete Post')}
        cancelButtonIndex={moreArray.length - 1}
        onPress={onMoreDialogDone}
      />
      {lastCommentVisible && (
          lastComments.map((item, i) => (
              <CommentItem item={item} onPressComment={() => {}} />
          ))
      )}
    </TouchableOpacity>
  );
}


FeedItem.propTypes = {
  onPress: PropTypes.func,
  onOtherReaction: PropTypes.func,
  onLikeReaction: PropTypes.func,
  onUserItemPress: PropTypes.func,
  onCommentPress: PropTypes.func,
  onMediaPress: PropTypes.func,
  item: PropTypes.object,
  shouldUpdate: PropTypes.bool,
  iReact: PropTypes.bool,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default FeedItem;
