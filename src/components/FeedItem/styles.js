import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const reactionIconSize = Math.floor(AppStyles.WINDOW_WIDTH * 0.09);

const dynamicStyles = new DynamicStyleSheet({
  container: {
    alignSelf: 'center',
    marginVertical: 2,
    width: '100%',
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  userImageContainer: {
    borderWidth: 0,
    marginLeft: 10,
    marginRight: 4,
    backgroundColor: '#CCC'
  },
  userImage: {
    ...ifIphoneX({
      width: 50,
      height: 50,
    },{
      width: 45,
      height: 45,
    }),
  },
  titleContainer: {
    flex: 6,
    justifyContent: 'center',
    marginTop: 5,
  },
  title: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: '800',
  },
  mainSubtitleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: 2,
  },
  subtitleContainer: {
    flex: 1.3,
  },
  subtitle: {
    color: AppStyles.colorSet.mainSubtextColor,
    fontSize: 10,
  },
  moreIconContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  moreIcon: {
    height: 18,
    width: 18,
    tintColor: AppStyles.colorSet.mainSubtextColor,
    margin: 0,
  },
  bodyTitleContainer: {
    marginHorizontal: 8,
  },
  body: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.middle,
    lineHeight: 18,
    paddingTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 12,
  },
  moreText: {
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontSize: 13,
    lineHeight: 18,
    paddingBottom: 15,
    paddingHorizontal: 12,
  },
  bodyImageContainer: {
    height: AppStyles.WINDOW_HEIGHT * 0.22,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 30
  },
  bodyImage: {
    height: '100%',
    width: '100%',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    overflow: 'hidden',
  },
  inactiveDot: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 6,
    height: 6,
    borderRadius: 3,
    marginLeft: 3,
    marginRight: 3,
  },
  activeDot: {
    backgroundColor: '#fff',
    width: 6,
    height: 6,
    borderRadius: 3,
    marginLeft: 3,
    marginRight: 3,
  },
  reactionContainer: {
    flexDirection: 'row',
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
    position: 'absolute',
    bottom: 2,
    width: Math.floor(AppStyles.WINDOW_WIDTH * 0.68),
    height: 48,
    borderRadius: Math.floor(AppStyles.WINDOW_WIDTH * 0.07),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 2,
  },
  reactionIconContainer: {
    margin: 3,
    padding: 0,
    backgroundColor: 'powderblue',
    width: reactionIconSize,
    height: reactionIconSize,
    borderRadius: reactionIconSize / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  reactionIcon: {
    width: reactionIconSize,
    height: reactionIconSize,
    margin: 0,
  },
  footerContainer: {
    flexDirection: 'row',
    ...ifIphoneX({
      marginRight: '18%',
    },{
      marginRight: '16%',
    }),
    justifyContent: 'space-between'
  },
  footerIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
  },
  footerIcon: {
    margin: 3,
    ...ifIphoneX({
      height: 28,
      width: 28,
    },{
      height: 22,
      width: 22,
    }),
  },
  mediaVideoLoader: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  centerItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  tintColor: {tintColor: AppStyles.colorSet.mainThemeForegroundColor},
  itemContainer: {
    padding: 12,
    alignItems: 'center',
    flexDirection: 'row',
  },
  chatIconContainer: {
    flex: 6,
    flexDirection: 'row',
    alignItems: 'center',
  },
  modalRowContainer:{
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: "center",
    padding: 10
  },
  modalTextTitle:{
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: "600"
  },
  modalCloseIcon: {
    height: 40,
    width: 40,
    tintColor: AppStyles.colorSet.black
  },
  divider: {
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
    height: 0.5,
    backgroundColor: AppStyles.colorSet.hairlineColor,
  },
  photo: {
    height: 50,
    borderRadius: 30,
    width: 50,
  },
  name: {
    marginLeft: 20,
    alignSelf: 'center',
    flex: 1,
    fontSize: AppStyles.fontSet.middleminus,
    fontWeight: '500',
    color: AppStyles.colorSet.mainTextColor,
  },
});

export default dynamicStyles;
