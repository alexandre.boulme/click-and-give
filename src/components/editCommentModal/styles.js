import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {I18nManager} from "react-native";



const dynamicStyles = new DynamicStyleSheet({
    modalBackground: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        marginTop: '15%',
        height: '100%',
        justifyContent: "center"
    },
    InputContainer: {
        height: 120,
        paddingLeft: 0,
        color: AppStyles.colorSet.mainTextColor,
        width: '100%',
        alignSelf: 'stretch',
        marginTop: 5,
        alignItems: 'center',
        fontSize:   AppStyles.fontSet.middle,
        fontWeight: '300',
        borderRadius: 0,
        borderBottomWidth :1,
        borderBottomColor: "#000",
        textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    modalContainer: {
        borderRadius: 30,
        backgroundColor: "#FFF",
        padding: 20,
        marginHorizontal: 10
    },
    modalButtonContainer : {
        flexDirection: 'row',
        borderRadius: 30,
        backgroundColor: "#FFF",
        justifyContent: 'space-between',
        paddingTop: 20,
    },
    modalButton:{},
    modalTextButton: {
        fontSize: AppStyles.fontSet.middleplus,
        fontWeight: '400'
    },
    headlineText: {
        fontSize: AppStyles.fontSet.large,
        fontWeight: '600',
        marginBottom: 15
    }
});

export default dynamicStyles;
