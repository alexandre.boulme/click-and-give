import React, {useState, useEffect} from 'react';
import {Text, View, Modal, TouchableOpacity, Keyboard, TextInput} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import {firebaseComment} from '../../Core/socialgraph/feed/firebase';
import AppStyles from '../../AppStyles';
import dynamicStyles from './styles';
import TNActivityIndicator from '../../Core/truly-native/TNActivityIndicator';
import {IMLocalized} from "../../Core/localization/IMLocalization";
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';

const EditCommentModal = (props) => {

    const {visible, comment, hideModal} = props;
    const [isPosting, setIsPosting] = useState(false);
    const [commentText, setCommentText] = useState('');
    const styles = useDynamicStyleSheet(dynamicStyles);

    useEffect(() => {
        if(comment && comment.commentText){
            setCommentText(comment.commentText);
        }
    }, [comment]);


    const editComment = async () => {

        setIsPosting(true);

        firebaseComment.updateComment(comment.commentID, commentText);

        setIsPosting(false);

        hideModal();

    }


    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
        >
            <KeyboardAwareView
                animated={true}>
            <View
                style={styles.modalBackground}
            >
                <View style={styles.modalContainer}>
                    <Text style={styles.headlineText}>Edit your comment</Text>
                    <TextInput
                        style={styles.InputContainer}
                        placeholder={IMLocalized('Your comment text')}
                        placeholderTextColor="#CCC"
                        value={commentText}
                        onChangeText={text => setCommentText(text)}
                        multiline={true}
                        underlineColorAndroid="transparent"
                    />
                    <View style={styles.modalButtonContainer}>
                        <TouchableOpacity
                            style={[styles.modalButton]}
                            onPress={hideModal}>
                            <Text style={[styles.modalTextButton,{color: AppStyles.colorSet.black}]}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.modalButton]}
                            onPress={() => editComment()}>
                            <Text style={[styles.modalTextButton,{color: AppStyles.colorSet.mainThemeForegroundColor}]}>Done</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            {isPosting && (
                <TNActivityIndicator appStyles={AppStyles} />
            )}
            </KeyboardAwareView>
        </Modal>
    );

};

export default EditCommentModal;
