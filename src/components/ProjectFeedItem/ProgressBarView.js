import React from 'react';
import {Text, Image, View} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import { daysLeftCalculation } from '../../Core';
import AppStyles from '../../AppStyles';
import * as Progress from 'react-native-progress';
import dynamicStyles from './styles';
import numeral from 'numeral';


const ProgressBarView = (props) => {

  const { donation, currency, fundTarget, volunteerTarget, volunteerAmount, needfund, needvolunteer, endDate, disableEndDate, replaceEndDateText, displayTime, displayVolunteerProgressBar} = props;

  const daysLeft = endDate ? daysLeftCalculation(endDate) : '';
  let timeLeft = "";

  //Handle the differents case to display project deadline
  if (disableEndDate) {
    timeLeft = replaceEndDateText;
  } else if (daysLeft < -10){
    timeLeft = 'CLOSED';
  } else if (daysLeft < 0){
    timeLeft = 'LAST MINUTE';
  } else if (daysLeft < 60){
    timeLeft = daysLeft > 1 ? daysLeft + " DAYS LEFT" :  daysLeft + " DAY LEFT";
  } else if (daysLeft < 360 ){
    const monthsLeft = daysLeft / 30;
    timeLeft = monthsLeft > 1 ? monthsLeft.toFixed(0) +" MONTHS LEFT" : monthsLeft.toFixed(0) +" MONTH LEFT";
  } else {
    const yearsLeft = daysLeft / 360;
    timeLeft = yearsLeft > 1 ? yearsLeft.toFixed(0) + " YEARS LEFT" : yearsLeft.toFixed(0) + " YEAR LEFT";
  }

  var barColor = "";
  var progress = 0;

  //Handle money vs. time progress bar design
  if(needfund && !displayVolunteerProgressBar){
    progress = donation.amount / fundTarget;
    var amountDisplayed = numeral(donation.amount).format('0,0');
    var targetDisplayed = numeral(fundTarget).format('0,0');
    barColor = AppStyles.colorSet.moneyColor;
  } else {
    progress = volunteerAmount / volunteerTarget;
    barColor = AppStyles.colorSet.timeColor;
  }

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View>
      <View style={styles.progressBarInfoContainer}>
        {needfund && !displayVolunteerProgressBar ? (
          <View style={styles.rowContainer} >
            <Text style={[styles.smallText, {color: barColor, fontWeight:'900'}]}>{currency.sign}{amountDisplayed}</Text>
            <Text  style={[styles.smallText, {color: AppStyles.colorSet.black, fontWeight:'400' }]}> raised / {currency.sign}{targetDisplayed}</Text>
            {!displayTime && (
                <Text  style={[styles.smallText, {color: AppStyles.colorSet.black, fontStyle: "italic", fontWeight:'300' }]}> (Pre-matching)</Text>
            )}
          </View>
        ) : (
          <View style={[styles.rowContainer, {alignItems: 'center'}]} >
            <Text style={[styles.smallText, {color: barColor, fontWeight:'900'}]}>{volunteerAmount}</Text>
            <Text  style={[styles.smallText, {color: AppStyles.colorSet.black, fontWeight:'400' }]}> {volunteerAmount < 2 ? 'volunteer /' : 'volunteers /'} {volunteerTarget}</Text>
          </View>
        )}
        {displayTime && (
          <View style={[styles.rowContainer, {alignItems: 'center'}]}>
            <Image source={AppStyles.iconSet.timeLeft} style={styles.deadlineIcon}/>
            <Text style={styles.deadlineText}>{timeLeft}</Text>
          </View>
        )}
      </View>
      <View style={[styles.progressBarStyle]}>
        <Progress.Bar
            animated={false}
            color={barColor}
            progress={progress}
            borderWidth={0}
            width={null}
            borderColor={'transparent'}
            style={{backgroundColor: "#DDD", width: '100%'}} />
      </View>
    </View>
  );

};

export default ProgressBarView;
