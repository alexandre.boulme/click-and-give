import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../AppStyles';
import ProgressBarView from './ProgressBarView';
import FastImage from 'react-native-fast-image';

function ProjectFeedItem(props) {
  const {
    item,
    onUserItemPress,
    onItemPress,
    containerStyle,
    displayWaitingApprovalTag
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(()=> {
      console.log(item.name);
  }, [item])

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => onItemPress(item)}
      style={[styles.container, containerStyle]}>
      <View style={[styles.rowContainer, styles.topContentContainer]}>
        <View style={styles.imageContainer}>
          <FastImage
            style={styles.imageStyle}
            source={{uri: item.projectPictureURL }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>
        <View style={styles.itemInfoColumn}>
          <View style={[styles.rowContainer,{marginBottom: 5, flexShrink: 1}]} >
              <Text style={styles.projectTitleText}>{item.name}</Text>
              {item.type === 'foundation' && (
                <Image source={AppStyles.iconSet.foundation} style={styles.foundationIcon} />
              )}
          </View>
          <View style={styles.rowContainer} >
          {item.type === 'foundation' ? (
            <Text style={[styles.smallText,{color: "#F54237", fontWeight: '600'}]}>{'Ardian foundation'}</Text>
          ): (
            <View style={styles.rowContainer}>
            <Text style={styles.smallText}>By </Text>
            <TouchableOpacity
              onPress={() => onUserItemPress(item.author)}
              >
              <Text style={[styles.smallText,{color: "#F54237", fontWeight: '600'}]}>{item.author.firstName + ' '+ item.author.lastName[0].toUpperCase() + '.'}</Text>
            </TouchableOpacity>
            </View>
          )}
              <Text style={[styles.smallText,{color: AppStyles.colorSet.lightGrey, fontWeight: '600'}]}> | </Text>
              <Text style={styles.smallText}>{item.cityLocation.city}</Text>
          </View>
          <View style={[styles.rowContainer, {marginTop: 5}]} >
            {item.needFund && (
              <View style={[styles.tagStyle, {backgroundColor:AppStyles.colorSet.moneyColor}]}>
                <Text style={styles.tagTextStyle}>{"Money".toUpperCase()}</Text>
              </View>
            )}
            {item.needVolunteer && (
              <View style={[styles.tagStyle, {backgroundColor:AppStyles.colorSet.timeColor}]}>
                <Text style={styles.tagTextStyle}>{"Time".toUpperCase()}</Text>
              </View>
            )}
          </View>
          {displayWaitingApprovalTag && (
            <View style={styles.pendingTagContainer}>
                <Text style={styles.pendingTag}>Waiting for approval</Text>
            </View>
          )}
        </View>
      </View>
      <View style={{width: '90%', marginLeft: '5%'}}>
      <ProgressBarView
        donation={item.donations.fund}
        needfund={item.needFund}
        currency={item.currency}
        fundTarget={item.fundRaisingTarget}
        needvolunteer={item.needVolunteer}
        volunteerTarget={item.volunteerTarget}
        volunteerAmount={item.donations.volunteer}
        endDate={item.endDate}
        disableEndDate={item.disableEndDate}
        replaceEndDateText={item.timeText}
        displayTime={true}
        displayVolunteerProgressBar={item.displayVolunteerProgressBarOnList}
      />
      </View>
    </TouchableOpacity>
  );
}

ProjectFeedItem.propTypes = {
  onPress: PropTypes.func,
  onUserItemPress: PropTypes.func,
  item: PropTypes.object,
};

export default ProjectFeedItem;
