import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const reactionIconSize = Math.floor(AppStyles.WINDOW_WIDTH * 0.09);

const dynamicStyles = new DynamicStyleSheet({
  container: {
    width: Math.floor(AppStyles.WINDOW_WIDTH * 0.97),
    alignSelf: 'center',
    ...ifIphoneX({
      marginVertical: 5,
    },{
      marginVertical: 3,
    }),
    borderRadius: 15,
    width: '90%',
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
    shadowOffset:{  width: 0,  height: 2 },
    shadowColor: AppStyles.colorSet.lightGrey,
    shadowOpacity: 1.0
  },
  rowContainer: {
    flexDirection: 'row'
  },
  topContentContainer:{
    ...ifIphoneX({
      marginBottom: '3%'
    },{
      marginBottom: '1%'
    }),
  },
  imageContainer: {
    ...ifIphoneX({
      width: '30%'
    },{
      width: '28%'
    })
  },
  imageStyle: {
    borderRadius: 10,
    width: '100%',
    height: undefined,aspectRatio: 1,
  },
  itemInfoColumn: {
    flexDirection: 'column',
    flexGrow: 1,
    flex: 1,
    ...ifIphoneX({
      padding: 10,
    },{
      padding: 5,
    })
  },
  projectTitleText: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: '800'
  },
  foundationIcon: {
    marginLeft: 5,
    width: 20,
    height: 20,
    borderRadius: 10,
    alignSelf: 'center'
  },
  smallText: {
    fontSize: AppStyles.fontSet.normal
  },
  tagStyle: {
    ...ifIphoneX({
      marginTop: 5,
    },{
      marginTop: 0,
    }),
    marginRight: 5,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  tagTextStyle: {
    fontSize: AppStyles.fontSet.xsmall,
    color: AppStyles.colorSet.white,
    fontWeight:'800'
  },
  progressBarInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignSelf: 'center'
  },
  deadlineIcon: {
    ...ifIphoneX({
      width: 18,
      height: 18,
    },{
      width: 14,
      height: 14,
    }),
    marginRight: 2,
    alignSelf: 'center',
    tintColor: AppStyles.colorSet.mainTextColor
  },
  deadlineText: {
    fontSize: AppStyles.fontSet.small,
    color: AppStyles.colorSet.black,
    fontWeight:'800'
  },
  progressBarStyle: {
    ...ifIphoneX({
      marginVertical: 10,
    },{
      marginVertical: 7,
    }),
    width: '100%',
    alignSelf: 'center'
  },
  pendingTagContainer: {
    backgroundColor: AppStyles.colorSet.yellow40,
    borderColor: AppStyles.colorSet.yellow,
    borderWidth: 1,
    margin: 5,
    padding: 4,
    borderRadius: 5,
  },
  pendingTag: {
    textAlign: "center",
    color: AppStyles.colorSet.yellow,
    fontWeight: "700",
    fontStyle: "italic"
  }
});

export default dynamicStyles;
