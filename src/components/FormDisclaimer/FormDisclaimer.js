import React, {useState} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert, Touch, TouchableOpacity, TextInput, Image, ScrollView} from 'react-native';
import ClickAndGiveConfig from '../../ClickAndGiveConfig';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import dynamicStyles from './style';

const FormDisclaimer = (props) => {

    const { disclaimerForm  } = props;

    const [modalVisible, setModalVisible] = useState(false);

    const styles = useDynamicStyleSheet(dynamicStyles);

    return (
        <View style={{alignSelf: 'flex-start', marginVertical: 10}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <ScrollView
                    style={styles.scrollViewStyle}>
                    <View style={styles.container}>
                        <TouchableOpacity
                            style={styles.cancelButtonContainer}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}>
                            <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                        </TouchableOpacity>
                        <Text>{ClickAndGiveConfig.formDisclaimer[disclaimerForm]}</Text>
                    </View>
                </ScrollView>
            </Modal>
            <TouchableHighlight
                onPress={() => {
                    setModalVisible(true);
                }}>
                <View style={[styles.rowContainer, {margin: 25}]}>
                    <Image source={AppStyles.iconSet.info} style={{width: 20, height: 20}}/>
                    <Text style={{fontSize: AppStyles.fontSet.middle}}> Disclaimer</Text>
                </View>
            </TouchableHighlight>
        </View>
    );
}

export default FormDisclaimer;
