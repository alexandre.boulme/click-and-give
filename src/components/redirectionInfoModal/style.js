import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
    modalViewContainer: {
        alignSelf: 'center',
        ...ifIphoneX({
            padding: 0,
        },{
            padding: 0,
        }),
        height: "100%",
        backgroundColor: "#FFF",
    },
    scrollViewStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: "#FFF",
        justifyContent: "space-between"
    },
    rowContainer:{
        margin: 20,
        marginTop: 50,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignContent: 'center',
        alignItems: 'center',
        marginVertical: 5
    },
    headlineText: {
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '500',
        color: AppStyles.colorSet.mainTextColor,
        ...ifIphoneX({
            marginVertical: 15
        },{
            marginVertical: 10
        }),
    },
    basicButton: {
        ...ifIphoneX({
            padding: 15,
        },{
            padding: 10,
        }),
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: 10,
        borderColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    buttonText: {
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '400',
        color: AppStyles.colorSet.mainThemeForegroundColor,
        textAlign: 'center'
    },
    cancelButtonContainer: {
        ...ifIphoneX({
            padding: 20,
        },{
            padding: 10,
        }),
    },
    updateButton:{
        margin:20,
        ...ifIphoneX({
            padding: 20,
        },{
            padding: 10,
        }),
        backgroundColor: '#F7375095',
        borderRadius: 30,
        marginBottom: 70
    },
    updateButtonText: {
        fontSize: AppStyles.fontSet.large,
        fontWeight: '600',
        color: AppStyles.colorSet.mainThemeBackgroundColor,
        textAlign: 'center'
    },
    applyButtonContainer: {
        ...ifIphoneX({
            padding: 20,
            paddingLeft: 30,
            paddingRight: 30,
        },{
            padding: 10,
            paddingLeft: 20,
            paddingRight: 20,
        }),
        borderRadius: 30,
        backgroundColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    applyButton : {
        fontSize: AppStyles.fontSet.normal,
        fontWeight: '600',
        color: "#FFF",
        textAlign: 'center'
    },
    resetFiltersButton: {
        marginVertical: 15,
        alignItems: 'center',
        padding: 15,
        backgroundColor: "#FFF",
        borderColor: AppStyles.colorSet.mainThemeForegroundColor,
        borderWidth: 2,
        borderRadius: 10
    },
    resetFiltersButtonText: {
        color: AppStyles.colorSet.mainThemeForegroundColor,
        fontSize: AppStyles.fontSet.middleminus,
        fontWeight: '400'
    },
    containerInactive: {
        borderColor: "#222"
    },
    textInactive: {
        color: "#222"
    },
    modalVisibleContainer: {
        alignSelf: 'center',
        ...ifIphoneX({
            width: 40,
            height: 40,
            marginRight: 15,
            shadowOffset:{  width: 0,  height: 5,  },
        },{
            width: 35,
            height: 35,
            marginRight: 10,
            shadowOffset:{  width: 0,  height: 3,  },
        }),
        backgroundColor: "#FFF",
        shadowColor: "#999",
        shadowOpacity: 1.0,
        borderRadius: 5
    },
    modalVisibleIcon: {
        alignSelf: 'flex-end',
        ...ifIphoneX({
            width: 30,
            height: 30,
            margin: 5,
        },{
            width: 23,
            height: 23,
            margin: 5,
        }),
        tintColor: AppStyles.colorSet.black
    },
    textContainer: {
      backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
      margin: 20,
      borderRadius: 20,
      padding: 10,
    },
    popUpText: {
        color: AppStyles.colorSet.mainThemeBackgroundColor,
        fontSize: AppStyles.fontSet.middle,
        fontWeight: '600',
        marginVertical: 15,
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    popUpTextTitle: {
        color: AppStyles.colorSet.mainTextColor,
        fontSize: AppStyles.fontSet.large,
        fontWeight: '800'
    }
});

export default dynamicStyles;
