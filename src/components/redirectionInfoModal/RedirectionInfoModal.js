import React, {useState} from 'react';
import {
    Modal,
    Text,
    View,
    Alert,
    TouchableOpacity,
    Image,
    ScrollView,
    Linking
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../AppStyles';
import dynamicStyles from './style';
import {APPSTORE_URL} from 'react-native-dotenv';

const RedirectionInfoModal = (props) => {

    const { modalVisible, hideModal, goToPlatform  } = props;
    const styles = useDynamicStyleSheet(dynamicStyles);

    return (
        <View style={{alignSelf: 'flex-start', marginVertical: 0}}>
            <Modal
                transparent={true}
                animationType="none"
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View
                    style={styles.scrollViewStyle}>
                        <View style={styles.rowContainer}>
                            <Text style={styles.popUpTextTitle}>{"Your attention please !"}</Text>
                            <TouchableOpacity
                                style={styles.cancelButtonContainer}
                                onPress={() => {
                                    hideModal();
                                }}>
                                <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                            </TouchableOpacity>
                        </View>
                        <Image source={AppStyles.imageSet.notificationRedirection} style={{width: "80%", height: "30%", alignSelf: 'center'}}/>
                        <View style={styles.textContainer}>
                          <Text style={styles.popUpText}>
                              <Text >{"You are about to be sent to another page to complete your donation. \n\nPlease "}</Text>
                              <Text style={{fontWeight:'800', textDecorationLine:"underline"}}>{"come back to the app"}</Text>
                              <Text>{" to confirm your donation once finalized."}</Text>
                          </Text>
                        </View>
                        <TouchableOpacity
                            style={styles.updateButton}
                            onPress={() => {
                                hideModal();
                                goToPlatform();
                            }}>
                            <Text style={styles.updateButtonText}>OK</Text>
                        </TouchableOpacity>
                </View>
            </Modal>
        </View>
    );
}

export default RedirectionInfoModal;
