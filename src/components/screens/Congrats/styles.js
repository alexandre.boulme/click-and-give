import {DynamicStyleSheet} from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import AppStyles from '../../../AppStyles';

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    margin: 20,
    height: "100%",
    justifyContent: "space-between",
  },
  congratsContentContainer: {
    alignItems: 'center',
    ...ifIphoneX({
      marginTop: '4%'
    },{
      marginTop: '0%'
    }),
    height: "70%"
  },
  congratsImage: {
    width: '70%',
    resizeMode: 'contain',
    padding: 0,
    ...ifIphoneX({
      maxHeight: '45%'
    },{
      maxHeight: '35%'
    })
  },
  title: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.xxlarge,
    fontWeight: '800',
    ...ifIphoneX({
      marginTop: 15
    },{
      marginTop: 7
    })
  },
  subTitle: {
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontSize: AppStyles.fontSet.middleplus,
    textAlign: 'center',
    fontWeight: '700',
    ...ifIphoneX({
      marginVertical: 12
    },{
      marginVertical: 8
    })
  },
  normalText:{
    ...ifIphoneX({
      marginTop: "1%"
    },{
    }),
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.small,
    fontWeight: '700',
    textAlign: 'center'
  },
  buttonText:{
    color: AppStyles.colorSet.mainTextColor,
    textDecorationLine: 'underline',
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '300',
    textAlign: 'center'
  },
  bottomCTA: {
    margin: 10
  },
  modalContainer: {
    backgroundColor: "#FFF",
    borderRadius: 15,
    padding: 20,
    paddingTop: 0,
    marginTop: "50%",
    width: '90%',
    alignSelf: "center"
  },
  rowContainer:{
    flexDirection: 'row',
    justifyContent: "space-between",
    alignContent: 'center',
    alignItems: 'center',
    marginVertical: 5
  },
  cancelButtonContainer: {
    ...ifIphoneX({
      padding: 20,
    },{
      padding: 10,
    }),
  },
  modalVisibleIcon:{
    alignSelf: 'flex-end',
    ...ifIphoneX({
      width: 30,
      height: 30,
      margin: 5,
    },{
      width: 23,
      height: 23,
      margin: 5,
    }),
    tintColor: AppStyles.colorSet.black
  },
  popUpText: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '400',
    marginVertical: 15,
    marginBottom: 30
  },
  popUpTextTitle: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.large,
    fontWeight: '800'
  },
  updateButton:{
    ...ifIphoneX({
      padding: 20,
    },{
      padding: 10,
    }),
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    borderRadius: 30
  },
  updateButtonText: {
    fontSize: AppStyles.fontSet.normal,
    fontWeight: '600',
    color: AppStyles.colorSet.mainThemeBackgroundColor,
    textAlign: 'center'
  },
});

export default dynamicStyles;
