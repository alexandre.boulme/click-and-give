import React, {useState} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity, Modal, Alert, ScrollView,
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import PostProjectOnNewsfeedModal from "../../postProjectOnNewsfeedModal/PostProjectOnNewsfeedModal";
import {NavigationEvents} from "react-navigation";

function Congrats(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {
    user,
    onBottomButtonPress,
    owner,
    message,
    shareProjectCreation,
    modalVisible
  } = props;

  const [showPostModal, setShowPostModal] = useState(modalVisible ? modalVisible : false);

  const textToDisplay = ClickAndGiveConfig.congratsTextList[message];

  const triggerPreFilledPost = () => {
      switch (message) {
          case "createProjectTimeAndMoney":
              setShowPostModal(true);
              break;
          case "createProjectMoney":
              setShowPostModal(true);
              break;
          case "createProjectTime":
              setShowPostModal(true);
              break;
          default:
      }
  };

  return (
    <View style={{flex:1, height: "100%", alignItems: 'center', justifyContent: "center"}}>
      <NavigationEvents onDidFocus={() => triggerPreFilledPost()} />
      <View style={styles.container}>
        <View style={styles.congratsContentContainer}>
            <Image source={AppStyles.imageSet.yayRed} style={styles.congratsImage}/>
            <Text style={styles.title}>{textToDisplay.title}{user.firstName} !</Text>
            <Text style={styles.subTitle}>{textToDisplay.subTitle}</Text>
            <Text style={styles.normalText}>{message == "timeDonation" ? owner.firstName : null}{textToDisplay.text}</Text>
        </View>
        <TouchableOpacity
            onPress={() => onBottomButtonPress(message)}
            style={styles.bottomCTA}>
            <Text style={styles.buttonText}>{textToDisplay.buttonText}</Text>
        </TouchableOpacity>
      </View>
        <Modal
            animationType="slide"
            transparent={true}
            visible={false}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
            <View style={{flex: 1, width: '100%', height: '100%', backgroundColor: "#00000070"}}>
                <View style={[styles.modalContainer,{flex: 1, height: 250}]}>
                    <View style={[styles.rowContainer, {backgroundColor: "#FFF"}]}>
                        <Text style={styles.popUpTextTitle}>{"Dear project leader !"}</Text>
                        <TouchableOpacity
                            style={styles.cancelButtonContainer}
                            onPress={() => {
                                setShowPostModal(false);
                            }}>
                            <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.popUpText}>
                        <Text>{"Do you want to share your project on the news feed ?"}</Text>
                    </Text>
                    <View style={styles.rowContainer}>
                        <TouchableOpacity
                            style={[styles.updateButton,{backgroundColor: AppStyles.colorSet.lightGrey}]}
                            onPress={() => {
                                setShowPostModal(false);
                            }}>
                            <Text style={[styles.updateButtonText,{width: 100}]}>NOT NOW</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.updateButton}
                            onPress={() => {
                                setShowPostModal(false);
                            }}>
                            <Text style={[styles.updateButtonText,{width: 100}]}>SURE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
        <PostProjectOnNewsfeedModal
            modalVisible={showPostModal}
            hideModal={() => setShowPostModal(false)}
            goToPlatform={shareProjectCreation}
        />
    </View>
  );
};

Congrats.propTypes = {
  onBottomButtonPress: PropTypes.func,
  user: PropTypes.object,
  message: PropTypes.string,
  shareProjectCreation: PropTypes.func,
};

export default Congrats;
