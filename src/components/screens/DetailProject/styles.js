import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const commentItemHeight = 80;
const commentBodyPaddingLeft = 8;

const dynamicStyles = new DynamicStyleSheet({
  container:{
    flex: 1,
    backgroundColor: "#FFF"
  },
  coverDarkFilm: {
    flex: 1,
    backgroundColor: "#00000060",

  },
  ardianFoundationLogo: {
    width: 130,
    height: 50,
    borderRadius: 10
  },
  rowContainer: {
    flexDirection: 'row',
  },
  topImage:{
    position: 'absolute',
    top:-90,
    ...ifIphoneX({
      height: 350,
    },{
      height: 250,
    }),
    width:'100%'
  },
  bigSpacer: {
    ...ifIphoneX({
      height: 230
    },{
      height: 130
    })
  },
  containerStyle: {
    minHeight: '80%',
    flexDirection: 'column',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  foundationIcon: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginLeft: 10
  },
  foundationSubText: {
    marginTop: 4,
    color: "#F7375080",
    fontSize: AppStyles.fontSet.normal,
    fontWeight: '700'
  },
  projectDescriptionText: {
    fontSize: AppStyles.fontSet.middle,
    color: AppStyles.colorSet.mainTextColor,
    fontWeight: '400',
    marginVertical: 14
  },
  creatorInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 10
  },
  profilePicture: {
    ...ifIphoneX({
      width: 40,
      height: 40,
    },{
      width: 35,
      height: 35,
    }),
    borderRadius: 30,
    marginRight: 5
  },
  smallRedText: {
    fontSize: AppStyles.fontSet.normal,
    marginLeft: 3,
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontWeight: '600'
  },
  smallDarkText: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.normal
  },
  verticalSeparator:{
    height: '100%',
    width: 1,
    backgroundColor: "#CCC",
    marginLeft: 8,
    marginRight: 8
  },
  textSeparator:{
    fontSize: AppStyles.fontSet.large,
    color: "#CCC"
  },
  locationIcon: {
    tintColor: AppStyles.colorSet.mainThemeForegroundColor,
    width: 25,
    height: 25
  },
  textArdianEngage: {
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: '700',
    color: AppStyles.colorSet.mainThemeForegroundColor
  },
  ardianDescriptionText: {
    fontSize: AppStyles.fontSet.middleminus,
    color: AppStyles.colorSet.mainTextColor,
    marginVertical: 20,
    fontWeight: '300'
  },
  subHeaderDark: {
    fontSize: AppStyles.fontSet.large,
    color: AppStyles.colorSet.mainTextColor,
    fontWeight: '700'
  },
  spacerBottom: {
    ...ifIphoneX({
      height: 120
    },{
      height: 100
    })
  },
  floatingButton: {
    position: 'absolute',
    bottom: '3%',
    width: '30%',
    padding: 25,
    borderRadius: 40,
    backgroundColor: "#222",
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#999",
    shadowOpacity: 1.0,
  },
  floatingButtonText: {
    color: "#FFF",
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '800',
    textAlign: 'center'
  }
});

export default dynamicStyles;
