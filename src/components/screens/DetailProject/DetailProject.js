import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, Image, TouchableOpacity, Alert} from 'react-native';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ProgressBarView from '../../ProjectFeedItem/ProgressBarView';
import FastImage from 'react-native-fast-image';
import Toast from 'react-native-tiny-toast';
import { daysLeftCalculation } from '../../../Core';
import { sendProjectWaitingListEmailNotification } from '../../../Core/project/firebase/donation';
import Hyperlink from "react-native-hyperlink";
import firebase from "@react-native-firebase/app";
import "@react-native-firebase/dynamic-links";
import {buildProjectDeepLink} from "../../../Core/project/firebase/project";

function DetailProject(props) {
  const {
    project,
    onGiveFundButtonPress,
    donationsList,
    onDonatorItemPress,
    user
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);
  const [projectDonations, setProjectDonations] = useState([]);
  const [anonymousDonationCount, setAnonymousDonationCount] = useState(0);
  const displayGiveButtons = daysLeftCalculation(project.endDate) > -700 || project.disableEndDate;

  useEffect(() => {

    if (donationsList) {
      const filteredDonations = donationsList.filter((donation) =>{
        if(donation && donation.projectId == project.id && donation.status == "Confirmed" && !donation.anonymous)
          return donation;
      });

      const anonymousDonations = donationsList.filter((donation) =>{
        if(donation && donation.projectId == project.id && donation.status == "Confirmed" && donation.anonymous)
            return donation;
      });
      console.log(JSON.stringify(filteredDonations));
      console.log(filteredDonations.length);
      console.log(anonymousDonations.length);
      setAnonymousDonationCount(anonymousDonations.length);
      setProjectDonations(filteredDonations);
    }

    if(!project.projectDeeplinkUrl){
      buildProjectDeepLink(project);
    } else {
      console.log("DEEPLINK EXIST");
    }
  }, [donationsList]);

  const onGiveTimeButtonPress = () => {

    if(project.volunteerTarget <= project.donations.volunteer){
      Alert.alert(IMLocalized('Oops'), IMLocalized("The volunteer target has already been reached. Do you want to be on waiting list ? Please reach out to the project leader if you have any question."), [{ text: IMLocalized('OK'), onPress: () => onWaitingListApproval()},{text: "Cancel",style: "cancel"}], {
        cancelable: true,
      });
    } else {
      if(projectDonations){

        const userTimeDonation = projectDonations.filter((donation) =>{
          if(donation.type == 'time' && donation.author.userID == user.userID)
            return donation;
          });

        if(userTimeDonation.length > 0){
          Alert.alert(IMLocalized('Oops'), IMLocalized("You've already subscribed as volunteer for this project."), [{ text: IMLocalized('OK') }], {
            cancelable: false,
          });
        } else {
          onGiveFundButtonPress(project, "time");
        }
      } else {
        onGiveFundButtonPress(project, "time");
      }
    }
  }

  const onWaitingListApproval = () => {
     sendProjectWaitingListEmailNotification(project, user);
     Toast.show(IMLocalized('You\'ve been added to waiting list and received a confirmation email.'), {duration: 2000});
  };

  return (
    <View style={styles.container}>
      <FastImage
        style={styles.topImage}
        source={{uri: project.projectPictureURL }}
        resizeMode={FastImage.resizeMode.cover}
      />
      <ScrollView
        style={styles.coverDarkFilm}
        indicatorStyle={'white'}
        bounces={false}
      >
        <View style={styles.bigSpacer} />
        <View style={styles.containerStyle}>
            <View style={styles.rowContainer}>
              <Text style={[AppStyles.styleSet.darkHeaderText, {width:"85%"}]}>{project.name}</Text>
              {project.type === "foundation" &&(
                <Image source={AppStyles.iconSet.foundation} style={styles.foundationIcon} />
              )}
            </View>
            {project.type === "foundation" ? (
              <Text style={styles.foundationSubText}>Foundation project</Text>
            ) : (
              <Text style={styles.foundationSubText}>Employee project</Text>
            )}
          <Hyperlink
              linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
              linkDefault>
              <Text style={styles.projectDescriptionText}>{project.description}</Text>
          </Hyperlink>
            <View style={styles.creatorInfoContainer}>
            {project.type === "foundation" ? (
              <Image source={AppStyles.imageSet.ardianFoundationLogoW} style={styles.ardianFoundationLogo} />
            ) : (
              <TouchableOpacity
                style={styles.creatorInfoContainer}
                onPress={()=> onDonatorItemPress(project.author)}>
                <FastImage source={{uri: project.author.profilePictureURL}} style={styles.profilePicture}/>
                <Text style={[styles.smallDarkText, {marginLeft: 5}]}>By</Text>
                <Text style={[styles.smallRedText, {}]}>{project.author.firstName + ' ' + project.author.lastName[0].toUpperCase() + '.'}</Text>
              </TouchableOpacity>
            )}
              <View style={styles.verticalSeparator}/>
              <Image source={AppStyles.iconSet.location} style={styles.locationIcon} />
              <Text style={[styles.smallDarkText, {marginLeft: 3}]}>{project.cityLocation.city}</Text>
              {project.foundationMatching > 0 && (
                <View style={{flexDirection: 'row', height: '100%'}}>
                  <View style={styles.verticalSeparator}/>
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={styles.smallRedText}>{project.foundationMatching} for 1</Text>
                    <Text style={styles.smallDarkText}> Matching system</Text>
                  </View>
                </View>
              )}
            </View>

            <View style={{marginVertical: 15}}>
            <ProgressBarView
              needfund={project.needFund}
              fundTarget={project.fundRaisingTarget}
              donation={project.donations.fund}
              currency={project.currency}
              needvolunteer={project.needVolunteer}
              volunteerTarget={project.volunteerTarget}
              volunteerAmount={project.donations.volunteer}
              endDate={project.endDate}
              displayTime={false}
            />
            {project.needFund && project.needVolunteer && (
              <View style={{marginTop: 15}}>
              <ProgressBarView
                needfund={false}
                needvolunteer={project.needVolunteer}
                volunteerTarget={project.volunteerTarget}
                volunteerAmount={project.donations.volunteer}
                endDate={project.endDate}
                displayTime={false}
              />
              </View>
            )}
          </View>
          {(project.needVolunteer || project.type === "foundation") && (
            <View>
            <Text style={styles.textArdianEngage}>{project.type === "foundation"? "Ardian's engagement" : "How can you help ?"}</Text>
            <Hyperlink
                linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
                linkDefault>
              <Text style={styles.ardianDescriptionText}>{project.type === "foundation" ? project.foundationEngagement : project.volunteerInstruction}</Text>
            </Hyperlink>
            </View>
          )}
          {projectDonations && projectDonations.length > 0  && (
            <View style={{flex:1}}>
              <Text style={styles.subHeaderDark}>They already gave</Text>
              <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: "center", }}>
              {
                projectDonations.map((item, i) => (
                  <TouchableOpacity
                    onPress={()=> onDonatorItemPress(item.author)}
                    style={{marginHorizontal: 5, alignItems: "center"}}
                  >
                    <FastImage
                      style={{margin: 10, height: 40, width: 40, borderRadius: 20}}
                      source={{uri: item.author.profilePictureURL }}
                      resizeMode="cover"
                    />
                    <Text>{item.author.firstName} {item.author.lastName[0].toUpperCase()}.</Text>
                  </TouchableOpacity>

                ))
              }
              {anonymousDonationCount > 0 && (
                  <View
                      style={{marginHorizontal: 5, alignItems: "center"}}>
                   <View
                     style={{margin: 10, height: 40, width: 40, borderRadius: 20, backgroundColor: "#F73750", alignContent: "center", justifyContent:"center"}}>
                       <Text style={{fontSize: AppStyles.fontSet.large, fontWeight: '700', color: "#FFF", alignSelf: "center"}}>{anonymousDonationCount}</Text>
                   </View>
                   <Text>Anonymous</Text>
                  </View>
              )}
              </View>
            </View>
          )}
          <View style={styles.spacerBottom} />
      </View>
    </ScrollView>
    {project.needVolunteer && displayGiveButtons && (
      <TouchableOpacity
        style={[styles.floatingButton, {backgroundColor: AppStyles.colorSet.timeColor}, project.needFund ? {left: '6%', width:'42%'} : {width: "90%", alignSelf: 'center'}]}
        onPress={() => {
            onGiveTimeButtonPress()
        }}
      >
        <Text style={styles.floatingButtonText}>Give Time</Text>
      </TouchableOpacity>
    )}
    {project.needFund && displayGiveButtons && (
      <TouchableOpacity
        style={[styles.floatingButton, {backgroundColor: AppStyles.colorSet.moneyColor}, project.needVolunteer ? {right: '6%', width:'42%'} : {width: "90%", alignSelf: 'center'}]}
        onPress={() => {
            onGiveFundButtonPress(project, "money")
        }}
      >
        <Text style={styles.floatingButtonText}>Give Money</Text>
      </TouchableOpacity>
    )}
  </View>
  );
}

DetailProject.propTypes = {
  item: PropTypes.object,
};

export default DetailProject;
