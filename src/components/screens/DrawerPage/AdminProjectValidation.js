import React, { useRef, useState, useEffect } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Modal,
  TextInput,
  TouchableHighlight
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import { TNEmptyStateView } from '../../../Core/truly-native';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ProjectFeedItem from '../../ProjectFeedItem/ProjectFeedItem';
import { updateProject, sendProjectValidationEmailNotification } from '../../../Core/project/firebase/project';

function AdminProjectValidation(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {
    onFeedUserItemPress,
    onFeedItemPress,
    feed,
    loading
  } = props;

  const [feedFiltered, setFeedFiltered] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const [matching, setMatching] = useState(1);
  const [matchingProject, setMatchingProject] = useState(1);

  useEffect(() => {
    if (feed) {
      setFeedFiltered(filterProject(feed));
    }
  }, [feed]);

  const filterProject = feed => {
    const filteredProject = feed.filter(project => (!project.approved && !project.refused) || (!project.refused && !project.matchingRefused && project.requestMatching && !(project.foundationMatching > 0)));
    return filteredProject;
  };

  const approveProject = project => {
    project.approved = true;
    updateProject(project.id, project);

    sendProjectValidationEmailNotification(project);
  };

  const refuseProject = project => {
    project.refused = true;
    updateProject(project.id, project);
  };

  const showModalMatching = project => {
    setModalVisible(true);
    setMatching(1);
    setMatchingProject(project);
  };

  const approveProjectMatching = () => {
    const project = matchingProject;
    project.foundationMatching = matching;
    updateProject(project.id, project);
  };

  const refuseProjectMatching = project => {
    project.matchingRefused = true;
    updateProject(project.id, project);
  };

  const renderItem = ({ item, index }) => {
    let shouldUpdate = false;
    if (item.shouldUpdate) {
      shouldUpdate = item.shouldUpdate;
    }
    return (
      <View
        style={styles.itemContainer}>
        <ProjectFeedItem
          key={index + ''}
          onUserItemPress={onFeedUserItemPress}
          item={item}
          onItemPress={onFeedItemPress}
        />
        <View>
          {!item.approved && (
            <View
              style={[styles.rowContainer, styles.buttonRow]}>
              <TouchableOpacity
                style={[styles.buttonShape,]}
                onPress={() => approveProject(item)}>
                <Text style={styles.buttonText}>Approve project</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.buttonShape,]}
                onPress={() => refuseProject(item)}>
                <Text style={styles.buttonText}>Refuse project</Text>
              </TouchableOpacity>
            </View>
          )}
          {item.requestMatching && (
            <View
              style={[styles.rowContainer, styles.buttonRow]}>
              <TouchableOpacity
                style={[styles.buttonShape,]}
                onPress={() => showModalMatching(item)}>
                <Text style={styles.buttonText}>Approve matching</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.buttonShape,]}
                onPress={() => refuseProjectMatching(item)}>
                <Text style={styles.buttonText}>Refuse matching</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  };

  const renderEmptyComponent = () => {
    if (!feed) {
      return null;
    }
    const emptyStateConfig = {
      title: IMLocalized("No validation work today !"),
      description: IMLocalized("There's no project currently waiting for approval."),
    };

    return (
      <TNEmptyStateView
        style={styles.emptyStateView}
        emptyStateConfig={emptyStateConfig}
        appStyles={AppStyles}
      />
    );
  }

  if (loading) {
    return (
      <View style={styles.feedContainer}>
        <ActivityIndicator style={{ marginTop: 50 }} size="small" />
      </View>
    );
  }

  return (
        <View
          style={[{flex:1}]}>
          <Text style={styles.headerText} >Projects to approve</Text>
          <FlatList
            scrollEventThrottle={16}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={renderEmptyComponent}
            data={feedFiltered}
            renderItem={renderItem}
            onEndReachedThreshold={0.5}
          />
          <Modal
           animationType="slide"
           transparent={true}
           visible={modalVisible}
           onRequestClose={() => {
             Alert.alert("Modal has been closed.");
           }}
         >
         <View style={{backgroundColor: 'rgba(0,0,0,0.5)', height: '150%'}}>
           <View style={{margin: 20, marginTop: '60%', height: '50%'}}>
             <View style={{backgroundColor: "#FFF", borderRadius: 20}}>
               <View style={{flexDirection: 'row'}}>
                 <Text style={styles.headerText}>Choose matching value</Text>
                 <TouchableHighlight
                   style={{justifyContent: 'center', marginLeft: 5}}
                   onPress={() => {
                     setModalVisible(!modalVisible);
                   }}>
                   <Image source={AppStyles.iconSet.close} style={{width: 35, height: 35}}/>
                 </TouchableHighlight>
               </View>
               <TextInput
                 style={styles.InputContainer}
                 defaultValue="1"
                 onChangeText={text => setMatching(text.replace(/[^0-9]/g, ''))}
                 keyboardType='numeric'
                 underlineColorAndroid="transparent"
               />
               <TouchableOpacity
                 style={[styles.buttonShape,{alignSelf: 'center', marginBottom: 20}]}
                 onPress={() => {
                   approveProjectMatching(matching);
                   setModalVisible(false);
                 }}>
                 <Text style={styles.buttonText}>Validate matching</Text>
               </TouchableOpacity>
             </View>
           </View>
           </View>
         </Modal>
        </View>
  );
}

AdminProjectValidation.propTypes = {
  onBottomButtonPress: PropTypes.func,
  user: PropTypes.object,
  message: PropTypes.string,
};

export default AdminProjectValidation;
