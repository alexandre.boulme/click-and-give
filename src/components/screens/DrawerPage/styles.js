import { Dimensions } from 'react-native';
import {DynamicStyleSheet} from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import AppStyles from '../../../AppStyles';

const { width } = Dimensions.get('window');
const squareSize = width * 0.40;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    margin: 20,
  },
  headerText: {
    marginVertical: 15,
    marginLeft: 20,
    fontSize: AppStyles.fontSet.xlarge,
    fontWeight: '800'
  },
  itemContainer:{
    marginLeft: 10,
    marginRight: 10,
    marginVertical: 15,
    borderRadius: 10,
    backgroundColor: "#FFF",
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent: 'center',
    marginVertical: 0
  },
  buttonRow:{
    backgroundColor: AppStyles.colorSet.white,
    paddingVertical: 10,
    borderRadius: 10,
  },
  buttonShape: {
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    width: '45%',
    marginHorizontal: 10,
    borderRadius: 10,
  },
  buttonText: {
    color: AppStyles.colorSet.white,
    fontSize: AppStyles.fontSet.middleminus,
    fontWeight: '500'
  },
  squareContainer:{
    backgroundColor: AppStyles.colorSet.white,
    height:squareSize,
    width: squareSize,
    margin: squareSize * 0.1
  },
  image:{
    height:squareSize,
    width: squareSize,
  },
  title: {
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontSize: AppStyles.fontSet.xxlarge,
    textAlign:'center',
    fontWeight: '300',
    ...ifIphoneX({
      marginTop: 15
    },{
      marginTop: 10
    })
  },
  aboutText: {
    color: AppStyles.colorSet.black,
    fontSize: AppStyles.fontSet.middle,
    textAlign: 'justify',
    fontWeight: '300',
    ...ifIphoneX({
      marginVertical: 25
    },{
      marginVertical: 20
    })
  },
  aimView: {
    ...ifIphoneX({
      marginVertical: 8
    },{
      marginVertical: 6
    })
  },
  aimTitleText: {
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '500',
  },
  aimDescriptionText: {
    color: AppStyles.colorSet.black,
    fontSize: AppStyles.fontSet.middleminus,
    fontWeight: '300',
  },
  InputContainer:{
    borderColor: "#222",
    color: "#222",
    fontSize: AppStyles.fontSet.large,
    alignSelf: 'center',
    marginLeft: 40,
    marginRight: 40,
    marginVertical: 20
  },
  numberSection:{
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    ...ifIphoneX({
      marginTop: 20
    },{
      marginTop: 15
    })
  }
});

export default dynamicStyles;
