import React, { useRef } from 'react';
import {
  View,
  Image,
  Text,
  ScrollView
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';

function AboutArdianFoundation(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {} = props;

  const aboutArdianFoundation = ClickAndGiveConfig.aboutArdianFoundation;

  return (
        <View>
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.title}>MISSION</Text>
            <Text style={styles.aboutText}>{aboutArdianFoundation.about}</Text>
            <View>
              {aboutArdianFoundation.aims.map((item, i) => (
                <Text style={styles.aimView}>
                  <Text style={styles.aimTitleText}>{item.title}</Text>
                  <Text style={styles.aimDescriptionText}> {item.description}</Text>
                </Text>
              ))}
            </View>
          </View>
          <View style={styles.numberSection}>
            <View style={styles.container}>
              <Text style={[styles.title, {color: AppStyles.colorSet.white}]}>2018/2019 KEY FIGURES</Text>
              <View style={styles.rowContainer}>
                <View style={styles.squareContainer}>
                  <Image source={AppStyles.imageSet.foundationBeneficiaries} style={styles.image}/>
                </View>
                <View style={styles.squareContainer}>
                  <Image source={AppStyles.imageSet.foundationBudget} style={styles.image}/>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.squareContainer}>
                  <Image source={AppStyles.imageSet.foundationEmployee} style={styles.image}/>
                </View>
                <View style={styles.squareContainer}>
                    <Image source={AppStyles.imageSet.foundationProjects} style={styles.image}/>
                </View>
              </View>
              <View style={styles.rowContainer}>
                <View style={styles.squareContainer}>
                  <Image source={AppStyles.imageSet.foundationTimeDedicated} style={styles.image}/>
                </View>
                <View style={styles.squareContainer}>
                  <Image source={AppStyles.imageSet.foundationCountries} style={styles.image}/>
                </View>
              </View>
            </View>
          </View>
          </ScrollView>
        </View>
  );
}

AboutArdianFoundation.propTypes = {
  onBottomButtonPress: PropTypes.func,
  user: PropTypes.object,
  message: PropTypes.string,
};

export default AboutArdianFoundation;
