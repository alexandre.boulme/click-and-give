import React from 'react';
import {Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import {IMIconButton} from "../../../Core/truly-native";
import appStyles from '../../../AppStyles';
import AppStyles from "../../../AppStyles";
import Hyperlink from "react-native-hyperlink";

function CommentItem(props) {
  const {item, onPressComment, showEdit} = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
        <View style={styles.commentItemContainer}>
            <View style={styles.commentItemImageContainer}>
                <FastImage
                    style={styles.commentItemImage}
                    source={{
                        uri: item.profilePictureURL,
                    }}
                />
            </View>
            <View style={styles.commentItemBodyContainer}>
                <View style={styles.commentItemBodyRadiusContainer}>
                    <Text style={styles.commentItemBodyUserName}>{item.firstName} {item.lastName}</Text>
                    <Hyperlink
                        linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
                        linkDefault>
                        <Text style={styles.commentItemBodySubtitle}>{item.commentText}</Text>
                    </Hyperlink>
                </View>
            </View>
            {showEdit && (
                <View style={{marginTop:"4%"}}>
                    <IMIconButton
                        source={appStyles.iconSet.more}
                        tintColor={appStyles.colorSet.grey12}
                        onPress={() => onPressComment(item)}
                        marginRight={15}
                        width={20}
                        height={20}
                    />
                </View>
            )}
        </View>
  );
}

CommentItem.propTypes = {
  item: PropTypes.object,
};

export default CommentItem;
