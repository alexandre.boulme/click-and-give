import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';

const commentItemHeight = 80;
const commentBodyPaddingLeft = 8;

const dynamicStyles = new DynamicStyleSheet({
  detailPostContainer: {
    flex: 1,
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
  },
  commentItemContainer: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginVertical: 2,
  },
  commentItemImageContainer: {
    flex: 1,
    alignItems: 'center',
  },
  commentItemImage: {
    height: 36,
    width: 36,
    borderRadius: 18,
    marginVertical: 5,
    marginLeft: 5,
  },
  commentItemBodyContainer: {
    flex: 5,
  },
  commentItemBodyRadiusContainer: {
    width: Math.floor(AppStyles.WINDOW_WIDTH * 0.71),
    padding: 5,
    borderRadius: 20,
    marginVertical: 5,
    backgroundColor: AppStyles.colorSet.whiteSmoke,
  },
  commentItemBodyTitle: {
    fontSize: 12,
    fontWeight: '500',
    color: AppStyles.colorSet.mainTextColor,
    paddingVertical: 3,
    paddingLeft: commentBodyPaddingLeft,
    lineHeight: 12,
  },
  commentItemBodyUserName: {
    fontSize: AppStyles.fontSet.normal,
    color: AppStyles.colorSet.mainTextColor,
    paddingVertical: 3,
    fontWeight: '800',
    paddingLeft: commentBodyPaddingLeft,
  },
  commentItemBodySubtitle: {
    fontSize: AppStyles.fontSet.normal,
    color: AppStyles.colorSet.mainTextColor,
    paddingVertical: 3,
    fontWeight: '300',
    paddingLeft: commentBodyPaddingLeft,
  },
  commentInputContainer: {
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
    flexDirection: 'row',
    width: '100%',
    height: '12%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderTopColor: AppStyles.colorSet.whiteSmoke
  },
  commentTextInputContainer: {
    flex: 6,
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
    color: AppStyles.colorSet.mainTextColor,
    height: '100%',
    width: '90%',
    marginLeft: 8,
    marginRight: 8,
    justifyContent: 'center',
  },
  commentTextInput: {
    padding: 10,
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.middle,
  },
  commentInputIconContainer: {
    flex: 0.7,
    justifyContent: 'center',
    marginLeft: 3,
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor
  },
  commentInputIcon: {
    height: 30,
    width: 30,
    tintColor: AppStyles.colorSet.mainThemeForegroundColor,
  },
  placeholderTextColor: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: AppStyles.fontSet.large
  },
});

export default dynamicStyles;
