import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, ActivityIndicator, View} from 'react-native';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import FeedItem from '../../FeedItem/FeedItem';
import CommentItem from './CommentItem';
import CommentInput from './CommentInput';
import TNMediaViewerModal from '../../../Core/truly-native/TNMediaViewerModal';
import dynamicStyles from './styles';
import {IMLocalized} from "../../../Core/localization/IMLocalization";
import ActionSheet from "react-native-actionsheet";
import EditCommentModal from "../../editCommentModal/EditCommentModal";

function DetailPost(props) {
  const {
    feedItem,
    feedItems,
    commentItems,
    onCommentSend,
    scrollViewRef,
    onMediaPress,
    onReaction,
    userLikesList,
    shouldUpdate,
    onMediaClose,
    isMediaViewerOpen,
    selectedMediaIndex,
    onFeedUserItemPress,
    onSharePost,
    onDeletePost,
    onUserReport,
    user,
    commentsLoading,
    commentDialogRef,
    onCommentPress,
    onCommentItemPress,
    onCommentDialogDone,
    showEditCommentModal,
    selectedComment,
    hideEditCommentModal
  } = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={{flex: 1}}>
      <KeyboardAwareView
        animated={true}
        style={styles.detailPostContainer}>
        <ScrollView ref={scrollViewRef}>
          <FeedItem
            item={feedItem}
            onUserItemPress={onFeedUserItemPress}
            onCommentPress={onCommentPress}
            onMediaPress={onMediaPress}
            onReaction={onReaction}
            shouldUpdate={shouldUpdate}
            onSharePost={onSharePost}
            onDeletePost={onDeletePost}
            onUserReport={onUserReport}
            user={user}
            detailPage={true}
            userLikesList={userLikesList}
          />
          {commentsLoading ? (
            <ActivityIndicator style={{marginVertical: 7}} size="small" />
          ) : (
            commentItems.map(comment => <CommentItem item={comment} onPressComment={onCommentItemPress} showEdit={comment.authorID == user.id || user.role == "admin"} />)
          )}
        </ScrollView>
        <CommentInput onCommentSend={onCommentSend} />
        <TNMediaViewerModal
          mediaItems={feedItems}
          isModalOpen={isMediaViewerOpen}
          onClosed={onMediaClose}
          selectedMediaIndex={selectedMediaIndex}
        />
        <ActionSheet
            ref={commentDialogRef}
            title={IMLocalized('Comment')}
            options={[IMLocalized('Edit comment'), IMLocalized('Delete comment'), IMLocalized('Cancel')]}
            destructiveButtonIndex={1}
            cancelButtonIndex={2}
            onPress={onCommentDialogDone}
        />
        <EditCommentModal
            visible={showEditCommentModal}
            comment={selectedComment}
            hideModal={hideEditCommentModal}
        />
      </KeyboardAwareView>
    </View>
  );
}

DetailPost.propTypes = {
  item: PropTypes.object,
  scrollViewRef: PropTypes.any,
  onMediaPress: PropTypes.func,
  onOtherReaction: PropTypes.func,
  onReaction: PropTypes.func,
  onFeedUserItemPress: PropTypes.func,
  onMediaClose: PropTypes.func,
  shouldUpdate: PropTypes.bool,
  feedItems: PropTypes.array,
  isMediaViewerOpen: PropTypes.bool,
  selectedMediaIndex: PropTypes.number,
};

export default DetailPost;
