import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Platform
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ActionSheet from 'react-native-actionsheet';
import FastImage from 'react-native-fast-image';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import { TNStoryItem } from '../../../Core/truly-native';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import { IMLocalized } from '../../../Core/localization/IMLocalization';

function CreatePost(props) {
  const {
    onPostDidChange,
    onSetMedia,
    user,
    postToEdit,
    inputRef,
  } = props;
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [media, setMedia] = useState(postToEdit ? postToEdit.postMedia :[]);
  const [mediaSources, setMediaSources] = useState([]);
  const [value, setValue] = useState(postToEdit ? postToEdit.postText : '');
  const [displayAddPhoto, setDisplayAddPhoto] = useState(true);
  const [isCameralContainer, setIsCameralContainer] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const photoUploadDialogRef = useRef();
  const removePhotoDialogRef = useRef();

  const onChangeText = value => {
    const Post = {
      postText: value,
      commentCount: 0,
      reactionsCount: 0,
    };

    setValue(value);
    onPostDidChange(Post);
  };

  const onPressAddPhotoBtn = () => {
    const options = {
      title: IMLocalized('Select photo'),
      cancelButtonTitle: IMLocalized('Cancel'),
      takePhotoButtonTitle: IMLocalized('Take Photo'),
      chooseFromLibraryButtonTitle: IMLocalized('Choose from Library'),
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const newPhotos = [];
        const imageTab = [response];
        const sources = imageTab.map(image => {
          const mime = 'image';
          const filename = image.fileName;
          const uploadUri =
              Platform.OS === 'ios'
                  ? image.uri.replace('file://', '')
                  : image.uri;
          newPhotos.push({ source: image.uri, mime });

          return { filename, uploadUri, mime };
        });
        setMedia([...media, ...newPhotos]);
        setMediaSources([...mediaSources, ...sources]);
        onSetMedia([...mediaSources, ...sources]);
        setDisplayAddPhoto(false);
      }
    });
  };

  const onRemovePhotoDialogDone = index => {
    if (index === 0) {
      removePhoto();
    } else {
      setSelectedIndex(null);
    }
  };

  const onMediaPress = async index => {
    await setSelectedIndex(index);
    removePhotoDialogRef.current.show();
  };

  const removePhoto = async () => {
    const slicedMedia = [...media];
    const slicedMediaSources = [...mediaSources];
    await slicedMedia.splice(selectedIndex, 1);
    await slicedMediaSources.splice(selectedIndex, 1);
    setMedia([...slicedMedia]);
    setMediaSources([...slicedMediaSources]);
    onSetMedia([...slicedMediaSources]);
    setDisplayAddPhoto(true);
  };

  const onTextFocus = () => {
    setIsCameralContainer(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.topContainer}>
        <View style={styles.headerContainer}>
          <TNStoryItem
            item={user}
            appStyles={AppStyles}
          />
        </View>
        <View style={styles.postInputContainer}>
          <TextInput
            ref={inputRef}
            style={styles.postInput}
            onChangeText={onChangeText}
            placeholder="What's up?"
            value={value}
            multiline={true}
            returnKeyType = {"default"}
            onFocus={onTextFocus}
          />
        </View>
      </View>
      <View style={{flex:1,}}>
        {media.map((singleMedia, index) => {
          const { source, mime, url } = singleMedia;

          if (mime.startsWith('image')) {
            return (
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => onMediaPress(index)}
                style={{width: '80%', height: '70%',alignSelf: 'flex-end', marginRight: '6%'}}>
                <FastImage
                  style={[styles.imageItem,{borderRadius: 10}] }
                  source={url ?  { uri: url } : { uri: source }}
                />
              </TouchableOpacity>
            );
          }
        })}
      </View>
      {media.length == 0 && (
        <View style={[styles.bottomContainer]}>
          <View style={styles.postImageAndLocationContainer}>
            <TouchableOpacity
                  onPress={onPressAddPhotoBtn}
                  style={[styles.addPhotoContainer]}>
                  <Image
                    style={styles.addImageIcon}
                    source={AppStyles.iconSet.cameraFilled}
                  />
                  <Text style={styles.addPhotoText}>Add a photo</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
      <ActionSheet
        ref={removePhotoDialogRef}
        options={[IMLocalized('Remove'), IMLocalized('Cancel')]}
        destructiveButtonIndex={0}
        cancelButtonIndex={1}
        onPress={onRemovePhotoDialogDone}
      />
    </View>
  );
}

CreatePost.propTypes = {
  user: PropTypes.object,
  onPostDidChange: PropTypes.func,
  onSetMedia: PropTypes.func,
  onLocationDidChange: PropTypes.func,
  blurInput: PropTypes.func,
  inputRef: PropTypes.any,
};

export default CreatePost;
