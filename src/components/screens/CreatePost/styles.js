import {DynamicStyleSheet} from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import AppStyles from '../../../AppStyles';

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor,
  },
  topContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  titleContainer: {
    marginTop: 19,
  },
  title: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: 15,
    fontWeight: '600',
  },
  subtitle: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: 10,
  },
  postInputContainer: {
    flex: 6,
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  postInput: {
    height: '100%',
    width: '90%',
    fontSize: AppStyles.fontSet.middleplus,
    color: AppStyles.colorSet.mainTextColor,
  },
  bottomContainer: {
    flex: 0.5,
    justifyContent: 'flex-end',
  },
  blankBottom: {
    ...ifIphoneX(
      {
        flex: 1.1,
      },
      {
        flex: 1.4,
      },
    ),
  },
  postImageAndLocationContainer: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    backgroundColor: AppStyles.colorSet.backgroundColor,
  },
  imagesContainer: {
    width: '100%',
    marginBottom: 23,
  },
  imageItemcontainer: {
    flexDirection: 'row',
    margin: 3,
    marginTop: 5,
    borderRadius: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    overflow: 'hidden',
  },
  addPhotoContainer: {
    padding: 15,
    flexDirection: 'row',
    margin: 3,
    marginTop: 5,
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'white',
    overflow: 'hidden',
  },
  imageItem: {
    width: '100%',
    height: '100%',
  },
  addImageIcon: {
    width: 30,
    height: 30,
    tintColor: AppStyles.colorSet.mainThemeForegroundColor
  },
  addPhotoText: {
    marginLeft: 5,
    fontSize: AppStyles.fontSet.middleminus,
    color: AppStyles.colorSet.mainTextColor,
    fontWeight: '300'
  },
  addTitleAndlocationIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  addTitleContainer: {
    flex: 5.8,
    justifyContent: 'center',
  },
  addTitle: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: 13,
    padding: 8,
  },
  iconsContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  iconContainer: {
    height: 50,
    width: 50,
    marginHorizontal: 2,
  },
  icon: {
    height: 23,
    width: 23,
  },

  cameraFocusTintColor: {
    tintColor: AppStyles.colorSet.mainThemeForegroundColor,
  },
  cameraUnfocusTintColor: {
    tintColor: AppStyles.colorSet.mainTextColor,
  },
  pinpointTintColor: {tintColor: AppStyles.colorSet.mainTextColor},
});

export default dynamicStyles;
