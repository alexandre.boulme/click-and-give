import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Modal,
  Alert,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import PaymentManager from '../../../Core/payment/PaymentManager';
import ProjectLightFeedItem from '../../ProjectLightFeedItem/ProjectLightFeedItem';
import FastImage from 'react-native-fast-image';
import {IMLocalized} from '../../../Core/localization/IMLocalization';
import TNActivityIndicator from '../../../Core/truly-native/TNActivityIndicator';
import Toast from 'react-native-tiny-toast';
import numeral from 'numeral';
import {ALIZEE_PLATFORM_URL} from 'react-native-dotenv';
import {addDonation} from '../../../Core/project/firebase/donation';
import FormDisclaimer from '../../FormDisclaimer/FormDisclaimer';
import {firebaseAnalytics} from '../../../Core/firebase';
import stripe from 'tipsi-stripe';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import RedirectionInfoModal from '../../redirectionInfoModal/RedirectionInfoModal';

function GiveFund(props) {
  const {
    project,
    donationAmount,
    currency,
    paymentProcess,
    foundationDonation,
    volunteerDonation,
    onVolunteerDonationValidationButton,
    goToDonationValidationButton,
    goToWireTranfertPage,
    sendNotificationChatMessage,
    onMoneyDonationValidationButton,
    user,
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles);
  const volunteerDonationHours = ClickAndGiveConfig.projectVolunteerFrequency;
  const currencyRate = currency
    ? ClickAndGiveConfig.currencyExchangeRates[currency.code]
    : 1;

  const [anonymousDonation, setAnonymousDonation] = useState(false);
  const [cardPayment, setCardPayment] = useState(false);
  const [cardNumbers, setCardNumbers] = useState('');
  const [cardNumbersDisplayed, setCardNumbersDisplayed] = useState('');
  const [cardMonthExpiration, setCardMonthExpiration] = useState('');
  const [cardMonthExpirationValid, setCardMonthExpirationValid] = useState(
    true,
  );
  const [cardYearExpiration, setCardYearExpiration] = useState('');
  const [cardYearExpirationValid, setCardYearExpirationValid] = useState(true);
  const [cardCVC, setcardCVC] = useState('');
  const [redirectionInfoVisible, setRedirectionInfoVisible] = useState(false);
  const [encouragementMessage, setEncouragementMessage] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [webViewUrl, setWebViewUrl] = useState('');
  const [loading, setLoading] = useState(false);

  const [payUsingAlizee, setPayUsingAlizee] = useState(false);

  const handlingPayment = async paymentMethod => {
    this.PaymentManager = new PaymentManager();

    switch (paymentMethod) {
      case 'card':
        performStripePayment(this.PaymentManager);
        break;
      case 'paypal':
        setCardPayment(false);
        triggerPaypalPayment(this.PaymentManager);
        break;
      case 'applepay':
        Toast.show('An error occured, your payment has been canceled', {
          duration: 2000,
        });
        break;
      default:
    }
  };

  const performStripePayment = async paymentManager => {
    const cardInfo = checkCardInfos();
    if (cardInfo != null) {
      setLoading(true);
      const token = await paymentManager
        .createStripeToken(cardInfo)
        .catch(error => {
          setLoading(false);
          Alert.alert(
            IMLocalized('Error'),
            IMLocalized(error.message),
            [
              {
                text: IMLocalized('OK'),
              },
            ],
            {
              cancelable: false,
            },
          );
        });

      console.log('Card token : ', token);

      const projectName = foundationDonation
        ? 'Foundation donation'
        : project.name;
      const description =
        'Donation to : ' +
        projectName +
        ' | By : ' +
        user.firstName +
        ' ' +
        user.lastName +
        ' - ' +
        user.email;

      if (token) {
        const response = await paymentManager
          .doStripePayment(
            donationAmount,
            currency,
            token,
            description,
            user.email,
          )
          .catch(error => {
            console.log('Error : ', error);
            setLoading(false);
            Alert.alert(
              IMLocalized('Error'),
              error.message,
              [
                {
                  text: IMLocalized('OK'),
                },
              ],
              {
                cancelable: false,
              },
            );
          });

        console.log('Stripe payment : ', response);

        if (
          response &&
          response.type != 'StripeInvalidRequestError' &&
          response.type != 'StripeCardError'
        ) {
          setLoading(false);
          console.log('Stripe response : ' + JSON.stringify(response));
          onDonationSuccess({
            type: 'stripe',
            reference: {
              paymentId: response.id,
            },
          });
        } else {
          setLoading(false);
          Alert.alert(
            IMLocalized('Error'),
            response.raw.message,
            [
              {
                text: IMLocalized('OK'),
              },
            ],
            {
              cancelable: false,
            },
          );
        }
      }
    }
  };

  const triggerPaypalPayment = async paymentManager => {
    setLoading(true);

    const projectName = foundationDonation
      ? 'Foundation donation'
      : project.name;
    const description =
      'Donation to : ' +
      projectName +
      ' | By : ' +
      user.firstName +
      ' ' +
      user.lastName +
      ' - ' +
      user.email;

    const redirectPaypalUrl = await paymentManager
      .initiatePaypalPayment(donationAmount, currency, description)
      .catch(error => {
        setLoading(false);
        Alert.alert(
          IMLocalized('Error'),
          error.message,
          [
            {
              text: IMLocalized('OK'),
            },
          ],
          {
            cancelable: false,
          },
        );
      });

    console.log(JSON.stringify(redirectPaypalUrl));

    if (redirectPaypalUrl) {
      setWebViewUrl(redirectPaypalUrl.links[1].href);
      setLoading(false);
      setShowModal(true);
    }
  };

  const checkCardInfos = () => {
    if ((cardNumbers.length != 16) & (cardNumbers.length != 15)) {
      Alert.alert(
        IMLocalized('Error'),
        IMLocalized('Please enter valid card number.'),
        [
          {
            text: IMLocalized('OK'),
          },
        ],
        {
          cancelable: false,
        },
      );
    } else if (!cardMonthExpirationValid) {
      Alert.alert(
        IMLocalized('Error'),
        IMLocalized('Please enter valid card expiration date.'),
        [
          {
            text: IMLocalized('OK'),
          },
        ],
        {
          cancelable: false,
        },
      );
    } else if (!cardYearExpirationValid) {
      Alert.alert(
        IMLocalized('Error'),
        IMLocalized('Please enter valid card expiration date.'),
        [
          {
            text: IMLocalized('OK'),
          },
        ],
        {
          cancelable: false,
        },
      );
    } else if ((cardCVC.length != 3) & (cardCVC.length != 4)) {
      Alert.alert(
        IMLocalized('Error'),
        IMLocalized('Please enter valid card CVC.'),
        [
          {
            text: IMLocalized('OK'),
          },
        ],
        {
          cancelable: false,
        },
      );
    } else {
      return {
        number: cardNumbers,
        expMonth: parseInt(cardMonthExpiration, 10),
        expYear: parseInt(cardYearExpiration, 10),
        cvc: cardCVC,
        name: user.firstName + ' ' + user.lastName,
      };
    }
  };

  const buildAlizeePreFilledUrl = () => {
    return ALIZEE_PLATFORM_URL;

  };

  const onVolunteerDonation = () => {
    onDonationSuccess();
  };

  const onDonationSuccess = paymentInfos => {
    console.log('Donation success');

    var donationDetails = {
      paymentInfos: paymentInfos ? paymentInfos : '',
      foundationDonation: foundationDonation,
      type: !foundationDonation && volunteerDonation ? 'time' : 'money',
      projectId: foundationDonation ? null : project.id,
      projectName: foundationDonation ? 'The Ardian Foundation' : project.name,
      projectAuthor: foundationDonation ? null : project.author,
      anonymous: anonymousDonation,
      amount: donationAmount,
      currency: currency,
      status:
        (!foundationDonation && volunteerDonation) || paymentInfos != null
          ? 'Confirmed'
          : 'Pending',
    };

    console.log('Donation detail : ', donationDetails);



    var volunteerHoursDonation = 0;

    if(!foundationDonation){
      volunteerHoursDonation = volunteerDonationHours.filter(item => {
       if (item.frequency == project.volunteerFrequency) {
         return item;
       }
     });
    }

    console.log('After frequency : ', volunteerHoursDonation);

    const amountEuro = donationDetails.amount * currencyRate;

    console.log('Amount euro : ', amountEuro);

    if (volunteerDonation && !encouragementMessage.trim() == '') {
      sendNotificationChatMessage(
        project.type == 'foundation' || foundationDonation
          ? ''
          : project.author.id,
        encouragementMessage,
      );

      firebaseAnalytics.sendEvent('time_donation', {
        status: 'Confirmed',
        action: 'creation',
        projectType: project.type,
        projectSupportArdianFoundation: project.projectSupportArdianFoundation
          ? 'Yes'
          : 'No',
        projectLeader: project.author.firstName + ' ' + project.author.lastName,
        projectTarget: project.fundRaisingTarget,
        projectHourDonation: volunteerHoursDonation[0].hours,
        type: donationDetails.type,
        project: donationDetails.projectName,
        donator: user.firstName + ' ' + user.lastName,
      });

      addDonation(project, donationDetails, user);

      onVolunteerDonationValidationButton();
    } else if (
      paymentInfos != null &&
      (paymentInfos.type == 'paypal' || paymentInfos.type == 'stripe')
    ) {
      if (!foundationDonation) {
        firebaseAnalytics.sendEvent('money_donation', {
          status: 'Confirmed',
          action: 'creation',
          type: donationDetails.type,
          project: donationDetails.projectName,
          projectType: project.type,
          projectSupportArdianFoundation: project.projectSupportArdianFoundation
            ? 'Yes'
            : 'No',
          projectLeader:
            project.author.firstName + ' ' + project.author.lastName,
          projectTarget: project.fundRaisingTarget,
          amount: donationDetails.amount,
          amountEuro: amountEuro,
          currency: donationDetails.currency.sign,
          paymentType: donationDetails.paymentInfos.type
            ? donationDetails.paymentInfos.type
            : donationDetails.paymentInfos,
          donator: user.firstName + ' ' + user.lastName,
          anonymousDonation: donationDetails.anonymous ? 'Yes' : 'No',
        });
      } else {
        firebaseAnalytics.sendEvent('money_donation', {
          status: 'Confirmed',
          action: 'creation',
          type: donationDetails.type,
          project: 'Donation to the foundation',
          projectType: 'foundation',
          projectSupportArdianFoundation: 'Yes',
          projectLeader: '',
          projectTarget: '',
          currecy: donationDetails.currency.sign,
          amount: donationDetails.amount,
          amountEuro: amountEuro,
          paymentType: donationDetails.paymentInfos.type
            ? donationDetails.paymentInfos.type
            : donationDetails.paymentInfos,
          donator: user.firstName + ' ' + user.lastName,
          anonymousDonation: donationDetails.anonymous ? 'Yes' : 'No',
        });
      }
      addDonation(project, donationDetails, user);
      onMoneyDonationValidationButton();
    } else {
      if (volunteerDonation) {
        firebaseAnalytics.sendEvent('time_donation', {
          status: 'Confirmed',
          action: 'creation',
          type: donationDetails.type,
          project: donationDetails.projectName,
          projectType: project.type,
          projectSupportArdianFoundation: project.projectSupportArdianFoundation
            ? 'Yes'
            : 'No',
          projectLeader:
            project.author.firstName + ' ' + project.author.lastName,
          projectHourDonation: volunteerHoursDonation[0].hours,
          donator: user.firstName + ' ' + user.lastName,
        });
      } else {
        firebaseAnalytics.sendEvent('money_donation', {
          status: 'Confirmed',
          action: 'creation',
          type: donationDetails.type,
          project: 'Donation to the foundation',
          projectType: 'foundation',
          projectSupportArdianFoundation: 'Yes',
          projectLeader: '',
          projectTarget: '',
          currency: donationDetails.currency.sign,
          amount: donationDetails.amount,
          amountEuro: amountEuro,
          paymentType: donationDetails.paymentInfos.type
            ? donationDetails.paymentInfos.type
            : donationDetails.paymentInfos,
          donator: user.firstName + ' ' + user.lastName,
          anonymousDonation: donationDetails.anonymous ? 'Yes' : 'No',
        });
      }

      addDonation(project, donationDetails, user, currency);

      Toast.show(
        donationDetails.type == 'money'
          ? IMLocalized('Payment successfull.')
          : IMLocalized('Donation successfull.'),
        {
          duration: 2000,
        },
      );

      onVolunteerDonationValidationButton();
    }
  };

  const onDonationValidation = async url => {
    var donationDetails = {
      paymentInfos: paymentProcess,
      foundationDonation: foundationDonation ? true : false,
      type: 'money',
      projectId: foundationDonation ? null : project.id,
      projectAuthor: foundationDonation ? null : project.author,
      anonymous: anonymousDonation,
      amount: donationAmount,
      currency: currency,
      status: 'Pending',
    };

    console.log('In the donation validation');

    const amountEuro = Math.round(donationDetails.amount * currencyRate);

    firebaseAnalytics.sendEvent('money_donation', {
      status: 'Pending',
      action: 'creation',
      type: donationDetails.type,
      project: foundationDonation ? 'Donation to the foundation' : project.name,
      projectType: foundationDonation ? 'foundation' : project.type,
      projectSupportArdianFoundation:
        foundationDonation || project.projectSupportArdianFoundation
          ? 'Yes'
          : 'No',
      projectLeader: foundationDonation
        ? ''
        : project.author.firstName + ' ' + project.author.lastName,
      projectTarget: foundationDonation ? '' : project.fundRaisingTarget,
      currency: donationDetails.currency.sign,
      amount: donationDetails.amount,
      amountEuro: amountEuro,
      paymentType: donationDetails.paymentInfos.type
        ? donationDetails.paymentInfos.type
        : donationDetails.paymentInfos,
      donator: user.firstName + ' ' + user.lastName,
    });

    console.log('sending donnation');
    const response = await addDonation(project, donationDetails, user);

    goToDonationValidationButton(url, response.id);
  };

  const onwireTransferButtonPress = async () => {
    const currencyRate = currency
      ? ClickAndGiveConfig.currencyExchangeRates[currency.code]
      : 1;
    const amountEuro = Math.round(donationAmount * currencyRate);

    var donationDetails = {
      paymentInfos: 'WireTransfer',
      foundationDonation: foundationDonation ? true : false,
      type: 'money',
      projectId: foundationDonation ? null : project.id,
      projectAuthor: foundationDonation ? null : project.author,
      anonymous: anonymousDonation,
      amount: donationAmount,
      currency: currency,
      status: 'Pending',
    };

    firebaseAnalytics.sendEvent('money_donation', {
      status: 'Pending',
      action: 'creation',
      type: donationDetails.type,
      project: project.name,
      projectType: project.type,
      projectSupportArdianFoundation: project.projectSupportArdianFoundation
        ? 'Yes'
        : 'No',
      projectLeader: project.author.firstName + ' ' + project.author.lastName,
      projectTarget: project.fundRaisingTarget,
      currency: donationDetails.currency.sign,
      amount: donationDetails.amount,
      amountEuro: amountEuro,
      paymentType: donationDetails.paymentInfos.type
        ? donationDetails.paymentInfos.type
        : donationDetails.paymentInfos,
      donator: user.firstName + ' ' + user.lastName,
    });

    const response = await addDonation(project, donationDetails, user);
    goToWireTranfertPage(response.id);
  };

  const handleCardNumberTyping = text => {
    const numbers = text.replace(/[^0-9]/g, '');

    if (numbers.length < 5) {
      setCardNumbersDisplayed(numbers);
    } else if (4 < numbers.length && numbers.length < 9) {
      setCardNumbersDisplayed(
        numbers.slice(0, 4) + ' ' + numbers.slice(4, numbers.length),
      );
    } else if (8 < numbers.length && numbers.length < 13) {
      setCardNumbersDisplayed(
        numbers.slice(0, 4) +
          ' ' +
          numbers.slice(4, 8) +
          ' ' +
          numbers.slice(8, numbers.length),
      );
    } else {
      setCardNumbersDisplayed(
        numbers.slice(0, 4) +
          ' ' +
          numbers.slice(4, 8) +
          ' ' +
          numbers.slice(8, 12) +
          ' ' +
          numbers.slice(12, numbers.length),
      );
    }

    setCardNumbers(numbers);
    if (numbers.length == 16) {
      this.monthExpirationInput.focus();
    }
  };

  const handleCardExpirationMonthTyping = text => {
    const number = text.replace(/[^0-9]/g, '');
    setCardMonthExpiration(number);

    if (number.length == 2) {
      this.yearExpirationInput.focus();
      setCardMonthExpirationValid(number < 13);
    }
  };

  const handleCardExpirationYearTyping = text => {
    const number = text.replace(/[^0-9]/g, '');

    setCardYearExpiration(number);
    if (number.length == 2) {
      setCardYearExpirationValid(number > 19);
      this.ccvInput.focus();
    }
  };

  const onNavigationStateChange = webViewState => {
    var pageUrl = webViewState.url;
    console.log(pageUrl);

    if (pageUrl.includes('com.ardian.clickandgive')) {
      setShowModal(false);

      if (pageUrl.includes('com.ardian.clickandgive.paymentcreationError')) {
        console.log('Error payment creation');
        Toast.show(
          IMLocalized('An error occurred, payment has been cancelled.'),
          {
            duration: 1000,
          },
        );
      } else {
        setLoading(true);

        pageUrl = pageUrl.replace(/%26/g, '&');
        pageUrl = pageUrl.replace(/%3/g, '=');

        var regex = /[?&]([^=#]+)=([^&#]*)/g,
          params = {},
          match;

        while ((match = regex.exec(pageUrl))) {
          params[match[1]] = match[2];
        }

        console.log(params);

        this.PaymentManager.executePaypalPayment(
          params.paymentId,
          params.PayerID,
        )
          .then(response => {
            setLoading(false);
            console.log(response);
            if (response.state === 'approved') {
              console.log('SUCCESS');
              onDonationSuccess({
                type: 'paypal',
                reference: {
                  paymentId: params.paymentId,
                },
              });
            } else {
              console.log('ERROR payment execution');
              Toast.show(
                IMLocalized('An error occurred, payment has been cancelled.'),
                {
                  duration: 1000,
                },
              );
            }
          })
          .catch(error => {
            setLoading(false);
            Alert.alert(
              IMLocalized('Error'),
              error.message,
              [
                {
                  text: IMLocalized('OK'),
                },
              ],
              {
                cancelable: false,
              },
            );
          });
      }
    }
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'flex-start',
      }}>
      <KeyboardAwareView>
        <ScrollView>
          <View style={styles.mainContainer}>
            {!volunteerDonation && (
              <Text style={styles.redHeaderText}> Review donation </Text>
            )}
            <View
              style={[
                {
                  backgroundColor: '#FFF',
                  borderRadius: 10,
                },
                styles.shadow,
                volunteerDonation
                  ? {
                      marginTop: '5%',
                    }
                  : null,
              ]}>
              {foundationDonation ? (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      margin: 20,
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '25%',
                      aspectRatio: 1,
                      backgroundColor:
                        AppStyles.colorSet.mainThemeForegroundColor,
                      borderRadius: 10,
                    }}>
                    <Text
                      style={{
                        fontSize: AppStyles.fontSet.xxlarge,
                        color: '#FFF',
                        fontWeight: '800',
                      }}>

                      AF
                    </Text>
                  </View>
                  <Text
                    style={{
                      fontSize: AppStyles.fontSet.middleplus,
                      color: '#000',
                      fontWeight: '600',
                    }}>

                    Ardian Foundation
                  </Text>
                </View>
              ) : (
                <ProjectLightFeedItem item={project} onItemPress={() => {}} />
              )}
              <View
                style={{
                  paddingRight: 20,
                  paddingLeft: 20,
                  paddingBottom: 20,
                }}>
                <View
                  style={[
                    styles.spacer,
                    {
                      marginTop: 20,
                    },
                  ]}
                />
                {volunteerDonation ? (
                  <View
                    style={[
                      styles.rowSpaceBetweendContainer,
                      {
                        marginTop: 20,
                      },
                    ]}>
                    <Text
                      style={[
                        styles.smallText,
                        {
                          fontWeight: '300',
                        },
                      ]}>

                      {'Your donation'}
                    </Text>
                    <Text
                      style={[
                        styles.smallText,
                        {
                          fontWeight: '700',
                        },
                      ]}>

                      Volunteering
                    </Text>
                  </View>
                ) : (
                  <View>
                    <View
                      style={[
                        styles.rowSpaceBetweendContainer,
                        {
                          backgroundColor: '#F7423815',
                          margin: 10,
                          borderRadius: 10,
                          padding: 20,
                        },
                      ]}>
                      <Text
                        style={[
                          styles.smallText,
                          {
                            fontWeight: '700',
                          },
                        ]}>

                        {'You give'.toUpperCase()}
                      </Text>
                      {foundationDonation ? (
                        <Text
                          style={[
                            styles.smallText,
                            {
                              fontWeight: '700',
                              fontSize: AppStyles.fontSet.middle,
                            },
                          ]}>

                          {currency.sign}
                          {numeral(donationAmount).format('0,0')}
                        </Text>
                      ) : (
                        <Text
                          style={[
                            styles.smallText,
                            {
                              fontWeight: '700',
                              fontSize: AppStyles.fontSet.middle,
                            },
                          ]}>

                          {project.currency.sign}
                          {numeral(donationAmount).format('0,0')}
                        </Text>
                      )}
                    </View>
                    {project && project.foundationMatching > 0 && (
                      <View>
                        <View
                          style={[
                            styles.rowSpaceBetweendContainer,
                            {
                              marginVertical: 10,
                            },
                          ]}>
                          <Text style={styles.smallText}>

                            Ardian 's matching System (
                            {project.foundationMatching} for 1)
                          </Text>
                          <Text
                            style={[
                              styles.smallText,
                              styles.boldText,
                              {
                                color: '#f73750',
                              },
                            ]}>

                            {foundationDonation
                              ? currency.sign
                              : project.currency.sign}
                            {numeral(
                              project.foundationMatching * donationAmount,
                            ).format('0,0')}
                          </Text>
                        </View>
                        <View style={styles.rowSpaceBetweendContainer}>
                          <Text style={styles.smallText}> Giving total</Text>
                          <Text style={[styles.smallText, styles.boldText]}>

                            {foundationDonation
                              ? currency.sign
                              : project.currency.sign}
                            {numeral(
                              (Number(project.foundationMatching) + 1) *
                                donationAmount,
                            ).format('0,0')}
                          </Text>
                        </View>
                      </View>
                    )}
                  </View>
                )}
              </View>
            </View>
            <View
              style={[
                styles.spacer,
                {
                  marginTop: 10,
                },
              ]}
            />
            {volunteerDonation ? (
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={[
                      styles.subHeadlinesText,
                      {
                        fontWeight: '600',
                        fontSize: AppStyles.fontSet.middleminus,
                      },
                    ]}>

                    Summary of the involvement
                  </Text>
                  <Text
                    style={[
                      styles.smallText,
                      {
                        fontWeight: '300',
                        fontSize: AppStyles.fontSet.middleminus,
                      },
                    ]}>

                    {project.volunteerInstruction}
                  </Text>
                  <Text
                    style={[
                      styles.subHeadlinesText,
                      {
                        fontWeight: '600',
                        fontSize: AppStyles.fontSet.middleminus,
                        marginBottom: 3,
                      },
                    ]}>

                    Write an encouragement message
                  </Text>
                  <Text
                    style={[
                      styles.smallText,
                      {
                        fontWeight: '300',
                        fontSize: AppStyles.fontSet.small,
                        color: AppStyles.colorSet.mainThemeForegroundColor,
                        fontStyle: 'italic',
                        marginBottom: 15,
                      },
                    ]}>

                    {foundationDonation
                      ? 'This message will be sent through chat to the foundation administrator.'
                      : "This message will be sent through chat to the project's author."}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <FastImage
                      style={[
                        styles.userImage,
                        {
                          width: '15%',
                          aspectRatio: 1,
                        },
                      ]}
                      source={{
                        uri: user.profilePictureURL,
                      }}
                      resizeMode="cover"
                    />
                    <TextInput
                      style={[styles.encouragementInputContainer]}
                      placeholder={'Write your message here'}
                      placeholderTextColor="#CCC"
                      onChangeText={text => {
                        setEncouragementMessage(text);
                      }}
                      value={encouragementMessage}
                      maxLength={500}
                      blurOnSubmit={true}
                      returnKeyType={'next'}
                      multiline={true}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                </View>
                <TouchableOpacity
                  style={[
                    styles.shadow,
                    {
                      padding: 25,
                      width: '90%',
                      alignSelf: 'center',
                      backgroundColor: AppStyles.colorSet.timeColor,
                      borderRadius: 50,
                      marginBottom: 35,
                    },
                  ]}
                  onPress={() => onVolunteerDonation()}>
                  <Text
                    style={[
                      {
                        fontSize: AppStyles.fontSet.middle,
                        color: '#FFF',
                        fontWeight: '600',
                        textAlign: 'center',
                      },
                    ]}>

                    Confirm time gift
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View>

                {paymentProcess == 'card' && (
                  <View>
                    <TouchableOpacity
                      style={styles.rowSpaceBetweendContainer}
                      onPress={() => setAnonymousDonation(!anonymousDonation)}>
                      <Text
                        style={[
                          styles.subHeadlinesText,
                          {
                            marginVertical: 30,
                          },
                        ]}>

                        Donate anonymously
                      </Text>
                      <Image
                        source={
                          anonymousDonation
                            ? AppStyles.iconSet.radioButtonFilled
                            : AppStyles.iconSet.radioButtonUnfilled
                        }
                        style={[
                          styles.paymentMethodIcon,
                          anonymousDonation
                            ? {
                                tintColor: '#000',
                              }
                            : null,
                        ]}
                      />
                    </TouchableOpacity>
                    <View style={styles.spacer} />
                    <Text style={styles.subHeadlinesText}>

                      Select payment method
                    </Text>
                    <View
                      style={[
                        {
                          borderWidth: 1,
                          borderColor: '#CCC',
                          borderRadius: 10,
                          padding: 5,
                          marginVertical: 5,
                        },
                        ,
                        cardPayment
                          ? {
                              borderColor:
                                AppStyles.colorSet.mainThemeForegroundColor,
                            }
                          : {},
                      ]}>
                      <TouchableOpacity
                        style={[
                          styles.paymentMethodRow,
                          {
                            margin: 0,
                            padding: 0,
                            borderWidth: 0,
                            marginBottom: 10,
                          },
                        ]}
                        onPress={() => setCardPayment(!cardPayment)}>
                        <View style={styles.rowContainer}>
                          <Image
                            source={
                              cardPayment
                                ? AppStyles.iconSet.radioButtonFilled
                                : AppStyles.iconSet.radioButtonUnfilled
                            }
                            style={[
                              styles.paymentMethodIcon,
                              cardPayment
                                ? {
                                    tintColor:
                                      AppStyles.colorSet
                                        .mainThemeForegroundColor,
                                  }
                                : {},
                            ]}
                          />
                          <Text style={styles.projectDescriptionText}>

                            Credit Card
                          </Text>
                        </View>
                        <Image
                          source={AppStyles.iconSet.creditCard}
                          style={styles.paymentMethodImage}
                        />
                      </TouchableOpacity>
                      {cardPayment && (
                        <View>
                          <Text style={styles.cardText}>

                            Enter card number
                          </Text>
                          <TextInput
                            ref={input => {
                              this.cardNumbersInput = input;
                            }}
                            style={styles.cardInputText}
                            placeholder={'1000 1000 1000 1000'}
                            placeholderTextColor="#aaaaaa"
                            onChangeText={text => handleCardNumberTyping(text)}
                            value={cardNumbersDisplayed}
                            maxLength={19}
                            autoCompleteType={'cc-number'}
                            keyboardType="numeric"
                            autoFocus={true}
                          />
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <View
                              style={{
                                width: '25%',
                                marginRight: 20,
                              }}>
                              <Text style={styles.cardText}> Expires </Text>
                              <View style={styles.cardExpirationContainer}>
                                <TextInput
                                  ref={input => {
                                    this.monthExpirationInput = input;
                                  }}
                                  style={[
                                    styles.cardInputTextExpiration,
                                    cardMonthExpirationValid
                                      ? {}
                                      : {
                                          color:
                                            AppStyles.colorSet
                                              .mainThemeForegroundColor,
                                        },
                                  ]}
                                  placeholder={'MM'}
                                  placeholderTextColor="#aaaaaa"
                                  onChangeText={text =>
                                    handleCardExpirationMonthTyping(text)
                                  }
                                  value={cardMonthExpiration}
                                  maxLength={2}
                                  keyboardType="numeric"
                                />
                                <Text style={styles.cardInputTextExpiration}>

                                  /
                                </Text>
                                <TextInput
                                  ref={input => {
                                    this.yearExpirationInput = input;
                                  }}
                                  style={[
                                    styles.cardInputTextExpiration,
                                    cardYearExpirationValid
                                      ? null
                                      : {
                                          color:
                                            AppStyles.colorSet
                                              .mainThemeForegroundColor,
                                        },
                                  ]}
                                  placeholder={'AA'}
                                  placeholderTextColor="#aaaaaa"
                                  onChangeText={text =>
                                    handleCardExpirationYearTyping(text)
                                  }
                                  value={cardYearExpiration}
                                  maxLength={2}
                                  keyboardType="numeric"
                                />
                              </View>
                            </View>
                            <View
                              style={{
                                width: '25%',
                              }}>
                              <Text style={styles.cardText}> CCV </Text>
                              <TextInput
                                ref={input => {
                                  this.ccvInput = input;
                                }}
                                style={styles.cardInputText}
                                placeholder={'123'}
                                placeholderTextColor="#aaaaaa"
                                onChangeText={text => {
                                  setcardCVC(text.replace(/[^0-9]/g, ''));
                                  if (text.length == 4) {
                                    this.ccvInput.blur();
                                  }
                                }}
                                blurOnSubmit={true}
                                maxLength={4}
                                value={cardCVC}
                                keyboardType="numeric"
                              />
                            </View>
                          </View>
                        </View>
                      )}
                      {cardPayment && (
                        <TouchableOpacity
                          style={[styles.floatingButton]}
                          onPress={() => handlingPayment('card')}>
                          <Text style={styles.floatingButtonText}>

                            Confirm donation
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                    <TouchableOpacity
                      style={styles.paymentMethodRow}
                      onPress={() => handlingPayment('paypal')}>
                      <View style={styles.rowContainer}>
                        <Image
                          source={AppStyles.iconSet.radioButtonUnfilled}
                          style={styles.paymentMethodIcon}
                        />
                        <Text style={styles.projectDescriptionText}>

                          Paypal
                        </Text>
                      </View>
                      <Image
                        source={AppStyles.iconSet.paypal}
                        style={styles.paymentMethodImage}
                      />
                    </TouchableOpacity>
                    <View
                      style={{
                        height: 5,
                      }}
                    />
                  </View>
                )}
                {paymentProcess == 'alizee' && (
                  <View>
                    <Text style={styles.alizeePaymentText}>
                      <Text>Thank you so much for your generosity ! We are almost done. {'\n'} {'\n'}</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                        }}>The Ardian Foundation</Text>
                      <Text> uses </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                        }}>
                        Alizée
                      </Text>
                      <Text> to record its philanthropic activity. You will be prompted to a secure space on a pre - populated payment interface. Please enter your card details, verify your information and validate your donation. {'\n'}</Text>
                      {foundationDonation ? null : (
                        <Text>
                          <Text>{'\n'} Please note that </Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                            }}>

                            {project.name}
                          </Text>
                          <Text> is part of </Text>
                          {project.categories.map((item, key) => {
                            if (key == 1) {
                              return (
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                  }}>
                                  , {item}
                                </Text>
                              );
                            } else {
                              return (
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                  }}>

                                  {item}
                                </Text>
                              );
                            }
                          })}
                          <Text> in our philanthropic portfolio.</Text>
                        </Text>
                      )}
                    </Text>
                    <TouchableOpacity
                      style={[
                        styles.floatingButton,
                        {
                          backgroundColor:
                            AppStyles.colorSet.mainThemeForegroundColor,
                        },
                      ]}
                      onPress={() => {
                        console.log('Paiement : ', paymentProcess);
                        setRedirectionInfoVisible(true);
                      }}>
                      <Text style={styles.floatingButtonText}> GO! </Text>
                    </TouchableOpacity>
                  </View>
                )}
                {paymentProcess == 'wireTransfer' && (
                  <View>
                    <Text style={styles.alizeePaymentText}>
                      <Text>

                        Thank you so much for your generosity ! We are almost
                        done. {'\n'} {'\n'}
                        To finalize your donation:
                      </Text>
                    </Text>
                    <TouchableOpacity
                      style={[
                        styles.floatingButton,
                        {
                          backgroundColor:
                            AppStyles.colorSet.mainThemeForegroundColor,
                        },
                      ]}
                      onPress={() => {
                        console.log('Paiement : ', paymentProcess);
                        setPayUsingAlizee(true);
                        setRedirectionInfoVisible(true);
                      }}>
                      <Text style={styles.floatingButtonText}>

                        CLICK HERE
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        console.log('Paiement : ', paymentProcess);
                        setRedirectionInfoVisible(true);
                      }}>
                      <Text
                        style={{
                          flexWrap: 'wrap',
                          marginBottom: 10,
                        }}>
                        <Text>

                          If you want to benefit from a tax credit please follow
                          the further detail
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: AppStyles.colorSet.mainThemeForegroundColor,
                          }}> here</Text>
                        <Text>. </Text>
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
                {paymentProcess == 'donationPlatform' && (
                  <View>
                    <Text style={styles.alizeePaymentText}>
                      <Text>

                        Thank you so much for your generosity!{'\n'} {'\n'}
                        To complete the donation:
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                        }}>{foundationDonation ? 'Ardian foundation': project.name}
                      </Text>
                      <Text>uses a dedicated fundraising platform very easy to use.Please enter your card details, verify your information and validate your donation.</Text>
                    </Text>
                    <TouchableOpacity
                      style={[
                        styles.floatingButton,
                        {
                          backgroundColor:
                            AppStyles.colorSet.mainThemeForegroundColor,
                        },
                      ]}
                      onPress={() => {
                        console.log('Paiement : ', paymentProcess);
                        setRedirectionInfoVisible(true);
                      }}>
                      <Text style={styles.floatingButtonText}> GO! </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            )}
          </View>
          <FormDisclaimer disclaimerForm={'givingMoney'} />
        </ScrollView>
      </KeyboardAwareView>
      <Modal visible={showModal} onRequestClose={() => setShowModal(false)}>
        <TouchableOpacity
          style={{
            marginTop: 60,
            marginLeft: 10,
            flexDirection: 'row',
          }}
          onPress={() => {
            setShowModal(false);
            Toast.show(IMLocalized('Payment cancelled by user.'), {
              duration: 1000,
            });
          }}>
          <Image
            source={AppStyles.iconSet.backArrow}
            style={{
              width: 20,
              aspectRatio: 1,
              marginRight: 5,
              tintColor: AppStyles.colorSet.black,
            }}
          />
          <Text
            style={{
              fontSize: AppStyles.fontSet.middleminus,
            }}>

            Cancel
          </Text>
        </TouchableOpacity>
        <WebView
          ref={input => {
            this.webView = input;
          }}
          source={{
            uri: webViewUrl,
          }}
          onNavigationStateChange={onNavigationStateChange.bind(this)}
        />
      </Modal>
      <RedirectionInfoModal
        modalVisible={redirectionInfoVisible}
        hideModal={() => {
          setPayUsingAlizee(false);
          setRedirectionInfoVisible(false);
        }}
        goToPlatform={() => {
          switch (paymentProcess) {
            case "alizee":
              onDonationValidation(buildAlizeePreFilledUrl());
              break;
            case "wireTransfer":
              if(payUsingAlizee){
                onDonationValidation(buildAlizeePreFilledUrl());
              } else {
                onwireTransferButtonPress();
              }
              break;
            case "donationPlatform":
              onDonationValidation(project.donationPlatformUrl);
              break;
            default:

          }

        }}
      />
      {loading && <TNActivityIndicator appStyles={AppStyles} />}
    </View>
  );
}

GiveFund.propTypes = {
  item: PropTypes.object,
};

export default GiveFund;
