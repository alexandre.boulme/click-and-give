import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import { Icon, Picker} from "native-base";
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import numeral from 'numeral';

function DonationAmount(props) {
  const {
    project,
    onNextButtonPress,
    foundationDonation
  } = props;

  const [donationAmount, setDonationAmount] = useState("");
  const [donationCurrency, setDonationCurrency] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const currencyList = ClickAndGiveConfig.currencyList;

  const onKeyboardButtonPress = (number) => {
      var newDonationAmount = donationAmount + number;
      setDonationAmount(newDonationAmount);
  };

  const removeLastNumber = () => {
    var newDonationAmount = donationAmount.substring(0, donationAmount.length - 1);
    setDonationAmount(newDonationAmount);
  };

  const onPickerValueChange = value => {
      setDonationCurrency(value);
  };

  return (
    <View style={[styles.container, {justifyContent: 'space-between'}]}>
      <ScrollView contentContainerStyle={{justifyContent: 'space-between'}}>
      <View style={[styles.donationAmountContainer,foundationDonation ? {marginTop: '1%'} : {marginTop: '20%'}]}>
        {foundationDonation && (
          <Text>
            <Text style={styles.foundationInfoText}>The Ardian Foundation is active in 7 countries, supporting social mobility via education and entrepreneurship. Your donation will fund projects addressing primary-age schoolchildren up to underprivileged adults working to better their economic opportunities.</Text>
            <Text style={[styles.foundationInfoText,{fontWeight: '700'}]}>{"\n"} Thank you for your generosity ! {'\n'}</Text>
          </Text>
        )}
        <Text style={styles.donationText}>Donation Amount</Text>
        <View style={[styles.amountInputContainer, {marginTop: '3%'}]}>
          {!foundationDonation ? (
            <Text style={styles.InputContainer}>{project.currency.sign}</Text>
          ) : (
            <View style={styles.pickerContainer}>
              <Picker
                mode="dropdown"
                placeholder="Currency"
                headerBackButtonTextStyle={{ color: AppStyles.colorSet.mainThemeForegroundColor }}
                iosIcon={<Icon name="chevron-down-sharp" style={{color: "#FFF"}} />}
                placeholderStyle={{ color: "#FFF", fontSize: AppStyles.fontSet.middleminus, fontWeight: '400'}}
                textStyle={styles.pickerTextStyle}
                itemTextStyle={{fontSize: AppStyles.fontSet.middleplus, fontWeight: '400'}}
                selectedValue={donationCurrency}
                onValueChange={value => onPickerValueChange(value)}
              >
                {
                  currencyList.map((item, i) => (
                    <Picker.Item label={item.sign} value={item} />
                  ))
                }
              </Picker>
            </View>
          )}
          <Text style={[styles.InputContainer,foundationDonation ? {textAlign: 'right'} : {textAlign: 'right', marginLeft: '20%'}]}>{numeral(donationAmount).format('0,0')}</Text>
        </View>
        <View style={styles.tagContainer}>
          {(donationCurrency || (project && project.currency.sign)) && (ClickAndGiveConfig.preTypedAmounts.map(item => {
            return     (
              <TouchableOpacity
                  style={[styles.tagStyle,foundationDonation ? {} :{}]}
                  onPress={() => onNextButtonPress(item, donationCurrency, foundationDonation)}>
                <Text style={styles.tagTextStyle}>{item.toUpperCase()} {foundationDonation ? donationCurrency.sign : project.currency.sign}</Text>
              </TouchableOpacity>
            )
          }))}
        </View>
      </View>
      <View style={styles.numericKeyboard}>
        <View style={styles.numericKeyboardRow}>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(1)}
            >
            <Text style={styles.numericKeyboardButtonText}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(2)}>
            <Text style={styles.numericKeyboardButtonText}>2</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(3)}>
            <Text style={styles.numericKeyboardButtonText}>3</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.numericKeyboardRow}>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(4)}>
            <Text style={styles.numericKeyboardButtonText}>4</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(5)}>
            <Text style={styles.numericKeyboardButtonText}>5</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(6)}>
            <Text style={styles.numericKeyboardButtonText}>6</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.numericKeyboardRow}>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(7)}>
            <Text style={styles.numericKeyboardButtonText}>7</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(8)}>
            <Text style={styles.numericKeyboardButtonText}>8</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(9)}>
            <Text style={styles.numericKeyboardButtonText}>9</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.numericKeyboardRow}>
          <TouchableOpacity
            style={styles.numericKeyboardButtonWidth}>
            <Text style={styles.numericKeyboardButtonText}></Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButton}
            onPress={() => onKeyboardButtonPress(0)}>
            <Text style={styles.numericKeyboardButtonText}>0</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numericKeyboardButtonWidth}
            onPress={() => removeLastNumber()}>
            <Image source={AppStyles.iconSet.backSpace} resizeMode='contain' style={styles.numericKeyboardButtonIcon} />
          </TouchableOpacity>
        </View>
      <TouchableOpacity
        disabled={donationAmount.length === 0}
        style={donationAmount.length > 0 && donationAmount !== "0"? styles.nextButton : styles.nextButtonDisabled}
        onPress={() => onNextButtonPress(donationAmount, donationCurrency, foundationDonation)}>
        <Text style={styles.nextButtonText}>Next</Text>
      </TouchableOpacity>
    </View>
    </ScrollView>
  </View>
  );
}

DonationAmount.propTypes = {
  item: PropTypes.object,
};

export default DonationAmount;
