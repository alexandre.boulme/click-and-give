import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ProjectLightFeedItem from '../../ProjectLightFeedItem/ProjectLightFeedItem';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import Toast from 'react-native-tiny-toast';
import {updateDonation, getDonation } from "../../../Core/project/firebase/donation";
import {firebaseAnalytics} from "../../../Core/firebase";
import ClickAndGiveConfig from "../../../ClickAndGiveConfig";

function DonationValidation(props) {
    const {
        project,
        donationId,
        foundationDonation,
        currency,
        onDonationValidationButton
    } = props;

    const styles = useDynamicStyleSheet(dynamicStyles);
    const currencyRate = ClickAndGiveConfig.currencyExchangeRates[currency ? currency.code : project.currency.code];

    const onDonationStatusUpdate = async (newStatus) => {

        console.log("before donation " + donationId);

        const donationData = await getDonation(donationId);
        const donation = donationData.data;
        const amountEuro = Math.round(donation.amount * currencyRate);

        console.log("after donation");

        console.log(JSON.stringify(donation));

        firebaseAnalytics.sendEvent("money_donation", {
            status: newStatus,
            action: "update",
            type: donation.type,
            currency: currency ? currency.sign : project.currency.sign,
            project: project ? project.name : "Foundation",
            projectType: project ? project.type : "Foundation donation",
            projectSupportArdianFoundation: project ? project.projectSupportArdianFoundation : "True",
            projectLeader: project ? project.author.firstName + ' ' + project.author.lastName : "Ardian foundation",
            projectTarget: project ? project.fundRaisingTarget : "",
            amount: donation.amount,
            amountEuro: amountEuro,
            paymentType: donation.paymentInfos.type ? donation.paymentInfos.type : donation.paymentInfos,
            donator:  donation.author.firstName + ' ' + donation.author.lastName,
            anonymousDonation: donation.anonymous ? "true" : "false"
        });

        updateDonation(donationId, newStatus);

        Toast.show(IMLocalized('Donation marked as "' + newStatus + '".'), {duration: 2000});

        onDonationValidationButton(newStatus);
    }

    return (
        <View style={{flex: 1, justifyContent: 'flex-start',}}>
            <KeyboardAwareView>
                <ScrollView
                    ref={(scroll) => {this.scroll = scroll;}}>
                    <View style={styles.mainContainer}>
                        <Text style={styles.redHeaderText}>Confirm donation</Text>
                        <View style={[{backgroundColor: "#FFF", borderRadius: 10}, styles.shadow]}>
                            {foundationDonation ? (
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <View style= {{margin: 20, alignItems: 'center', justifyContent: 'center', width: '25%', aspectRatio: 1, backgroundColor: AppStyles.colorSet.mainThemeForegroundColor, borderRadius: 10}}>
                                        <Text style={{fontSize: AppStyles.fontSet.xxlarge, color:"#FFF", fontWeight: '800'}}>AF</Text>
                                    </View>
                                    <Text style={{fontSize: AppStyles.fontSet.middleplus, color:"#000", fontWeight: '600'}}>Ardian Foundation</Text>
                                </View>
                            ) : (
                                <View>
                                    <ProjectLightFeedItem
                                        item={project}
                                        onItemPress={()=> {}} />
                                    <View style={{paddingRight: 20,paddingLeft: 20,paddingBottom: 20}}>
                                        <View style={[styles.spacer, {marginTop: 20}]} />
                                    </View>
                                </View>
                            )}
                        </View>
                    </View>
                    <View style={[{margin: 20}]}>
                    <Text style={[styles.subHeadlinesText,{textAlign: 'center', fontSize: AppStyles.fontSet.middleplus}]}>Did you complete successfully the donation process ?</Text>
                    <TouchableOpacity
                        style={[styles.updateStatusButton,{borderColor: AppStyles.colorSet.greenTag, borderWidth: 2}]}
                        onPress={() => onDonationStatusUpdate('Confirmed')}>
                        <Text style={[styles.updateStatusButtonText, {color: AppStyles.colorSet.greenTag, fontWeight: '700'}]}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.updateStatusButton, {borderColor: AppStyles.colorSet.yellow, borderWidth: 2}]}
                        onPress={() => onDonationStatusUpdate('Pending')}>
                        <Text style={[styles.updateStatusButtonText, {color: AppStyles.colorSet.yellow, fontWeight: '700'}]}>I'll do it later</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.updateStatusButton, {borderColor: AppStyles.colorSet.mainThemeForegroundColor, borderWidth: 2}]}
                        onPress={() => onDonationStatusUpdate('Canceled')}>
                        <Text style={[styles.updateStatusButtonText, {color: AppStyles.colorSet.mainThemeForegroundColor, fontWeight: '700'}]}>No</Text>
                        <Text style={[styles.updateStatusButtonText,{fontSize: AppStyles.fontSet.normal, fontStyle: "italic", color: AppStyles.colorSet.mainThemeForegroundColor}]}>Your donation will be canceled</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </KeyboardAwareView>
    </View>
    );
}

DonationValidation.propTypes = {
    item: PropTypes.object,
};

export default DonationValidation;
