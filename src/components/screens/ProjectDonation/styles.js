import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
  mainContainer: {
    width: '90%',
    alignSelf: 'center'
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  redHeaderText : {
    fontSize: 30,
    fontWeight: '600',
    color: AppStyles.colorSet.mainThemeForegroundColor,
    marginVertical: 20
  },
  shadow: {
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#CCC",
    shadowOpacity: 1.0,
  },
  projectImage: {
    width: 64,
    height: 64,
    backgroundColor: "#CCC",
    borderRadius: 8
  },
  smallText: {
    fontSize: AppStyles.fontSet.normal,
    color: AppStyles.colorSet.mainTextColor,
  },
  boldText: {
    fontWeight: '800'
  },
  spacer: {
    height: 1,
    backgroundColor: "#CCC"
  },
  rowSpaceBetweendContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  subHeadlinesText: {
    fontSize: AppStyles.fontSet.middleminus,
    fontWeight: '600',
    color: AppStyles.colorSet.mainTextColor,
    marginVertical: 20
  },
  paymentMethodRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#CCC",
    padding: 10,
    backgroundColor: "#FFF"
  },
  paymentMethodIcon: {
    width: 25,
    height: 25,
    tintColor: "#CCC",
    marginRight: 15
  },
  paymentMethodText: {
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '300',
    color: AppStyles.colorSet.mainTextColor
  },
  paymentMethodImage: {
    width: 50,
    height: 25
  },
  topImage:{
    position: 'absolute',
    top:-50,
    height: '40%'
  },
  containerStyle: {
    height: '70%',
    flexDirection: 'column',
    padding: 25,
    backgroundColor: "#FFF",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  foundationIcon: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginLeft: 10
  },
  foundationSubText: {
    marginTop: 4,
    color: "#F7375080",
    fontSize: AppStyles.fontSet.normal,
    fontWeight: '700'
  },
  projectDescriptionText: {
    fontSize: AppStyles.fontSet.middle,
    color: AppStyles.colorSet.mainTextColor,
    marginVertical: 14
  },
  creatorInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10
  },
  profilePicture: {
    width: 30,
    height: 30,
    borderRadius: 30,
    backgroundColor: AppStyles.colorSet.mainThemeBackgroundColor
  },
  smallRedText: {
    marginLeft: 3,
    fontSize: 16,
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontWeight: '600'
  },
  smallDarkText: {
    color: AppStyles.colorSet.mainTextColor,
    fontSize: 16
  },
  textSeparator:{
    fontSize: 26,
    color: "#CCC"
  },
  locationIcon: {
    tintColor: AppStyles.colorSet.mainThemeForegroundColor,
    width: 25,
    height: 25
  },
  progressBarContainer: {
    margin: 10,
    width: '100%',
    alignSelf: 'center'
  },
  progressBar: {
    backgroundColor: "#DDD",
    width: '100%'
  },
  collectAmountText: {
    fontSize: AppStyles.fontSet.middle,
    color: "#5CF5A9",
    fontWeight:'900'
  },
  collectTargetText: {
    fontSize: AppStyles.fontSet.middle,
    color: "#000",
    fontWeight:'400' },
  textArdianEngage: {
    marginTop: 10,
    fontSize: 24,
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontWeight: '800'
  },
  ardianDescriptionText: {
    fontSize: AppStyles.fontSet.middleminus,
    color: AppStyles.colorSet.mainTextColor,
    marginVertical: 20,
    fontWeight: '300'
  },
  subHeaderDark: {
    fontSize: AppStyles.fontSet.large,
    color: AppStyles.colorSet.mainTextColor,
    fontWeight: '700'
  },
  placeholderTextColor: {
    color: AppStyles.colorSet.mainTextColor,
  },
  foundationInfoText: {
    marginBottom: 20,
    width: '85%',
    fontWeight: '300',
    alignSelf: 'center',
    fontSize: AppStyles.fontSet.normal,
    textAlign : 'justify',
    color: AppStyles.colorSet.mainThemeBackgroundColor,
  },
  donationAmountContainer:{
    flexDirection: 'column',
    width: '100%',
    paddingLeft: 35,
    paddingRight: 35,
    alignItems: 'center',
  },
  donationText: {
    fontSize: AppStyles.fontSet.middleplus,
    textAlign : 'center',
    color: AppStyles.colorSet.mainThemeBackgroundColor,
    fontWeight: '500'
  },
  InputContainerCurrency: {
    marginLeft: 10,
    width: 30,
    height: 50,
    alignSelf: 'flex-start',
    tintColor: "#FFF"
  },
  InputContainer: {
    margin: 15,
    marginBottom: 5,
    height: 50,
    fontSize: AppStyles.fontSet.xxlarge,
    color: "#FFF",
    fontWeight: '800',
    textAlign: 'left',
    alignSelf: 'flex-end'
  },
  numericKeyboard: {
    width: '90%',
    marginTop: '5%',
    alignSelf: 'center'
  },
  numericKeyboardRow : {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 2
  },
  numericKeyboardButton :{
    ...ifIphoneX({
      padding: 20,
    },{
      padding: 10,
    }),
    margin: 1,
    width: '32%',
    backgroundColor: "#FFFFFF20",
    borderRadius: 20,
    alignItems: 'center'
  },
  numericKeyboardButtonText : {
    fontSize: AppStyles.fontSet.middleplus,
    fontWeight: '700',
    color: "#FFF"
  },
  inputTextContainer: {
    borderBottomColor: "#FFF",
    borderBottomWidth: 1,
    marginBottom: '30%',
  },
  numericKeyboardButtonWidth: {
    width: '32%',
    alignItems: 'center'
  },
  numericKeyboardButtonIcon:{
    tintColor: "#FFFFFF80",
    ...ifIphoneX(
      {
        height: 50,
      },{
        height: 30,
      }
    )
  },
  nextButton: {
    ...ifIphoneX(
      {
        padding: 20,
        paddingLeft: 35,
        paddingRight: 35,
      },{
        padding: 12,
        paddingLeft: 15,
        paddingRight: 15
      }
    ),
    marginVertical: 10,
    width: '40%',
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    alignItems: 'center',
    alignSelf: 'center'
  },
  nextButtonDisabled: {
    ...ifIphoneX(
      {
        padding: 20,
        paddingLeft: 35,
        paddingRight: 35,
      },{
        padding: 12,
        paddingLeft: 15,
        paddingRight: 15,
      }
    ),
    marginVertical: 10,
    margin: 2,
    width: '40%',
    backgroundColor: "#FFFFFF30",
    borderRadius: 30,
    alignItems: 'center',
    alignSelf: 'center'
  },
  nextButtonText: {
    fontSize: 18,
    fontWeight: '700',
    color: AppStyles.colorSet.mainThemeForegroundColor
  },
  userImage: {
    width: '35%',
    height: undefined,
    aspectRatio: 1,
    borderRadius: 50,
    marginRight: 20,
  },
  encouragementInputContainer: {
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '300',
    height: 100
  },
  pickerContainer: {
    height:50,
    marginBottom: 5,
    marginTop: 10,
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    color: AppStyles.colorSet.mainThemeBackgroundColor,
    fontSize: AppStyles.fontSet.middle,
    alignItems: 'flex-start'
  },
  pickerTextStyle: {
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    color: AppStyles.colorSet.mainThemeBackgroundColor,
    fontSize: AppStyles.fontSet.xlarge,
    fontWeight: '600'
  },
  pickerItemStyle: {
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    color: AppStyles.colorSet.mainThemeBackgroundColor,
    fontSize: AppStyles.fontSet.middle
  },
  container: {
    height: '100%',
    flexDirection: 'column',
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor
  },
  amountInputContainer: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: "#FFF"
  },
  cardText: {
    fontSize: AppStyles.fontSet.normal,
    fontWeight: '300',
    color: "#222",
    margin: 5
  },
  cardInputText: {
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '400',
    color: "#000",
    margin: 5,
    marginBottom: 10,
    borderBottomColor: AppStyles.colorSet.mainThemeForegroundColor,
    borderBottomWidth: 1,
    paddingBottom: 15
  },
  cardInputTextExpiration: {
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '400',
    color: "#000",
  },
  cardExpirationContainer: {
    borderBottomColor: AppStyles.colorSet.mainThemeForegroundColor,
    borderBottomWidth: 1,
    paddingBottom: 15,
    flexDirection: 'row',
    margin: 5,
  },
  floatingButton: {
    width: '90%',
    padding: 25,
    borderRadius: 40,
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#999",
    shadowOpacity: 1.0,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 30
  },
  floatingButtonText: {
    color: "#FFF",
    fontSize: AppStyles.fontSet.middle,
    fontWeight: '800',
    textAlign: 'center'
  },
  updateStatusButton:{
    ...ifIphoneX(
        {
          paddingVertical: 15,
          paddingLeft: 5,
          paddingRight: 5,
        },{
          paddingVertical: 12,
          paddingLeft: 5,
          paddingRight: 5,
        }
    ),
    marginVertical: 10,
    width: '80%',
    borderRadius: 10,
    alignItems: 'center',
    alignSelf: 'center'
  },
  updateStatusButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: AppStyles.colorSet.white
  },
  alizeePaymentText: {
    marginVertical: 10,
    fontSize: AppStyles.fontSet.middle,
    textAlign: "center"
  },
  wireTransferIntro: {
    marginVertical: 5,
    fontSize: AppStyles.fontSet.middle,
  },
  wireTransferAccount: {
    fontSize: AppStyles.fontSet.middle,
  },
  tagContainer: {
    alignSelf: "center",
    alignItems: "center",
    margin: 10,
    flexDirection: 'row',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  tagStyle: {
    alignSelf: "center",
    backgroundColor: AppStyles.colorSet.white,
    paddingVertical: 7,
    paddingHorizontal: 10,
    borderRadius: 25,
    marginHorizontal: 5,
    marginBottom: 10
  },
  tagTextStyle: {
    ...ifIphoneX({
      fontSize: AppStyles.fontSet.middleplus,
    },{
      fontSize: AppStyles.fontSet.middleminus,
    }),
    color: AppStyles.colorSet.black,
    fontWeight:'700'
  },
});

export default dynamicStyles;
