import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import {useDynamicStyleSheet} from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import numeral from 'numeral';
import ClickAndGiveConfig from "../../../ClickAndGiveConfig";
import { ALIZEE_PLATFORM_URL } from 'react-native-dotenv';


function wireTransferInfo(props) {
    const {
        userCountry,
        donationAmount,
        currency,
        goToDonationValidationScreen
    } = props;

    const styles = useDynamicStyleSheet(dynamicStyles);

    const [wireTransferCountryContent, setwireTransferCountryContent]= useState(ClickAndGiveConfig.wireTransferCountry[userCountry]);
    //const [wireTransferCountryContent, setwireTransferCountryContent]= useState(ClickAndGiveConfig.wireTransferCountry["Spain"]);

    const goToDonationValidationButton = () => {
        goToDonationValidationScreen();
    };

    const goToSimpleDonation = () => {
        goToDonationValidationScreen(ALIZEE_PLATFORM_URL);
    };

    return (
        <View style={{flex: 1, justifyContent: 'flex-start',}}>
            <KeyboardAwareView>
                <ScrollView
                    ref={(scroll) => {this.scroll = scroll;}}>
                    <View style={styles.mainContainer}>
                        <Text style={styles.redHeaderText}>Wire transfer information</Text>
                        <View style={[styles.spacer, {marginVertical: 5}]} />
                        {wireTransferCountryContent && (
                            <View>
                                <Text style={styles.alizeePaymentText}>
                                    <Text>To finalize your donation,</Text>
                                    <Text style={{fontWeight: 'bold'}}> if you want to benefit from a tax credit</Text>
                                    <Text> please wire {currency.sign}{numeral(donationAmount).format('0,0')} to the following TGE partner bank account. {"\n"}{"\n"}For a simple donation (no tax credit) just click</Text>
                                    <Text style={{fontWeight: 'bold', color: AppStyles.colorSet.mainThemeForegroundColor}} onPress={() => goToSimpleDonation()}> HERE</Text>
                                </Text>
                                <View style={[styles.spacer, {marginVertical: 5}]} />
                                <Text style={styles.wireTransferIntro}>{wireTransferCountryContent.intro}</Text>
                                {wireTransferCountryContent.accountInfo && (
                                    <View>
                                        <Text style={[styles.subHeaderDark, {marginVertical: 15}]}>Account info</Text>
                                        <Text style={[styles.wireTransferAccount,{fontWeight: "800"}]}>{wireTransferCountryContent.accountInfo.holder}</Text>
                                        <Text style={styles.wireTransferAccount}>{wireTransferCountryContent.accountInfo.bankName}</Text>
                                        <Text style={styles.wireTransferAccount}>Address: {wireTransferCountryContent.accountInfo.address}</Text>
                                        {wireTransferCountryContent.accountInfo.bankAccount && (<Text style={styles.wireTransferAccount}>Account - {wireTransferCountryContent.accountInfo.bankAccount}</Text>)}
                                        <Text style={styles.wireTransferAccount}>Iban - {wireTransferCountryContent.accountInfo.iban}</Text>
                                        <Text style={styles.wireTransferAccount}>BIC - {wireTransferCountryContent.accountInfo.bic}</Text>
                                        {wireTransferCountryContent.accountInfo.bankCode && (<Text style={styles.wireTransferAccount}>BankCode - {wireTransferCountryContent.accountInfo.bankCode}</Text>)}
                                    </View>
                                )}
                                {wireTransferCountryContent.accountInfo && (
                                    <View>
                                        <Text style={[styles.subHeaderDark, {marginVertical: 15}]}>Communication</Text>
                                        <Text style={[styles.wireTransferAccount,{fontWeight: "800"}]}>{wireTransferCountryContent.communication.label}</Text>
                                        <Text style={styles.wireTransferAccount}>{wireTransferCountryContent.communication.name}</Text>
                                        <Text style={styles.wireTransferAccount}>{wireTransferCountryContent.communication.country}</Text>
                                    </View>
                                )}
                                {wireTransferCountryContent.contact && (
                                    <View>
                                        <Text style={[styles.subHeaderDark, {marginVertical: 15}]}>Contact</Text>
                                        <Text style={[styles.wireTransferAccount,{fontWeight: "800"}]}>{wireTransferCountryContent.contact.name}</Text>
                                        <Text style={styles.wireTransferAccount}>{wireTransferCountryContent.contact.email}</Text>
                                        <Text style={styles.wireTransferAccount}>{wireTransferCountryContent.contact.phone}</Text>
                                    </View>
                                )}
                                <View style={[styles.spacer, {marginVertical: 5}]} />
                                <TouchableOpacity
                                    style={[styles.floatingButton, {backgroundColor: AppStyles.colorSet.mainThemeForegroundColor}]}
                                    onPress={() => goToDonationValidationButton()}
                                >
                                    <Text style={styles.floatingButtonText}>VALIDATE DONATION</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </ScrollView>
            </KeyboardAwareView>
        </View>
    );
}

wireTransferInfo.propTypes = {
    item: PropTypes.object,
};

export default wireTransferInfo;
