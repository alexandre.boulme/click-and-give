import React, { useRef, useState, useEffect } from 'react';
import {
  View,
  FlatList,
  Text,
  ActivityIndicator,
  ScrollView
} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import ImagePicker from 'react-native-image-picker';
import { TNStoryItem } from '../../../Core/truly-native';
import ProjectLightFeedItem from '../../ProjectLightFeedItem/ProjectLightFeedItem';
import ProjectFeedItem from '../../ProjectFeedItem/ProjectFeedItem';
import ProfileButton from './ProfileButton';
import dynamicStyles from './styles';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import { TNEmptyStateView } from '../../../Core/truly-native';
import AppStyles from '../../../AppStyles';
import {TNActivityIndicator, TNMediaViewerModal} from '../../../Core/truly-native';

function Profile(props) {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const {
    onChatButtonPress,
    feed,
    donations,
    onMainButtonPress,
    user,
    authUser,
    mainButtonTitle,
    isMediaViewerOpen,
    feedItems,
    onMediaClose,
    selectedMediaIndex,
    startUpload,
    uploadProgress,
    loading,
    isOtherUser,
    onEmptyStatePress,
    onSubButtonTitlePress,
    subButtonTitle,
    displaySubButton,
    onFeedItemPress,
    editPictureButton
  } = props;

  const [backedProjectCount, setBackedProjectCount] = useState(0);
  const [backedProjects, setBackedProjects] = useState([]);
  const [createdProjects, setCreatedProjects] = useState([]);

  useEffect(() => {
    console.log("inside the useEffect");
    if (donations && feed) {
      console.log("updatingbacked project ");
      setBackedProjects(filterBackedProject(feed, donations));
    }
  }, [feed, donations]);

  useEffect(() => {
    if (donations && feed) {
      setCreatedProjects(filterCreatedProject(feed));
    }
  }, [feed]);

  const filterBackedProject = (feed, donations) => {
    var backedProjectsFiltered = [];

    feed.map(project => {
      project.donationPending = null;
      project.donationPending = 0;
    });

    donations.filter(donation => {
      if (donation && donation.author && donation.author.id == user.id && donation.status != "Canceled" && (!donation.anonymous || !isOtherUser)){
        const projectBacked = feed.find(project => {
          if(project.id == donation.projectId){
            if(donation.status == "Pending" && !project.donationPending){
              project.donationWaiting = 1;
              project.donationPending = donation.id;
            } else if (donation.status == "Pending" && project.donationPending != donation.id){
              project.donationWaiting += 1;
            }
            return project;
          }
        });

        console.log(projectBacked);

        //prevents duplicates
        if(projectBacked && backedProjectsFiltered.indexOf(projectBacked) == -1){
          backedProjectsFiltered.push(projectBacked)
        }
      }
    });

    setBackedProjectCount(backedProjectsFiltered.length);

    return backedProjectsFiltered;
  };

  const filterCreatedProject = feed => {

    var createdProjectFiltered = feed.filter(project => {
      if (project.author && project.author.id == user.id && !project.refused)
        return project;
    });

    return createdProjectFiltered;
  };

  const onProfilePicturePress = () => {
    if (isOtherUser) {
      return;
    }
    onPressAddPhotoBtn();
  };

  const onPressAddPhotoBtn = () => {
    const options = {
      title: IMLocalized('Update profile picture'),
      cancelButtonTitle: IMLocalized('Cancel'),
      takePhotoButtonTitle: IMLocalized('Take Photo'),
      chooseFromLibraryButtonTitle: IMLocalized('Choose from Library'),
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = response.uri;
        startUpload(source);
      }
    });
  };

  const renderBackedProject = ({item, index}) => {

    return (
      <ProjectLightFeedItem
        item={item}
        onItemPress={onFeedItemPress}
        containerStyle={styles.setShadows}
        displayValidationTag={user.id == authUser.id && item.donationPending}
      />
    );
  }

  const renderCreatedProject = ({item, index}) => {

    return (
      <ProjectFeedItem
        item={item}
        onItemPress={onFeedItemPress}
        onUserItemPress={() => {}}
        displayWaitingApprovalTag={item.author.id == authUser.id && !item.approved}
      />
    );

  }

  const renderListHeader = () => {
    return (
      <View style={styles.subContainer}>
        <ProfileButton title={mainButtonTitle} onPress={onMainButtonPress} />
        {displaySubButton && (
          <ProfileButton
            title={subButtonTitle}
            onPress={onSubButtonTitlePress}
            disabled={!displaySubButton}
            containerStyle={[
              styles.subButtonColor,
              {marginTop: 0},
              loading && {display: 'none'},
            ]}
            titleStyle={styles.titleStyleColor}
          />
        )}

        {loading && (
          <View style={styles.container}>
            <ActivityIndicator
              style={{ marginTop: 15, alignSelf: 'center' }}
              size="large"
            />
          </View>
        )}
      </View>
    );
  };

  const renderBackedProjectEmptyComponent = () => {
    var emptyStateConfig = {
      description: IMLocalized("Make your first donation right now !"),
      buttonName: IMLocalized("Make a donation"),
      onPress: onEmptyStatePress,
    };

    return (
      <TNEmptyStateView
        emptyStateConfig={emptyStateConfig}
        appStyles={AppStyles}
      />
    );
  }

  const renderCreatedProjectEmptyComponent = () => {
    var emptyStateConfig = {
      description: IMLocalized("Fundraise for your first project right now !"),
      buttonName: IMLocalized("Create fundraising"),
      onPress: onEmptyStatePress,
    };

    return (
      <View>
        { loading  ? (
          <View style={[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>
            <ActivityIndicator
              style={{ marginTop: 50, alignSelf: 'center' }}
              size="small"
            />
          </View>
        ) : (
          <View>
            <TNEmptyStateView
              emptyStateConfig={emptyStateConfig}
              appStyles={AppStyles}
            />
          </View>
        )}
      </View>
    );
  }


  return (
    <View style={styles.container}>
      <TNStoryItem
        item={user}
        imageStyle={styles.userImage}
        imageContainerStyle={styles.userImageContainer}
        containerStyle={styles.userImageMainContainer}
        activeOpacity={1}
        title={true}
        editPictureButton={editPictureButton}
        onPress={onProfilePicturePress}
        textStyle={styles.userName}
        appStyles={AppStyles}
        isProfile={true}
        isOtherUser={isOtherUser}
        backedProjectCount={backedProjectCount}
        onChatButtonPress={onChatButtonPress}
      />
      <View style={[styles.progressBar, { width: `${uploadProgress}%` }]} />
      <ScrollView>
        <Text style={[styles.userName, styles.titleText, {color: "#000"}]}>Projects Backed</Text>
        {backedProjects.length == 0 && (
          renderBackedProjectEmptyComponent()
        )}
        { loading && backedProjects.lenght != 0 ? (
          <View style={[styles.container, , {alignItems: 'center', justifyContent: 'center'}]}>
            <ActivityIndicator
              style={{ marginTop: 50, alignSelf: 'center' }}
              size="small"
            />
          </View>
        ) : (
          <FlatList
              data={backedProjects}
              renderItem={renderBackedProject}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              />
        )}
        <Text style={[styles.userName, styles.titleText, {color: "#000", marginBottom: 15}]}>Projects Created</Text>
          <FlatList
            data={createdProjects}
            renderItem={renderCreatedProject}
            onEndReachedThreshold={0.5}
            horizontal={false}
            ListEmptyComponent={renderCreatedProjectEmptyComponent}
            showsVerticalScrollIndicator={false}
          />
      </ScrollView>
      {false  && <TNActivityIndicator appStyles={AppStyles} />}
      <TNMediaViewerModal
        mediaItems={feedItems}
        isModalOpen={isMediaViewerOpen}
        onClosed={onMediaClose}
        selectedMediaIndex={selectedMediaIndex}
      />
    </View>
  );
}

Profile.propTypes = {
  onCommentPress: PropTypes.func,
  startUpload: PropTypes.func,
  removePhoto: PropTypes.func,
  onMainButtonPress: PropTypes.func,
  onSubButtonTitlePress: PropTypes.func,
  onFriendItemPress: PropTypes.func,
  onFeedUserItemPress: PropTypes.func,
  onFeedItemPress: PropTypes.func,
  user: PropTypes.object,
  friends: PropTypes.array,
  mainButtonTitle: PropTypes.string,
  subButtonTitle: PropTypes.string,
  feedItems: PropTypes.array,
  onMediaClose: PropTypes.func,
  isMediaViewerOpen: PropTypes.bool,
  onMediaPress: PropTypes.func,
  displaySubButton: PropTypes.bool,
  selectedMediaIndex: PropTypes.number,
  uploadProgress: PropTypes.number,
};

export default Profile;
