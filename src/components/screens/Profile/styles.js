import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

// const imageContainerWidth = 66;
const imageWidth = 110;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  progressBar: {
    backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
    height: 3,
    shadowColor: '#000',
    width: 0,
  },
  subContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop:0
  },
  userImage: {
    ...ifIphoneX({
      width: imageWidth,
      height: imageWidth,
    },{
      width: imageWidth*0.85,
      height: imageWidth*0.85,
    }),
    borderRadius: Math.floor(imageWidth / 2),
    borderWidth: 0,
  },
  userImageContainer: {
    ...ifIphoneX({
      width: imageWidth,
      height: imageWidth,
      marginTop: 18,
    },{
      width: imageWidth*0.85,
      height: imageWidth*0.85,
      marginTop: 13,
    }),
    borderWidth: 0,
  },
  userImageMainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  userName: {
    ...ifIphoneX({
      paddingTop: 15,
    },{
      paddingTop: 10,
    }),
    fontSize: AppStyles.fontSet.xlarge,
    textAlign: 'center',
    fontWeight: '800',
    color: AppStyles.colorSet.mainTextColor,
  },
  profileSettingsButtonContainer: {
    width: '92%',
    height: 40,
    borderRadius: 8,
    backgroundColor: AppStyles.colorSet.mainButtonColor,
    marginVertical: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileSettingsTitle: {
    color: AppStyles.colorSet.mainThemeForegroundColor,
    fontSize: 13,
    fontWeight: '600',
  },
  subButtonColor: {
    backgroundColor: AppStyles.colorSet.subButtonColor
  },
  titleStyleColor: {color: AppStyles.colorSet.mainTextColor},
  titleText: {
    fontSize: AppStyles.fontSet.xlarge,
    textAlign: 'left',
    marginLeft: 20,
    color: AppStyles.colorSet.mainSubtextColor
  },
  setShadows: {
    shadowOffset:{  width: 0,  height: 3,  },
    shadowColor: "#CCC",
    shadowOpacity: 1.0,
    margin: 10
  }
});

export default dynamicStyles;
