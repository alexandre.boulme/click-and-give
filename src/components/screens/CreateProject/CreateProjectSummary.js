import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FastImage from 'react-native-fast-image';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import TNActivityIndicator from '../../../Core/truly-native/TNActivityIndicator';
import numeral from 'numeral';

function CreateProjectSummary(props) {
  const {
    loading,
    projectDetails,
    onValidateButtonPress,
    onModifyButtonPress
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  useEffect(() => {
    console.log(projectDetails);
  },[]);

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView style={{ flex: 1, width: '100%'}} keyboardShouldPersistTaps='always'>
        <View style={[styles.rowContainer, {flexDirection: 'row', justifyContent: 'space-between'}]}>
        <Text style={styles.title}>{IMLocalized('Summary')}</Text>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center', marginRight: 35}}
          onPress={() => onModifyButtonPress(projectDetails)}
        >
          <Image source={AppStyles.iconSet.edit} style={{tintColor: "#000", width: 22, height: 22}}/>
          <Text style={{marginLeft: 5,fontSize: 20, color:"#000"}}>{IMLocalized('Modify')}</Text>
        </TouchableOpacity>
        </View>
        <View style={{padding: 35}}>
          {projectDetails.highlighted && (
            <View>
              <View style={{width: '30%',padding: 5, backgroundColor: AppStyles.colorSet.mainThemeForegroundColor, borderRadius: 5}}>
                <Text style={{fontSize: AppStyles.fontSet.xsmall, color: AppStyles.colorSet.white, fontWeight: '800', textAlign: 'center'}}>{IMLocalized('Highlighted').toUpperCase()}</Text>
              </View>
            </View>
          )}
          <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'space-between'}}>
              <Text style={styles.projectTitle}>{projectDetails.name}</Text>
              {projectDetails.type === "foundation" && (
                <Image source={AppStyles.iconSet.foundation} style={{width: 30, height: 30, marginLeft: 15, alignSelf: 'center', marginTop: 15}}/>
              )}
            </View>
            <View style={styles.imageSummaryContainer}>
              <FastImage
                style={styles.imageSummaryStyle}
                source={{uri: projectDetails.projectPictureURL }}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
          </View>
          <Text style={styles.normalText}>{projectDetails.description}</Text>
          <View style={[styles.rowContainer, {flexDirection: 'row',justifyContent: 'space-between'}]}>
            {projectDetails.donationPlatformUrl && (
                <View>
                  <Text style={[styles.subTitle,{marginBottom: 3}]}>{IMLocalized('Donation page url')}</Text>
                  <Text style={styles.normalText}>{projectDetails.donationPlatformUrl}</Text>
                </View>
            )}
          </View>
          <View style={[styles.rowContainer, {flexDirection: 'row',justifyContent: 'space-between'}]}>
          {projectDetails.needFund && (
            <View>
              <Text style={styles.subTitle}>{IMLocalized('Target')}</Text>
              <Text style={styles.normalText}>{projectDetails.currency.sign} {numeral(projectDetails.fundRaisingTarget).format('0,0')}</Text>
            </View>
          )}
            <View style={{width: '65%'}}>
              <Text style={[styles.subTitle, {color: "#000", fontSize: 20, fontWeight: '700', marginTop: 25, marginBottom: 10}]}>{IMLocalized('Category')}</Text>
              <View style={{flexDirection: 'row', flexWrap: 'wrap', marginRight: 10}}>
                <Text style={[styles.normalText, {color: "#000", padding: 2 }]}>
                {projectDetails.categories.map((item, i) => {
                    return i > 0 ? ', ' + item : item;
                  })}
                </Text>
              </View>
            </View>
          </View>
          {projectDetails.needVolunteer && (
            <View>
              <View style={styles.rowContainer}>
                <View>
                  <Text style={styles.subTitle}>{IMLocalized('Volunteer needed')}</Text>
                  <Text style={styles.normalText}>{projectDetails.volunteerTarget}</Text>
                </View>
                <View style={{marginLeft: 35}}>
                  <Text style={[styles.subTitle]}>{IMLocalized('Frequency')}</Text>
                  <Text style={styles.normalText}>{projectDetails.volunteerFrequency}</Text>
                </View>
              </View>
              <Text style={styles.subTitle}>{IMLocalized('How can volunteer engage ?')}</Text>
              <Text style={styles.normalText}>{projectDetails.volunteerInstruction}</Text>
            </View>
          )}
          {projectDetails.type === "foundation" && (
            <View>
              <Text style={styles.subTitle}>{IMLocalized('Foundation engagement')}</Text>
              <Text style={styles.normalText}>{projectDetails.foundationEngagement}</Text>
            </View>
          )}
        </View>
        <TouchableOpacity
          style={styles.buttonFullContainer}
          onPress={() => onValidateButtonPress()}
        >
          <Text style={styles.buttonText}>{projectDetails.createdAt ? IMLocalized('Save project changes') : IMLocalized('Publish my fundraising')}</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
      {loading && (
        <TNActivityIndicator appStyles={AppStyles} />
      )}
    </View>
  );
}

CreateProjectSummary.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object,
  onModifyButtonPress: PropTypes.func,
  onValidateButtonPress: PropTypes.func
};

export default CreateProjectSummary;
