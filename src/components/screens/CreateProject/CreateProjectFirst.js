import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  Switch
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Container, Content, Icon, Picker, Form, } from "native-base";
import TNProjectPictureSelector from '../../../Core/truly-native/TNProjectPictureSelector/TNProjectPictureSelector';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import DatePicker from 'react-native-datepicker';

function CreateProjectFirst(props) {
  const {
    user,
    project,
    onNextButtonPress
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  const [name, setName] = useState(project.name ? project.name : '');
  const [description, setDescription] = useState(project.description ? project.description : '');
  const [disableEndDate, setDisableEndDate] = useState(project.disableEndDate ? project.disableEndDate : false);
  const [endDate, setEndDate] = useState(project.endDate && project.endDate.seconds ? new Date(project.endDate.seconds * 1000 + project.endDate.nanoseconds / 1000000): new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
  const [timeText, setTimeText] = useState(project.timeText ? project.timeText : '');
  const [projectPictureURL, setProjectPictureURL] = useState(project.projectPictureURL ? project.projectPictureURL : '');
  const [place, setPlace] = useState(project.cityLocation ? project.cityLocation.city : '');
  const [supportArdianFoundation, setSupportArdianFoundation] = useState(project.supportArdianFoundation ? project.supportArdianFoundation : false);
  const [cityLocation, setCityLocation] = useState(project.cityLocation ? project.cityLocation : '');

  const [city, setCity] = useState(place ? place.city : '');
  const [country, setCountry] = useState(project.cityLocation && project.cityLocation.country ? project.cityLocation.country : '');
  const [region, setRegion] = useState(project.cityLocation && project.cityLocation.region ? project.cityLocation.region :'');

  const cityList = ClickAndGiveConfig.cityLocationList;
  const regionList = ClickAndGiveConfig.regionList;
  var radio_props = [
    {label: 'Employee project', value: false },
    {label: 'Ardian Foundation project', value: true }
  ];

  const checkProjectInfoNotEmpty = () => {
    if (name.trim() == ""){
      Alert.alert(IMLocalized('Error'), IMLocalized('Please choose a name for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (description.trim() == "") {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please write a description for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (projectPictureURL.trim() == "") {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please set a picture for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (!disableEndDate && endDate == "") {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please choose a different end date for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (cityLocation == "" || place == "Other location" && (city == "" || country == "" || region == "")) {
      console.log(cityLocation);
      if (cityLocation == ""){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please choose a location for your project.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      } else if (place == "Other location"  && (city == "" || country == "" || region == "")){
        Alert.alert(IMLocalized('Error'), IMLocalized('You must set a city, country and a location area for your custom location.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      }
      return false;
    } else {
      return true;
    }
  };

  const onButtonPress = () => {

    console.log(project.endDate);

    if (cityLocation.city == "Other location"){
      setCityLocation({city: city, country: country, region: region})
    }

    console.log("INIT PROJECT : ",project);

    const projectInit = project ? project : {needFund: false, needVolunteer: false, requestMatching: false, displayVolunteerProgressBarOnList: false, highlighted: false, };

    const projectDetails = {
      ...projectInit,
      name,
      description,
      endDate: new Date(endDate),
      disableEndDate,
      timeText,
      projectPictureURL,
      cityLocation: cityLocation,
      supportArdianFoundation: supportArdianFoundation,
    };

    console.log("PROJECT DETAILS : ",projectDetails);

    if (checkProjectInfoNotEmpty())
      onNextButtonPress(user, projectDetails);
  };

  const onPickerValueChange = value => {
      cityList.filter(item => {
        if (item.city === value)
          setCityLocation(item);
      });
      setPlace(value);
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%' }}
          keyboardShouldPersistTaps='always'
          enableResetScrollToCoords={false}>
        <Text style={styles.title}>{IMLocalized('Project Identity')}</Text>
        <View style={styles.spacer}>
        <Text style={styles.subTitle}>{IMLocalized('Project\'s name')}</Text>
        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('ex: Coastal Cleaning')}
          placeholderTextColor="#CCC"
          onSubmitEditing={() => { this.descriptionInput.focus(); }}
          returnKeyType = { "next" }
          onChangeText={text => setName(text)}
          value={name}
          underlineColorAndroid="transparent"
        />
        <Text style={styles.subTitle}>{IMLocalized('Description')}</Text>
        <TextInput
          ref={(input) => { this.descriptionInput = input; }}
          style={[styles.InputContainer,{height: '10%'}]}
          returnKeyType = { "default" }
          placeholder={IMLocalized('Write a few words about the project')}
          placeholderTextColor="#CCC"
          onChangeText={text => setDescription(text)}
          value={description}
          maxLength={1000}
          multiline={true}
          underlineColorAndroid="transparent"
        />
        <Text style={styles.textCount}>{description.length + IMLocalized('/1000')}</Text>
        <Text style={styles.subTitle}>{IMLocalized('Photo of the project')}</Text>
        <TNProjectPictureSelector
            projectPictureURL={project.projectPictureURL}
            setProfilePictureURL={setProjectPictureURL}
            appStyles={AppStyles}
          />
        <View style={{alignItems: 'flex-start'}}>
          <Text style={[styles.subTitle,{marginBottom: 10}]}>{IMLocalized('Project Type')}</Text>
        </View>
        <RadioForm
            radio_props={radio_props}
            initial={0}
            buttonColor={AppStyles.colorSet.mainThemeForegroundColor}
            selectedButtonColor={AppStyles.colorSet.mainThemeForegroundColor}
            onPress={(value) => {
              setSupportArdianFoundation(value);
            }}

        />
        {user.role == 'admin' && (
          <View style={styles.switchContainer}>
            <Text style={styles.subTitle}>{IMLocalized('Set a deadline')}</Text>
            <Switch
              style={styles.switchStyle}
              trackColor={{ false: "#CCCCCC50", true: "#F7375050" }}
              thumbColor={!disableEndDate ? AppStyles.colorSet.mainThemeForegroundColor : "#CCCCCC"}
              ios_backgroundColor="#FFF"
              onValueChange={() => setDisableEndDate(!disableEndDate)}
              value={!disableEndDate}
            />
          </View>
        )}
        {disableEndDate ? (
          <View>
            <Text style={styles.subTitle}>{IMLocalized('Text replacing endDate')}</Text>
            <TextInput
              style={styles.InputContainer}
              placeholder={IMLocalized('ex : Foundation support frequency')}
              placeholderTextColor="#CCC"
              returnKeyType = { "next" }
              onChangeText={text => setTimeText(text)}
              value={timeText}
              underlineColorAndroid="transparent"
            />
          </View>
        ) : (
          <View>
            <Text style={styles.subTitle}>{IMLocalized('End Date')}</Text>
            <View style={styles.pickerContainer}>
              <DatePicker
                style={{width: '100%'}}
                date={endDate}
                mode="date"
                showIcon={false}
                placeHolderText="Select end date"
                format="L"
                minDate={new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())}
                maxDate={new Date(2040, new Date().getMonth(), new Date().getDate())}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateInput: {
                    borderWidth: 0,
                    borderBottomWidth: 1,
                    borderBottomColor: AppStyles.colorSet.black,
                    alignItems: 'flex-start',
                    paddingLeft: '5%'
                  },
                  dateText: styles.pickerTextStyle,
                  btnTextConfirm: [styles.pickerTextStyle,{color: AppStyles.colorSet.mainThemeForegroundColor}],
                  btnTextCancel:styles.pickerTextStyle
                }}
                onDateChange={(newDate) => {
                  setEndDate(newDate);
                }}
              />
            </View>
          </View>
        )}
        <Text style={styles.subTitle}>{IMLocalized('Location')}</Text>
        <View style={styles.pickerContainer}>
          <Container>
            <Content>
              <Form style={{
             borderBottomWidth: 1,
             borderBottomColor: "#000"}}>
                <Picker
                  mode="dropdown"
                  iosHeader="Choose one"
                  headerBackButtonTextStyle={{ color: AppStyles.colorSet.mainThemeForegroundColor }}
                  placeholder="Select your city"
                  iosIcon={<Icon name="chevron-down-sharp" style={styles.pickerIcon}/>}
                  placeholderStyle={styles.placeHolderTextStyle}
                  textStyle={styles.pickerTextStyle}
                  itemStyle={styles.pickerItemStyle}
                  itemTextStyle={styles.pickerTextStyle}
                  selectedValue={place}
                  onValueChange={value => onPickerValueChange(value)}
                >
                  {
                    cityList.map((item, i) => (
                      <Picker.Item label={item.country ? (item.city + ", " + item.country) : (item.city)} value={item.city} />
                    ))
                  }
                </Picker>
              </Form>
            </Content>
          </Container >
        </View>
        {place == "Other location" && (
          <View>
            <View style={[styles.rowContainer, {justifyContent: 'space-between'}]}>
              <View style={{width: '45%'}}>
                <Text style={styles.subTitle}>{IMLocalized('City')}</Text>
                <TextInput
                  style={styles.InputContainer}
                  autofocus={true}
                  placeholder={IMLocalized('ex: Paris')}
                  placeholderTextColor="#CCC"
                  onSubmitEditing={() => { this.country.focus(); }}
                  returnKeyType = { "next" }
                  onChangeText={text => setCity(text)}
                  value={city}
                  underlineColorAndroid="transparent"
                />
              </View>
              <View style={{width: '45%'}}>
                <Text style={styles.subTitle}>{IMLocalized('Country')}</Text>
                <TextInput
                  ref={(input) => { this.country = input; }}
                  style={styles.InputContainer}
                  placeholder={IMLocalized('ex: France')}
                  placeholderTextColor="#CCC"
                  onSubmitEditing={() => { this.country.blur(); }}
                  returnKeyType = { "next" }
                  onChangeText={text => setCountry(text)}
                  value={country}
                  underlineColorAndroid="transparent"
                />
              </View>
            </View>
            <Text style={styles.subTitle}>{IMLocalized('Area')}</Text>
            <View style={styles.pickerContainer}>
              <Container>
                <Content>
                  <Form style={{
                 borderBottomWidth: 1,
                 borderBottomColor: "#000"}}>
                    <Picker
                      mode="dropdown"
                      placeholder="Select the corresponding location area"
                      iosIcon={<Icon name="chevron-down-sharp" style={styles.pickerIcon}/>}
                      placeholderStyle={styles.placeHolderTextStyle}
                      textStyle={styles.pickerTextStyle}
                      itemStyle={styles.pickerItemStyle}
                      itemTextStyle={styles.pickerTextStyle}
                      selectedValue={region}
                      onValueChange={value => setRegion(value)}
                    >
                      {
                        regionList.map((item, i) => (
                          <Picker.Item label={item} value={item} />
                        ))
                      }
                    </Picker>
                  </Form>
                </Content>
              </Container >
            </View>
          </View>
        )}
        </View>
        <TouchableOpacity
          style={[styles.buttonContainer,{marginBottom: 130}]}
          onPress={() => onButtonPress()}
        >
          <Text style={styles.buttonText}>Next</Text>
          <Image source={AppStyles.iconSet.rightArrow} style={styles.buttonImage}/>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
}

CreateProjectFirst.propTypes = {
  user: PropTypes.object,
  onPostDidChange: PropTypes.func,
  onSetMedia: PropTypes.func,
  onLocationDidChange: PropTypes.func,
  blurInput: PropTypes.func,
  inputRef: PropTypes.any,
};

export default CreateProjectFirst;
