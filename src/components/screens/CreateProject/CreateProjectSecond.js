import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  Switch
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Container, Content, Icon, Picker, Form } from "native-base";
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import CategoryRadioButtonSelector from './components/CategoryRadioButtonSelector';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import numeral from 'numeral';
import RadioForm from "react-native-simple-radio-button";

function CreateProjectSecond(props) {
  const {
    user,
    projectDetails,
    onNextButtonPress
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));
  const currencyList = ClickAndGiveConfig.currencyList;

  const [needFund, setNeedFund] = useState(projectDetails.needFund);
  const [fundRaisingAmount, setFundRaisingAmount] = useState(projectDetails.fundRaisingTarget ? projectDetails.fundRaisingTarget : 0);
  const [fundraisingCurrency, setFundraisingCurrency] = useState(projectDetails.currency ? currencyList[currencyList.findIndex(function(item, i){return item.name === projectDetails.currency.name})] : null);
  const [needVolunteer, setNeedVolunteer] = useState(projectDetails.needVolunteer);
  const [volunteerTarget, setVolunteerTarget] = useState(projectDetails.volunteerTarget ? projectDetails.volunteerTarget.toString() : 0);
  const [volunteerFrequency, setVolunteerFrequency] = useState(projectDetails.volunteerFrequency ? projectDetails.volunteerFrequency : '');
  const [volunteerInstruction, setVolunteerInstruction] = useState(projectDetails.volunteerInstruction ? projectDetails.volunteerInstruction : '');
  const [requestMatching, setRequestMatching] = useState(projectDetails.requestMatching);
  const [donationPlatformUrl, setDonationPlatformUrl] = useState(projectDetails.donationPlatformUrl ? projectDetails.donationPlatformUrl : null);
  const [displayVolunteerProgressBar, setDisplayVolunteerProgressBar] = useState(projectDetails.displayVolunteerProgressBarOnList);
  const [projectInterestList, setProjectInterestList] = useState(projectDetails.categories ? projectDetails.categories : []);
  const [categoryOfInterestList, setCategoryOfInterestList] = useState(ClickAndGiveConfig.categoryOfInterestNames);
  const volunteerFrequencyList = ClickAndGiveConfig.projectVolunteerFrequency;

  var radio_props = [
    {label: 'Money', value: false },
    {label: 'Time', value: true }
  ];

  const onButtonPress = () => {

    if(checkProjectInfoNotEmpty(projectInterestList)){

      var projectAllInfos = {
        ...projectDetails,
        categories: projectInterestList,
        donationPlatformUrl: donationPlatformUrl,
        needFund: needFund,
        fundRaisingTarget: fundRaisingAmount,
        currency: fundraisingCurrency,
        needVolunteer: needVolunteer,
        volunteerFrequency: volunteerFrequency,
        volunteerTarget: volunteerTarget,
        volunteerInstruction: volunteerInstruction,
        displayVolunteerProgressBarOnList: displayVolunteerProgressBar,
        requestMatching: requestMatching
      };

      if(user.role != 'admin'){
        projectAllInfos.type =  'employee';
      }

      console.log("PROJECT : ", projectAllInfos);

      onNextButtonPress(user, projectAllInfos);
    }

  };

  const switchCategorySelectionStatus = (item, index) => {

    if (index > -1){
      projectInterestList.splice(index, 1);
    } else {
      projectInterestList.push(item);
    }

  };


  const checkProjectInfoNotEmpty = selectedCategory => {
    if (selectedCategory.length == 0) {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please choose at least one category for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (!needFund && !needVolunteer){
      Alert.alert(IMLocalized('Error'), IMLocalized('Please choose at least one type of need.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (needFund && (fundRaisingAmount == "" || fundRaisingAmount == 0)) {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please set a fundraising target for your project.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (needFund && (fundraisingCurrency == "")) {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please select a currency for your fundraising.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else if (needVolunteer && ((volunteerTarget == "" || volunteerTarget == 0) || volunteerFrequency.trim() == "" || volunteerInstruction.trim() == "")) {
      if (volunteerInstruction.trim() == ""){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please write volunterring instruction for your project.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      } else if (volunteerFrequency.trim() == ""){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please choose an approximate frequency of volunteering for your project.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      } else if(volunteerTarget == "" || volunteerTarget == 0){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please set the target volunteer number for your project.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      } else if(isFoundationProject && foundationEngagement == ""){
        Alert.alert(IMLocalized('Error'), IMLocalized('Please fill Foundation engagement description field.'), [{ text: IMLocalized('OK') }], {
          cancelable: false,
        });
      }
      return false;
    } else if( !projectDetails.supportArdianFoundation && needFund && (donationPlatformUrl == null || !donationPlatformUrl.toLowerCase().includes("https://"))){
      Alert.alert(IMLocalized('Error'), IMLocalized('Please make sure to use a "https" URL address.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else {
      return true;
    }
  };

  const onPickerValueChange = value => {
      setFundraisingCurrency(value);
  };

  return (
    <View style={[styles.container,]}>
      <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%', padding: 15}}
          keyboardShouldPersistTaps='always'
          enableResetScrollToCoords={false}>
        <Text style={styles.title}>{IMLocalized('Details')}</Text>
        <Text style={styles.subTitle}>{IMLocalized('Project\'s Category')}</Text>
        {
          categoryOfInterestList.map((item, i) => (
            <CategoryRadioButtonSelector
              title={item}
              selectedInterest={projectInterestList}
              selected={(projectInterestList.indexOf(item) > -1)}
              onPress={() => switchCategorySelectionStatus(item, projectInterestList.indexOf(item))}
            />
          ))
        }
        <View style={[styles.rowContainer,{alignItems: 'flex-end'}]}>
          <Text style={styles.subTitle}>{IMLocalized('Type of need ')}</Text>
          <Text style={styles.smallText}>{IMLocalized('(You can choose both)')}</Text>
        </View>
        <View style={styles.rowCenterContainer}>
          <TouchableOpacity
            onPress={() =>{
              setNeedFund(!needFund);
            }}
            style={needFund ? styles.categoryContainerSelected45 : styles.categoryContainer45}
          >
            <Text style={needFund ? styles.selectorTextSelected : styles.selectorText}>Donations</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>{
              setNeedVolunteer(!needVolunteer)
            }}
            style={needVolunteer ? styles.categoryContainerSelected45 : styles.categoryContainer45}
          >
            <Text style={needVolunteer ? styles.selectorTextSelected : styles.selectorText}>Volunteers</Text>
          </TouchableOpacity>
        </View>
        {needFund && (
          <View>
            <View style={[styles.rowContainer,{alignItems: 'flex-end'}]}>
              <Text style={styles.subTitle}>{IMLocalized('Fundraising target')}</Text>
              <Text style={styles.smallText}>{IMLocalized(' (Pre-matching)')}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={[{width: '30%', alignItems: 'center', alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: "#000"}]}>
                <Picker
                  theme={{primaryColor: "#222"}}
                  mode="dropdown"
                  headerBackButtonTextStyle={{ color: AppStyles.colorSet.mainThemeForegroundColor }}
                  placeholder="Currency"
                  iosIcon={<Icon name="chevron-down-sharp" style={styles.pickerIcon}/>}
                  placeholderStyle={[styles.placeHolderTextStyle, {marginLeft: 10, paddingRight: 0}]}
                  textStyle={styles.pickerTextStyle}
                  itemStyle={styles.pickerItemStyle}
                  itemTextStyle={styles.pickerTextStyle}
                  selectedValue={fundraisingCurrency}
                  onValueChange={value => onPickerValueChange(value)}
                >
                  {
                    currencyList.map((item, i) => (
                      <Picker.Item label={item.sign} value={item} />
                    ))
                  }
                </Picker>
              </View>
              <TextInput
                style={[styles.InputContainer, {width: '60%'}]}
                placeholder={IMLocalized('ex : 10000')}
                placeholderTextColor="#aaaaaa"
                onChangeText={text => setFundRaisingAmount(text.replace(/[^0-9]/g, ''))}
                value={fundRaisingAmount}
                keyboardType='numeric'
                underlineColorAndroid="transparent"
              />
            </View>
            {!projectDetails.supportArdianFoundation && (
                <View>
                  <Text style={[styles.subTitle,{marginBottom: 3}]}>{IMLocalized('Donation page url')}</Text>
                  <TextInput
                      style={styles.InputContainer}
                      placeholder={IMLocalized('Fundraising page URL')}
                      placeholderTextColor="#aaaaaa"
                      onChangeText={text => setDonationPlatformUrl(text)}
                      value={donationPlatformUrl}
                      underlineColorAndroid="transparent"
                  />
                </View>
            )}
            {projectDetails.notifications.length > 0 && (
                <View style={[styles.rowContainer, {justifyContent: 'space-between', alignItems: 'center'}]}>
                  <View style={{alignItems: 'flex-start'}}>
                    <Text style={[styles.subTitle,{marginBottom: 3}]}>{IMLocalized('Request Ardian Foundation Matching')}</Text>
                    <Text style={[styles.smallText,{textAlign: 'left'}]}>{IMLocalized('A request will be sent to administrators')}</Text>
                  </View>
                  <Switch
                      style={{marginTop: '5%', marginRight: '5%'}}
                      trackColor={{ false: "#CCCCCC50", true: "#F7375050" }}
                      thumbColor={requestMatching ? AppStyles.colorSet.mainThemeForegroundColor : "#CCCCCC"}
                      ios_backgroundColor="#FFF"
                      onValueChange={() => setRequestMatching(!requestMatching)}
                      value={requestMatching}
                  />
                </View>
            )}
          </View>
        )}
        {needVolunteer && (
          <View>
            <Text style={styles.subTitle}>{IMLocalized('How can volunteers engage ?')}</Text>
            <TextInput
              style={[styles.InputContainer,{height: 82}]}
              placeholder={IMLocalized('Describe engagement')}
              placeholderTextColor="#aaaaaa"
              multiline={true}
              onChangeText={text => setVolunteerInstruction(text)}
              value={volunteerInstruction}
              underlineColorAndroid="transparent"
            />
            <Text style={styles.subTitle}>{IMLocalized('Frequency of volunteering ?')}</Text>
            <View style={styles.pickerContainer}>
              <Container>
                <Content>
                  <Form style={{borderBottomWidth: 1, borderBottomColor: "#000"}}>
                    <Picker
                      mode="dropdown"
                      headerBackButtonTextStyle={{ color: AppStyles.colorSet.mainThemeForegroundColor }}
                      placeholder="ex: Once a month"
                      iosIcon={<Icon name="chevron-down-sharp" style={styles.pickerIcon}/>}
                      placeholderStyle={styles.placeHolderTextStyle}
                      textStyle={styles.pickerTextStyle}
                      itemStyle={styles.pickerItemStyle}
                      itemTextStyle={styles.pickerTextStyle}
                      selectedValue={volunteerFrequency}
                      onValueChange={value => {
                        console.log(volunteerFrequency);
                        console.log(value);
                        setVolunteerFrequency(value);
                      }}
                    >
                      {
                        volunteerFrequencyList.map((item, i) => (
                          <Picker.Item label={item.frequency} value={item.frequency} />
                        ))
                      }
                    </Picker>
                  </Form>
                </Content>
              </Container >
            </View>
            <Text style={styles.subTitle}>{IMLocalized('Number of volunteers needed ?')}</Text>
            <TextInput
              style={styles.InputContainer}
              placeholder={IMLocalized('ex : 3')}
              placeholderTextColor="#aaaaaa"
              onChangeText={text => setVolunteerTarget(text.replace(/[^0-9]/g, ''))}
              value={volunteerTarget}
              keyboardType='numeric'
              underlineColorAndroid="transparent"
            />
          </View>
        )}
        {needFund && needVolunteer && (
          <View>
            <View style={{alignItems: 'flex-start'}}>
              <Text style={[styles.subTitle, {marginBottom: 10}]}>{IMLocalized('Giving option shown on project\'s front page')}</Text>
              <Text style={[styles.smallText,]}>{IMLocalized('Both options will be displayed in the project description but only one will appear on the front page')}</Text>
            </View>
            <RadioForm
                radio_props={radio_props}
                initial={projectDetails.displayVolunteerProgressBarOnList ? 1 : 0}
                buttonColor={AppStyles.colorSet.mainThemeForegroundColor}
                selectedButtonColor={AppStyles.colorSet.mainThemeForegroundColor}
                onPress={(value) => {
                  setDisplayVolunteerProgressBar(value);
                }}
            />
          </View>
        )}
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => onButtonPress()}
        >
          <Text style={styles.buttonText}>Next</Text>
          <Image source={AppStyles.iconSet.rightArrow} style={styles.buttonImage}/>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
}

CreateProjectSecond.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object,
  onNextButtonPress: PropTypes.func
};

export default CreateProjectSecond;
