import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  Switch
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import { IMLocalized } from '../../../Core/localization/IMLocalization';

function CreateProjectAdmin(props) {
  const {
    user,
    projectDetails,
    onNextButtonPress
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  const [isHighLighted, setIsHighLighted] = useState(projectDetails.highlighted);
  const [isFoundationProject, setIsFoundationProject] = useState(projectDetails.type ? projectDetails.type == 'foundation' : false);
  const [foundationEngagement, setFoundationEngagement] = useState(projectDetails.foundationEngagement ? projectDetails.foundationEngagement : '');

  const onButtonPress = () => {

    if(checkProjectInfoNotEmpty()){

      var projectType = '';
      projectDetails.approved = true;

      const projectAllInfos = {
        ...projectDetails,
        highlighted: isHighLighted,
        type: isFoundationProject ? 'foundation' : 'employee',
        foundationEngagement: foundationEngagement,
        foundationMatching: isFoundationProject ? 1 : 0
      };

      onNextButtonPress(user, projectAllInfos);
    }

  };


  const checkProjectInfoNotEmpty = () => {

   if(isFoundationProject && foundationEngagement == "") {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please fill Foundation engagement description field.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
      return false;
    } else {
      return true;
    }
  };

  return (
    <View style={[styles.container,]}>
      <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%', padding: 15}}
          keyboardShouldPersistTaps='always'
          enableResetScrollToCoords={false}>
          <View>
            <Text style={styles.title}>{IMLocalized('Administrator parameters')}</Text>
            <View>
              <View style={styles.switchContainer}>
                <Text style={[styles.subTitle,]}>{IMLocalized('Highlight')}</Text>
                <Switch
                  style={styles.switchStyle}
                  trackColor={{ false: "#CCCCCC50", true: "#F7375050" }}
                  thumbColor={isHighLighted ? AppStyles.colorSet.mainThemeForegroundColor : "#CCCCCC"}
                  ios_backgroundColor="#FFF"
                  onValueChange={() => setIsHighLighted(!isHighLighted)}
                  value={isHighLighted}
                />
              </View>
              <View style={styles.switchContainer}>
                <Text style={[styles.subTitle,]}>{IMLocalized('Foundation project')}</Text>
                <Switch
                  style={styles.switchStyle}
                  trackColor={{ false: "#CCCCCC50", true: "#F7375050" }}
                  thumbColor={isFoundationProject ? AppStyles.colorSet.mainThemeForegroundColor : "#CCCCCC"}
                  ios_backgroundColor="#FFF"
                  onValueChange={() => setIsFoundationProject(!isFoundationProject)}
                  value={isFoundationProject}
                />
              </View>
            </View>
            {isFoundationProject &&(
              <View>
                <Text style={styles.subTitle}>{IMLocalized('Foundation engagement')}</Text>
                <TextInput
                  style={styles.InputContainer}
                  placeholder={IMLocalized('Describe foundation engagement')}
                  placeholderTextColor="#aaaaaa"
                  onChangeText={text => setFoundationEngagement(text)}
                  value={foundationEngagement}
                  underlineColorAndroid="transparent"
                />
              </View>
            )}
          </View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => onButtonPress()}
        >
          <Text style={styles.buttonText}>Next</Text>
          <Image source={AppStyles.iconSet.rightArrow} style={styles.buttonImage}/>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
}

CreateProjectAdmin.propTypes = {
  user: PropTypes.object,
  projectDetails: PropTypes.object,
  onNextButtonPress: PropTypes.func
};

export default CreateProjectAdmin;
