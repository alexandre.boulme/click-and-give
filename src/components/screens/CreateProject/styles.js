import { Dimensions, I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    pickerContainer:{
      height: 50,
      width: '100%',
      alignSelf: 'center'
    },
    pickerItemStyle:{
      backgroundColor: "#FFF",
      marginLeft: 0,
      paddingLeft: 20
    },
    pickerTextStyle:{
      color: "#000",
      fontWeight: '300',
      textAlign: 'left',
      fontSize: appStyles.fontSet.middleminus
    },
    headerTitle:{
      fontSize: appStyles.fontSet.middle,
      width: '80%',
      textAlign: 'center',
      marginTop: Platform.OS === 'ios' ? 50 : 20,
    },
    title: {
      fontSize: appStyles.fontSet.xlarge,
      fontWeight: 'bold',
      color: appStyles.colorSet.mainThemeForegroundColor,
      marginTop: 25,
      marginBottom: 10,
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 20,
    },
    spacer : {
      ...ifIphoneX({
        padding: 22,
      },{
        padding: 15,
      }),
      paddingBottom: 0
    },
    subTitle: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
      color: appStyles.colorSet.black,
      marginTop: 25,
      marginBottom: 15,
      alignSelf: 'stretch',
      textAlign: 'left',
    },
    textCount: {
      marginTop: 7,
      fontSize: appStyles.fontSet.normal,
      color: appStyles.colorSet.mainTextColor,
      textAlign: 'right'
    },
    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeForegroundColor,
    },
    loginContainer: {
      width: appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 30,
    },
    placeholder: {
      color: 'red',
    },
    placeHolderTextStyle: {
      color: "#ccc",
      fontSize: appStyles.fontSet.middle,
      fontWeight: '300',
    },
    datePickerPlaceholderStyle: {
      color: "#ccc",
      fontSize: appStyles.fontSet.middle,
      fontWeight: '300',
      marginLeft: 3
    },
    datePickerTextStyle: {
      color: appStyles.colorSet.mainThemeForegroundColor,
      fontSize: appStyles.fontSet.middle,
      fontWeight: '300'
    },
    pickerIcon: {
      color: "#CCC"
    },
    InputContainer: {
      height: 42,
      paddingLeft: 0,
      color: appStyles.colorSet.mainTextColor,
      width: '100%',
      alignSelf: 'stretch',
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      fontWeight: '300',
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    InputContainer50: {
      height: 42,
      paddingLeft: 0,
      color: appStyles.colorSet.mainTextColor,
      width: '40%',
      alignSelf: 'stretch',
      marginLeft: 35,
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    endEmailText: {
      color: appStyles.colorSet.mainThemeForegroundColor,
      width: '40%',
      alignSelf: 'stretch',
      marginTop: 10,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
    },
    signupContainer: {
      alignSelf: 'center',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      paddingTop: 25,
      paddingBottom: 25,
      marginTop: 50,
      width: '50%',
    },
    signupText: {
      color: appStyles.colorSet.mainThemeBackgroundColor,
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
    },
    image: {
      width: '100%',
      height: '100%',
    },
    imageBlock: {
      flex: 2,
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: '#006',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: 'hidden',
    },
    formContainer: {
      width: '100%',
      flex: 4,
      alignItems: 'center',
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },
    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d9d9d9',
      opacity: 0.8,
      zIndex: 2,
    },
    categoryOfInterest:{
      borderColor: "#999",
      borderWidth : 1,
      padding: 10,
      margin: 5,
      borderRadius: 5,
    },
    categoryOfInterestSelected:{
      borderColor: appStyles.colorSet.mainThemeForegroundColor,
      borderWidth : 1,
      padding: 10,
      margin: 5,
      borderRadius: 5,
    },
    categoryTextSelected:{
      color: appStyles.colorSet.mainThemeForegroundColor,
      fontSize: appStyles.fontSet.middle,
    },
    buttonContainer: {
      flexDirection: 'row',
      padding: 20,
      margin: 30,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: 40,
      alignSelf: 'flex-end',
      alignItems: 'center',
      width: '35%',
      justifyContent: 'center'
    },
    buttonFullContainer: {
      flexDirection: 'row',
      padding: 20,
      margin: 30,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: 40,
      alignSelf: 'center',
      alignItems: 'center',
      width: '90%',
      justifyContent: 'center'
    },
    buttonText:{
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeBackgroundColor,
      textAlign: 'center',
      fontWeight: '600'
    },
    buttonImage:{
      tintColor: appStyles.colorSet.mainThemeBackgroundColor,
      width: 30,
      height: 30,
      marginLeft: 10
    },
    rowContainer:{
      flexDirection: 'row'
    },
    projectTitle: {
      fontSize: appStyles.fontSet.xlarge,
      fontWeight: 'bold',
      color: appStyles.colorSet.mainTextColor,
      marginTop: 25,
      marginBottom: 10,
      alignSelf: 'stretch',
      textAlign: 'left',
      maxWidth: '50%',
      ...ifIphoneX({
        minWidth: 160
      },{
        minWidth: 120
      })
    },
    normalText: {
      fontSize: appStyles.fontSet.normal,
      color: appStyles.colorSet.black,
      marginBottom: 15,
      alignSelf: 'stretch',
      textAlign: 'left',
    },
    categoryContainer: {
      flexDirection: 'row',
      padding: 15,
      borderRadius: 10,
      borderColor: "#CCC",
      borderWidth: 1,
      margin: 5,
      width: '90%',
      alignSelf: 'center',
      alignItems: 'center'
    },
    categoryText: {
      color: appStyles.colorSet.mainTextColor,
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      marginLeft: 15,
    },
    categoryImage: {
      tintColor: appStyles.colorSet.mainTextColor,
      width: 30,
      height: 30
    },
    smallText:{
      fontSize: appStyles.fontSet.small,
      color: appStyles.colorSet.black,
      textAlign: 'left',
      fontWeight: '300',
      paddingBottom : 15
    },
    categoryContainer45: {
      flexDirection: 'row',
      padding: 15,
      borderRadius: 10,
      borderColor: "#CCC",
      borderWidth: 1,
      margin: 5,
      width: '41%',
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center'
    },
    categoryContainerSelected45: {
      flexDirection: 'row',
      padding: 15,
      borderRadius: 10,
      borderColor: appStyles.colorSet.mainThemeForegroundColor,
      borderWidth: 1,
      margin: 5,
      width: '41%',
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center'
    },
    selectorText: {
      color: appStyles.colorSet.mainTextColor,
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      marginLeft: 15,
      textAlign: 'center'
    },
    selectorTextSelected: {
      color: appStyles.colorSet.mainThemeForegroundColor,
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      marginLeft: 15,
      textAlign: 'center'
    },
    rowCenterContainer: {
      flexDirection: 'row',
      justifyContent: 'center'
    },
    currencyPickerTextStyle: {
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainTextColor,
      fontWeight: '300'
    },
    switchContainer:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin:0,
      alignItems: 'center',
    },
    imageSummaryContainer: {
      ...ifIphoneX({
        width: '40%'
      },{
        width: '30%'
      })
    },
    imageSummaryStyle: {
      borderRadius: 10,
      width: '100%',
      height: undefined,aspectRatio: 1,
    },
  })
};

export default dynamicStyles;
