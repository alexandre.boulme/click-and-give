import React, {useState} from 'react';
import {TouchableOpacity, Text, Image, View} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../../AppStyles';
import dynamicStyles from './styles';

const CategoryRadioButtonSelector = (props) => {

  const [selected, setSelected] = useState(props.selected);
  const {title, onPress } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  return (
      <View
          key={title}>
          {selected ? (
              <TouchableOpacity
                  onPress={() =>{
                      setSelected(!selected);
                      onPress();
                  }}
                  style={styles.categoryContainerSelected}
              >
                  <Image source={selected ? AppStyles.iconSet.radioButtonFilled : AppStyles.iconSet.radioButtonUnfilled} style={selected ? styles.categoryImageSelected : styles.categoryImage}/>
                  <Text style={styles.categoryText}>{title}</Text>
              </TouchableOpacity>
          ):(
              <TouchableOpacity
                  onPress={() =>{
                      setSelected(!selected);
                      onPress();
                  }}
                  style={styles.categoryContainer}
              >
                  <Image source={selected ? AppStyles.iconSet.radioButtonFilled : AppStyles.iconSet.radioButtonUnfilled} style={selected ? styles.categoryImageSelected : styles.categoryImage}/>
                  <Text style={styles.categoryText}>{title}</Text>
              </TouchableOpacity>
          )}
      </View>
  );

};

export default CategoryRadioButtonSelector;
