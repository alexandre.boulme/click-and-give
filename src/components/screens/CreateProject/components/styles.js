import { Dimensions, I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    categoryContainer: {
      flexDirection: 'row',
      padding: 15,
      borderRadius: 10,
      borderColor: "#CCC",
      borderWidth: 1,
      margin: 5,
      width: '100%',
      alignSelf: 'center',
      alignItems: 'center'
    },
    categoryContainerSelected: {
      flexDirection: 'row',
      padding: 15,
      borderRadius: 10,
      borderColor: appStyles.colorSet.mainThemeForegroundColor,
      borderWidth: 1,
      margin: 5,
      width: '100%',
      alignSelf: 'center',
      alignItems: 'center'
    },
    categoryText: {
      color: appStyles.colorSet.mainTextColor,
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      marginLeft: 15
    },
    categoryTextSelected: {
      color: appStyles.colorSet.mainThemeForegroundColor,
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      marginLeft: 15
    },
    categoryImage: {
      tintColor: appStyles.colorSet.mainTextColor,
      width: 30,
      height: 30
    },
    categoryImageSelected: {
      tintColor: appStyles.colorSet.mainThemeForegroundColor,
      width: 30,
      height: 30
    },
    rowContainer:{
      flexDirection: 'row',
      justifyContent: 'center'
    },
  })
};

export default dynamicStyles;
