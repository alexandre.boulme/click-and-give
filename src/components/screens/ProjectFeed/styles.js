import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
  feedContainer: {
    flex: 1,
  },
  emptyStateView: {
    marginTop: '15%'
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    flexDirection:'row',
    paddingLeft: 25,
    paddingTop: 25,
    paddingBottom: 25,
    backgroundColor: "#FFF",
    borderRadius: 50,
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#999",
    shadowOpacity: 1.0,
  },
  buttonIcon:{
    tintColor:"#F53950",
    resizeMode: 'contain',
    width: '15%',
    height: '100%'
  },
  buttonText: {
    fontSize: 20,
    color: "#F53950",
    fontWeight: '800',
    marginLeft: 10
  },
  headerListText: {
    fontSize: AppStyles.fontSet.xlarge,
    fontWeight: '700',
    color: "#000",
    ...ifIphoneX({
      margin: 12
    },{
      marginLeft: 15,
      marginVertical: 10
    })
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: "#FFF"
  },
  verticalSpacerFooter: {
    ...ifIphoneX({
      height: 80
    },{
      height: 80
    })
  },
  floatingButton:{
    ...ifIphoneX({},{
      bottom: 7,
      right: 7
    })
  }
});

export default dynamicStyles;
