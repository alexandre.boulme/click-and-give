import React, { useState, useEffect } from 'react';
import { FlatList, View, ActivityIndicator, Dimensions, Text} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import ProjectFeedItem from '../../ProjectFeedItem/ProjectFeedItem';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import { TNEmptyStateView} from '../../../Core/truly-native';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import AppStyles from '../../../AppStyles';
import FilterModal from '../HighlightedProjectFeed/components/FilterModal';
import { daysLeftCalculation } from '../../../Core';
import {buildProjectDeepLink} from "../../../Core/project/firebase/project";

const initialFilter = {
  needFund: true,
  needVolunteer: true,
  regions: ["Asia", "Americas", "Europe"],
  categories: ["Artistic projects", "Primary education", "Secondary and Higher education", "Social integration", "Environnement", "Disasters", "Humanitarian causes"]
};

function ProjectFeed(props) {
  const {
    feed,
    onFeedItemPress,
    onFeedUserItemPress,
    loading,
  } = props;

  const [visibleProjects, setVisibleProjects] = useState([]);
  const [invisibleProject, setInvisibleProjects] = useState([]);
  const [feedFiltered, setFeedFiltered] = useState(null);
  const [currentFilter, setCurrentFilter] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    if (feed) {
      setVisibleProjects(filterVisibleFeed(feed));
      setFeedFiltered(filterFeed(!currentFilter || currentFilter.displayCurrentProject ? filterVisibleFeed(feed) : invisibleProject, currentFilter));
    }
  }, [feed]);

  const filterCategories = (project, filter) => {

    if (filter.categories.length > 0){
      var count = 0;
      filter.categories.map(item => {
        for (var i = 0; i < project.categories.length; i++){
          if (project.categories[i] === item){
              count += 1;
          }
        }
      })
      return count > 0;
    } else {
      return true;
    }
  };

  const filterFeed = (feed, filters) => {

    setCurrentFilter(filters);

    if (filters){
      var filteredProject = feed.filter(project => {
        if (
          ((!filters.needVolunteer &&  !filters.needFund) || (filters.needFund && project.needFund == filters.needFund) || (filters.needVolunteer && project.needVolunteer == filters.needVolunteer))
          && (filters.countries.length == 0 || filters.countries.includes(project.cityLocation.country))
          && (filters.regions.length == 0 || filters.regions.includes(project.cityLocation.region))
          && filterCategories(project, filters)
          && (filters.projectsType.length == 0 || filters.projectsType.includes(project.type))
          && (!filters.searchName || project.name.toLowerCase().includes(filters.searchName.toLowerCase()))
        ){
          return project;
        }
      });
      return filteredProject;
    } else {
      return feed;
    }
  };

  const filterVisibleFeed = (feed) => {

    var inVisibleProjects = [];

    var visibleProjects = feed.filter(project => {

      if (project.approved && (daysLeftCalculation(project.endDate) > -7 || project.disableEndDate)){
        return project;
      } else {
        if(project.approved){
          inVisibleProjects.push(project);
        }
      }
    });

    setInvisibleProjects(inVisibleProjects);

    return visibleProjects;
  }

  const onApplyFilterButtonPress = filtersFromModal => {
    setFeedFiltered(filterFeed(!filtersFromModal || filtersFromModal.displayCurrentProject ? visibleProjects : invisibleProject, filtersFromModal));
  };

  const renderItem = ({ item, index }) => {
    let shouldUpdate = false;
    if (item.shouldUpdate) {
      shouldUpdate = item.shouldUpdate;
    }
    return (
      <ProjectFeedItem
        key={index + ''}
        onUserItemPress={onFeedUserItemPress}
        item={item}
        onItemPress={onFeedItemPress}
      />
    );
  };

  const renderEmptyComponent = () => {
    if (!feed) {
      return null;
    }
    const emptyStateConfig = {
      title: IMLocalized("Oops"),
      description: IMLocalized("No project matching current filters."),
    };

    return (
      <TNEmptyStateView
        style={styles.emptyStateView}
        emptyStateConfig={emptyStateConfig}
        appStyles={AppStyles}
      />
    );
  }

  if (loading) {
    return (
      <View style={styles.feedContainer}>
        <ActivityIndicator style={{ marginTop: 50 }} size="small" />
      </View>
    );
  }

  return (
    <View style={styles.feedContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerListText} >All Projects</Text>
          <FilterModal
            onApplyFilterButtonPress={onApplyFilterButtonPress}
          />
        </View>
        <FlatList
          style={{backgroundColor: "#FFF"}}
          scrollEventThrottle={16}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={renderEmptyComponent}
          data={feedFiltered}
          renderItem={renderItem}
          onEndReachedThreshold={0.5}
        />
    </View>
  );
}

ProjectFeed.propTypes = {
  feedItems: PropTypes.array,
  feedFiltered: PropTypes.array,
  userStories: PropTypes.object,
  stories: PropTypes.array,
  onMediaClose: PropTypes.func,
  onPostStory: PropTypes.func,
  onUserItemPress: PropTypes.func,
  onCameraClose: PropTypes.func,
  isCameraOpen: PropTypes.bool,
  displayStories: PropTypes.bool,
  isMediaViewerOpen: PropTypes.bool,
  onFeedUserItemPress: PropTypes.func,
  onMediaPress: PropTypes.func,
  selectedMediaIndex: PropTypes.number,
  onLikeReaction: PropTypes.func,
  onOtherReaction: PropTypes.func,
};

export default ProjectFeed;
