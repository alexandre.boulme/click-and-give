import {DynamicStyleSheet} from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = new DynamicStyleSheet({
  feedContainer: {
    flex: 1,
    height: 300
  },
  listHeight: {
    ...ifIphoneX({
      height: 200
    },{
      height: 165
    })
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    flexDirection:'row',
    paddingLeft: 25,
    paddingTop: 25,
    paddingBottom: 25,
    backgroundColor: "#FFF",
    borderRadius: 50,
    shadowOffset:{  width: 0,  height: 5,  },
    shadowColor: "#999",
    shadowOpacity: 1.0,
  },
  buttonIcon:{
    tintColor:"#F53950",
    resizeMode: 'contain',
    width: '15%',
    height: '100%'
  },
  buttonText: {
    fontSize: 20,
    color: "#F53950",
    fontWeight: '800',
    marginLeft: 10
  },
  emptyStateView: {
    height: 200
  }
});

export default dynamicStyles;
