import React, {useEffect, useState} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert, Touch, TouchableOpacity, TextInput, Image, ScrollView} from 'react-native';
import ClickAndGiveConfig from '../../../../ClickAndGiveConfig';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../../AppStyles';
import dynamicStyles from './styles';
import CategoryRadioButtonSelector from '../../CreateProject/components/CategoryRadioButtonSelector'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Icon from 'react-native-vector-icons/MaterialIcons';

const FilterModal = (props) => {

  const { onApplyFilterButtonPress } = props;
  const [SectionedMultiSelectComp, setSectionedMultiSelectComp] = useState(null);
  const [displayPlaceHolder,setDisplayPlaceHolder] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [needFund, setNeedFund] = useState(false);
  const [needVolunteer, setNeedVolunteer] = useState(false);
  const [foundationProject, setFoundationProject] = useState(false);
  const [employeeProject, setEmployeeProject] = useState(false);
  const [regionAsia, setRegionAsia] = useState(false);
  const [regionAmericas, setRegionAmericas] = useState(false);
  const [regionEurope, setRegionEurope] = useState(false);
  const [selectedCountries, setSelectedCountries] = useState([]);
  const [selectedCountriesName, setSelectedCountriesName] = useState([]);
  const [searchName, setSearchName] = useState('');
  const [uniqueLocations, setUniqueLocations] = useState([]);
  const [displayCurrentProject, setDisplayCurrentProject] = useState(true);
  var categoryOfInterestList = ClickAndGiveConfig.categoryOfInterest;
  var locations = ClickAndGiveConfig.cityLocationList;

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
      if(uniqueLocations.length == 0){
          getUniqueCountries(locations);
      }
  },[])

  const getUniqueCountries = (locations) => {
      const uniqueLocations = [];
      for(let i=0; i < locations.length; i++){
          if(locations[i].country != "" && uniqueLocations.filter(function(location) {
              return location.country == locations[i].country;
          }).length == 0) {
              uniqueLocations.push(locations[i]);
          }
      }
      setUniqueLocations(uniqueLocations);
  };

  const switchCategorySelectionStatus = (i) => {
    categoryOfInterestList[i].selected = !categoryOfInterestList[i].selected;
  };

  const resetFilters = () => {
    setNeedFund(false);
    setNeedVolunteer(false);
    setRegionAsia(false);
    setRegionAmericas(false);
    setRegionEurope(false);
    setFoundationProject(false);
    setEmployeeProject(false);
    setSearchName('');
    setSelectedCountries([]);
    setSelectedCountriesName([]);
    setDisplayCurrentProject(true);
    categoryOfInterestList.map(item => item.selected = false)
    onApplyFilterButtonPress(null);
    setDisplayPlaceHolder(true);
  };

  const getFiltersValues = () => {

    const filters = {
      needFund: needFund,
      needVolunteer:needVolunteer,
      countries: selectedCountriesName,
      regions: getRegionFilterValues(),
      projectsType: getProjectsTypeFilterValues(),
      categories: getCategoriesFilterValues(),
      searchName: searchName,
      displayCurrentProject: displayCurrentProject
    };

    onApplyFilterButtonPress(filters);
  };

  const getRegionFilterValues = () => {
    const regions = [];
    if (regionAsia)
      regions.push("Asia")
    if (regionAmericas)
      regions.push("Americas")
    if (regionEurope)
      regions.push("Europe")

    return regions;
  };

  const getProjectsTypeFilterValues = () => {
    const types = [];

    if (foundationProject)
      types.push("foundation")
    if (employeeProject)
      types.push("employee")

    return types;
  };

  const onSelectedItemsChange = selectedItem => {

  };

  const getCategoriesFilterValues = () => {

    const selectedCategories = categoryOfInterestList.filter(item => {
      if (item.selected){
        return item;
      }
    });

    const selectedCategoriesNames = [];
    selectedCategories.map(item => selectedCategoriesNames.push(item.name));

    return selectedCategoriesNames;
  };


    return (
      <View style={{alignSelf: 'center'}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <ScrollView
            style={styles.scrollViewStyle}>
          <View
            style={styles.modalViewContainer}>
              <View style={[styles.rowContainer, {justifyContent: 'space-between', marginVertical: 0}]}>
                <TouchableOpacity
                  style={styles.cancelButtonContainer}
                  onPress={() => {
                    //resetFilters();
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.cancelButton}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.applyButtonContainer}
                  onPress={() => {
                    getFiltersValues();
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.applyButton}>Apply filters</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={styles.resetFiltersButton}
                onPress={() => {
                  resetFilters();
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.resetFiltersButtonText}>Reset filters</Text>
              </TouchableOpacity>
              <Text style={styles.headlineText}>Search project</Text>
              <TextInput
                returnKeyType = { "done" }
                style={styles.InputContainer}
                placeholder={'Type search'}
                placeholderTextColor="#aaaaaa"
                onChangeText={text => setSearchName(text)}
                value={searchName}
                underlineColorAndroid="transparent"
              />
              <Text style={styles.headlineText}>You want to...</Text>
              <View style={styles.rowContainer}>
                <TouchableOpacity
                  style={[styles.basicButton, {width: '45%'}, needFund ? {} : styles.containerInactive]}
                  onPress={() => {
                    setNeedFund(!needFund);
                  }}>
                  <Text style={[styles.buttonText, needFund ? {} : styles.textInactive]}>Give Money</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.basicButton, {width: '45%'}, needVolunteer ? {} : styles.containerInactive]}
                  onPress={() => {
                    setNeedVolunteer(!needVolunteer);
                  }}>
                  <Text style={[styles.buttonText, needVolunteer ? {} : styles.textInactive]}>Give Time</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.headlineText}>Project type</Text>
              <View style={styles.rowContainer}>
                <TouchableOpacity
                  style={[styles.basicButton, {width: '45%'}, foundationProject ? {} : styles.containerInactive]}
                  onPress={() => {
                    setFoundationProject(!foundationProject);
                  }}>
                  <Text style={[styles.buttonText, foundationProject ? {} : styles.textInactive]}>Foundation</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.basicButton, {width: '45%'}, employeeProject ? {} : styles.containerInactive]}
                  onPress={() => {
                    setEmployeeProject(!employeeProject);
                  }}>
                  <Text style={[styles.buttonText, employeeProject ? {} : styles.textInactive]}>Employee</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.headlineText}>Country</Text>
              <View style={styles.rowContainer}>
                  {displayPlaceHolder && (
                      <TouchableOpacity
                          style={{width: '90%'}}
                          onPress={() => SectionedMultiSelectComp._toggleSelector()}
                      >
                          <Text style={{fontSize: AppStyles.fontSet.middle}}>Select countries</Text>
                      </TouchableOpacity>
                  )}
                  <SectionedMultiSelect
                      ref={SectionedMultiSelect => setSectionedMultiSelectComp(SectionedMultiSelect)}
                      items={uniqueLocations}
                      IconRenderer={Icon}
                      uniqueKey={"country"}
                      displayKey={"country"}
                      selectText={""}
                      showDropDowns={true}
                      hideSearch={true}
                      showRemoveAll={true}
                      selectedItemTextColor={AppStyles.colorSet.mainThemeForegroundColor}
                      selectedItemIconColor={AppStyles.colorSet.mainThemeForegroundColor}
                      styles={{
                          selectedItemText:{color: AppStyles.colorSet.mainThemeForegroundColor},
                          container: {marginVertical: 70},
                          modalWrapper: {width: "100%"},
                          selectToggle: {borderColor: AppStyles.colorSet.mainThemeForegroundColor},
                          selectToggleText: {color: AppStyles.colorSet.mainThemeForegroundColor},
                          itemText: {fontSize: AppStyles.fontSet.middleplus, fontWeight: '400'}
                      }}
                      colors={{primary: AppStyles.colorSet.mainThemeForegroundColor, success: AppStyles.colorSet.mainThemeForegroundColor}}
                      onSelectedItemsChange={(selectedItemsNames) => {
                          setSelectedCountriesName(selectedItemsNames);
                          setDisplayPlaceHolder(selectedItemsNames.length == 0);
                      }}
                      selectedItems={selectedCountriesName}
                  />
              </View>
              <Text style={styles.headlineText}>Region of origin</Text>
              <View style={styles.rowContainer}>
                <TouchableOpacity
                  style={[styles.basicButton, regionAsia ? {} : styles.containerInactive]}
                  onPress={() => setRegionAsia(!regionAsia)}>
                  <Text style={[styles.buttonText, regionAsia ? {} : styles.textInactive]}>Asia</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.basicButton,  regionAmericas ? {} : styles.containerInactive]}
                  onPress={() => setRegionAmericas(!regionAmericas)}>
                  <Text style={[styles.buttonText, regionAmericas ? {} : styles.textInactive]}>Americas</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.basicButton,  regionEurope ? {} : styles.containerInactive]}
                  onPress={() => setRegionEurope(!regionEurope)}>
                  <Text style={[styles.buttonText, regionEurope ? {} : styles.textInactive]}>Europe</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.headlineText}>Categories</Text>
              {
                categoryOfInterestList.map((item, i) => (
                  <CategoryRadioButtonSelector
                    title={item.name}
                    selected={item.selected}
                    onPress={() => switchCategorySelectionStatus(i)}
                  />
                ))
              }
              <Text style={styles.headlineText}>Display</Text>
              <View style={styles.rowContainer}>
                  <TouchableOpacity
                      style={[styles.basicButton, displayCurrentProject ? {} : styles.containerInactive]}
                      onPress={() => setDisplayCurrentProject(!displayCurrentProject)}>
                      <Text style={[styles.buttonText, displayCurrentProject ? {} : styles.textInactive]}>Current projects</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                      style={[styles.basicButton,  !displayCurrentProject ? {} : styles.containerInactive]}
                      onPress={() => setDisplayCurrentProject(!displayCurrentProject)}>
                      <Text style={[styles.buttonText, !displayCurrentProject ? {} : styles.textInactive]}>Previous projects</Text>
                  </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </Modal>
        <TouchableHighlight
          style={styles.modalVisibleContainer}
          onPress={() => {
           setModalVisible(true);
          }}>
          <Image source={AppStyles.iconSet.filter} style={styles.modalVisibleIcon}/>
        </TouchableHighlight>
      </View>
    );
}

export default FilterModal;
