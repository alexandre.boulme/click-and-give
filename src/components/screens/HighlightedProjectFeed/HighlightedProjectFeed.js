import React, { useState, useEffect } from 'react';
import { FlatList, View, ActivityIndicator} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import HighlightedProjectFeedItem from '../../HighlightedProjectFeedItem/HighlightedProjectFeedItem';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import { TNEmptyStateView } from '../../../Core/truly-native';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import AppStyles from '../../../AppStyles';
import { daysLeftCalculation } from '../../../Core';

function HighlightedProjectFeed(props) {
  const {
    feed,
    user,
    loading,
    onFeedItemPress
  } = props;

  const [highlightedFeed, setHighlightedFeed] = useState(feed);

  useEffect(() => {
    if (feed) {
      setHighlightedFeed(filterHighlighted(feed));
    }
  }, [feed]);

  const styles = useDynamicStyleSheet(dynamicStyles);

  const filterHighlighted = feed => {
    return feed.filter(project => {
      if (project.approved && (daysLeftCalculation(project.endDate) > -7 || project.disableEndDate) && project.highlighted) {
        return project
      }
    });
  };

  const renderItem = ({ item }) => {
    return (
      <HighlightedProjectFeedItem
        onItemPress={() => onFeedItemPress(item)}
        item={item}
        user={user}
      />
    );
  };

  const renderEmptyComponent = () => {
    if (!feed) {
      return null;
    }
    const emptyStateConfig = {
      title: IMLocalized("No highlighted projects"),
    };

    return (
      <TNEmptyStateView
        style={styles.emptyStateView}
        emptyStateConfig={emptyStateConfig}
        appStyles={AppStyles}
      />
    );
  }

  if (loading) {
    return (
      <View style={styles.feedContainer}>
        <ActivityIndicator style={{ marginTop: 15 }} size="small" />
      </View>
    );
  }

  return (
    <View style={[styles.feedContainer, styles.listHeight]}>
      <FlatList
        scrollEventThrottle={16}
        horizontal={true}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={renderEmptyComponent}
        data={highlightedFeed}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        onEndReachedThreshold={0.5}
      />
    </View>
  );
}

HighlightedProjectFeed.propTypes = {
  feedItems: PropTypes.array,
  userStories: PropTypes.object,
  stories: PropTypes.array,
  onMediaClose: PropTypes.func,
  onCommentPress: PropTypes.func,
  onPostStory: PropTypes.func,
  onUserItemPress: PropTypes.func,
  onCameraClose: PropTypes.func,
  isCameraOpen: PropTypes.bool,
  displayStories: PropTypes.bool,
  isMediaViewerOpen: PropTypes.bool,
  onFeedUserItemPress: PropTypes.func,
  onMediaPress: PropTypes.func,
  selectedMediaIndex: PropTypes.number,
  onLikeReaction: PropTypes.func,
  onOtherReaction: PropTypes.func,
};

export default HighlightedProjectFeed;
