import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {initialMode, eventEmitter} from 'react-native-dark-mode';
import SplashScreen from 'react-native-splash-screen';
import configureStore from './redux/store';
import AppContainer from './screens/AppContainer';
import { setI18nConfig } from './Core/localization/IMLocalization';
import * as RNLocalize from 'react-native-localize';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/messaging';
import '@react-native-firebase/dynamic-links';
import { isEmulator } from 'react-native-device-info';
import {initPush} from './Core/pushNotifications/PushServices';


if (!isEmulator()) {
  console.log('Push init');
  initPush();
}

const store = configureStore();
const handleLocalizationChange = () => {
  setI18nConfig();
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: initialMode,
    };
  }


  componentDidMount() {
    SplashScreen.hide();
    console.disableYellowBox = true;
    setI18nConfig();
    RNLocalize.addEventListener('change', handleLocalizationChange);

    eventEmitter.on('currentModeChanged', mode => {
      this.setState({mode});
    });

    firebase.dynamicLinks()
        .getInitialLink()
        .then(url => {
          if (url) this.setState({ url });
          console.log("GET INITIAL LINK : " + url);
        });
    firebase.dynamicLinks().onLink(this.handleDynamicLink);

  }

  handleDynamicLink = link => {
    console.log("DYNAMIC LINK HANDLED !", link);
    // Handle dynamic link inside your own application
  };

  render() {
    return (
      <Provider store={store}>
        <AppContainer screenProps={{theme: this.state.mode}} />
      </Provider>
    );
  }
}


