import React, { useState } from 'react';
import { Text, TextInput, View, Alert, Image, TouchableOpacity } from 'react-native';
import Button from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import { IMLocalized } from '../../localization/IMLocalization';
import { connect } from 'react-redux';
import authManager from '../utils/authManager';
import { localizedErrorMessage } from '../utils/ErrorCode';
import { setUserData } from '../redux/auth';
import Toast from 'react-native-tiny-toast';


const ForgotPasswordScreen = props => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);

  const appConfig = (props.navigation.state.params.appConfig || props.navigation.getParam('appConfig'));
  const appStyles = (props.navigation.state.params.appStyles || props.navigation.getParam('appStyles'));
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const onResetPassword = () => {
    setLoading(true);

    const userDetails = {
      email,
      appIdentifier: appConfig.appIdentifier
    };

    authManager.sendPasswordResetEmail(email)
    .then(() => {
      setLoading(false);
      Toast.show(IMLocalized('Reset password email sent.'), {duration: 2000});
      props.navigation.goBack();
    }).catch(error =>{
      console.log(error);
      Alert.alert('', 'An error occured, please make sure an account exist for this email.', [{ text: IMLocalized('OK') }], {
          cancelable: false,
      });
      setLoading(false);
    });
  };

  const renderForgotPasswordForm = () => {
    return (
      <>
        <TextInput
          style={styles.InputContainer}
          placeholder={IMLocalized('E-mail')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setEmail(text)}
          value={email}
          underlineColorAndroid="transparent"
          autoCapitalize='none'
        />
        <Button
          containerStyle={styles.signupContainer}
          style={styles.signupText}
          onPress={() => onResetPassword()}
        >
          {IMLocalized('Reset my password')}
        </Button>
      </>
    );
  };

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%' }}
          keyboardShouldPersistTaps='always'
          enableResetScrollToCoords={false}>
        <View style={{ flex: 1, flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image style={appStyles.styleSet.backArrowStyle} source={appStyles.iconSet.backArrow} />
          </TouchableOpacity>
          <Text style={styles.headerTitle}>{IMLocalized('Reset password')}</Text>
        </View>
        <Text style={styles.title}>{IMLocalized('Enter your email')}</Text>
        {renderForgotPasswordForm()}
      </KeyboardAwareScrollView>
      {loading && <TNActivityIndicator appStyles={appStyles} />}
    </View>
  );
};

export default connect(null, {
  setUserData
})(ForgotPasswordScreen);
