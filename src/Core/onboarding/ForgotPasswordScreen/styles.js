import { Dimensions, I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    headerTitle:{
      fontSize: appStyles.fontSet.middle,
      width: '80%',
      textAlign: 'center',
      marginTop: Platform.OS === 'ios' ? 40 : 20,
    },
    title: {
      fontSize: 30,
      fontWeight: 'bold',
      color: appStyles.colorSet.mainThemeForegroundColor,
      marginTop: '55%',
      marginBottom: '10%',
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 35,
    },
    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeForegroundColor,
    },
    loginContainer: {
      width: appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 30,
    },
    loginText: {
      color: appStyles.colorSet.mainThemeBackgroundColor,
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      paddingLeft: 0,
      color: appStyles.colorSet.mainTextColor,
      width: '80%',
      alignSelf: 'stretch',
      marginLeft: 35,
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    signupContainer: {
      alignSelf: 'center',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      ...ifIphoneX(
        {
          paddingTop: 25,
          paddingBottom: 25,
          marginTop: 50,
        },
        {
          paddingTop: 20,
          paddingBottom: 20,
          paddingLeft: 5,
          paddingRight:5,
          marginTop: 30,
        },
      ),
      width: '50%',
    },
    signupText: {
      color: appStyles.colorSet.mainThemeBackgroundColor,
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
    },
    image: {
      width: '100%',
      height: '100%',
    },
    imageBlock: {
      flex: 2,
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: '#006',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: 'hidden',
    },
    formContainer: {
      width: '100%',
      flex: 4,
      alignItems: 'center',
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },

    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d9d9d9',
      opacity: 0.8,
      zIndex: 2,
    },
    orTextStyle: {
      color: 'black',
      marginTop: 20,
      marginBottom: 10,
      alignSelf: 'center',
      color: appStyles.colorSet.mainTextColor,
    },
    PhoneNumberContainer: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
    },
    smsText: {
      color: '#4267b2',
    },
  })
};

export default dynamicStyles;
