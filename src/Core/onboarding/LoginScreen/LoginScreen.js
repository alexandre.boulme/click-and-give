import React, { useState, useEffect } from 'react';
import { Text, TextInput, View, Alert, TouchableOpacity, Image } from 'react-native';
import Toast from 'react-native-tiny-toast';
import Button from 'react-native-button';
import { connect } from 'react-redux';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import { IMLocalized } from '../../localization/IMLocalization';
import dynamicStyles from './styles';
import { setUserData } from '../redux/auth';
import authManager from '../utils/authManager';
import { localizedErrorMessage } from '../utils/ErrorCode';
import {firebaseAnalytics} from "../../firebase";

const LoginScreen = props => {
  const [isLoading, setIsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const appStyles = (props.navigation.state.params.appStyles || props.navigation.getParam('appStyles'));
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const appConfig = (props.navigation.state.params.appConfig || props.navigation.getParam('appConfig'));

  useEffect(() => {
    tryToLoginFirst();
  }, []);

  const tryToLoginFirst = async () => {
    setIsLoading(true);
    authManager
      .retrievePersistedAuthUser()
      .then(response => {
        setIsLoading(false);

        if (response && response.user.user.emailVerified) {
          const user = response.user;

          firebaseAnalytics.sendEvent("user_login_reopening_app", null);
          firebaseAnalytics.setupUserInfos(user.user);

          props.setUserData(user);
          props.navigation.navigate('MainStack', { user: user });
        }
      })
  };

  if (isLoading == true) {
    return <TNActivityIndicator appStyles={appStyles} />;
  }

  const onPressLogin = () => {
      setLoading(true);

      authManager.loginWithEmailAndPassword(email, password)
          .then(response => {
              if (response.user) {

                  const user = response.user;
                  if (user.user.emailVerified) {
                      //Analytics
                      firebaseAnalytics.sendEvent("user_login", null);
                      console.log("USER : "  + JSON.stringify(user));
                      console.log("USER USER : " + JSON.stringify(user.user));
                      firebaseAnalytics.setupUserInfos(user.user);

                      //Navigate to app
                      props.setUserData(user);
                      props.navigation.navigate('MainStack', { user: user });
                  } else {
                      Alert.alert(
                          IMLocalized('Email validation needed'),
                          IMLocalized('Please verify your email address to log in.'),
                          [{ text: IMLocalized('Re-send verification email'),
                              onPress: () => {
                                  authManager.sendVerificationEmail();
                                  Toast.show(IMLocalized('Verification email sent.'), {duration: 2000});
                              } },{ text: IMLocalized('Cancel')}], {
                              cancelable: true,
                          });
                  }
              } else {
                  Alert.alert('', localizedErrorMessage(response.error), [{ text: IMLocalized('OK') }], {
                      cancelable: false,
                  });
              }
              setLoading(false);
          });
  };


  return (
    <View style={styles.container}>
      <Image source={appStyles.imageSet.loginBackground} style={styles.backgroundImage}/>
      <KeyboardAwareScrollView
          style={styles.scrollviewContainer}
          enableResetScrollToCoords={true}
          enableAutomaticScroll={true}>
        <Text style={styles.appTitle}>{IMLocalized('Click \n& Give')}</Text>
        <Text style={styles.appSubTitle}>{IMLocalized('Ardian foundation').toUpperCase()}</Text>
        <View style={styles.loginContentContainer}>
          <Text style={styles.title}>{IMLocalized('Please Sign In')}</Text>
          <View>
          {email.includes("@ardian.com") && (<Image source={appStyles.iconSet.done} tintColor="#36D068" style={styles.textInputValidationIcon}/>)}
          <TextInput
            returnKeyType = { "next" }
            onSubmitEditing={() => { this.passwordInput.focus(); }}
            style={styles.InputContainer}
            placeholder={IMLocalized('E-mail')}
            placeholderTextColor="#aaaaaa"
            onChangeText={text => setEmail(text)}
            value={email}
            autoCompleteType='email'
            underlineColorAndroid="transparent"
            autoCapitalize='none'
          />
          </View>
          <TextInput
              ref={(input) => { this.passwordInput = input; }}
              style={styles.InputContainer}
              placeholderTextColor="#aaaaaa"
              secureTextEntry
              placeholder={IMLocalized('Password')}
              onChangeText={text => setPassword(text)}
              onSubmitEditing={() => {}}
              value={password}
              autoCompleteType='password'
              underlineColorAndroid="transparent"
              autoCapitalize='none'
            />
          <Button
            containerStyle={styles.loginContainer}
            style={styles.loginText}
            onPress={() => onPressLogin()}
          >
            {IMLocalized('Log In')}
          </Button>
          <View style={styles.centeredRow}>
            <Button
                style={styles.linkText}
                onPress={() =>
                  props.navigation.navigate('ForgotPassword', {
                    isSigningUp: false,
                    appStyles,
                    appConfig,
                  })
                }
              >
                {IMLocalized('Forgot your password')}
            </Button>
            <Button
                style={styles.linkText}
                onPress={() =>
                  props.navigation.navigate('Signup', {
                    isSigningUp: false,
                    appStyles,
                    appConfig,
                  })
                }
              >
                {IMLocalized('New account')}
            </Button>
          </View>
        </View>
        {loading && <TNActivityIndicator appStyles={appStyles} />}
      </KeyboardAwareScrollView>
    </View>
  );
};

export default connect(null, {
  setUserData
})(LoginScreen);
