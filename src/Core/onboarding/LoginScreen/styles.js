import { I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '60%'
    },
    scrollviewContainer : {
      flex: 1,
      width: '100%',
      height: '100%',
      flexDirection: 'column-reverse'
    },
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
    },
    appTitle: {
      ...ifIphoneX(
        {
          fontSize: 70,
        },
        {
          fontSize: 40,
        },
      ),
      marginLeft: 30,
      color: '#FFF',
      fontWeight: 'bold',
      alignSelf: 'stretch',
      textAlign: 'left',
    },
    appSubTitle: {
      ...ifIphoneX(
        {
          fontSize: 18,
        },
        {
          fontSize: 10,
        },
      ),
      color: '#FFF',
      marginTop: 5,
      marginBottom: 60,
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 30,
    },
    loginContentContainer: {
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20,
      backgroundColor: '#FFF',
      ...ifIphoneX(
        {
          paddingBottom: 60,
        },
        {
          paddingBottom: 50,
        },
      ),
    },
    title: {
      ...ifIphoneX(
        {
          fontSize: 30,
          marginTop: 45,
          marginBottom: 20,
        },
        {
          fontSize: 24,
          marginTop: 25,
          marginBottom: 10,
        },
      ),
      color: '#000000',
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 30,
    },
    loginContainer: {
      ...ifIphoneX(
        {
          marginTop: 30,
          paddingTop: 25,
          paddingBottom: 25,
        },
        {
          marginTop: 20,
          paddingTop: 15,
          paddingBottom: 15,
        },
      ),
      width: '45%',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      marginRight: '10%',
      alignSelf: 'flex-end',
      borderRadius: appStyles.sizeSet.radius,
    },
    loginText: {
      color: "#FFF",
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      ...ifIphoneX(
        {
          height: 42,
          marginBottom: 25,
        },
        {
          height: 32,
          marginBottom: 15,
        },
      ),
      color: "#000",
      width: '80%',
      alignSelf: 'stretch',
      marginLeft: 35,
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: '#000000',
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    centeredRow: {
      flex: 1,
      flexDirection: 'row',
      width: '80%',
      alignSelf: 'center',
      justifyContent: 'space-between'
    },
    linkText: {
      ...ifIphoneX(
        {
          marginTop: 60,
        },
        {
          marginTop: 40,
        },
      ),
      paddingBottom: 5,
      fontSize: appStyles.fontSet.middleminus,
      color: appStyles.colorSet.mainThemeForegroundColor,
      textDecorationLine: 'underline',
    },
    textInputValidationIcon: {
      ...ifIphoneX(
        {
          top: 15,
        },
        {
          top: 0,
        },
      ),
      width: 25,
      height: 25,
      position: 'absolute',
      right: '13%',
      tintColor: "#36D068"
    },
  })
};

export default dynamicStyles;
