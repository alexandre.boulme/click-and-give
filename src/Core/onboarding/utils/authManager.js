import { firebaseAuth } from '../../firebase';
import { firebaseStorage } from '../../firebase/storage';
import AppStyle from '../../../AppStyles';
import { requestTrackingPermission } from 'react-native-tracking-transparency';

const defaultProfilePhotoURL = "https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg";
const defaultProfilePhotoURI = AppStyle.imageSet.emptyAvatar;

const retrievePersistedAuthUser = () => {
    return new Promise(resolve => {
        firebaseAuth
            .retrievePersistedAuthUser()
            .then(user => {
                if (user) {

                    handleSuccessfulLogin(user, false)
                        .then(user => {
                            // Persisted login successful, push token stored, login credential persisted, so we log the user in.
                            resolve({ user });
                        })
                } else {
                    resolve(null);
                }
            })
    });
}

const loginWithEmailAndPassword = (email, password) => {


    return new Promise(function (resolve, _reject) {
        firebaseAuth
            .loginWithEmailAndPassword(email, password)
            .then(response => {
                console.log("Inside authManager : " + JSON.stringify(response));
                if (!response.error) {
                    handleSuccessfulLogin({ ...response.user }, false)
                        .then(user => {
                            // Login successful, push token stored, login credential persisted, so we log the user in.
                            resolve({ user });
                        })
                } else {
                    resolve({ error: response.error });
                }
            })
    });
}

const sendPasswordResetEmail = (email) => {

  return new Promise((resolve, reject) => {
    firebaseAuth
      .sendPasswordResetEmail(email)
      .then(() => {
        resolve();
      }).catch(function(error) {
        reject(error);
      });
  });
}

const reloadUser = (callback) => firebaseAuth.reloadUser(callback)

const logout = (user) => {
    const userData = {
        ...user,
        isOnline: false
    };

    firebaseAuth.updateUser(user.id || user.userID, userData);

    firebaseAuth.logout();
}

const createAccountWithEmailAndPassword = (userDetails, appIdentifier) => {
    const { photoURI } = userDetails;

    const accountCreationTask = (userData) => {
        return new Promise(function (resolve, _reject) {
            firebaseAuth
                .register(userData, appIdentifier)
                .then(response => {
                    if (response.error) {
                        console.log("dans firebaseauth" + JSON.stringify(response.error));
                        resolve({ error: response.error });
                    } else {
                        // We created the user succesfully, time to upload the profile photo and update the users table with the correct URL
                        let user = response.user;
                        if (photoURI) {
                            firebaseStorage
                                .uploadImage(photoURI)
                                .then(response => {
                                    if (response.error) {
                                        // if account gets created, but photo upload fails, we still log the user in
                                        resolve({ nonCriticalError: response.error, user: { ...user, profilePictureURL: defaultProfilePhotoURL } });
                                    } else {
                                        firebaseAuth
                                            .updateProfilePhoto(user.id, response.downloadURL)
                                            .then(_result => {
                                                resolve({ user: { ...user, profilePictureURL: response.downloadURL } });
                                            })
                                    }
                                })
                        } else {
                            resolve({ user: { ...response.user, profilePictureURL: defaultProfilePhotoURL } });
                        }
                    }
                })
        });
    };

    return new Promise(function (resolve, _reject) {
        const userData = { ...userDetails, profilePictureURL: defaultProfilePhotoURL }

        accountCreationTask(userData)
            .then(response => {
                if (response.error) {
                    resolve({ error: response.error })
                } else {
                    // We signed up successfully, so we are logging the user in (as well as updating push token, persisting credential,s etc.)

                    sendVerificationEmail();

                    resolve(response);

                }
            })
    });
}

const sendVerificationEmail = () => {

  return new Promise((resolve, reject) => {
    firebaseAuth.sendEmailVerification();
  });
};


const handleSuccessfulLogin = (user, accountCreated) => {
    // After a successful login, we fetch & store the device token for push notifications, location, online status, etc.
    // we don't wait for fetching & updating the location or push token, for performance reasons (especially on Android)
    fetchAndStoreExtraInfoUponLogin(user, accountCreated);
    fetchAndStoreDataTackingAuthorization(user);
    return new Promise(resolve => {
        resolve({ user: { ...user } });
    });
}

const fetchAndStoreExtraInfoUponLogin = async (user, accountCreated) => {
    const pushToken = await firebaseAuth.fetchPushTokenIfPossible();
    user.badgeCount = 0;
    const userData = {
        ...user,
        pushToken: pushToken,
        isOnline: true
    };
    firebaseAuth.updateUser(user.id || user.userID, userData);
}

const fetchAndStoreDataTackingAuthorization = async (user) => {
    console.log("Get track auth");
    const trackingStatus = await requestTrackingPermission();
    console.log("After track auth");
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
        const userData = {
            ...user,
            allowAnonymousDataTacking: true
        };
        firebaseAuth.updateUser(user.id || user.userID, userData);
    }
}

const getCurrentLocation = geolocation => {
    return new Promise(resolve => {
        geolocation.getCurrentPosition(
            resolve,
            () => resolve({ coords: { latitude: "", longitude: "" } }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    })
};

const authManager = {
    retrievePersistedAuthUser,
    loginWithEmailAndPassword,
    logout,
    createAccountWithEmailAndPassword,
    sendPasswordResetEmail,
    sendVerificationEmail,
    reloadUser
};

export default authManager;
