import { Dimensions, I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = () => {
  return new DynamicStyleSheet({
    categoryOfInterest:{
      borderColor: "#999",
      borderWidth : 1,
      padding: 15,
      margin: 5,
      borderRadius: 10,
    },
    categoryText:{
      color:"#000",
      fontSize: AppStyles.fontSet.middle,
    },
    categoryOfInterestSelected:{
      borderColor: AppStyles.colorSet.mainThemeForegroundColor,
      borderWidth : 1,
      padding: 15,
      margin: 5,
      borderRadius: 10,
    },
    categoryTextSelected:{
      color: AppStyles.colorSet.mainThemeForegroundColor,
      fontSize: AppStyles.fontSet.middle,
    }
  })
};

export default dynamicStyles;
