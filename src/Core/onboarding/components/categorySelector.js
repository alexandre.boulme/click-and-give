import React, {useState} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import dynamicStyles from './styles';

const CategorySelector = (props) => {

  const [selected, setSelected] = useState(props.selected);
  const {title, onPress, customStyle} = props;

  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  return (
    <TouchableOpacity
      onPress={() =>{
        setSelected(!selected);
        onPress();
      }}
      style={[customStyle,selected ? styles.categoryOfInterestSelected : styles.categoryOfInterest]}
    >
      <Text style={selected ? styles.categoryTextSelected : styles.categoryText}>{title}</Text>
    </TouchableOpacity>
  );

};

export default CategorySelector;
