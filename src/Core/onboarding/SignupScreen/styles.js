import { Dimensions, I18nManager } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const { height } = Dimensions.get('window');
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    rowContainer: {
      flex: 1,
      flexDirection: 'row',
      margin: 20,
      marginBottom: 0,
      marginTop: 40
    },
    pickerContainer:{
      height: 50,
      width: '80%',
      alignSelf: 'center'
    },
    pickerItemStyle:{
      backgroundColor: "#FFF",
      marginLeft: 0,
      paddingLeft: 20
    },
    pickerTextStyle:{
      color: "#000",
      fontSize: appStyles.fontSet.middle,
      fontWeight: '300'
    },
    headerTitle:{
      fontSize: appStyles.fontSet.middle,
      width: '80%',
      textAlign: 'center',
      marginTop: Platform.OS === 'ios' ? 50 : 20,
    },
    title: {
      fontSize: appStyles.fontSet.xlarge,
      fontWeight: 'bold',
      color: appStyles.colorSet.mainThemeForegroundColor,
      marginTop: 25,
      marginBottom: 25,
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 20,
    },
    subTitle: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
      color: appStyles.colorSet.black,
      marginTop: 25,
      marginBottom: 15,
      alignSelf: 'stretch',
      textAlign: 'left',
      marginLeft: 20,
    },
    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: 'center',
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeForegroundColor,
    },
    loginContainer: {
      width: appStyles.sizeSet.buttonWidth,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      padding: 10,
      marginTop: 30,
    },
    loginText: {
      color: appStyles.colorSet.mainThemeBackgroundColor,
    },
    placeholder: {
      color: 'red',
    },
    InputContainer: {
      height: 42,
      paddingLeft: 0,
      color: appStyles.colorSet.mainTextColor,
      width: '85%',
      alignSelf: 'stretch',
      marginLeft: 20,
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    InputContainer50: {
      height: 42,
      paddingLeft: 0,
      color: appStyles.colorSet.mainTextColor,
      width: '40%',
      alignSelf: 'stretch',
      marginLeft: 20,
      marginTop: 5,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
      borderRadius: 0,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: I18nManager.isRTL ? 'right' : 'left',
    },
    textInputValidationIcon: {
      position: 'absolute',
      right: '13%',
      top: 15,
      width: 25,
      height: 25,
      tintColor: "#36D068"
    },
    endEmailText: {
      color: appStyles.colorSet.mainThemeForegroundColor,
      width: '40%',
      alignSelf: 'stretch',
      marginTop: 10,
      alignItems: 'center',
      fontSize:   appStyles.fontSet.middle,
    },
    signupContainer: {
      alignSelf: 'center',
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      borderRadius: appStyles.sizeSet.radius,
      paddingTop: 25,
      paddingBottom: 25,
      marginTop: 20,
      marginBottom: 30,
      width: '50%',
    },
    signupText: {
      color: appStyles.colorSet.mainThemeBackgroundColor,
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
    },
    image: {
      width: '100%',
      height: '100%',
    },
    imageBlock: {
      flex: 2,
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: '#006',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: 'hidden',
    },
    formContainer: {
      width: '100%',
      flex: 4,
      alignItems: 'center',
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },
    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d9d9d9',
      opacity: 0.8,
      zIndex: 2,
    },
    orTextStyle: {
      color: 'black',
      marginTop: 20,
      marginBottom: 10,
      alignSelf: 'center',
      color: appStyles.colorSet.mainTextColor,
    },
    PhoneNumberContainer: {
      marginTop: 10,
      marginBottom: 10,
      alignSelf: 'center',
    },
    smsText: {
      color: '#4267b2',
    },
    categoryOfInterest:{
      borderColor: "#999",
      borderWidth : 1,
      padding: 10,
      margin: 5,
      borderRadius: 5,
    },
    categoryText:{
      color:"#000",
      fontSize: appStyles.fontSet.middle,
    },
    categoryOfInterestSelected:{
      borderColor: appStyles.colorSet.mainThemeForegroundColor,
      borderWidth : 1,
      padding: 10,
      margin: 5,
      borderRadius: 5,
    },
    categoryTextSelected:{
      color: appStyles.colorSet.mainThemeForegroundColor,
      fontSize: appStyles.fontSet.middle,
    }
  })
};

export default dynamicStyles;
