import React, { useState } from 'react';
import { Text, TextInput, View, Alert, Image, TouchableOpacity } from 'react-native';
import Button from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import TNActivityIndicator from '../../truly-native/TNActivityIndicator';
import TNProfilePictureSelector from '../../truly-native/TNProfilePictureSelector/TNProfilePictureSelector';
import { IMLocalized } from '../../localization/IMLocalization';
import { setUserData } from '../redux/auth';
import { connect } from 'react-redux';
import authManager from '../utils/authManager';
import { localizedErrorMessage } from '../utils/ErrorCode';
import CategorySelector from '../components/categorySelector';
import Toast from 'react-native-tiny-toast';
import { Container, Content, Icon, Picker, Form } from "native-base";
import ClickAndGiveConfig from '../../../ClickAndGiveConfig';
import CheckBox from '@react-native-community/checkbox';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import FormDisclaimer from "../../../components/FormDisclaimer/FormDisclaimer";
import {firebaseAnalytics} from "../../firebase";

const SignupScreen = props => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [title, setTitle] = useState('');
  const [place, setPlace] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [emailStart, setEmailStart] = useState('');
  const [password, setPassword] = useState('');
  const [changePassword, setChangePassword] = useState('');
  const [profilePictureURL, setProfilePictureURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const [toggleCheckBox, setToggleCheckBox] = useState(false)

  const appConfig = (props.navigation.state.params.appConfig || props.navigation.getParam('appConfig'));
  const appStyles = (props.navigation.state.params.appStyles || props.navigation.getParam('appStyles'));
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const cityList = ClickAndGiveConfig.cityLocationList;
  const categoryOfInterestList = ClickAndGiveConfig.categoryOfInterest;

  const onRegister = () => {

    if (checkUserInformationsConformity()){
      setLoading(true);

      const email = emailStart + "@ardian.com";
      const selectedInterests = getUserSelectedCategoryOfInterest();
      const role = "user";

      const userDetails = {
        firstName,
        lastName,
        title,
        email,
        cityLocation,
        password,
        selectedInterests,
        photoURI: profilePictureURL,
        role,
        termsAndDataPrivacyApproved : toggleCheckBox,
        appIdentifier: appConfig.appIdentifier
      };

      authManager
        .createAccountWithEmailAndPassword(userDetails, appConfig.appIdentifier)
        .then(response => {
          const user = response.user;
          if (user) {
            Toast.show(IMLocalized('Account created, please verify your email address to log in.'), {duration: 2000});

            console.log(firebase.auth.currentUser);

            firebaseAnalytics.sendEvent('new_user_signup', {
                country: user.cityLocation.country
            });

            props.navigation.goBack();
          } else {
            Alert.alert('', localizedErrorMessage(response.error), [{ text: IMLocalized('OK') }], {
              cancelable: false,
            });
          }
          setLoading(false);
        });

      firebaseAnalytics.sendEvent("user_account_creation", {user: userDetails.firstName + ' ' + userDetails.lastName});
    }
  };

  const checkUserInformationsConformity = () => {
    if (checkUserEmailConformity() && checkUserPasswordConformity() && checkTermsAndDataCheckbox()){
      return true;
    }
  }

  const checkUserEmailConformity = () => {
    if (emailStart.length > 0){
      return true;
    } else {
      Alert.alert(IMLocalized('Email error'), IMLocalized('Email field can\'t be empty.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
    }
  };

  const checkTermsAndDataCheckbox = () => {
    if (toggleCheckBox){
      return true;
    } else {
      Alert.alert(IMLocalized('Error'), IMLocalized('Please accept Terms of use and Data privacy policy.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
    }
  };

  const checkUserPasswordConformity = () => {

    if (password.length >= 6 && password === changePassword){
      return true;
    } else if (password.length < 6){

      Alert.alert(IMLocalized('Password error'), IMLocalized('Please use a password at least 6 characters long.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
    } if (password !== changePassword) {

      Alert.alert(IMLocalized('Password error'), IMLocalized('Password confirmation differ from password field.'), [{ text: IMLocalized('OK') }], {
        cancelable: false,
      });
    }
  };

  const  switchCategorySelectionStatus = (i) => {
    categoryOfInterestList[i].selected = !categoryOfInterestList[i].selected;
  };

  const getUserSelectedCategoryOfInterest = () => {
    const selectedCategoryOfInterestNames = [];
    categoryOfInterestList.filter((category)=> {
      if (category.selected)
        selectedCategoryOfInterestNames.push(category.name);
    });
    return selectedCategoryOfInterestNames;
  };

  const onPickerValueChange = value => {
      cityList.filter(item => {
        if (item.city === value)
          setCityLocation(item);
      });
      setPlace(value);
  };

  const getLocation = locationString => {
    console.log("Sur le form" + locationString);
  };


  const renderSignupWithEmail = () => {
    return (
      <>
        <Text style={styles.subTitle}>{IMLocalized('First Name')}</Text>
        <TextInput
          returnKeyType = { "next" }
          onSubmitEditing={() => { this.lastNameInput.focus(); }}
          style={styles.InputContainer}
          placeholder={IMLocalized('First Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setFirstName(text)}
          value={firstName}
          underlineColorAndroid="transparent"
        />
        <Text style={styles.subTitle}>{IMLocalized('Last Name')}</Text>
        <TextInput
          ref={(input) => { this.lastNameInput = input; }}
          onSubmitEditing={() => { this.titleInput.focus(); }}
          returnKeyType = { "next" }
          style={styles.InputContainer}
          placeholder={IMLocalized('Last Name')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => setLastName(text)}
          value={lastName}
          underlineColorAndroid="transparent"
        />
        <Text style={styles.subTitle}>{IMLocalized('Job Title')}</Text>
        <TextInput
          ref={(input) => { this.titleInput = input; }}
          style={styles.InputContainer}
          placeholder={IMLocalized('Job Title')}
          placeholderTextColor="#aaaaaa"
          onChangeText={jobTitle => setTitle(jobTitle)}
          value={title}
          underlineColorAndroid="transparent"
        />
        <Text style={styles.subTitle}>{IMLocalized('Location')}</Text>
        <View style={styles.pickerContainer}>
          <Container>
            <Content>
              <Form>
                <Picker
                  ref={(input) => { this.locationInput = input; }}
                  mode="dropdown"
                  placeholder="Select your city"
                  headerBackButtonTextStyle={{ color: appStyles.colorSet.mainThemeForegroundColor }}
                  iosIcon={<Icon name="chevron-down-sharp" />}
                  placeholderStyle={{ color: "#aaaaaa", fontSize: appStyles.fontSet.middle, fontWeight: '400'}}
                  textStyle={styles.pickerTextStyle}
                  itemStyle={styles.pickerItemStyle}
                  itemTextStyle={styles.pickerTextStyle}
                  selectedValue={place}
                  onValueChange={value => onPickerValueChange(value)}
                >
                  {
                    cityList.map((item, i) => (
                      <Picker.Item label={item.city+", "+item.country} value={item.city} />
                    ))
                  }
                </Picker>
              </Form>
            </Content>
          </Container >
        </View>
        <Text style={styles.subTitle}>{IMLocalized('E-mail Address')}</Text>
        <View style={{ flex: 1, flexDirection: 'row'}}>
          <TextInput
            returnKeyType = { "next" }
            onSubmitEditing={() => { this.passwordInput.focus(); }}
            style={styles.InputContainer50}
            placeholder={IMLocalized('E-mail placeholder')}
            placeholderTextColor="#aaaaaa"
            onChangeText={text => setEmailStart(text)}
            value={emailStart}
            underlineColorAndroid="transparent"
            autoCapitalize='none'
          />
          <Text style={styles.endEmailText}>{IMLocalized('E-mail Address End')}</Text>
        </View>
        <Text style={styles.subTitle}>{IMLocalized('Choose Password')}</Text>
        <View>
          {password.length > 5 && (<Image source={appStyles.iconSet.done} tintColor="#36D068" style={styles.textInputValidationIcon}/>)}
          <TextInput
            ref={(input) => { this.passwordInput = input; }}
            returnKeyType = { "next" }
            onSubmitEditing={() => { this.passwordChangeInput.focus(); }}
            style={styles.InputContainer}
            placeholder={IMLocalized('Keep this very personnal')}
            placeholderTextColor="#aaaaaa"
            secureTextEntry
            onChangeText={text => setPassword(text)}
            value={password}
            underlineColorAndroid="transparent"
          />
        </View>
        <Text style={styles.subTitle}>{IMLocalized('Confirm Password')}</Text>
        <View>
          {(password === changePassword && changePassword.length > 5) && (<Image source={appStyles.iconSet.done} style={styles.textInputValidationIcon}/>)}
          <TextInput
            ref={(input) => { this.passwordChangeInput = input;}}
            style={styles.InputContainer}
            placeholder={IMLocalized('')}
            placeholderTextColor="#aaaaaa"
            secureTextEntry
            onChangeText={t => setChangePassword(t)}
            value={changePassword}
            underlineColorAndroid="transparent"
          />
        </View>
        <Text style={styles.subTitle}>{IMLocalized('Project category of interest')}</Text>
        <View style={{width: '90%', alignSelf: 'center', flexWrap: 'wrap', flexDirection: 'row'}}>
          {
            categoryOfInterestList.map((item, i) => (
              <CategorySelector
                title={item.name}
                selected={item.selected}
                onPress={() => switchCategorySelectionStatus(i)}
              />
            ))
          }
        </View>
        <View
          style={[styles.rowContainer,{marginBottom: 10}]}>
          <CheckBox
            value={toggleCheckBox}
            lineWidth={1.0}
            onCheckColor={appStyles.colorSet.mainThemeForegroundColor}
            onTintColor={appStyles.colorSet.mainThemeForegroundColor}
            onValueChange={() => toggleCheckBox ? setToggleCheckBox(false) : setToggleCheckBox(true)}
          />
          <View
            style={[{marginLeft: 8, flexDirection: 'row', flexWrap: 'wrap', width: "80%"}]}>
            <Text style={{marginRight: 3}}>
              I accept the
            </Text>
            <Text
              style={{color: appStyles.colorSet.mainThemeForegroundColor, marginRight: 3}}
              onPress={() => props.navigation.navigate('TermsAndConditions', {showHeader: true})}>
                Terms of use
              </Text>
              <Text>
                and the
              </Text>
              <Text
                style={{color: appStyles.colorSet.mainThemeForegroundColor, marginLeft: 3}}
                onPress={() => props.navigation.navigate('PrivacyNotice', {showHeader: true})}>
                Data protection notice
              </Text>
              <Text>
                .
              </Text>
          </View>
        </View>
        <FormDisclaimer
            disclaimerForm={'signUp'}
        />
        <Button
          containerStyle={styles.signupContainer}
          style={styles.signupText}
          onPress={() => onRegister()}
        >
          {IMLocalized('Sign Up')}
        </Button>
      </>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', paddingBottom: 20}}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image style={appStyles.styleSet.backArrowStyle} source={appStyles.iconSet.backArrow} />
        </TouchableOpacity>
        <Text style={styles.headerTitle}>{IMLocalized('Account creation')}</Text>
      </View>
      <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%' }}
          enableResetScrollToCoords={false}>
        <Text style={styles.title}>{IMLocalized('Enter your information')}</Text>
        <TNProfilePictureSelector
          setProfilePictureURL={setProfilePictureURL}
          appStyles={appStyles}
        />
        {renderSignupWithEmail()}
      </KeyboardAwareScrollView>
      {loading && <TNActivityIndicator appStyles={appStyles} />}
    </View>
  );
};

export default connect(null, {
  setUserData
})(SignupScreen);
