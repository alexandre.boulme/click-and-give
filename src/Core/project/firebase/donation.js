import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
import { IMLocalized } from '../../localization/IMLocalization';
import {firebaseAnalytics, firebaseUser} from '../../firebase';
import ClickAndGiveConfig from "../../../ClickAndGiveConfig";

export const donationRef = firebase
  .firestore()
  .collection("ClickAndGive_Donations");

export const projectRef = firebase
  .firestore()
  .collection("ClickAndGive_Projects");

const usersRef = firebase
    .firestore()
    .collection("ClickAndGive_Users");

export const subscribeToFeedDonations = (callback) => {

  return donationRef
      .where('status', 'in', ['Confirmed', 'Pending'])
      .onSnapshot(
        { includeMetadataChanges: true },
        querySnapshot => {
          let donations = [];

          querySnapshot.forEach(doc => {
            const donation = doc.data();
            if(donation.status != "Canceled"){
              const donation = doc.data();
              donation.id = doc.id;
              donations.push(donation);
            }

          });

          donations = donations.map(async donation => {
            if(donation.author != null){
              const userDoc = await usersRef.doc(donation.author.id).get();
              return  {
                ...donation,
                author: userDoc.data(),
              };
            }
          });

          Promise.all(donations).then(newDonations => {
            return callback(newDonations);
          });
        },
        _error => {
          console.log(_error);
          callback([]);
        });
};

export const addDonation = async (project, donation, author) => {

  console.log(donation.currency.code);
  const currencyRate = ClickAndGiveConfig.currencyExchangeRates[donation.currency.code];
  console.log(currencyRate);
  const ref = donationRef.doc();
  console.log("Ref before creation : ",ref);
  donation.id = ref.id;
  donation.createdAt = firebase.firestore.FieldValue.serverTimestamp();
  donation.author = author;
  const amountEuro = Math.round(donation.amount * currencyRate);

  try {
    const ref = await donationRef.add(donation).catch(err => {
      console.log("Project creation error : " + err);
    });
    console.log("Ref after creation : ", ref);

    if((!donation.foundationDonation && donation.status === "Confirmed") || donation.type == "time"){
      updateDonationsCountsOnProject(donation);
    }

    if(donation.status !== "Confirmed" && !donation.type == "time"){
      if(donation.status == "Pending"){
        firebaseAnalytics.sendEvent("money_donation", {
          status: "Pending",
          action: "creation",
          type: donation.type,
          currency: donation.currency.sign,
          project: donation.projectName,
          projectType: project.type,
          projectSupportArdianFoundation: project.projectSupportArdianFoundation ? "Yes" : "No",
          projectLeader: project.author.firstName + ' ' + project.author.lastName,
          projectTarget: project.fundRaisingTarget,
          amount: donation.amount,
          amountEuro: amountEuro,
          paymentType: donation.paymentInfos.type ? donation.paymentInfos.type : donation.paymentInfos,
          donator:  donation.author.firstName + ' ' + donation.author.lastName,
          anonymousDonation: donation.anonymous ? "Yes" : "No"
        });
      } else if (donation.status == "Canceled"){
        firebaseAnalytics.sendEvent("money_donation", {
          status: "Canceled",
          action: "creation",
          type: donation.type,
          currency: donation.currency.sign,
          project: donation.projectName,
          projectType: project.type,
          projectSupportArdianFoundation: project.projectSupportArdianFoundation ? "Yes" : "No",
          projectLeader: project.author.firstName + ' ' + project.author.lastName,
          projectTarget: project.fundRaisingTarget,
          amount: donation.amount,
          amountEuro: amountEuro,
          paymentType: donation.paymentInfos.type ? donation.paymentInfos.type : donation.paymentInfos,
          donator:  donation.author.firstName + ' ' + donation.author.lastName,
          anonymousDonation: donation.anonymous ? "Yes" : "No"
        });
      }
    }

    return { success: true, id: ref.id };

  } catch (error) {
    console.log(error);
    return { error, success: false };
  }

};

export const getDonation = async donationId => {
  try {
    const donation = await donationRef.doc(donationId).get();
    return { data: { ...donation.data(), id: donation.id }, success: true };
  } catch (error) {
    return { error: 'Oops! an error occured. Please try again', success: false };
  }
};

export const updateDonation = async (donationId, newStatus) => {
  try {
    const donationData = await donationRef.doc(donationId).get();
    const donation = donationData.data();

    await donationRef.doc(donationId).update({status: newStatus});

    if(!donation.foundationDonation && newStatus === "Confirmed"){
      updateDonationsCountsOnProject(donation);
    }

  } catch (error) {
    console.log(error);
    return { error: 'Oops! an error occured while updating status. Please try again', success: false };
  }
};

export const deleteDonation = async (donation) => {
  try {
    await donationRef.doc(donation.id).delete();
    // Update posts count
    const donationForThisAuthor = await donationRef
      .where('authorID', '==', project.authorID)
      .get();
    const donationCount = donationForThisAuthor.docs.length;

    firebaseUser.updateUserData(donation.authorID, { donationCount: donationCount });

    return { message: IMLocalized("Donation was successfully deleted."), success: true };
  } catch (error) {
    console.log(error);
    return { error: IMLocalized("Oops! an error occured. Please try again"), success: false };
  }
};

export const updateDonationsCountsOnProject = async (donation) => {
  console.log("Update of project started");

  // Fetch the current comment count
  const documents = await donationRef
    .where('projectId', '==', donation.projectId)
    .where('status', '==', "Confirmed")
    .get();

  const allDonations = documents.docs;

  console.log("Alldonation confirmed infos : " + allDonations);
  console.log("Alldonation confirmed length: " + allDonations.length);

  var donationsTimeCount =  0;
  var donationsMoneyCount =  0;
  var donationsMoneyAmount = 0;

  allDonations.forEach(doc  => {
    var donation = doc.data();
    console.log(JSON.stringify(donation));
    switch (donation.type) {
      case 'time':
        donationsTimeCount += 1;
        break;
      case 'money':
        donationsMoneyCount += 1;
        break;
      default:
    }
  });

  const projectDoc = await projectRef.doc(donation.projectId).get();
  const projectData = projectDoc.data();

  if(donation.amount && donation.type == 'money'){
    donationsMoneyAmount = parseInt(projectData.donations.fund.amount) + parseInt(donation.amount);
  } else {
    donationsMoneyAmount = parseInt(projectData.donations.fund.amount)
  }

  // Update project with correct values
  projectRef.doc(donation.projectId).update({
    donations : {
      fund: {
        number: donationsMoneyCount,
        amount: donationsMoneyAmount
      },
      volunteer: donationsTimeCount
    }
  }).catch(err => console.log(err));
}

export const sendProjectWaitingListEmailNotification = async (project, user) => {

  //Sent to leader
  firebase.firestore().collection('ClickAndGive_Emails').add({
    to: project.author.email,
    template: {
      name: 'projectWaitingListLeader',
      data: {
        learderName: project.author.firstName + " " + project.author.lastName,
        projectName: project.name,
        volunteerName: user.firstName + " " + user.lastName
      }
    },
  })

  //Sent to volunteer
  firebase.firestore().collection('ClickAndGive_Emails').add({
    to: user.email,
    template: {
      name: 'projectWaitingListVolunteer',
      data: {
        learderName: project.author.firstName + " " + project.author.lastName,
        projectName: project.name,
        volunteerName: user.firstName + " " + user.lastName
      }
    },
  })
};
