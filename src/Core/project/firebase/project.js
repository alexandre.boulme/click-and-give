import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import { IMLocalized } from '../../localization/IMLocalization';
import moment from 'moment';
import numeral from 'numeral';
import {ADMINISTATORS_EMAILS, DYNAMIC_LINKS_BASE} from 'react-native-dotenv';
import {firebaseStorage} from "../../firebase/storage";

export const projectRef = firebase
  .firestore()
  .collection("ClickAndGive_Projects");

  const usersRef = firebase
      .firestore()
      .collection("ClickAndGive_Users");

  export const subscribeToFeedProjects = (callback) => {

    const feedProjectsRef =
      projectRef
        .orderBy('createdAt', 'desc')
        .limit(500)
        .onSnapshot(
          { includeMetadataChanges: true },
          querySnapshot => {
            let projects = [];
            querySnapshot.forEach(doc => {
              const project = doc.data();
              project.id = doc.id;
              projects.push(project);
            });

            console.log("Number of projects : " + projects.length);

            projects = projects.map(async project => {

              const userDoc = await usersRef.doc(project.author.id).get();
              const promiseproject = {
                ...project,
                author: userDoc.data(),
              };
              return promiseproject;
            });

            Promise.all(projects).then(newProjects => {
              return callback(newProjects);
            });
          },
          _error => {
            callback([]);
          });
    return feedProjectsRef;
  };



  const filterForDonations = (donations, project) => {
    donations.forEach(donation => {
      if (donation.projectID === project.id) {
        switch (donation.type) {
          case "time":
            project.donations.volunteer += 1;
            break;
          case "fund":
            project.donations.fund.number += 1;
            project.donations.fund.amount += donation.amount;
            break;
          default:
        }
      }
    });
  };


export const addProject = async (project, author) => {

  const ref = projectRef.doc();
  project.id = ref.id;
  project.createdAt = firebase.firestore.FieldValue.serverTimestamp();
  project.author = author;
  project.fundRaisingTarget = numeral(project.fundRaisingTarget).format('00');

  console.log("Ready to send project ! addProject");

  try {

    const ref = await projectRef.add(project).catch(err => {
      console.log("Project creation error : " + err);
    });

    if (project.type == "employee"){
      sendProjectCreationEmailNotification(project);
    }

    return { success: true, id: ref.id };

  } catch (error) {
    return { error, success: false };
  }

};

export const updateProject = async (projectId, project) => {
  return new Promise((resolve, _reject) => {

      console.log("send project updates");
        if(project.projectPictureURL.includes('https')){
          console.log("Upload sans image");
          projectRef.doc(projectId).update({ ...project }).then(() => {
            console.log("project updates success");
            resolve();
          });
        } else {
          console.log("Upload avec image");
          firebaseStorage.uploadImage(project.projectPictureURL).then(fileUploaded => {
            project.projectPictureURL = fileUploaded.downloadURL;
            projectRef.doc(projectId).update({...project}).then(() => {
              console.log("project updates success");
              resolve();
            });
          });
        }
  });
}

export const updateProjectFundingAmount = async (projectId, project) => {
  console.log("send project updates");
  try {
    await projectRef.doc(projectId).update({ ...project });
    console.log("project updates success");

    return { success: true };
  } catch (error) {
    console.log(error);
    return { error, success: false };
  }
};

export const getProject = async projectId => {
  try {
    const project = await projectRef.doc(projectId).get();
    return { data: {...project.data(), id: projectId }, success: true };
  } catch (error) {
    console.log(error);
    return { error: 'Oops! an error occured. Please try again', success: false };
  }
};

export const buildProjectDeepLink = async project => {

  if(!project.projectDeepLink){
    console.log("DEEPLINK CREATION");
    console.log("Base link : ", DYNAMIC_LINKS_BASE);

    let deepLink = "https://apps.apple.com/us/app/click-give-by-ardian/id1528186730?projectId=" + project.id;
    deepLink = encodeURIComponent(deepLink);
    const link = DYNAMIC_LINKS_BASE + "?link=" + deepLink + "&apn=com.ardian.ios.clickandgive";

    console.log("Url encoded : ",link);

    project.projectDeeplinkUrl = link;
    updateProject(project.id, project);

  } else {
    console.log("DEEPLINK EXIST");
  }

};


export const deleteProject = async (project) => {
  console.log("Delete project");
  try {
    await projectRef.doc(project.id).delete();

    return { message: IMLocalized("Project was successfully deleted."), success: true };
  } catch (error) {
    console.log(error);
    return { error: IMLocalized("Oops! an error occured. Please try again"), success: false };
  }
};

const sendProjectCreationEmailNotification = async (project) => {

  var emailTemplate = 'projectApprovalRequest';

  if(project.requestMatching){
    emailTemplate = 'projectApprovalWithMatchingRequest';
  }

  console.log("Sending email : " + emailTemplate);

  //Sent to creator
  firebase.firestore().collection('ClickAndGive_Emails').add({
    to: project.author.email,
    template: {
      name: 'projectWaitingValidationEmail',
      data: {
        firstname: project.author.firstName,
        name: project.name,
      }
    },
  }).catch(err => {
    console.log("Error email creator : " + err);
  })

  //Sent to admins
  firebase.firestore().collection('ClickAndGive_Emails').add({
    to: ADMINISTATORS_EMAILS,
    template: {
      name: emailTemplate,
      data: {
        name: project.name,
        description: project.description,
        categories: project.categories,
        location: project.cityLocation.city + ', ' + project.cityLocation.country,
        endDate: moment(project.endDate).subtract(10, 'days').calendar(),
        projectPictureUrl: project.projectPictureURL,
        authorInfo: project.author.firstName + ' ' + project.author.lastName + ' - ' + project.author.email,
        fundraisingTarget:  project.needFund ? project.currency.sign + numeral(project.fundRaisingTarget).format('0,0') : 0,
        donationPageUrl: project.donationPlatformUrl ? project.donationPlatformUrl : 'None, this project supports Ardian foundation.',
        volunteerTarget: project.needVolunteer ? project.volunteerTarget : 0,
        volunteerFrequency:  project.volunteerFrequency,
        volunteerInstruction:  project.volunteerInstruction,
      }
    },
  }).catch(err => {
    console.log("Error admin : " + err);
  })
};

export const sendProjectValidationEmailNotification = async (project) => {

  //Sent to creator
  firebase.firestore().collection('ClickAndGive_Emails').add({
    to: project.author.email,
    template: {
      name: 'projectValidation',
      data: {
        firstname: project.author.firstName,
        name: project.name,
      }
    },
  }).catch(err => {
    console.log("Error email creator : " + err);
  })

};
