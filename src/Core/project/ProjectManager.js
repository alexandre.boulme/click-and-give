import * as firebaseProject from './firebase/project';
import * as firebaseDonations from './firebase/donation';
import {
  setFeedProjects,
  setFeedProjectsDonations,
  setProjectsFeedListenerDidSubscribe
} from './redux';
import {firebaseAnalytics} from "../firebase";

export default class ProjectManager {

  constructor(reduxStore, currentUserID) {
    this.reduxStore = reduxStore;
    this.currentUserID = currentUserID;
  }

  subscribeIfNeeded = () => {
    const state = this.reduxStore.getState();

    if (!state.projectFeed.didSubscribeToProjectFeed) {
      this.reduxStore.dispatch(setProjectsFeedListenerDidSubscribe());

      this.discoverUnsubscribe = firebaseProject.subscribeToFeedProjects(this.onProjectUpdate);

      this.donationsUnsubscribe = firebaseDonations.subscribeToFeedDonations(this.onDonationUpdate);

    }
  };

  unsubscribe = () => {

    if (this.discoverUnsubscribe) {
      this.discoverUnsubscribe();
    }

    if (this.donationsUnsubscribe) {
      this.donationsUnsubscribe();
    }

    console.log("UNSCUBSCRIBE PROJECT");
  };

  applyReaction = (reaction, item, followEnabled = true) => {
    const state = this.reduxStore.getState();
    const reactions = state.feed.feedPostReactions;
    const existingReaction = reactions.find(reaction => (reaction.postID == item.id));
    var newReactions;
    if (followEnabled) {
      if (existingReaction) {
        // like => unlike
        newReactions = reactions.filter(reaction => reaction.postID != item.id);
        firebaseAnalytics.sendEvent("unlike_post", {post: item.postText});
      } else {
        // like
        newReactions = reactions.concat([
          {
            postID: item.id,
            reaction: reaction
          }
        ])
        firebaseAnalytics.sendEvent("like_post", {post: item.postText});
      }
    } else {
      if (existingReaction) {
        if (reaction == null) {
          // undo previous reaction e.g. angry => null
          newReactions = reactions.filter(reaction => reaction.postID != item.id);
          firebaseAnalytics.sendEvent("unlike_post", {post: item.postText});
        } else {
          // replace previous reaction
          newReactions = reactions.filter(reaction => reaction.postID != item.id); // remove previous reaction
          newReactions = newReactions.concat([ // add new different reaction
            {
              postID: item.id,
              reaction: reaction
            }
          ])
          firebaseAnalytics.sendEvent("like_post",{post: item.postText});
        }
      } else {
        // got a reaction, with no previous reaction. Add it
        if (reaction != null) {
          newReactions = reactions.concat([ // add brand new reaction
            {
              postID: item.id,
              reaction: reaction
            }
          ])
        }
      }
    }
    this.reactions = newReactions;
    this.hydratePostsIfNeeded();
  }

  hydratePostsWithReduxReactions = posts => {
    const state = this.reduxStore.getState();
    const reactions = state.feed.feedPostReactions;
    const bannedUserIDs = state.userReports.bannedUserIDs;
    return this.hydratedPostsWithReactions(posts, reactions, bannedUserIDs);
  }

  onFeedPostsUpdate = (posts) => {
    this.posts = posts;
    this.hydratePostsIfNeeded();
  }

  onDiscoverPostsUpdate = (projects) => {
    this.reduxStore.dispatch(setFeedProjects(projects));
  }

  onProjectUpdate = (projects) => {
    this.reduxStore.dispatch(setFeedProjects(projects));
  }

  onDonationUpdate = (donations) => {
    this.reduxStore.dispatch(setFeedProjectsDonations(donations));
  }

  onReactionsUpdate = (reactions) => {
    this.reactions = reactions;
    this.hydratePostsIfNeeded();
  }

  onProfileFeedUpdate = (posts) => {
    this.reduxStore.dispatch(setCurrentUserFeedPosts(posts));
  }

  onAbusesUpdate = (abuses) => {
    var bannedUserIDs = [];
    abuses.forEach(abuse => bannedUserIDs.push(abuse.dest));
    this.reduxStore.dispatch(setBannedUserIDs(bannedUserIDs));
    this.bannedUserIDs = bannedUserIDs;
    this.hydratePostsIfNeeded();
  }

  hydratePostsIfNeeded = (skipReactionReduxUpdate = false) => {

    // if (!this.bannedUserIDs) {
    //   // we are still waiting to fetch banned users
    //   return;
    // }
    // // main feed
    // if (this.reactions && this.posts) {
    //   const hydratedPosts = this.hydratedPostsWithReactions(this.posts, this.reactions, this.bannedUserIDs);
    //   this.reduxStore.dispatch(setMainFeedPosts(hydratedPosts));
    //   if (!skipReactionReduxUpdate) {
    //     this.reduxStore.dispatch(setFeedPostReactions(this.reactions));
    //   }
    // }
    // discover feed
    if (this.discoverPosts) {
      this.reduxStore.dispatch(setFeedProjects(this.discoverPosts));
    }
  }

  hydratedPostsWithReactions = (posts, reactions, bannedUserIDs) => {
    if (reactions && posts) {
      const hydratedPosts =
        posts
          .map(post => {
            const reaction = reactions.find(reaction => (reaction.postID == post.id));
            if (reaction) {
              return {
                ...post,
                myReaction: reaction.reaction
              }
            }
            return {
              ...post,
              myReaction: null // we need to explicitly remove any previous reaction
            }
          })
          .filter(post => (!bannedUserIDs || !bannedUserIDs.includes(post.authorID)));
      return hydratedPosts;
    }
    return posts;
  };
}
