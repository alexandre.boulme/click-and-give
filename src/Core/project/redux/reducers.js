import IMFeedActionsConstants from './types';

const initialState = {
  feedProjects: null,
  feedProjectsDonations: null,
  didSubscribeToProjectFeed: false,
};

export const projectFeed = (state = initialState, action) => {
  switch (action.type) {
    case IMFeedActionsConstants.SET_FEED_PROJECTS:
      return { ...state, feedProjects: [...action.data] };
    case IMFeedActionsConstants.SET_FEED_PROJECT_DONATIONS:
      return { ...state, feedProjectsDonations: [...action.data] };
    case IMFeedActionsConstants.DID_SUBSCRIBE:
      return { ...state, didSubscribeToProjectFeed: true };
    case 'LOG_OUT':
      return initialState;
    default:
      return state;
  }
};
