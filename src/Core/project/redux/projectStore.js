import { createStore } from 'redux';
import {projectFeed} from './reducers';

const projectStore = createStore(projectFeed);

export default projectStore;
