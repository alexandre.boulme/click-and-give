import IMFeedActionsConstants from './types';

export const setFeedProjects = data => ({
    type: IMFeedActionsConstants.SET_FEED_PROJECTS,
    data,
});

export const setFeedProjectsDonations = data => ({
    type: IMFeedActionsConstants.SET_FEED_PROJECT_DONATIONS,
    data,
});

export const setProjectsFeedListenerDidSubscribe = () => ({
    type: IMFeedActionsConstants.DID_SUBSCRIBE
});
