import moment from 'moment';

export const timeFormatAndroid = timeStamp => {
  time = '';
  if (timeStamp) {
    if (moment().diff(timeStamp, 'days') == 0) {
      timeh = moment(timeStamp).format('H:mm');
      time = timeh.replace(':', 'h');
    } else {
      time = moment(timeStamp).fromNow();
    }
  }
  // time = postTime.toUTCString();
  return time;
};

export const timeFormatIos = timeStamp => {
  time = '';
  if (timeStamp) {
    if (moment().diff(moment.unix(timeStamp.seconds), 'days') == 0) {
      timeh = moment.unix(timeStamp.seconds).format('HH:mm');
      time = timeh.replace(':', 'h');
    } else {
      time = moment.unix(timeStamp.seconds).fromNow();
    }
  }
  return time;
};

// export const timeFormat =
//   Platform.OS === 'ios' ? timeFormatIos : timeFormatAndroid;
export const timeFormat = timeFormatIos;


export const daysLeftCalculation = (endDate) => {
  var date = 0;
  if(endDate && endDate.seconds) {
    date = moment(endDate.seconds * 1000);
    var todaysDate = moment(new Date());
    var daysLeft = date.diff(todaysDate, 'days');

    return daysLeft;
  } else {
    return 0;
  }

};
