import firebase from '@react-native-firebase/app';
import '@react-native-firebase/storage';
import { Platform } from 'react-native';
import { ErrorCode } from '../onboarding/utils/ErrorCode';

const uploadFileWithProgressTracking = async (
  filename,
  uploadUri,
  callbackSuccess,
  callbackError,
) => {
  firebase
    .storage()
    .ref(filename)
    .putFile(uploadUri)
    .on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => callbackSuccess(snapshot, firebase.storage.TaskState.SUCCESS),
      callbackError,
    ).catch(error => console.log(error));
};

const uploadImage = (uri) => {
  return new Promise((resolve, _reject) => {
    console.log('uri ==', uri);
    const filename = uri.substring(uri.lastIndexOf('/') + 1);
    console.log('filename ==', filename);
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;

    const task = firebase
        .storage()
        .ref(filename)
        .putFile(uploadUri).then(() => {
          console.log('Image uploaded to the bucket!');
          firebase.storage()
              .ref(filename)
              .getDownloadURL().then(url => {
            resolve({downloadURL: url});
          })
              .catch(_error => {
                resolve({error: ErrorCode.photoUploadFailed});
              });
        })
        .catch(_error => {
          resolve({error: ErrorCode.photoUploadFailed});
        });
  });
}

const firebaseStorage = {
  uploadImage,
  uploadFileWithProgressTracking
}

export {
  firebaseStorage
};
