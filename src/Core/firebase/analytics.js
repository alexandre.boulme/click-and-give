import firebase from '@react-native-firebase/app';
import '@react-native-firebase/analytics';
import { getTrackingStatus } from 'react-native-tracking-transparency';

const Analytics = firebase.analytics();
Analytics.setAnalyticsCollectionEnabled(true);

export const sendEvent = async (screenName, parameter) => {
    const trackingStatus = await getTrackingStatus();
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
        try {
            console.log("Event sent : " + screenName);
            if(parameter){
                Analytics.logEvent(screenName, parameter);
            } else {
                Analytics.logEvent(screenName);
            }
        } catch (error){
            console.log(error);
        }
    }
};

export const setupUserInfos = async (user) => {
    const trackingStatus = await getTrackingStatus();
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
        console.log(JSON.stringify(user));
        const id = user.id || user.userID;
        Analytics.setUserId(id);
        Analytics.setUserProperties({"City": user.cityLocation.city});
        Analytics.setUserProperties({"Country": user.cityLocation.country});
        Analytics.setUserProperties({"Role": user.role});
        Analytics.setUserProperties({"Title": user.title});
        if (user.selectedInterests > 0) {
            Analytics.setUserProperties({"Interest1": user.selectedInterests[0]});
        }
        if (user.selectedInterests > 1) {
            Analytics.setUserProperties({"Interest2": user.selectedInterests[1]});
        }
        if (user.selectedInterests > 2) {
            Analytics.setUserProperties({"Interest3": user.selectedInterests[2]});
        }
        if (user.selectedInterests > 3) {
            Analytics.setUserProperties({"Interest4": user.selectedInterests[3]});
        }
    }
};
