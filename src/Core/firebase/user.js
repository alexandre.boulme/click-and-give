import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

export const usersRef = firebase
  .firestore()
  .collection("ClickAndGive_Users");

export const updateUserData = async (userId, userData) => {
  try {
    const userRef = usersRef.doc(userId);

    await userRef.update({
      ...userData,
    });

    return { success: true };
  } catch (error) {
    return { error, success: false };
  }
};

export const getUserById = async (userId) => {
  try {
    const userRef = usersRef.doc(userId);

    const userToRetrieve = await userRef.get();

    return userToRetrieve.data();

  } catch (error) {
    console.log('ERROR : '+ error);
    return { error, success: false };
  }
};

export const subscribeUsers = (userId, callback) => {
  return usersRef.onSnapshot(querySnapshot => {
    const data = [];
    const completeData = [];
    querySnapshot.forEach(doc => {
      const user = doc.data();
      data.push({ ...user, id: doc.id });
      completeData.push({ ...user, id: doc.id });
    });
    return callback(data, completeData);
  });
};

export const subscribeCurrentUser = (userId, callback) => {
  const ref =
    usersRef
      .where("id", "==", userId)
      .onSnapshot({ includeMetadataChanges: true },
        querySnapshot => {
          const docs = querySnapshot.docs;
          if (docs.length > 0) {
            callback(docs[0].data());
          }
        }
      );
  return ref;
};
