import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import '@react-native-firebase/firestore';
import '@react-native-firebase/messaging';
import { ErrorCode } from '../onboarding/utils/ErrorCode';
import {BASE_URL, VERIFICATION_EMAIL_URL} from "react-native-dotenv";

const usersRef = firebase
  .firestore()
  .collection("ClickAndGive_Users");

export const retrievePersistedAuthUser = () => {
  return new Promise(resolve => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        usersRef
          .doc(user.uid)
          .get()
          .then(document => {
            resolve(document.data());
          })
          .catch(errror => {
            resolve(null);
          })
      } else {
        resolve(null);
      }
    });
  });
};

const signInWithCredential = (credential, appIdentifier) => {
  return new Promise((resolve, _reject) => {
    firebase
      .auth()
      .signInWithCredential(credential)
      .then(response => {
        const isNewUser = response.additionalUserInfo.isNewUser;
        const { first_name, last_name } = response.additionalUserInfo.profile;
        const { uid, email, phoneNumber, photoURL } = response.user._user;
        if (isNewUser) {
          const timestamp = firebase.firestore.FieldValue.serverTimestamp();
          const userData = {
            id: uid,
            email: email,
            firstName: first_name,
            lastName: last_name,
            phone: phoneNumber,
            profilePictureURL: photoURL,
            userID: uid,
            createdAt: timestamp
          };

          usersRef
            .doc(uid)
            .set(userData)
            .then(() => {
              resolve({ user: userData, accountCreated: true });
            })
        }
        usersRef
          .doc(uid)
          .get()
          .then(document => {
            resolve({ user: document.data(), accountCreated: false });
          })
      })
      .catch(_error => {
        resolve({ error: ErrorCode.serverError });
      })
  });
};

export const register = (userDetails, appIdentifier) => {
  const { email, firstName, lastName, password, title, role, selectedInterests, profilePictureURL, cityLocation, termsAndDataPrivacyApproved } = userDetails;
  return new Promise(function (resolve, _reject) {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(response => {
        const timestamp = firebase.firestore.FieldValue.serverTimestamp();
        const uid = response.user.uid;
        const data = {
          id: uid,
          userID: uid, // legacy reasons
          email,
          firstName,
          lastName,
          title,
          profilePictureURL,
          cityLocation,
          selectedInterests,
          role,
          termsAndDataPrivacyApproved,
          badgeCount: 0,
          createdAt: timestamp
        };

        usersRef
          .doc(uid)
          .set(data)
          .then(() => {

            //Subscribe user to his interest
            selectedInterests.forEach(interest => {
              firebase.messaging().subscribeToTopic(interest.toLowerCase().replace(/\s/g, ''))
                  .then(data => console.log("Subscribed to topic" + data));
            });

            resolve({ user: data });
          })
          .catch(error => {
            alert(error);
            resolve({ error: ErrorCode.serverError });
          })
      })
      .catch(error => {
        console.log("Dans auth : " + error);
        var errorCode = ErrorCode.serverError;
        if (error.code === 'auth/email-already-in-use') {
          errorCode = ErrorCode.emailInUse;
        }
        resolve({ error: errorCode });
      });
  });
};

export const loginWithEmailAndPassword = async (email, password) => {
  return new Promise(function (resolve, reject) {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => {

        const uid = response.user._user.uid;
        const emailVerified = response.user.emailVerified;
        const userData = {
          email,
          id: uid,
          emailVerified: emailVerified
        };

        usersRef
          .doc(uid)
          .get()
          .then(function (firestoreDocument) {
            if (!firestoreDocument.exists) {
              resolve({ errorCode: ErrorCode.noUser });
              return;
            }
            const user = firestoreDocument.data();
            const newUserData = {
              ...user,
              ...userData
            };

            resolve({ user: newUserData });
          })
          .catch(function (_error) {
            resolve({ error: ErrorCode.serverError });
          });
      })
      .catch(error => {
        var errorCode = ErrorCode.serverError;
        switch (error.code) {
          case 'auth/wrong-password':
            errorCode = ErrorCode.invalidPassword;
            break;
          case 'auth/network-request-failed':
            errorCode = ErrorCode.serverError;
            break;
          case 'auth/user-not-found':
            errorCode = ErrorCode.noUser;
            break;
          default:
            errorCode = ErrorCode.serverError;
        }
        resolve({ error: errorCode });
      });
  });
};

export const sendPasswordResetEmail = (email) => {
  return new Promise((resolve, reject) => {
    firebase.auth().sendPasswordResetEmail(email)
    .then(() => {
      resolve();
    })
    .catch(error => {
      reject(error);
    });
  });
};

export const logout = () => {

  firebase.auth().signOut().then(function() {
    console.log('Signed Out');
  }, function(error) {
    console.error('Sign Out Error', error);
  });
};

export const generateEmailVerificationLink = async () => new Promise((resolve, reject) => {
  fetch(BASE_URL + '/generateEmailVerificationLink', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/form-data',
    },
    body: JSON.stringify({
      email: firebase.auth().currentUser.email
    }),
  })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("Response : " + JSON.stringify(responseJson));
        resolve(responseJson);
      })
      .catch((error) => {
        console.log("Error promise" + JSON.stringify(error.details));
        reject(error);
      });
});

export const sendEmailVerification = () => {

  firebase.auth().currentUser.sendEmailVerification()
    .then(function() {
      // Verification email sent.
      console.log("Email sent");
    })
    .catch(function(error) {
      console.log(error);
      // Error occurred. Inspect error.code.
    });
};

export const reloadUser = (callback) => {
  firebase.auth().currentUser.reload().then(()=> callback);
};

export const updateProfilePhoto = (userID, profilePictureURL) => {
  return new Promise((resolve, _reject) => {
    usersRef
      .doc(userID)
      .update({ profilePictureURL: profilePictureURL })
      .then(() => {
        resolve({ success: true });
      })
      .catch(error => {
        resolve({ error: error });
      })
  });
}

export const registerAppWithFCM = async () => {
    await firebase.messaging().registerDeviceForRemoteMessages();
    await firebase.messaging().setAutoInitEnabled(true);
}

export const fetchPushTokenIfPossible = async () => {
  const messaging = firebase.messaging();
  const hasPushPermissions = await messaging.hasPermission();

  if (hasPushPermissions) {
    const token =  await messaging.getToken();
    return token;
  }
  await messaging.requestPermission();
  return await messaging.getToken();
}

export const updateUser = async (userID, newData) => {
  const dataWithOnlineStatus = {
    ...newData,
    lastOnlineTimestamp: firebase.firestore.FieldValue.serverTimestamp()
  }
  return await usersRef.doc(userID).update({ ...dataWithOnlineStatus });
}

export const resetNotificationBadge = (userID) => {
  return new Promise((resolve, _reject) => {
    usersRef
        .doc(userID)
        .update({ badgeCount: 0 })
        .then(() => {
          console.log("badgeCount reseted");
          resolve({ success: true });
        })
        .catch(error => {
          resolve({ error: error });
          console.log("badgeCount reseted");
        })
  });
}
