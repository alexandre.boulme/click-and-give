import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    headerButtonContainer: {
      ...ifIphoneX({
        padding: 10,
      },{
        padding: 7,
      }),
    },
    Image: {
      ...ifIphoneX({
        width: 45,
        height: 45,
        margin: 10,
      },{
        width: 30,
        height: 30,
        margin: 7,
      }),
    },
    title: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: appStyles.fontSet.middleminus,
    },
  })
};

export default dynamicStyles;
