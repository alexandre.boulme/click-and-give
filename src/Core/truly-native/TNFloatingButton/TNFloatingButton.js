import React from 'react';
import { View, Text, TouchableOpacity, Image, Icon } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';

const TNFloatingButton = props => {
  const styles = new useDynamicStyleSheet(dynamicStyles(AppStyles));
  const { isRound, onPress, buttonStyle } = props;

  return (
    <View style={[styles.container, buttonStyle]}>
    {isRound ? (
      <TouchableOpacity
        style={styles.buttonContainerRound}
        onPress={onPress}
      >
        <Image style={styles.buttonIconRound} source={AppStyles.iconSet.add} />
      </TouchableOpacity>
    ): (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={onPress}
      >
        <Image style={styles.buttonIcon} source={AppStyles.iconSet.easyGive} />
        <Text style={styles.buttonText}>Easy Give</Text>
      </TouchableOpacity>
    )}
    </View>
    )
};

export default TNFloatingButton;
