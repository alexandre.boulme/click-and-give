import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      position: 'absolute',
      ...ifIphoneX({
        bottom: 30,
        right: 30
      },{
        bottom: 22,
        right: 22
      }),
    },
    buttonContainer: {
      flexDirection:'row',
      ...ifIphoneX({
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 50,
      },{
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 30,
      }),
      backgroundColor: "#FFF",
      shadowOffset:{  width: 0,  height: 5,  },
      shadowColor: "#999",
      shadowOpacity: 1.0,
    },
    buttonContainerRound: {
      flexDirection:'row',
      justifyContent: 'center',
      backgroundColor: "#F53950",
      ...ifIphoneX({
        width: 80,
        height: 80,
        borderRadius: 50,
      },{
        width: 60,
        height: 60,
        borderRadius: 30,
      }),
      shadowOffset:{  width: 0,  height: 5,  },
      shadowColor: "#999",
      shadowOpacity: 1.0,
    },
    buttonIconRound:{
      tintColor:"#FFFFFF",
      alignSelf: 'center',
      ...ifIphoneX({
        width: 50,
        height: 50
      },{
        width: 40,
        height: 40
      }),
    },
    buttonIcon:{
      tintColor:"#F53950",
      flexDirection:'row',
      ...ifIphoneX({
        width: 35,
        height: 35
      },{
        width: 25,
        height: 25
      }),
    },
    buttonText: {
      alignSelf: 'center',
      fontSize: appStyles.fontSet.middle,
      color: "#F53950",
      fontWeight: '800',
      marginLeft: 10
    }
})
};

export default dynamicStyles;
