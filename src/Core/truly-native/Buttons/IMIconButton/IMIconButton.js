import React from 'react';
import { Image, TouchableOpacity } from 'react-native';

function IMIconButton(props) {
  const {
    tintColor,
    onPress,
    source,
    marginRight,
    marginLeft,
    width,
    height
  } = props;
  return (
    <TouchableOpacity style={{ marginRight: marginRight, marginLeft: marginLeft}} onPress={onPress}>
      <Image
        style={{ width: width, height: height, tintColor: tintColor }}
        source={source}
      />
    </TouchableOpacity>
  );
};

export default IMIconButton;
