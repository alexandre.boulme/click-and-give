import { Dimensions } from 'react-native';
import { DynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';

const { height, width } = Dimensions.get('window');
const imageSize = width * 0.80;
const photoIconSize = imageSize * 0.27;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    image: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: 20
    },
    imageBlock: {
      flex: 1,
      width: '100%',
      height: 230,
      alignItems: 'center',
      alignSelf: 'center'
    },
    imageConversationBlock: {
      height: 200,
      width: '100%',
      marginVertical: 15,
      alignItems: 'center',
      alignSelf: 'center',
    },
    imageContainer: {
      flex:1,
      borderWidth: 3,
      borderStyle: 'dashed',
      borderColor: '#f7423840',
      backgroundColor: "#f7423810",
      width: '100%',
      borderRadius: 20,
    },
    container: {
      flex:1,
      width: '100%',
    },
    logoAndTextContainer: {
      position: 'absolute',
      top: '20%',
      width: '100%'
    },
    iconStyle:{
      alignSelf: 'center'
    },
    redText:{
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeForegroundColor,
      textAlign: 'center',
      fontWeight: '600',
      marginTop: 10
    },
    blackText:{
      fontSize: appStyles.fontSet.normal,
      color: appStyles.colorSet.mainTextColor,
      textAlign: 'center',
      fontWeight: '400',
      marginTop: 10
    },
    addButton: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#d6d6d6',
      opacity: 0.8,
      zIndex: 2,
      marginTop: '40%',
      marginLeft: '40%',
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },
    closeButton: {
      alignSelf: 'flex-end',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 40,
      marginRight: 15,
      backgroundColor: appStyles.colorSet.grey6,
      width: 28,
      height: 28,
      borderRadius: 20,
      overflow: 'hidden',
    },
    closeIcon: {
      width: 27,
      height: 27,
    },
  })
};

export default dynamicStyles;
