import { DynamicStyleSheet } from 'react-native-dark-mode';
import AppStyles from '../../../AppStyles';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: "center",
      backgroundColor: "#FFFFFF",
      borderBottomColor: AppStyles.colorSet.whiteSmoke,
      borderBottomWidth: 2,
      ...ifIphoneX(
        {
          padding: 15,
        },
        {
          padding: 10,
        },
      ),
    },
    containerRedTheme: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      ...ifIphoneX(
        {
          padding: 10,
        },
        {
          paddingLeft: 20,
          padding: 0,
        },
      ),
      backgroundColor: "#F53950",
      borderBottomColor: "#F53950",
      borderBottomWidth: 2
    },
    darkText: {
      fontSize: AppStyles.fontSet.xlarge,
      fontWeight: '700',
      ...ifIphoneX(
        {
          paddingTop: 10,
        },
        {
          padding: 0,
        },
      ),
      color: "#000"
    },
    lightText: {
      fontSize: AppStyles.fontSet.xlarge,
      fontWeight: '700',
      ...ifIphoneX({
        paddingTop: 8,
      },{
        paddingBottom: 3,
      }),
      color: "#FFF"
    },
    buttonContainer: {
      flexDirection:'row',
      ...ifIphoneX(
        {
          padding: 7,
          paddingTop: 15,
          paddingBottom: 15,
        },
        {
          padding: 7,
          paddingTop: 12,
          paddingBottom: 12,
        },
      ),
      backgroundColor: "#F53950",
      borderRadius: 30
    },
    buttonIcon:{
      tintColor:'#FFFFFF',
      resizeMode: 'contain',
      height: 20,
      width: 20
    },
    buttonIconWrite:{
      tintColor: AppStyles.colorSet.mainThemeForegroundColor,
      resizeMode: 'contain',
      ...ifIphoneX({
        height: 30,
        width: 30
      },{
        height: 28,
        width: 28
      }),
      marginRight: 5
    },
    buttonText: {
      fontSize: AppStyles.fontSet.middle,
      color: "#FFF",
      fontWeight: '600',
      marginLeft: 10
    }
  })
};

export default dynamicStyles;
