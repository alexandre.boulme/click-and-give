import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';

const TNHeaderView = props => {
  const styles = new useDynamicStyleSheet(dynamicStyles(props.appStyles));
  const { headerViewConfig, onPress } = props;

  return (
    <View>
      {headerViewConfig.displayButton ? (
        <View style={styles.container}>
            <Text style={styles.darkText}>{headerViewConfig.text}</Text>
          {headerViewConfig.buttonText ? (
            <TouchableOpacity
              style={[styles.buttonContainer,]}
              onPress={onPress}
            >
              <Image style={styles.buttonIcon} source={AppStyles.iconSet.edit} />
              <Text style={styles.buttonText}>{headerViewConfig.buttonText}</Text>
            </TouchableOpacity>
          ):(
            <TouchableOpacity
                style={[styles.buttonContainer,{backgroundColor: AppStyles.colorSet.white}]}
                onPress={onPress}
            >
              <Image style={styles.buttonIconWrite} source={AppStyles.iconSet.write} />
            </TouchableOpacity>
          )}
        </View>
      ):(
        <View style={styles.containerRedTheme}>
            <Text style={styles.lightText}>{headerViewConfig.text}</Text>
        </View>
      )}
    </View>

    )
};

export default TNHeaderView;
