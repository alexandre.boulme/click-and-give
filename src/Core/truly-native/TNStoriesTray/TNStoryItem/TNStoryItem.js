import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import AppStyles from '../../../../AppStyles';
import Icon from "react-native-vector-icons/FontAwesome";

const defaultAvatar =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

function StoryItem(props) {
  const {
    item,
    index,
    onPress,
    containerStyle,
    imageStyle,
    imageContainerStyle,
    textStyle,
    activeOpacity,
    title,
    editPictureButton,
    appStyles,
    showOnlineIndicator,
    onChatButtonPress,
    backedProjectCount,
    isOtherUser,
    onConversationButtonPress
  } = props;

  const refs = useRef();
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const lastName = item.lastName || '';

  return (
    <View>
    <TouchableOpacity
      key={index}
      ref={refs}
      activeOpacity={activeOpacity}
      onPress={() => onPress(item, index, refs)}
      style={[styles.container, containerStyle]}>
      <View style={[styles.imageContainer, imageContainerStyle]}>
        <FastImage
          style={[styles.image, imageStyle]}
          source={{ uri: item.profilePictureURL || defaultAvatar }}
        />
        {editPictureButton && (
            <TouchableOpacity onPress={() => onPress(item, index, refs)} style={{backgroundColor: appStyles.colorSet.grey, padding:10, borderRadius:50, zIndex: 2,marginTop:  -25,
          marginLeft: 50}}>
          <Icon name="camera" size={20} color="white" />
        </TouchableOpacity>
        )}
      </View>
    </TouchableOpacity>
    {title && (
      <View style={{ alignItems: 'center'}}>
          <View style={{flexDirection:'column', alignSelf: 'center'}}>
            <Text
              style={[
                styles.titleText,
                textStyle,
              ]}>{`${item.firstName} ${lastName}`}</Text>
            <Text
              style={[
                styles.text
              ]}>{`${item.title.toUpperCase()}, ${item.cityLocation.city.toUpperCase()}`}</Text>
          </View>
          {isOtherUser && (
            <TouchableOpacity
              style={styles.chatButton}
              onPress={() => onChatButtonPress([item])}
            >
              <Image source={AppStyles.iconSet.chatUnfilled} style={styles.chatIconButton} />
            </TouchableOpacity>
          )}
          {backedProjectCount > 0 && (
            <View>
            <Text
              style={[
                styles.subTitleText, {fontSize: AppStyles.fontSet.middleminus}
              ]}>Backed</Text>
            <Text
              style={[
                styles.highlightedText
              ]}>{backedProjectCount == 1 ? backedProjectCount + " project" : backedProjectCount + " projects"}</Text>
            </View>
          )}
      </View>
    )}
    </View>
  );
}

StoryItem.propTypes = {
  onPress: PropTypes.func,
  imageStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  item: PropTypes.object,
  index: PropTypes.number,
  activeOpacity: PropTypes.number,
  title: PropTypes.bool,
};

export default StoryItem;
