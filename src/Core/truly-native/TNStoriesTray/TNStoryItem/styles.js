import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
        marginTop: 8,
    },
    imageContainer: {
      ...ifIphoneX({
        width: 50,
        height: 50,
      },{
        width: 45,
        height: 45,
      }),
      borderRadius: Math.floor(50 / 2),
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      ...ifIphoneX({
        width: 50,
        height: 50,
      },{
        width: 45,
        height: 45,
      }),
      borderRadius: Math.floor(50 / 2),
      borderColor: appStyles.colorSet.mainThemeBackgroundColor,
      borderWidth: 1,
      overflow: 'hidden',
    },
    titleText: {
      fontSize: appStyles.fontSet.xlarge,
      textAlign: 'center',
      color: appStyles.colorSet.mainSubtextColor
    },
    subTitleText:{
      fontSize: appStyles.fontSet.middle,
      fontWeight: '600',
      textAlign: 'center',
      color: appStyles.colorSet.mainTextColor,
      ...ifIphoneX({
          marginTop: 20
        },{
          marginTop: 10
      }),
    },
    text: {
      fontSize: appStyles.fontSet.xsmall,
      fontWeight: '900',
      textAlign: 'center',
      color: appStyles.colorSet.grey12,
      paddingTop: 5,
    },
    highlightedText: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: '800',
      textAlign: 'center',
      color: appStyles.colorSet.mainThemeForegroundColor,
      ...ifIphoneX({
        paddingTop: 10,
      },{
        paddingTop: 5,
      })
    },
    isOnlineIndicator: {
      position: 'absolute',
      backgroundColor: '#4acd1d',
      height: 16,
      width: 16,
      borderRadius: 16 / 2,
      borderWidth: 3,
      borderColor: appStyles.colorSet.mainThemeBackgroundColor,
      right: 5,
      bottom: 0,
    },
    chatButton:{
      marginLeft: 10,
      padding: 7,
      borderRadius: 10,
      position: 'absolute',
      ...ifIphoneX({
        top: 20,
        right: 20,
      },{
        top: 10,
        right: 20,
      }),
      backgroundColor: "#FFF",
      shadowOffset:{  width: 0,  height: 5 },
      shadowColor: "#CCC",
      shadowOpacity: 1.0
  },
  chatIconButton: {
    ...ifIphoneX({
      width: 30,
      height: 30
    },{
      width: 25,
      height: 25
    }),
  },

  })
};

export default dynamicStyles;
