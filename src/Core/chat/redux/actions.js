import IMChannelsActionsConstants from './types';

export const setChannels = data => ({
  type: IMChannelsActionsConstants.SET_CHANNELS,
  data,
});

export const setChannelParticipations = data => ({
  type: IMChannelsActionsConstants.SET_CHANNELS_PARTICIPATIONS,
  data,
});

export const setChannelsSubcribed = data => ({
  type: IMChannelsActionsConstants.SET_CHANNELS_SUBCRIBED,
  data,
});
