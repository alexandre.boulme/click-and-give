import IMChannelsActionsConstants from './types';

const initialState = {
  channels: [],
  areChannelsSubcribed: false,
  channelParticipations: [],
};

export const chat = (state = initialState, action) => {
  switch (action.type) {
    case IMChannelsActionsConstants.SET_CHANNELS_SUBCRIBED:
      return {
        ...state,
        areChannelsSubcribed: action.data,
      };
    case IMChannelsActionsConstants.SET_CHANNELS:
      return {
        ...state,
        channels: [...action.data],
      };
    case IMChannelsActionsConstants.SET_CHANNELS_PARTICIPATIONS:
      return {
        ...state,
        channelParticipations: [...action.data],
      };
    case IMChannelsActionsConstants.LOG_OUT:
      return initialState;
    default:
      return state;
  }
};
