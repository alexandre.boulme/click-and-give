import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, FlatList, ActivityIndicator, Image, TextInput } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import IMConversationView from '../IMConversationView';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import { IMLocalized } from '../../localization/IMLocalization';
import { TNEmptyStateView } from '../../truly-native';

function IMConversationList(props) {
  const {
    onConversationPress,
    conversations,
    loading,
    user,
    appStyles
  } = props;

  const [searchQuery, setSearchQuery] = useState('');
  const [displayedConversations, setDisplayedConversations] = useState(null);

  const emptyStateConfigNoChannels = {
    title: IMLocalized('No Conversations'),
    description: IMLocalized(
      'Start chatting with someone. Your conversations will show up here.',
    ),
  };

  const emptyStateConfigNoMatchingChannels = {
    title: IMLocalized('No Conversations'),
    description: IMLocalized(
      'No conversations matching your search.',
    ),
  };

  useEffect(() => {
    if (conversations) {
      setDisplayedConversations(conversations);
    }
  }, [conversations]);

  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));


  const formatMessage = item => {
    if (item.lastMessage && item.lastMessage.mime && item.lastMessage.mime.startsWith('video')) {
      return IMLocalized('Someone sent a video.');
    } else if (
      item.lastMessage &&
      item.lastMessage.mime &&
      item.lastMessage.mime.startsWith('image')
    ) {
      const messagePreview = item.lastSenderID == user.id ? IMLocalized('You sent a photo.'): IMLocalized('Someone sent a photo.');
      return messagePreview;
    } else if (item.lastMessage) {
      return item.lastMessage;
    }
    return '';
  };



  const onSearchQueryChanged = searchQuery => {

    setSearchQuery(searchQuery);

    const query = searchQuery.toLowerCase();
    var conversationsFiltered = conversations.filter(conversation => {

      if(query){
        var containUser = conversation.participants.filter(user => user.firstName.toLowerCase().includes(query)|| user.lastName.toLowerCase().includes(query)).length != 0;

        if(conversation.name && conversation.name.toLowerCase().includes(query) || containUser){
          return conversation;
        };
      } else {
        return conversation;
      }
    });
    setDisplayedConversations(conversationsFiltered);

  }

  const renderConversationView = ({ item }) => (
    <IMConversationView
      formatMessage={formatMessage}
      onChatItemPress={onConversationPress}
      item={item}
      user={user}
      appStyles={appStyles}
    />
  );

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={{ marginTop: 15 }} size="small" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        showsHorizontalScrollIndicator={false}
      >
        <View style={styles.rowContainer} >
          <Image source={AppStyles.iconSet.search} style={styles.searchImage} />
          <TextInput
            style={styles.searchInput}
            placeholder={IMLocalized('Search conversation')}
            placeholderTextColor="#aaaaaa"
            onChangeText={text => onSearchQueryChanged(text)}
            value={searchQuery}
            underlineColorAndroid="transparent"
          />
        </View>
        <View style={styles.chatsChannelContainer}>
          {displayedConversations && displayedConversations.length > 0 && (
            <FlatList
              vertical={true}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              data={displayedConversations}
              renderItem={renderConversationView}
              keyExtractor={item => `${item.id}`}
            />
          )}
          {displayedConversations && displayedConversations.length <= 0 && (
            <View style={styles.emptyViewContainer}>
              <TNEmptyStateView
                emptyStateConfig={conversations.length == 0 ? emptyStateConfigNoChannels : emptyStateConfigNoMatchingChannels}
                appStyles={appStyles}
              />
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

IMConversationList.propTypes = {
  onConversationPress: PropTypes.func,
  conversations: PropTypes.array,
};

export default IMConversationList;
