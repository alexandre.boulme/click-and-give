import React, { useState, useRef } from 'react';
import {Alert, Image, Modal, SafeAreaView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import ActionSheet from 'react-native-actionsheet';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import TNMediaViewerModal from '../../truly-native/TNMediaViewerModal';
import DialogInput from 'react-native-dialog-input';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import BottomInput from './BottomInput';
import MessageThread from './MessageThread';
import ParticipantsModal from './ParticipantsModal';
import dynamicStyles from './styles';
import { IMLocalized } from '../../localization/IMLocalization';
import AppStyles from "../../../AppStyles";
import TNProjectPictureSelector from "../../truly-native/TNProjectPictureSelector/TNProjectPictureSelector";

function IMChat(props) {
  const {
    onSendInput,
    thread,
    participants,
    isGroupConversation,
    inputValue,
    onChangeTextInput,
    user,
    onLaunchCamera,
    onOpenPhotos,
    onAddMediaPress,
    uploadProgress,
    sortMediafromThread,
    isMediaViewerOpen,
    selectedMediaIndex,
    onChatMediaPress,
    onMediaClose,
    onChangeName,
    isRenameDialogVisible,
    groupSettingsActionSheetRef,
    privateSettingsActionSheetRef,
    showRenameDialog,
    onLeave,
    appStyles,
    onUserBlockPress,
    onUserReportPress,
    onSenderProfilePicturePress,
    onParticipantItemPress,
    seenByUsers,
    name,
    pictureUrl
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const [channel] = useState({});
  const [isParticipantsModalVisible, setIsParticipantsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [conversationName, setConversationName] = useState(name);
  const [conversationPictureUrl, setConversationPictureUrl] = useState(pictureUrl);
  const [commentModalIsVisible, setCommentModalIsVisible] = useState(false);

  const photoUploadDialogRef = useRef();

  const onChangeText = text => {
    onChangeTextInput(text);
  };

  const onSend = () => {
    onSendInput();
  };

  const onPhotoUploadDialogDone = index => {
    if (index == 0) {
      onLaunchCamera();
    }

    if (index == 1) {
      onOpenPhotos();
    }
  };

  const onGroupSettingsActionDone = index => {
    if (index == 0) {
      setIsParticipantsModalVisible(true);
    } else if (index == 1) {
      setIsEditModalVisible(true);
    } else if (index == 2) {
      onLeave();
    }
  };

  const displayMessageThread = thread => {


    console.log("THREAD : ", thread);
    console.log("Thread length : ", thread.length);

    thread.forEach((item, i) => {
      const y = i-1;
      const z = i+1;
      if (thread[y]){
        thread[i].lastMessageOfThread = thread[y].senderID != thread[i].senderID ;
      } else {
        thread[i].lastMessageOfThread = true;
      }

      if (thread[z]){
        thread[i].firstMessageOfThread = thread[z].senderID != thread[i].senderID ;
      } else {
        thread[i].firstMessageOfThread = true;
      }

      if(i == 0){
        thread[i].lastMessageOfConversation = true;
      }

    });
    return thread;
  };

  const onPrivateSettingsActionDone = index => {
    if (index == 2) {
      return
    }
    var message, actionCallback;
    if (index == 0) {
      actionCallback = onUserBlockPress;
      message = IMLocalized("Are you sure you want to block this user? You won't see their messages again.");
    } else if (index == 1) {
      actionCallback = onUserReportPress;
      message = IMLocalized("Are you sure you want to report this user? You won't see their messages again.");
    }
    Alert.alert(
      IMLocalized("Are you sure?"),
      message,
      [
        {
          text: IMLocalized("Yes"),
          onPress: actionCallback
        },
        {
          text: IMLocalized("Cancel"),
          style: 'cancel',
        }
      ]
    );
  };

  const onModalCloseButtonPress = () => {
    setIsParticipantsModalVisible(false);
  }

  const onUpdateConversation = () => {
    console.log("Update");
    setIsEditModalVisible(false);
    console.log("Send info, name : ", name);
    console.log("url : ", pictureUrl);
    onChangeName(conversationName, conversationPictureUrl);
  }

  return (
    <SafeAreaView style={[styles.personalChatContainer]}>
      <KeyboardAwareView style={[styles.personalChatContainer]}>
        <MessageThread
          thread={displayMessageThread(thread)}
          user={user}
          isGroupConversation={isGroupConversation}
          appStyles={appStyles}
          onChatMediaPress={onChatMediaPress}
          onSenderProfilePicturePress={onSenderProfilePicturePress}
          seenByUsers={seenByUsers}
          participants={participants}
        />
        <BottomInput
          uploadProgress={uploadProgress}
          value={inputValue}
          onChangeText={onChangeText}
          onSend={onSend}
          appStyles={appStyles}
          onAddMediaPress={() => onAddMediaPress(photoUploadDialogRef)}
        />
        <ActionSheet
          title={IMLocalized('Group Settings')}
          options={[IMLocalized('Rename Group'), IMLocalized('Leave Group'), IMLocalized('Cancel')]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
        />
        <ActionSheet
          title={'Are you sure?'}
          options={['Confirm', 'Cancel']}
          cancelButtonIndex={1}
          destructiveButtonIndex={0}
        />
        <ActionSheet
          ref={photoUploadDialogRef}
          title={IMLocalized('Photo Upload')}
          options={[IMLocalized('Launch Camera'), IMLocalized('Open Photo Gallery'), IMLocalized('Cancel')]}
          cancelButtonIndex={2}
          onPress={onPhotoUploadDialogDone}
        />
        <ActionSheet
          ref={groupSettingsActionSheetRef}
          title={IMLocalized("Group Settings")}
          options={[IMLocalized('Show participants'), IMLocalized('Update Group'), IMLocalized('Leave Group'), IMLocalized('Cancel')]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={onGroupSettingsActionDone}
        />
        <ActionSheet
          ref={privateSettingsActionSheetRef}
          title={IMLocalized("Actions")}
          options={[IMLocalized('Block user'), IMLocalized('Report user'), IMLocalized('Cancel')]}
          cancelButtonIndex={2}
          onPress={onPrivateSettingsActionDone}
        />
        <TNMediaViewerModal
          mediaItems={sortMediafromThread}
          isModalOpen={isMediaViewerOpen}
          onClosed={onMediaClose}
          selectedMediaIndex={selectedMediaIndex}
        />
        <ParticipantsModal
          isVisible={isParticipantsModalVisible}
          appStyles={appStyles}
          participants={participants}
          onCloseButtonPress={() => onModalCloseButtonPress()}
          onUserItemPress={onParticipantItemPress}
        />
        <Modal
            style={{justifyContent: 'center', alignItems:"center"}}
            animationType="slide"
            transparent={true}
            visible={isEditModalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
          <View  style={[styles.modalScrollViewStyle]}>
            <View style={styles.modalContainer}>
              <View style={styles.modalRowContainer}>
                <Text style={styles.modalPopUpTextTitle}>{"Update conversation"}</Text>
                <TouchableOpacity
                    style={styles.cancelButtonContainer}
                    onPress={() => {
                      setIsEditModalVisible(false);
                    }}>
                  <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                </TouchableOpacity>
              </View>
              <Text style={styles.subTitle}>{'Conversation name'}</Text>
              <TextInput
                  onChangeText={text => {
                    console.log("change text : ", text);
                    setConversationName(text);
                  }}
                  value={conversationName}
                  style={styles.searchInput}
                  underlineColorAndroid="transparent"
                  placeholder={'Type a name'}
                  placeholderTextColor="#aaaaaa"
              />
              <Text style={styles.subTitle}>{"Conversation picture"}</Text>
              <TNProjectPictureSelector
                  projectPictureURL={conversationPictureUrl}
                  setProfilePictureURL={setConversationPictureUrl}
                  isConversationPicture={true}
                  appStyles={AppStyles}
              />
              <TouchableOpacity
                  style={styles.updateButton}
                  onPress={() => onUpdateConversation()}>
                <Text style={styles.updateButtonText}>UPDATE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </KeyboardAwareView>
    </SafeAreaView>
  );
}

IMChat.propTypes = {
  onSendInput: PropTypes.func,
  onChangeName: PropTypes.func,
  onChangeTextInput: PropTypes.func,
  onLaunchCamera: PropTypes.func,
  onOpenPhotos: PropTypes.func,
  onAddMediaPress: PropTypes.func,
  user: PropTypes.object,
  uploadProgress: PropTypes.number,
  isMediaViewerOpen: PropTypes.bool,
  isRenameDialogVisible: PropTypes.bool,
  selectedMediaIndex: PropTypes.number,
  onChatMediaPress: PropTypes.func,
  onMediaClose: PropTypes.func,
  showRenameDialog: PropTypes.func,
  onLeave: PropTypes.func,
};

export default IMChat;
