import { DynamicStyleSheet } from 'react-native-dark-mode';
import { size } from '../../helpers/devices';
import {ifIphoneX} from "react-native-iphone-x-helper";
import AppStyles from "../../../AppStyles";

const dynamicStyles = (appStyles) => {
  const chatBackgroundColor = appStyles.colorSet.mainThemeBackgroundColor;

  return new DynamicStyleSheet({
    personalChatContainer: {
      backgroundColor: chatBackgroundColor,
      flex: 1,
      flexDirection: 'column'
    },
    //Bottom Input
    inputBar: {
      justifyContent: 'center',
      alignItems: 'center',
      borderTopWidth: 1,
      borderTopColor: appStyles.colorSet.hairlineColor,
      flexDirection: 'row',
    },
    progressBar: {
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      height: 3,
      shadowColor: '#000',
      width: 0,
    },
    inputIconContainer: {
      margin: 10,
    },
    inputIcon: {
      tintColor: appStyles.colorSet.mainThemeForegroundColor,
      width: 35,
      height: 35,
    },
    input: {
      margin: 5,
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      flex: 1,
      backgroundColor: appStyles.colorSet.whiteSmoke,
      fontSize: appStyles.fontSet.middle,
      borderRadius: 20,
      color: appStyles.colorSet.mainTextColor,
    },
    // Message Thread
    messageThreadContainer: {
      margin: 6,
    },
    // Thread Item
    sendItemContainer: {
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      flexDirection: 'row',
      marginBottom: 2,
    },
    itemContent: {
      padding: 10,
      backgroundColor: appStyles.colorSet.hairlineColor,
      borderRadius: 20,
      maxWidth: '90%',
    },
    itemSmallRadiusTopLeftCorner:{
      borderTopLeftRadius: 3,
    },
    itemSmallRadiusBottomLeftCorner:{
      borderBottomLeftRadius: 3,
    },
    itemSmallRadiusTopRightCorner:{
      borderTopRightRadius: 3,
    },
    itemSmallRadiusBottomRightCorner:{
      borderBottomRightRadius: 3,
    },
    sendItemContent: {
      marginRight: 10,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
    },
    mediaMessage: {
      width: size(300),
      height: size(250),
      borderRadius: 10,
    },
    boederImgSend: {
      position: 'absolute',
      width: size(300),
      height: size(250),
      resizeMode: 'stretch',
      tintColor: chatBackgroundColor,
    },
    sendTextMessage: {
      fontSize: appStyles.fontSet.middle,
      color: appStyles.colorSet.mainThemeBackgroundColor,
    },
    userIcon: {
      width: 45,
      height: 45,
      borderRadius: 50,
    },
    receiveItemContainer: {
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      flexDirection: 'row',
      marginBottom: 2,
    },
    receiveItemContent: {
      marginLeft: 9,
    },
    boederImgReceive: {
      position: 'absolute',
      width: size(300),
      height: size(250),
      resizeMode: 'stretch',
      tintColor: chatBackgroundColor,
    },
    receiveTextMessage: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: appStyles.fontSet.middle,
    },
    textBoederImgReceive: {
      position: 'absolute',
      left: -5,
      bottom: 0,
      width: 20,
      height: 8,
      resizeMode: 'stretch',
      tintColor: appStyles.colorSet.hairlineColor,
    },
    mediaVideoLoader: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    centerItem: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    playButton: {
      position: 'absolute',
      top: '40%',
      alignSelf: 'center',
      width: 38,
      height: 38,
      backgroundColor: 'transparent',
    },
    messageTimeText: {
      marginTop: 2,
      color: "#666",
      fontSize: appStyles.fontSet.xsmall
    },
    messageNameText: {
      marginTop: 2,
      color: "#666",
      fontSize: appStyles.fontSet.small
    },
    modalContainer:{
      height: '100%',
      width: '100%',
      backgroundColor: '#00000030'
    },
    modalBackground:{
      alignSelf: 'center',
      marginTop: '20%',
      backgroundColor: appStyles.colorSet.white,
      borderRadius: 20,
      width: '90%',
      height: '60%',
    },
    rowContainer:{
      flexDirection:'row',
      justifyContent: 'flex-end',
      marginVertical: 5
    },
    modalCloseImage:{
      height: 40,
      width: 40,
      margin: 10
    },
    itemContainer: {
      padding: 12,
      alignItems: 'center',
      flexDirection: 'row',
    },
    chatIconContainer: {
      flex: 6,
      flexDirection: 'row',
      alignItems: 'center',
    },
    addFlexContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    divider: {
      bottom: 0,
      left: 0,
      right: 0,
      position: 'absolute',
      height: 0.5,
      backgroundColor: appStyles.colorSet.hairlineColor,
    },
    photo: {
      height: 50,
      borderRadius: 30,
      width: 50,
    },
    name: {
      marginLeft: 20,
      alignSelf: 'center',
      flex: 1,
      fontSize: appStyles.fontSet.middleminus,
      fontWeight: '500',
      color: appStyles.colorSet.mainTextColor,
    },


    modalScrollViewStyle: {
      flex:1,
      backgroundColor: "#22222240",
      width: '100%'
    },
    headlineText: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: '500',
      color: appStyles.colorSet.mainTextColor,
      ...ifIphoneX({
        marginVertical: 10
      },{
        marginVertical: 5
      }),
    },
    basicButton: {
      ...ifIphoneX({
        padding: 15,
      },{
        padding: 10,
      }),
      borderRadius: 10,
      borderWidth: 1,
      marginLeft: 10,
      borderColor: appStyles.colorSet.mainThemeForegroundColor
    },
    buttonText: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: '400',
      color: appStyles.colorSet.mainThemeForegroundColor,
      textAlign: 'center'
    },
    modalCancelButtonContainer: {
      ...ifIphoneX({
        padding: 5,
      },{
        padding: 5,
      }),
    },
    modalPopUpTextTitle: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: appStyles.fontSet.large,
      fontWeight: '800'
    },
    subTitle: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
      color: appStyles.colorSet.black,
      marginTop: 25,
      marginBottom: 10,
      alignSelf: 'stretch',
      textAlign: 'left',
    },
    modalViewContainer: {
      marginTop: '20%',
      alignSelf: 'center',
      width: '90%',
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
      backgroundColor: "#FFF",
      borderRadius: 20,
      shadowOffset:{  width: 0,  height: 5,  },
      shadowColor: "#999",
      shadowOpacity: 1.0,
    },
    scrollViewStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: "#00000070",

    },
    modalContainer: {
      backgroundColor: "#FFF",
      borderRadius: 15,
      padding: 20,
      paddingTop: 0,
      marginTop: "10%",
      width: '90%',
      alignSelf: "center"
    },
    modalRowContainer:{
      flexDirection: 'row',
      justifyContent: "space-between",
      alignContent: 'center',
      alignItems: 'center',
      marginVertical: 5
    },
    cancelButtonContainer: {
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
    },
    updateButton:{
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
      backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
      borderRadius: 30
    },
    updateButtonText: {
      fontSize: AppStyles.fontSet.normal,
      fontWeight: '600',
      color: AppStyles.colorSet.mainThemeBackgroundColor,
      textAlign: 'center'
    },
    applyButtonContainer: {
      ...ifIphoneX({
        padding: 20,
        paddingLeft: 30,
        paddingRight: 30,
      },{
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
      }),
      borderRadius: 30,
      backgroundColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    applyButton : {
      fontSize: AppStyles.fontSet.normal,
      fontWeight: '600',
      color: "#FFF",
      textAlign: 'center'
    },
    resetFiltersButton: {
      marginVertical: 15,
      alignItems: 'center',
      padding: 15,
      backgroundColor: "#FFF",
      borderColor: AppStyles.colorSet.mainThemeForegroundColor,
      borderWidth: 2,
      borderRadius: 10
    },
    resetFiltersButtonText: {
      color: AppStyles.colorSet.mainThemeForegroundColor,
      fontSize: AppStyles.fontSet.middleminus,
      fontWeight: '400'
    },
    containerInactive: {
      borderColor: "#222"
    },
    textInactive: {
      color: "#222"
    },
    modalVisibleContainer: {
      alignSelf: 'center',
      ...ifIphoneX({
        width: 40,
        height: 40,
        marginRight: 15,
        shadowOffset:{  width: 0,  height: 5,  },
      },{
        width: 35,
        height: 35,
        marginRight: 10,
        shadowOffset:{  width: 0,  height: 3,  },
      }),
      backgroundColor: "#FFF",
      shadowColor: "#999",
      shadowOpacity: 1.0,
      borderRadius: 5
    },
    modalVisibleIcon: {
      alignSelf: 'flex-end',
      ...ifIphoneX({
        width: 30,
        height: 30,
        margin: 5,
      },{
        width: 23,
        height: 23,
        margin: 5,
      }),
      tintColor: AppStyles.colorSet.black
    },
    InputContainer: {
      padding: 5,
      paddingBottom: 10,
      color: AppStyles.colorSet.mainTextColor,
      width: '100%',
      alignSelf: 'stretch',
      alignItems: 'center',
      fontSize:   AppStyles.fontSet.middle,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: 'left',
    },
    popUpText: {
      color: AppStyles.colorSet.mainTextColor,
      fontSize: AppStyles.fontSet.middle,
      fontWeight: '400',
      marginVertical: 15,
      marginBottom: 30
    },
    popUpTextTitle: {
      color: AppStyles.colorSet.mainTextColor,
      fontSize: AppStyles.fontSet.large,
      fontWeight: '800'
    }

  })
};

export default dynamicStyles;
