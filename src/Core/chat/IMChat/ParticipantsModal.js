import React, { useState, useRef } from 'react';
import {
  View,
  Modal,
  TouchableOpacity,
  Image,
  Text,
  FlatList
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { IMConversationIconView } from '../IMConversationView/IMConversationIconView/IMConversationIconView';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';

function ParticipantsModal(props) {
  const {isVisible, appStyles, participants, onCloseButtonPress, onUserItemPress} = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => {
        onUserItemPress(item);
        onCloseButtonPress();
      }}
      style={styles.itemContainer}>
      <View style={styles.chatIconContainer}>
        <FastImage
          style={styles.photo}
          source={{uri: item.profilePictureURL }}
          resizeMode={FastImage.resizeMode.cover}/>
        <Text style={styles.name}>{item.firstName + ' ' + item.lastName}</Text>
      </View>
      <View style={styles.divider} />
    </TouchableOpacity>
  );

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}>
      <View style={styles.modalContainer}>
      <View style={styles.modalBackground}>
        <View style={styles.rowContainer}>
          <TouchableOpacity
            onPress={onCloseButtonPress}>
            <Image source={appStyles.iconSet.close} style={styles.modalCloseImage}/>
          </TouchableOpacity>
        </View>
        <FlatList
          data={participants}
          renderItem={renderItem}
        />
      </View>
      </View>
    </Modal>
  );
}

ParticipantsModal.propTypes = {};

export default ParticipantsModal;
