import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  Platform,
  NativeModules,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import ThreadMediaItem from './ThreadMediaItem';
import dynamicStyles from './styles';
import { timeFormat } from '../..';
import Hyperlink from 'react-native-hyperlink';
import AppStyle from "../../../AppStyles";
import AppStyles from "../../../AppStyles";
import CategoryRadioButtonSelector
  from "../../../components/screens/CreateProject/components/CategoryRadioButtonSelector";

const { VideoPlayerManager } = NativeModules;
const defaultAvatar =
  'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg';

const assets = {
  boederImgSend: require('../assets/borderImage.png'),
  boederImgReceive: require('../assets/borderImage.png'),
}

function ThreadItem(props) {
  const { item, user, onChatMediaPress, isGroupConversation, onSenderProfilePicturePress, appStyles, seenByUser } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const [imgErr, setImgErr] = useState(false);
  const videoRef = useRef(null);

  const onImageError = () => {
    setImgErr(true);
  };

  const didPressMediaChat = () => {
    if (item.url && item.url.mime && item.url.mime.startsWith('video')) {
      if (Platform.OS === 'android') {
        VideoPlayerManager.showVideoPlayer(item.url.url);
      } else {
        if (videoRef.current) {
          videoRef.current.presentFullscreenPlayer();
        }
      }
    } else {
      onChatMediaPress(item);
    }
  };

  return (
    <View>
      {/* user thread item */}
      {item.senderID === user.userID && (
        <View style={styles.sendItemContainer}>
          {(item.url != null && item.url != '') && (
            <View style={{alignItems: 'flex-end', marginRight: 30}}>
              <TouchableOpacity
                onPress={didPressMediaChat}
                activeOpacity={0.9}
                style={[
                  styles.itemContent,
                  styles.sendItemContent,
                  { padding: 0, marginRight: -1 },
                  item.lastMessageOfThread && !item.firstMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusTopRightCorner : null,
                  item.firstMessageOfThread && !item.lastMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusBottomRightCorner : null
                ]}>
                <ThreadMediaItem
                  videoRef={videoRef}
                  dynamicStyles={styles}
                  item={item}
                />
                <Image
                  source={assets.boederImgSend}
                  style={styles.boederImgSend}
                />
              </TouchableOpacity>
              {item.lastMessageOfThread && (
                  <View style={{marginRight: -35}}>
                    <Text style={[styles.messageTimeText, {marginRight: 15, marginBottom: 10, textAlign:'right'}]}>{timeFormat(item.created)}</Text>
                    <View style={{flexDirection:"row"}}>
                      {item.lastMessageOfConversation && seenByUser && seenByUser.map((item, i) => (
                          <FastImage
                              style={{width: 17, height: 17, borderRadius: 10, marginHorizontal: 3}}
                              source={{ uri: item.profilePictureURL}}
                          />
                      ))}
                    </View>
                  </View>
                )}
            </View>
          )}
          {!item.url && (
            <View style={{alignItems: 'flex-end', maxWidth: '80%'}}>
              <View
                style={[
                  styles.itemContent,
                  styles.sendItemContent,
                  { maxWidth: '100%' },
                  item.lastMessageOfThread && !item.firstMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusTopRightCorner : null,
                  item.firstMessageOfThread && !item.lastMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusBottomRightCorner : null
                ]}>
                <Hyperlink
                    linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
                    linkDefault>
                  <Text style={styles.sendTextMessage}>{item.content}</Text>
                </Hyperlink>
              </View>
              {item.lastMessageOfThread && (
                <View>
                  <Text style={[styles.messageTimeText, {marginRight: 15, marginBottom: 10, textAlign:'right'}]}>{timeFormat(item.created)}</Text>
                  <View style={{flexDirection:"row"}}>
                    {item.lastMessageOfConversation && seenByUser && seenByUser.map((item, i) => (
                        <FastImage
                            style={{width: 17, height: 17, borderRadius: 10, marginHorizontal: 3}}
                            source={{ uri: item.profilePictureURL}}
                        />
                    ))}
                  </View>
                </View>
              )}
            </View>
          )}
          {item.lastMessageOfThread ? (
            <TouchableOpacity
              onPress={() => onSenderProfilePicturePress && onSenderProfilePicturePress(item)}>
              <FastImage
                style={styles.userIcon}
                source={
                  imgErr || !item.senderProfilePictureURL
                    ? { uri: defaultAvatar }
                    : { uri: item.senderProfilePictureURL }
                }
                onError={onImageError}
              />
            </TouchableOpacity>
          ):(
            <View style={{width: 45}}/>
          )}
        </View>
      )}

      {/* receiver thread item */}
      {item.senderID !== user.userID && (
        <View style={styles.receiveItemContainer}>
        {item.lastMessageOfThread ? (
          <TouchableOpacity onPress={() => onSenderProfilePicturePress && onSenderProfilePicturePress(item)}>
            <FastImage
              style={styles.userIcon}
              source={
                imgErr
                  ? { uri: defaultAvatar }
                  : { uri: item.senderProfilePictureURL }
              }
              onError={onImageError}
            />
          </TouchableOpacity>
        ) : (
          <View style={{width: 45}}/>
        )}
          {item.url != null && item.url != '' && (
            <View style={{alignItems: 'flex-start', marginRight: 30}}>
              {item.firstMessageOfThread && isGroupConversation && (
                <Text style={[styles.messageNameText,{marginLeft: 20, marginBottom: 3}]}>{item.senderFirstName}</Text>
              )}
              <TouchableOpacity
                activeOpacity={0.9}
                style={[
                  styles.itemContent,
                  styles.receiveItemContent,
                  { padding: 0, marginLeft: -1 },
                  item.lastMessageOfThread && !item.firstMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusTopLeftCorner : null,
                  item.firstMessageOfThread && !item.lastMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusBottomLeftCorner : null
                ]}
                onPress={() => onChatMediaPress(item)}>
                <ThreadMediaItem
                  videoRef={videoRef}
                  dynamicStyles={styles}
                  item={item}
                />
                <Image
                  source={assets.boederImgReceive}
                  style={styles.boederImgReceive}
                />
              </TouchableOpacity>
              {item.lastMessageOfThread && (
                <View style={{marginRight: -35}}>
                  <Text style={[styles.messageTimeText, {marginRight: 15, marginBottom: 10, textAlign:'right'}]}>{timeFormat(item.created)}</Text>
                  <View style={{flexDirection:"row"}}>
                    {item.lastMessageOfConversation && seenByUser && seenByUser.map((item, i) => (
                        <FastImage
                            style={{width: 17, height: 17, borderRadius: 10, marginHorizontal: 3}}
                            source={{ uri: item.profilePictureURL}}
                        />
                    ))}
                  </View>
                </View>
              )}
            </View>
          )}
          {!item.url && (
            <View style={{alignItems: 'flex-start', maxWidth: '80%'}}>
              {item.firstMessageOfThread && isGroupConversation && (
                <Text style={[styles.messageNameText,{marginLeft: 20, marginBottom: 3}]}>{item.senderFirstName}</Text>
              )}
              <View
                style={[
                  styles.itemContent,
                  styles.receiveItemContent,
                  { maxWidth: '100%', justifyContent: 'flex-start' },
                  item.lastMessageOfThread && !item.firstMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusTopLeftCorner : null,
                  item.firstMessageOfThread && !item.lastMessageOfThread || !item.lastMessageOfThread && !item.firstMessageOfThread ? styles.itemSmallRadiusBottomLeftCorner : null
                ]}>
                <Hyperlink
                    linkStyle={ { color: AppStyles.colorSet.mainThemeForegroundColor, textDecorationLine:'underline' } }
                    linkDefault>
                  <Text style={styles.receiveTextMessage}>{item.content}</Text>
                </Hyperlink>
              </View>
              {item.lastMessageOfThread && (
                <View>
                  <Text style={[styles.messageTimeText, {marginRight: 15, marginBottom: 10, textAlign:'right'}]}>{timeFormat(item.created)}</Text>
                  <View style={{flexDirection:"row"}}>
                    {item.lastMessageOfConversation && seenByUser && seenByUser.map((item, i) => (
                        <FastImage
                            style={{width: 17, height: 17, borderRadius: 10, marginHorizontal: 3}}
                            source={{ uri: item.profilePictureURL}}
                        />
                    ))}
                  </View>
                </View>
              )}
            </View>
          )}
        </View>
      )}
    </View>
  );
}

ThreadItem.propTypes = {};

export default ThreadItem;
