import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import dynamicStyles from './styles';
import { IMConversationListView } from '../..';

function IMChatHomeComponent(props) {
  const {
    navigation,
    appStyles,
    onEmptyStatePress,
  } = props;

  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  return (
    <View style={styles.container}>
      <ScrollView style={styles.container}>
        <View style={styles.chatsChannelContainer}>
          <IMConversationListView
            navigation={navigation}
            appStyles={appStyles}
          />
        </View>
      </ScrollView>
    </View>
  );
}

IMChatHomeComponent.propTypes = {
  channels: PropTypes.array,
};

export default IMChatHomeComponent;
