import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import TextButton from 'react-native-button';
import { connect } from 'react-redux';
import { IMCreateGroupComponent } from '../..';
import { channelManager } from '../../firebase'
import { IMLocalized } from '../../../localization/IMLocalization';
import AppStyles from '../../../../AppStyles';
import {firebaseAnalytics} from "../../../firebase";

class IMCreateGroupScreen extends Component {

  static navigationOptions = ({ screenProps, navigation }) => {
    let appStyles = navigation.state.params.appStyles;
    let donationCreation = navigation.state.params.donationCreation;
    let currentTheme = appStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: donationCreation ? IMLocalized('Choose donator'): IMLocalized('Choose People'),
      headerRight: (params.onCreate != null) && !donationCreation ? (
        <TextButton style={{ marginHorizontal: 7, color: currentTheme.activeTintColor }} onPress={params.onCreate}>
          {IMLocalized("Create chat")}
        </TextButton>
      ) : null,
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
      },
      headerTintColor: currentTheme.activeTintColor,
    };
  };

  constructor(props) {
    super(props);
    this.appStyles = this.props.navigation.getParam('appStyles');
    this.donationCreation = this.props.navigation.getParam('donationCreation');
    this.getUserFromSeletion = this.props.navigation.getParam('getUserFromSeletion');
    const userToDisplay = this.props.users.filter(userInList => userInList.id != this.props.user.id && !userInList.desactivated);

    this.state = {
      users: userToDisplay,
      isNameDialogVisible: false,
      groupName: '',
    };

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onCreate: (this.props.users.length >= 1) ? this.onCreate : null,
    });

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onCreate = () => {
    const checkedUsers = this.state.users.filter(user => user.checked);
    if (checkedUsers.length === 0) {
      alert('Please choose at least two users.');
    } else if (checkedUsers.length === 1) {
      this.onSubmitName(checkedUsers);
      firebaseAnalytics.sendEvent("creation_one_to_one_conversation", null);
    } else {
      this.setState({ isNameDialogVisible: true });
      firebaseAnalytics.sendEvent("creation_group_conversation", {numberOfParticipants: checkedUsers.length});
    }
  };

  onCheck = user => {
    if(this.donationCreation){
      this.props.navigation.goBack();
      this.getUserFromSeletion(user);
    } else {
      user.checked = !user.checked;
      const newUsers = this.state.users.map(item => {
        if (item.id == user.id) {
          return user;
        }
        return item;
      });
      this.setState({ users: newUsers });
    }
  };

  onCancel = () => {
    this.setState({
      groupName: '',
      isNameDialogVisible: false,
      users: this.props.users,
    });
  };

  onSubmitName = (name, pictureUrl) => {
    const self = this;
    const { users } = this.state;
    const participants = users.filter(user => user.checked);
    if (participants.length < 2) {
      // alert(IMLocalized('Choose at least 2 friends to create a group.'));
      // return;
      this.navigationToOneAndOneConversation(participants);
    } else {
        channelManager
            .createChannel(self.props.user, participants, name, pictureUrl)
            .then(response => {
              if (response.success == true) {

                const channel = {
                  id: response.channel.id,
                  name: response.channel.name,
                  pictureUrl: response.channel.pictureUrl,
                  participants: participants
                };

                self.onCancel();
                self.props.navigation.navigate('PersonalChat', {  channel, appStyles: AppStyles });
              }
            });
    }
  };

  navigationToOneAndOneConversation = participants => {
    const id1 = this.props.user.id || this.props.user.userID;
    const id2 = participants[0].id || participants[0].userID;
    const channel = {
      id: id1 < id2 ? id1 + id2 : id2 + id1,
      participants: participants
    };

    console.log("solo channel data : " + JSON.stringify(channel));

    this.props.navigation.navigate('PersonalChat', {
      channel,
      appStyles: AppStyles,
    });
  };

  onEmptyStatePress = () => {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <IMCreateGroupComponent
        onCancel={this.onCancel}
        isNameDialogVisible={this.state.isNameDialogVisible}
        users={this.state.users}
        onSubmitName={this.onSubmitName}
        onCheck={this.onCheck}
        appStyles={this.appStyles}
        onEmptyStatePress={this.onEmptyStatePress}
      />
    );
  }
}

IMCreateGroupScreen.propTypes = {
  users: PropTypes.array,
  user: PropTypes.object,
};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
    users: auth.users
  };
};

export default connect(mapStateToProps)(IMCreateGroupScreen);
