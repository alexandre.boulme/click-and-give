import {DynamicStyleSheet} from 'react-native-dark-mode';
import { Dimensions } from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import AppStyles from "../../../../AppStyles";

const { height } = Dimensions.get('window');

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
    },
    rowContainer: {
      flexDirection: 'row',
      backgroundColor: appStyles.colorSet.whiteSmoke,
      borderRadius: 20,
      marginTop: 5,
      marginLeft: 20,
      marginRight: 20
    },
    searchImage:{
      height: 20,
      width: 20,
      tintColor: appStyles.colorSet.grey,
      alignSelf: 'center',
      marginLeft: 15,
      marginRight: 5
    },
    searchInput:{
      height: 42,
      width: '70%',
      color: appStyles.colorSet.grey,
      fontSize: appStyles.fontSet.middleminus,
    },
    itemContainer: {
      padding: 12,
      alignItems: 'center',
      flexDirection: 'row',
    },
    chatIconContainer: {
      flex: 6,
      flexDirection: 'row',
      alignItems: 'center',
    },
    addFlexContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    divider: {
      bottom: 0,
      left: 0,
      right: 0,
      position: 'absolute',
      height: 0.5,
      backgroundColor: appStyles.colorSet.hairlineColor,
    },
    photo: {
      height: 50,
      borderRadius: 30,
      width: 50,
    },
    name: {
      marginLeft: 20,
      alignSelf: 'center',
      flex: 1,
      fontSize: appStyles.fontSet.middleminus,
      fontWeight: '500',
      color: appStyles.colorSet.mainTextColor,
    },
    chatItemIcon: {
      height: 70,
      // borderRadius: 45,
      width: 70,
    },
    checkContainer: {},
    checked: {
      width: 25,
      height: 25,
      tintColor: "#36D068"
    },
    emptyViewContainer: {
      marginTop: height / 6
    },
    modalScrollViewStyle: {
      flex:1,
      backgroundColor: "#22222240",
      width: '100%'
    },
    headlineText: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: '500',
      color: appStyles.colorSet.mainTextColor,
      ...ifIphoneX({
        marginVertical: 10
      },{
        marginVertical: 5
      }),
    },
    basicButton: {
      ...ifIphoneX({
        padding: 15,
      },{
        padding: 10,
      }),
      borderRadius: 10,
      borderWidth: 1,
      marginLeft: 10,
      borderColor: appStyles.colorSet.mainThemeForegroundColor
    },
    buttonText: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: '400',
      color: appStyles.colorSet.mainThemeForegroundColor,
      textAlign: 'center'
    },
    modalCancelButtonContainer: {
      ...ifIphoneX({
        padding: 5,
      },{
        padding: 5,
      }),
    },
    modalPopUpTextTitle: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: appStyles.fontSet.large,
      fontWeight: '800'
    },
    subTitle: {
      fontSize: appStyles.fontSet.middle,
      fontWeight: 'bold',
      color: appStyles.colorSet.black,
      marginTop: 25,
      marginBottom: 10,
      alignSelf: 'stretch',
      textAlign: 'left',
    },
    modalViewContainer: {
      marginTop: '20%',
      alignSelf: 'center',
      width: '90%',
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
      backgroundColor: "#FFF",
      borderRadius: 20,
      shadowOffset:{  width: 0,  height: 5,  },
      shadowColor: "#999",
      shadowOpacity: 1.0,
    },
    scrollViewStyle: {
      width: '100%',
      height: '100%',
      backgroundColor: "#00000070",

    },
    modalContainer: {
      backgroundColor: "#FFF",
      borderRadius: 15,
      padding: 20,
      paddingTop: 0,
      marginTop: "10%",
      width: '90%',
      alignSelf: "center"
    },
    modalRowContainer:{
      flexDirection: 'row',
      justifyContent: "space-between",
      alignContent: 'center',
      alignItems: 'center',
      marginVertical: 5
    },
    cancelButtonContainer: {
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
    },
    updateButton:{
      ...ifIphoneX({
        padding: 20,
      },{
        padding: 10,
      }),
      backgroundColor: AppStyles.colorSet.mainThemeForegroundColor,
      borderRadius: 30
    },
    updateButtonText: {
      fontSize: AppStyles.fontSet.normal,
      fontWeight: '600',
      color: AppStyles.colorSet.mainThemeBackgroundColor,
      textAlign: 'center'
    },
    applyButtonContainer: {
      ...ifIphoneX({
        padding: 20,
        paddingLeft: 30,
        paddingRight: 30,
      },{
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
      }),
      borderRadius: 30,
      backgroundColor: AppStyles.colorSet.mainThemeForegroundColor
    },
    applyButton : {
      fontSize: AppStyles.fontSet.normal,
      fontWeight: '600',
      color: "#FFF",
      textAlign: 'center'
    },
    resetFiltersButton: {
      marginVertical: 15,
      alignItems: 'center',
      padding: 15,
      backgroundColor: "#FFF",
      borderColor: AppStyles.colorSet.mainThemeForegroundColor,
      borderWidth: 2,
      borderRadius: 10
    },
    resetFiltersButtonText: {
      color: AppStyles.colorSet.mainThemeForegroundColor,
      fontSize: AppStyles.fontSet.middleminus,
      fontWeight: '400'
    },
    containerInactive: {
      borderColor: "#222"
    },
    textInactive: {
      color: "#222"
    },
    modalVisibleContainer: {
      alignSelf: 'center',
      ...ifIphoneX({
        width: 40,
        height: 40,
        marginRight: 15,
        shadowOffset:{  width: 0,  height: 5,  },
      },{
        width: 35,
        height: 35,
        marginRight: 10,
        shadowOffset:{  width: 0,  height: 3,  },
      }),
      backgroundColor: "#FFF",
      shadowColor: "#999",
      shadowOpacity: 1.0,
      borderRadius: 5
    },
    modalVisibleIcon: {
      alignSelf: 'flex-end',
      ...ifIphoneX({
        width: 30,
        height: 30,
        margin: 5,
      },{
        width: 23,
        height: 23,
        margin: 5,
      }),
      tintColor: AppStyles.colorSet.black
    },
    InputContainer: {
      padding: 5,
      paddingBottom: 10,
      color: AppStyles.colorSet.mainTextColor,
      width: '100%',
      alignSelf: 'stretch',
      alignItems: 'center',
      fontSize:   AppStyles.fontSet.middle,
      borderBottomWidth :1,
      borderBottomColor: "#000",
      textAlign: 'left',
    },
    popUpText: {
      color: AppStyles.colorSet.mainTextColor,
      fontSize: AppStyles.fontSet.middle,
      fontWeight: '400',
      marginVertical: 15,
      marginBottom: 30
    },
    popUpTextTitle: {
      color: AppStyles.colorSet.mainTextColor,
      fontSize: AppStyles.fontSet.large,
      fontWeight: '800'
    }
  })
};

export default dynamicStyles;
