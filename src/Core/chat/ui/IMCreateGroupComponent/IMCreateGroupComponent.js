import React, { useState, useEffect } from 'react';
import {
    FlatList,
    TouchableOpacity,
    Image,
    Text,
    View,
    ActivityIndicator,
    TextInput,
    Modal,
    ScrollView,
    Alert
} from 'react-native';
import DialogInput from 'react-native-dialog-input';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import PropTypes from 'prop-types';
import { IMConversationIconView } from '../..';
import AppStyles from '../../../../AppStyles';
import dynamicStyles from './styles';
import { TNEmptyStateView } from '../../../truly-native';
import { IMLocalized } from '../../../localization/IMLocalization';
import TNProjectPictureSelector from '../../../truly-native/TNProjectPictureSelector/TNProjectPictureSelector';
import TNActivityIndicator from "../../../truly-native/TNActivityIndicator";

function IMCreateGroupComponent(props) {
  const { onCancel, isNameDialogVisible, users, onSubmitName, onCheck, appStyles, onEmptyStatePress } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));

  const [imgErr, setImgErr] = useState(false);
  const [conversationPictureUrl, setConversationPictureUrl] = useState('');
  const [loading, setLoading] = useState(true);
  const [usersDisplayed, setUsersDisplayed] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');
  const [name, setName] = useState('');
  const [loadingConversation, setLoadingConversation] = useState(false);

  useEffect(() => {
    if (users) {
      users.sort((a,b) => a.lastName.localeCompare(b.lastName));
      setUsersDisplayed(users);
      setLoading(false);
    }
  }, [users]);

  onSearchQueryChanged = searchQuery => {
    setSearchQuery(searchQuery);
    const query = searchQuery.toLowerCase();

    var userFiltered = users.filter(user => {
      var firstLast = user.firstName.toLowerCase() + " " + user.lastName.toLowerCase()
      if(firstLast.includes(query) || user.title.toLowerCase().includes(query)){
        return user;
      };
    });

    setUsersDisplayed(userFiltered);
  }

    onNameChange = text => {
        setName(text);
       console.log(text);
    }

    onNewConversationCreation = () => {
      if(name){
          setLoadingConversation(true);
          onSubmitName(name, conversationPictureUrl);
          onCancel();
      } else {
          Alert.alert("Caution", "A name is mandatory to create a conversation.");
      }
    }

  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => onCheck(item)}
      style={[styles.itemContainer, item.checked ? {backgroundColor: "#F7375020"} : {}]}>
      <View style={styles.chatIconContainer}>
        <IMConversationIconView
          style={styles.photo}
          imageStyle={styles.photo}
          participants={[item]}
          appStyles={AppStyles}
        />
        <Text style={styles.name}>{item.firstName + ' ' + item.lastName}</Text>
      </View>
      <View style={styles.addFlexContainer}>
        {item.checked && (
          <Image style={styles.checked} source={AppStyles.iconSet.checked} />
        )}
      </View>
      <View style={styles.divider} />
    </TouchableOpacity>
  );

  onImageError = () => {
    setImgErr(true);
    console.log('oops an error occured');
  };

  const emptyStateConfig = {
    title: IMLocalized("No users"),
    description: IMLocalized("There is no user matching your search on Click & Give."),
  };

  return (
    <View style={styles.container}>
      <View style={styles.rowContainer} >
        <Image source={AppStyles.iconSet.search} style={styles.searchImage} />
        <TextInput
          style={styles.searchInput}
          placeholder={IMLocalized('Search collaborator')}
          placeholderTextColor="#aaaaaa"
          onChangeText={text => onSearchQueryChanged(text)}
          value={searchQuery}
          underlineColorAndroid="transparent"
        />
      </View>
      {usersDisplayed && usersDisplayed.length > 0 && (
        <FlatList
          data={usersDisplayed}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          initialNumToRender={5}
        />
      )}
      {loading && (
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%'}}>
          <ActivityIndicator size="small" />
        </View>
      )}
      {usersDisplayed && usersDisplayed.length == 0 && (
        <View style={styles.emptyViewContainer}>
          <TNEmptyStateView
            appStyles={appStyles}
            emptyStateConfig={emptyStateConfig}
          />
        </View>
      )}
      <DialogInput
        isDialogVisible={false}
        title={IMLocalized("Type group name")}
        hintInput="Group Name"
        textInputProps={{ selectTextOnFocus: true }}
        submitText="OK"
        closeDialog={onCancel}
      />
        <Modal
            style={{justifyContent: 'center', alignItems:"center"}}
            animationType="slide"
            transparent={true}
            visible={isNameDialogVisible}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
            <View  style={[styles.modalScrollViewStyle]}>
                <View style={styles.modalContainer}>
                <View style={styles.modalRowContainer}>
                    <Text style={styles.modalPopUpTextTitle}>{"New conversation"}</Text>
                    <TouchableOpacity
                        style={styles.cancelButtonContainer}
                        onPress={() => {
                            onCancel();
                        }}>
                        <Image source={AppStyles.iconSet.close} style={styles.modalVisibleIcon}/>
                    </TouchableOpacity>
                </View>
                <Text style={styles.subTitle}>{'Conversation name'}</Text>
                <TextInput
                    onChangeText={setName}
                    value={name}
                    style={styles.searchInput}
                    underlineColorAndroid="transparent"
                    placeholder={'Type a name'}
                    placeholderTextColor="#aaaaaa"
                />
                <Text style={styles.subTitle}>{"Conversation picture"}</Text>
                <TNProjectPictureSelector
                    projectPictureURL={conversationPictureUrl}
                    setProfilePictureURL={setConversationPictureUrl}
                    isConversationPicture={true}
                    appStyles={AppStyles}
                />
                <TouchableOpacity
                    style={styles.updateButton}
                    onPress={() => onNewConversationCreation()}>
                    <Text style={styles.updateButtonText}>CREATE</Text>
                </TouchableOpacity>
                </View>
            </View>
        </Modal>
        {loadingConversation && (
            <TNActivityIndicator appStyles={AppStyles} />
        )}
    </View>
  );
}

IMCreateGroupComponent.propTypes = {
  users: PropTypes.array,
  onCancel: PropTypes.func,
  isNameDialogVisible: PropTypes.bool,
  onSubmitName: PropTypes.func,
  onCheck: PropTypes.func,
};

export default IMCreateGroupComponent;
