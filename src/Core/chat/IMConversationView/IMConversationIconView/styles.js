import {DynamicStyleSheet} from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const VIEW_WIDTH = 70;
const MULTI_ICON_WIDTH = 45;
const RADIUS_BORDER_WIDTH = 2;
const TOP_ICON_WIDTH = MULTI_ICON_WIDTH + RADIUS_BORDER_WIDTH * 2;
const ONLINE_MARK_WIDTH = 9 + RADIUS_BORDER_WIDTH * 2;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {},
    singleParticipation: {
      ...ifIphoneX({
        height: VIEW_WIDTH,
        width: VIEW_WIDTH,
      },{
        height: VIEW_WIDTH*0.8,
        width: VIEW_WIDTH*0.8,
      })
    },
    singleChatItemIcon: {
      ...ifIphoneX({
        height: VIEW_WIDTH,
        width: VIEW_WIDTH,
      },{
        height: VIEW_WIDTH*0.8,
        width: VIEW_WIDTH*0.8,
      }),
      position: 'absolute',
      borderRadius: VIEW_WIDTH / 2,
      left: 0,
      top: 0,
    },
    onlineMark: {
      position: 'absolute',
      backgroundColor: '#4acd1d',
      height: ONLINE_MARK_WIDTH,
      width: ONLINE_MARK_WIDTH,
      borderRadius: ONLINE_MARK_WIDTH / 2,
      borderWidth: RADIUS_BORDER_WIDTH,
      borderColor: appStyles.colorSet.mainThemeBackgroundColor,
      right: 1.5,
      bottom: 1,
    },
    multiParticipation: {
      ...ifIphoneX({
        height: VIEW_WIDTH,
        width: VIEW_WIDTH,
      },{
        height: VIEW_WIDTH*0.8,
        width: VIEW_WIDTH*0.8,
      })
    },
    bottomIcon: {
      top: 0,
      right: 0,
    },
    topIcon: {
      left: 0,
      bottom: 0,
      ...ifIphoneX({
        height: TOP_ICON_WIDTH,
        width: TOP_ICON_WIDTH,
      },{
        height: TOP_ICON_WIDTH*0.8,
        width: TOP_ICON_WIDTH*0.8,
      }),
      borderRadius: TOP_ICON_WIDTH / 2,
      borderWidth: RADIUS_BORDER_WIDTH,
      borderColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    multiPaticipationIcon: {
      position: 'absolute',
      ...ifIphoneX({
        height: MULTI_ICON_WIDTH,
        width: MULTI_ICON_WIDTH,
      },{
        height: MULTI_ICON_WIDTH*0.8,
        width: MULTI_ICON_WIDTH*0.8,
      }),
      borderRadius: MULTI_ICON_WIDTH / 2,
    },
    multiPaticipationIcon: {
      position: 'absolute',
      borderRadius: MULTI_ICON_WIDTH / 2,
      ...ifIphoneX({
        height: MULTI_ICON_WIDTH,
        width: MULTI_ICON_WIDTH,
      },{
        height: MULTI_ICON_WIDTH*0.8,
        width: MULTI_ICON_WIDTH*0.8,
      }),
    },
  })
};

export default dynamicStyles;
