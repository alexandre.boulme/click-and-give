import { DynamicStyleSheet } from 'react-native-dark-mode';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const dynamicStyles = appStyles => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: appStyles.colorSet.mainThemeBackgroundColor,
    },
    userImageContainer: {
      borderWidth: 0,
    },
    chatsChannelContainer: {
      // flex: 1,
      padding: 0,
    },
    chatItemContainer: {
      flexDirection: 'row',
    },
    conversationItemStyle: {
      ...ifIphoneX({
        paddingVertical: 20,
      },{
        paddingVertical: 15, 
      }),
      borderBottomWidth: 1,
      borderBottomColor: "#CCC"
    },
    chatItemContent: {
      flex: 1,
      alignSelf: 'center',
      marginLeft: 0,
    },
    chatFriendName: {
      color: appStyles.colorSet.mainTextColor,
      fontSize: appStyles.fontSet.middle,
      fontWeight: '600',
      marginLeft: 20
    },
    content: {
      flexDirection: 'row',
      width: '90%'
    },
    message: {
      flex: 2,
      color: appStyles.colorSet.mainSubtextColor,
      fontSize: appStyles.fontSet.middleminus,
      fontWeight: '300',
      marginLeft: 20
    },
    unReadIndicator: {
      width: 10,
      height: 10,
      borderRadius: 10,
      backgroundColor: appStyles.colorSet.mainThemeForegroundColor
    }
  })
};

export default dynamicStyles;
