import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import IMConversationIconView from './IMConversationIconView/IMConversationIconView';
import { timeFormat } from '../..';
import dynamicStyles from './styles';

function IMConversationView(props) {
  const { onChatItemPress, formatMessage, item, user, appStyles } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  let title = item.name;

  if (!title) {
    if (item.participants.length > 0) {
      let friend = item.participants[0];
      title = friend.firstName + ' ' + friend.lastName;
    }
  }

  let unread  = (item.lastSenderID != user.userID && item.seenByID && !item.seenByID.includes(user.userID));

  return (
    <TouchableOpacity
      onPress={() => onChatItemPress(item, unread, item.seenByID)}
      style={[styles.chatItemContainer,styles.conversationItemStyle]}>
      <IMConversationIconView
        item={item}
        participants={item.participants}
        appStyles={appStyles} />
      <View style={styles.chatItemContent}>
        <Text style={styles.chatFriendName}>{title}</Text>
        <View style={styles.content}>
          <Text
            numberOfLines={1}
            ellipsizeMode={'tail'}
            style={[styles.message]}>
            {timeFormat(item.lastMessageDate)}
            {' - '}
            {formatMessage(item)}
          </Text>
        </View>
        {unread &&(
          <View style={[styles.unReadIndicator, {position: 'absolute', top: '12%', right: '12%'}]} />
        )}
      </View>
    </TouchableOpacity>
  );
}

IMConversationView.propTypes = {
  formatMessage: PropTypes.func,
  item: PropTypes.object,
  onChatItemPress: PropTypes.func,
};

export default IMConversationView;
