import PropTypes from 'prop-types';
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { channelManager } from '../firebase';
import {
  setChannels,
  setChannelParticipations,
  setChannelsSubcribed
} from '../redux';
import { setBannedUserIDs } from '../../user-reporting/redux';
import IMConversationList from '../IMConversationList';
import { reportingManager } from '../../user-reporting';

class IMConversationListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      appStyles: (
        (props.navigation && props.navigation.state && props.navigation.state.params && props.navigation.state.params.appStyles)
        || props.navigation.getParam('appStyles')
        || props.appStyles),
      refresh: false
    };
  }

  componentDidMount() {
    const self = this;
    const userId = self.props.user.id || self.props.user.userID;

    if (!self.props.areChannelsSubcribed) {
      self.unsubscribeAbuseDB = reportingManager.unsubscribeAbuseDB(userId, this.onAbuseDBUpdate);
      self.props.setChannelsSubcribed(true);
    } else {
      console.log("Channels number : " + self.props.channels.length)
      self.setState({ loading: false });
    }
  }

  componentWillUnmount() {

    if (this.channelPaticipationUnsubscribe) {
      this.channelPaticipationUnsubscribe();
    }
    if (this.channelsUnsubscribe) {
      this.channelsUnsubscribe();
    }
    if (this.unsubscribeAbuseDB) {
      this.unsubscribeAbuseDB();
    }

    if(this.props.channels.length == 0){
      this.setState({refresh : true});
    }
  }

  onChannelParticipationCollectionUpdate = (data) => {

    const channels = this.props.channels.filter(channel => {
      return (
        data.filter(participation => channel.id === participation.channel)
          .length > 0
      );
    });

    this.channelsUnsubscribe = channelManager.subscribeChannels(
        this.onChannelCollectionUpdate,
    );
    this.props.setChannelParticipations(data);
    this.props.setChannels(this.channelsWithNoBannedUsers(channels, this.props.bannedUserIDs));
  };

  onChannelCollectionUpdate = querySnapshot => {
    const self = this;
    const { data, channelPromiseArray } = channelManager.filterQuerySnapshot(
      self,
      querySnapshot,
      channelManager,
    );

    Promise.all(channelPromiseArray).then(() => {
      const sortedData = data.sort(function (a, b) {
        if (!a.lastMessageDate) {
          return 1;
        }
        if (!b.lastMessageDate) {
          return -1;
        }
        a = new Date(a.lastMessageDate.seconds);
        b = new Date(b.lastMessageDate.seconds);
        return a > b ? -1 : a < b ? 1 : 0;
      });

      console.log("Promise all finished, number : " + sortedData.length);

      self.props.setChannels(self.channelsWithNoBannedUsers(sortedData, self.props.bannedUserIDs));
      self.setState({ loading: false });
    });
  };

  onAbuseDBUpdate = (abuses) => {

    const bannedUserIDs = [];
    abuses.forEach(abuse => bannedUserIDs.push(abuse.dest));
    this.props.setBannedUserIDs(bannedUserIDs);
    this.props.setChannels(this.channelsWithNoBannedUsers(this.props.channels, bannedUserIDs));

    const userId = this.props.user.id || this.props.user.userID;
    this.channelParticipantUnsubscribe = channelManager.subscribeChannelParticipation(
      userId,
      this.onChannelParticipationCollectionUpdate,
    );

    this.channelsUnsubscribe = channelManager.subscribeChannels(
        this.onChannelCollectionUpdate,
    );
  }

  onConversationPress = (channel, unread, seenBy) => {

    const seenByUsers = seenBy;

    if(unread){
      channelManager.markSeen(this.props.user, channel);
      seenByUsers.push(this.props.user.id)
    }

    console.log("CHANNEL : ", channel);

    this.props.navigation.navigate('PersonalChat', { channel, appStyles: this.state.appStyles, seenByUsers: seenByUsers });
  };

  channelsWithNoBannedUsers = (channels, bannedUserIDs) => {
    const channelsWithNoBannedUsers = [];
    channels.forEach(channel => {
      if (
        !bannedUserIDs
        || !channel.participants
        || channel.participants.length != 1
        || !bannedUserIDs.includes(channel.participants[0].id)) {
        channelsWithNoBannedUsers.push(channel);
      }
    });
    return channelsWithNoBannedUsers;
  }

  render() {
    return (
      <IMConversationList
        loading={this.state.loading}
        conversations={this.props.channels}
        onConversationPress={this.onConversationPress}
        appStyles={this.state.appStyles}
        user={this.props.user}
      />
    );
  }
}

IMConversationListView.propTypes = {
  channels: PropTypes.array,
};

const mapStateToProps = ({ chat, auth, userReports }) => {
  return {
    channels: chat.channels,
    areChannelsSetup: chat.areChannelsSetup,
    channelParticipations: chat.channelParticipations,
    areChannelsSubcribed: chat.areChannelsSubcribed,
    user: auth.user,
    bannedUserIDs: userReports.bannedUserIDs,
  };
};

export default connect(mapStateToProps, {
  setChannels,
  setChannelParticipations,
  setChannelsSubcribed,
  setBannedUserIDs
})(IMConversationListView);
