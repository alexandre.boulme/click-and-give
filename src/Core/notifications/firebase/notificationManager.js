import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

const notificationsRef = firebase
    .firestore()
    .collection("ClickAndGive_Notifications");

const fcmURL = "https://fcm.googleapis.com/fcm/send";
const firebaseServerKey = "AAAAAAh9DIM:APA91bFgDFj7NeA7R4xvXBRkfnO7JABHySwYrEhypAj1IeBvnVkDNNM8Vuo2lBbvbtuwX4Ex0gBk9BR-o-pglokRqWH8dc3Ff9AgAOhxwbdEn5BPfgzlMjt4LSqcsfM7-sgXZV8Bv8G_";

const sendPushNotification = async (toUser, title, body, type, metadata = {}) => {
    if (metadata && metadata.fromUser && toUser.id == metadata.fromUser.id) {
        return;
    }
    const notification = {
        toUserID: toUser.id,
        title,
        body,
        metadata,
        toUser,
        type,
        seen: false,
    };

    const ref = await notificationsRef.add({
        ...notification,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    });

    //notificationsRef.doc(ref.id).update({ id: ref.id });
    if (!toUser.pushToken) {
        return;
    }

    const pushNotification = {
        to: toUser.pushToken,
        notification: {
            title: title,
            body: body
        },
        data: { ...metadata, type, toUserID: toUser.id }
    };

    fetch(fcmURL, {
        method: 'post',
        headers: new Headers({
            'Authorization': 'key=' + firebaseServerKey,
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(pushNotification)
    })
}

export const notificationManager = {
    sendPushNotification
};
