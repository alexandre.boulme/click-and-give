import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

export const notificationsRef = firebase
  .firestore()
  .collection("ClickAndGive_Notifications");

export const subscribeNotifications = (userId, callback) => {
  return notificationsRef
    .where('toUserID', '==', userId)
    .onSnapshot(notificationSnapshot => {
      const notifications = [];
      notificationSnapshot.forEach(notificationDoc => {
        const notification = notificationDoc.data();
        notification.id = notificationDoc.id;
        notifications.push(notification);
      });
      callback(notifications);
    });
};

export const updateNotification = async notification => {
  try {
    await notificationsRef.doc(notification.id).update({ ...notification });

    return { success: true };
  } catch (error) {
    console.log(error);
    return { error, success: false };
  }
};
