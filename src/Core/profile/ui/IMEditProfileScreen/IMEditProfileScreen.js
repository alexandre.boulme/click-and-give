import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {BackHandler, Text, View} from 'react-native';
import { connect } from 'react-redux';
import TextButton from 'react-native-button';
import { firebaseUser } from '../../../firebase';
import IMFormComponent from '../IMFormComponent/IMFormComponent';
import { setUserData } from '../../../onboarding/redux/auth';
import { IMLocalized } from '../../../localization/IMLocalization';
import {Container, Content, Form, Icon, Picker} from "native-base";
import ClickAndGiveConfig from "../../../../ClickAndGiveConfig";
import CategorySelector from "../../../onboarding/components/categorySelector";
import appStyles from "../../../../AppStyles";
import {KeyboardAwareView} from "react-native-keyboard-aware-view";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";


class IMEditProfileScreen extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    let screenTitle = navigation.state.params.screenTitle;
    let currentTheme = appStyles.navThemeConstants[screenProps.theme];
    const { params = {} } = navigation.state;

    return {
      headerTitle: screenTitle,
      headerRight: (
        <TextButton style={{ marginRight: 12, color: currentTheme.backgroundColor }} onPress={params.onFormSubmit}>
          Save
        </TextButton>
      ),
      headerStyle: {
        backgroundColor: appStyles.colorSet.mainThemeForegroundColor,
      },
      headerTintColor: currentTheme.backgroundColor,
    };
  };

  constructor(props) {
    super(props);
    this.appStyles = props.navigation.getParam('appStyles') || props.appStyles;
    this.form = props.navigation.getParam('form') || props.form;
    this.onUserChange = props.navigation.getParam('onUserChange');


    const currentCity = this.props.user.cityLocation.city;
    const indexCity = ClickAndGiveConfig.cityLocationList.findIndex(function(item, i){
      return item.city === currentCity
    });
    console.log('filter : ', indexCity);

    this.state = {
      form: props.form,
      alteredFormDict: {},
      currentCity: ClickAndGiveConfig.cityLocationList[indexCity]
    };

    this.selectedInterests = this.props.user.selectedInterests;

    this.didFocusSubscription = props.navigation.addListener(
      'didFocus',
      payload =>
        BackHandler.addEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onFormSubmit: this.onFormSubmit,
    });

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload =>
        BackHandler.removeEventListener(
          'hardwareBackPress',
          this.onBackButtonPressAndroid,
        ),
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription.remove();
    this.willBlurSubscription && this.willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  isInvalid = (value, regex) => {
    const regexResult = regex.test(value);

    if (value.length > 0 && !regexResult) {
      return true;
    }
    if (value.length > 0 && regexResult) {
      return false;
    }
  };

  onFormSubmit = () => {
    var newUser = this.props.user;
    const form = this.form;
    const alteredFormDict = this.state.alteredFormDict;
    var allFieldsAreValid = true;

    form.sections.forEach(section => {
      section.fields.forEach(field => {
        const newValue = alteredFormDict[field.key];
        if (newValue != null) {
          if (field.regex && this.isInvalid(newValue, field.regex)) {
            allFieldsAreValid = false;
          } else {
            newUser[field.key] = alteredFormDict[field.key];
          }
        }
      });
    });

    newUser.cityLocation = this.state.currentCity;
    newUser.selectedInterests = this.selectedInterests;

    console.log("User updated : ", newUser);

    if (allFieldsAreValid) {
      this.props.setUserData({ user: newUser });
      this.onUserChange(newUser);
      firebaseUser.updateUserData(this.props.user.id, newUser);
      this.props.navigation.goBack();
    } else {
      alert(
        IMLocalized('An error occured while trying to update your account. Please make sure all fields are valid.'),
      );
    }
  };

  onFormChange = alteredFormDict => {
    this.setState({ alteredFormDict });
  };

  switchCategorySelectionStatus = index => {
    const interest = ClickAndGiveConfig.categoryOfInterest[index];
    if(this.selectedInterests.includes(interest.name)){
      var index = this.selectedInterests.indexOf(interest.name)
      if (index !== -1) {
        this.selectedInterests.splice(index, 1);
      }
    } else {
      this.selectedInterests.push(interest.name);
    }
  }

  onPickerValueChange = city => {
    console.log(city);
    this.setState({currentCity : city});
  }

  render() {
  return (
      <View style={{height: "100%",backgroundColor:  appStyles.colorSet.whiteSmoke}}>
        <KeyboardAwareScrollView>
          <IMFormComponent
              form={this.form}
              initialValuesDict={this.props.user}
              onFormChange={this.onFormChange}
              navigation={this.props.navigation}
              appStyles={this.appStyles}
          />
          <Text style={{
            color: appStyles.colorSet.mainSubtextColor,
            backgroundColor:  appStyles.colorSet.whiteSmoke,
            paddingLeft: 10,
            fontSize: 14,
            paddingVertical: 20,
            paddingBottom: 6,
            fontWeight: '500',
          }}>{IMLocalized('LOCATION')}</Text>
          <View style={{height:50}}>
            <Container>
              <Content>
                <Form>
                  <Picker
                      mode="dropdown"
                      placeholder="Select your city"
                      headerBackButtonTextStyle={{ color: appStyles.colorSet.mainThemeForegroundColor, width:"100%" }}
                      iosIcon={<Icon name="chevron-down-sharp" />}
                      placeholderStyle={{ color: "#aaaaaa", fontSize: appStyles.fontSet.middle, fontWeight: '400'}}
                      textStyle={{
                        color: "#000",
                        fontSize: appStyles.fontSet.middle,
                        fontWeight: '300'
                      }}
                      itemStyle={{
                        backgroundColor: "#FFF",
                        marginLeft: 0,
                        paddingLeft: 20
                      }}
                      itemTextStyle={{
                        color: "#000",
                        fontSize: appStyles.fontSet.middle,
                        fontWeight: '300'
                      }}
                      selectedValue={this.state.currentCity}
                      onValueChange={value => this.onPickerValueChange(value)}
                  >
                    {
                      ClickAndGiveConfig.cityLocationList.map((item, i) => (
                          <Picker.Item label={item.city+", "+item.country} value={item} />
                      ))
                    }
                  </Picker>
                </Form>
              </Content>
            </Container >
          </View>
          <Text style={{
            color: appStyles.colorSet.mainSubtextColor,
            backgroundColor:  appStyles.colorSet.whiteSmoke,
            paddingLeft: 10,
            fontSize: 14,
            paddingVertical: 20,
            paddingBottom: 6,
            fontWeight: '500',
          }}>{IMLocalized('PROJECT CATEGORY OF INTEREST')}</Text>
          <View style={{width: '98%', alignSelf: 'center', flexWrap: 'wrap', flexDirection: 'row'}}>
            {
              ClickAndGiveConfig.categoryOfInterest.map((item, i) => (
                  <CategorySelector
                      title={item.name}
                      selected={this.selectedInterests.includes(item.name)}
                      customStyle={{backgroundColor: "#FFF"}}
                      onPress={() => this.switchCategorySelectionStatus(i)}
                  />
              ))
            }
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

IMEditProfileScreen.propTypes = {
  user: PropTypes.object,
  setUserData: PropTypes.func,
};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, { setUserData })(IMEditProfileScreen);
