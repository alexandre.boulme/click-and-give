import { createStore } from 'redux';
import {feed} from './reducers';

const feedStore = createStore(feed);

export default feedStore;
