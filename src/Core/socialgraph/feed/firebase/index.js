import * as firebasePost from './post';
import * as firebaseComment from './comment';

export {
    firebasePost,
    firebaseComment
};
