import { createStore } from 'redux';
import {friends} from './reducers';

const friendStore = createStore(friends);

export default friendStore;
