import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { useDynamicStyleSheet } from 'react-native-dark-mode';
import { IMConversationIconView } from '../../../../chat';
import PropTypes from 'prop-types';
import dynamicStyles from './styles';
import { FriendshipConstants } from '../..';
import AppStyles from '../../../../../AppStyles'

function IMFriendItem(props) {
  const {
    item,
    index,
    onChatAction,
    onUserItemPress,
    displayActions,
    appStyles,
    followEnabled
  } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const user = item.user;
  let actionTitle = (followEnabled ?
    FriendshipConstants.localizedFollowActionTitle(item.type) :
    FriendshipConstants.localizedActionTitle(item.type)
  );

  var name = "No name";
  if (user.firstName && user.lastName) {
    name = user.firstName + ' ' + user.lastName;
  } else if (user.fullname) {
    name = user.fullname;
  } else if (user.firstName) {
    name = user.firstName;
  }

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => onUserItemPress(user)}
      style={styles.friendItemContainer}>
      <View style={styles.chatIconContainer}>
        <IMConversationIconView
          style={styles.photo}
          imageStyle={styles.photo}
          participants={[user]}
          appStyles={appStyles}
        />
        {name && (<Text style={styles.name}>{name}</Text>)}
      </View>
      <TouchableOpacity
        style={[styles.addButton]}
        onPress={() => onChatAction(user)}>
        <Image source={AppStyles.iconSet.chatUnfilled} style={[{height: "80%", aspectRatio: 1, tintColor: AppStyles.colorSet.mainThemeForegroundColor}]} />
      </TouchableOpacity>
      <View style={styles.divider} />
    </TouchableOpacity>
  );
}

IMFriendItem.propTypes = {
  onChatAction: PropTypes.func,
  onUserItemPress: PropTypes.func,
  actionIcon: PropTypes.bool,
  item: PropTypes.object,
  index: PropTypes.number,
};

IMFriendItem.defaultProps = {
  displayActions: true,
};

export default IMFriendItem;
