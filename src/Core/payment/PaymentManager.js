import stripe from 'tipsi-stripe';
import { STRIPE_PUBLIC_KEY, BASE_URL } from 'react-native-dotenv';

export default class PaymentManager {

  constructor() {
   console.log("from .env : " + BASE_URL);
   console.log("from .env : " + STRIPE_PUBLIC_KEY);
   stripe.setOptions({publishableKey: STRIPE_PUBLIC_KEY});

  }

  createStripeToken = async (params) => {
      return await stripe.createTokenWithCard(params);
  };

  doStripePayment = async (amount, currency, token, description, email) => new Promise((resolve, reject) => {
    console.log("Base url : ", BASE_URL);
    console.log("Email de l'utilisateur : ",email);

    fetch(BASE_URL + '/payWithStripe', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount: amount,
        currency: currency.code.toLowerCase(),
        tokenId: token.tokenId,
        description: description,
        email: email
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log("Stripe payment : " + responseJson.toString());
      resolve(responseJson);
    })
    .catch(error => {
      console.log("Stripe payment error : " + responseJson);
      reject(error)
    });

  });

  deviceSupportsNativePay = async () => {
    const applepay = await stripe.deviceSupportsNativePay();
    console.log(applepay);
  };

  openNativePaySetup = () => {
    stripe.openNativePaySetup();
  };

  initiatePaypalPayment = async (amount, currency, description) => new Promise((resolve, reject) => {

      console.log(description);

    fetch(BASE_URL + '/initiatePaypalPayment', {
     method: 'POST',
     headers: {
       Accept: 'application/json',
       'Content-Type': 'application/form-data',
     },
     body: JSON.stringify({
       amount: amount,
       currency: currency.code.toUpperCase(),
       description: description
     }),
   })
   .then((response) => response.json())
   .then((responseJson) => {
     resolve(responseJson);
   })
   .catch((error) => {
     console.log(JSON.stringify(error.details));
     reject(error);
   });

 });

 executePaypalPayment = async (paymentId, payerId) => new Promise((resolve, reject) => {

    fetch(BASE_URL + '/executePaypalPayment', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/form-data',
        },
        body: JSON.stringify({
            paymentId: paymentId,
            payerId: payerId
        }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
        resolve(responseJson);
    })
    .catch((error) => {
        console.log(JSON.stringify(error.details));
        reject(error);
    });

 });

}
